﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace Mcd.MyStore.WinApp.Core.Extensions
{
    public static class UIElementExtensions
    {
        public static void Animate(this DependencyObject element, string property, double? from, double? to, int durationInMilliSeconds = 1000)
        {
            Storyboard tmpBoard = new Storyboard();

            DoubleAnimation tmpAnimation = new DoubleAnimation();
            tmpAnimation.Duration = TimeSpan.FromMilliseconds(durationInMilliSeconds);
            tmpAnimation.From = from;
            tmpAnimation.To = to;
            tmpAnimation.EasingFunction = new PowerEase() { EasingMode = EasingMode.EaseOut, Power = 5 };
            tmpBoard.Children.Add(tmpAnimation);
            Storyboard.SetTarget(tmpAnimation, element);
            Storyboard.SetTargetProperty(tmpAnimation, property);

            tmpBoard.Begin();
        }
    }
}
