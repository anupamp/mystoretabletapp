﻿using Mcd.MyStore.WinApp.Core.Logging;
using System;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Core.Extensions
{
    public static class SolidColorBrushExtensions
    {
        public static SolidColorBrush ConvertFromHex(string hexColor)
        {
            try
            {
                byte a, r, g, b;

                if (hexColor.Length == 7)
                {
                    a = 255;
                    r = hexColor.Substring(1, 2).ToByte();
                    g = hexColor.Substring(3, 2).ToByte();
                    b = hexColor.Substring(5, 2).ToByte();
                    return new SolidColorBrush(Color.FromArgb(a, r, g, b));
                }
                else if (hexColor.Length == 9)
                {
                    a = hexColor.Substring(1, 2).ToByte();
                    r = hexColor.Substring(3, 2).ToByte();
                    g = hexColor.Substring(5, 2).ToByte();
                    b = hexColor.Substring(7, 2).ToByte();
                    return new SolidColorBrush(Color.FromArgb(a, r, g, b));
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

            return new SolidColorBrush(Colors.Transparent);
        }
    }
}
