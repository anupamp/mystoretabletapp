﻿
using Mcd.MyStore.WinApp.Core.Manager.Language;

namespace Mcd.MyStore.WinApp.Core.Extensions
{
    public static class StringExtensions
    {
        public static byte ToByte(this string colorCodePart)
        {
            return byte.Parse(colorCodePart, System.Globalization.NumberStyles.HexNumber);
        }

        public static string TranslateIfNeeded(this string source)
        {
            if (string.IsNullOrWhiteSpace(source)) return null;
            if (!source.StartsWith("KEY_")) return source;
            return LanguageManager.Instance[source];
        }
    }
}
