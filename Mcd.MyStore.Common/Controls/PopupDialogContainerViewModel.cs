﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.ObjectModel;

namespace Mcd.MyStore.WinApp.Core.Controls
{
    public class PopupDialogContainerViewModel : ViewModelBase
    {
        public ObservableCollection<PopupDialogData> PopupItems { get; set; }

        #region Member - IsContainerActive
        public bool IsContainerActive
        {
            get { return (PopupItems.Count > 0); }
        }
        #endregion

        #region Member - IsVisibleOpacity
        public double IsVisibleOpacity
        {
            get { return (IsContainerActive ? 1 : 0); }
        }
        #endregion

        public event EventHandler OnChangePopupList;

        public PopupDialogContainerViewModel()
        {
            PopupItems = new ObservableCollection<PopupDialogData>();
        }

        internal void AddDialogData(PopupDialogData data)
        {
            PopupItems.Add(data);
            Notify("IsContainerActive");
            Notify("IsVisibleOpacity");

            if (OnChangePopupList != null)
            {
                OnChangePopupList(this, null);
            }
        }

        internal void RemoveDialogData(PopupDialogData dialogData)
        {
            PopupItems.Remove(dialogData);
            Notify("IsContainerActive");
            Notify("IsVisibleOpacity");

            if (OnChangePopupList != null)
            {
                OnChangePopupList(this, null);
            }
        }
    }
}
