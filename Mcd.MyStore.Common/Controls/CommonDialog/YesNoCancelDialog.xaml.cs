﻿using System;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public partial class YesNoCancelDialog
    {
        public YesNoCancelDialog(string caption, string text)
        {
            InitializeComponent();
            HeaderText = caption;
            MessageText = text;
        }

        public String HeaderText { get; set; }
        public String MessageText { get; set; }
    }
}
