﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public partial class InformationDialog : IPopupDialogContent
    {
        public InformationDialog()
        {
            InitializeComponent();
            DataContext = GenericViewModel = new InformationDialogViewModel();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
        public InformationDialogViewModel GenericViewModel
        {
            get { return ViewModel as InformationDialogViewModel; }
            set { ViewModel = value; }
        }
    }
}
