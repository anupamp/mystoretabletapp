using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Linq;
using System.Reflection;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    [Obsolete("Please use the new PopupMessageHelper")]
    public class PopupMessageHelper
    {
        private static PopupMessageHelper _instance;
        public static PopupMessageHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PopupMessageHelper();
                }
                return _instance;
            }
        }

        public Window MainWindow { get; set; }
        public object DialogContent { get; set; }

        public void ShowTemplatedWindowDialog<TUserControl, TDataContext>(TDataContext context = null)
            where TUserControl : UserControl, new()
            where TDataContext : ViewModelBase
        {
            Frame uiElement = Window.Current.Content as Frame;

            Popup popup = uiElement.GetDescendantsOfType<Popup>().FirstOrDefault(x => x.Name == "PopupContainer");
            if (popup == null) return;

            ContentControl contentControl = popup.FindName("PopupContent") as ContentControl;
            if (contentControl == null) return;

            var content = new TUserControl();
            content.DataContext = context;
            contentControl.Content = content;

            popup.IsOpen = true;
        }

        public void ShowException(string text)
        {

        }

        public void ShowMessage(string caption, string text)
        {

        }

        public void ClosePopups()
        {
            Frame uiElement = Window.Current.Content as Frame;

            Popup popup = uiElement.GetDescendantsOfType<Popup>().FirstOrDefault(x => x.Name == "PopupContainer");
            if (popup == null) return;

            popup.IsOpen = false;

            ContentControl contentControl = popup.FindName("PopupContent") as ContentControl;
            if (contentControl == null) return;

            var userControl = contentControl.Content as UserControl;
            if (userControl != null)
            {
                var cancelCommand = userControl.DataContext.GetType().GetRuntimeMethod("OnPopupClose", new Type[] { });
                if (cancelCommand != null)
                {
                    cancelCommand.Invoke(userControl.DataContext, null);
                }
            }

            contentControl.Content = null;
        }

        public bool GetPopupDisplayStatus()
        {
            Frame uiElement = Window.Current.Content as Frame;

            Popup popup = uiElement.GetDescendantsOfType<Popup>().FirstOrDefault(x => x.Name == "PopupContainer");
            if (popup == null) return false;

            ContentControl contentControl = popup.FindName("PopupContent") as ContentControl;
            if (contentControl == null) return false;

            return contentControl.Content == null;
        }


    }
}
