﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public class UserInputReasonDialogViewModel : PopupDialogContentViewModel
    {
        private string _caption;
        private string _headline;
        private string _message;
        private string _reasonText;

        public UserInputReasonDialogViewModel(string caption, string headline, string text)
        {
            Caption = caption;
            Headline = headline;
            Message = text;
        }

        public string Caption
        {
            get { return _caption; }
            set { SetValue(ref _caption, value); }
        }

        public string Headline
        {
            get { return _headline; }
            set { SetValue(ref _headline, value); }
        }

        public string Message
        {
            get { return _message; }
            set { SetValue(ref _message, value); }
        }

        public string ReasonText
        {
            get { return _reasonText; }
            set { SetValue(ref _reasonText, value); }
        }

        public delegate void SaveHandler();

        public void OnPopupClose()
        {

        }
    }
}