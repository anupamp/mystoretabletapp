﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public partial class UserInputReasonDialog : IPopupDialogContent
    {
        public UserInputReasonDialog()
        {
            InitializeComponent();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
    }
}
