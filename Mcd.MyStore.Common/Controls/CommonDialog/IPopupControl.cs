﻿namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public interface IPopupControl
    {
        void OnPopupClose();
    }
}