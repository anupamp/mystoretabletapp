﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public class QuestionDialogViewModel : PopupDialogContentViewModel
    {
        #region Member - HeaderText
        private string _headerText;
        public String HeaderText
        {
            get { return _headerText; }
            set { SetValue(ref _headerText, value); }
        }
        #endregion

        #region Member - MessageText
        private string _messageText;
        public String MessageText
        {
            get { return _messageText; }
            set { SetValue(ref _messageText, value); }
        }
        #endregion

        public ICommand OkayCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public QuestionDialogViewModel()
        {
            OkayCommand = new RelayCommand(OkayCommandExecute);
            CancelCommand = new RelayCommand(CancelCommandExecute);
        }

        public QuestionDialogViewModel(string caption, string text)
            : this()
        {
            HeaderText = caption;
            MessageText = text;
        }

        private void OkayCommandExecute()
        {
            DialogResult = true;
        }

        private void CancelCommandExecute()
        {
            DialogResult = false;
        }

    }
}