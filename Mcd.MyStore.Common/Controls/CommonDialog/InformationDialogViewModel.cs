﻿using System;
using System.Windows.Input;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.UI.Common;

namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public class InformationDialogViewModel : PopupDialogContentViewModel
    {
        private string _caption;
        private string _message;
        private string _iconText = "i";

        public InformationDialogViewModel()
        {
            OkayCommand = new RelayCommand(OkayCommandExecute);
        }

        public InformationDialogViewModel(string caption, string text, bool isException = false)
            : this()
        {
            Caption = caption;
            Message = text;

            if (isException)
                IconText = "!";
        }

        public String Caption
        {
            get { return _caption; }
            set { SetValue(ref _caption, value); }
        }
        public String Message
        {
            get { return _message; }
            set { SetValue(ref _message, value); }
        }
        public String IconText
        {
            get { return _iconText; }
            set { SetValue(ref _iconText, value); }
        }

        public ICommand OkayCommand { get; set; }

        private void OkayCommandExecute()
        {
            DialogResult = true;
        }
    }
}