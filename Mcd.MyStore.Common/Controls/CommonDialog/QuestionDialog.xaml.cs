﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
namespace Mcd.MyStore.WinApp.Core.Controls.CommonDialog
{
    public partial class QuestionDialog : IPopupDialogContent
    {
        public QuestionDialog()
        {
            InitializeComponent();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
    }
}
