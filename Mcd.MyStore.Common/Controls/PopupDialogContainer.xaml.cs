﻿using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Core.Controls
{
    public sealed partial class PopupDialogContainer : UserControl, IPopupDialogContainer
    {
        private PopupDialogContainerViewModel _viewModel = null;

        #region Member - IsFadeBackgroundActive
        private bool _isFadeBackgroundActive = true;
        public bool IsFadeBackgroundActive
        {
            get { return _isFadeBackgroundActive; }
            set { _isFadeBackgroundActive = value; }
        }
        #endregion


        public PopupDialogContainer()
        {
            this.InitializeComponent();

            _viewModel = new PopupDialogContainerViewModel();
            DataContext = _viewModel;

            _viewModel.OnChangePopupList += OnChangePopupList;
        }

        private void OnChangePopupList(object sender, System.EventArgs e)
        {
            if (_isFadeBackgroundActive)
            {
                if (_viewModel.IsContainerActive)
                {
                    BackgroundPanel.Animate("Opacity", null, 1, 400);
                }
                else
                {
                    BackgroundPanel.Animate("Opacity", null, 0, 200);
                }
            }
        }

        public void AddPopupDialogData(PopupDialogData dialogData)
        {
            _viewModel.AddDialogData(dialogData);
            UserControl control = (UserControl)dialogData.Content;
            control.Animate("Opacity", 0, 1, 600);

            control.RenderTransform = new CompositeTransform();
            control.RenderTransformOrigin = new Windows.Foundation.Point(0.5, 0.5);
            control.RenderTransform.Animate("ScaleX", 0, 1, 200);
            control.RenderTransform.Animate("ScaleY", 0, 1, 200);
        }

        public async void RemovePopupDialogData()
        {
            // DM: Changed to for-loop to prevent CollectionChangedException, locking not possible with await.
            for (int index = 0; index < _viewModel.PopupItems.Count; index++)
            {
                var dialogData = _viewModel.PopupItems[index];
                UserControl control = (UserControl)dialogData.Content;
                control.Animate("Opacity", null, 0, 200);

                if (_viewModel.PopupItems.Count <= 1)
                {
                    BackgroundPanel.Animate("Opacity", null, 0, 200);
                }

                await Task.Delay(200);
                _viewModel.RemoveDialogData(dialogData);
            }
        }

        public async void RemovePopupDialogData(PopupDialogData dialogData)
        {
            UserControl control = (UserControl)dialogData.Content;
            control.Animate("Opacity", null, 0, 200);

            if (_viewModel.PopupItems.Count <= 1)
            {
                BackgroundPanel.Animate("Opacity", null, 0, 200);
            }

            await Task.Delay(200);
            _viewModel.RemoveDialogData(dialogData);
        }

        private void ContentControl_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            ContentControl control = (ContentControl)sender;
            (control.DataContext as PopupDialogData).ViewModel.ManipulationDelta(control, OuterBox, e);
        }
    }
}
