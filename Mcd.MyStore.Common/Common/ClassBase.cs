﻿using Mcd.MyStore.Common.DataLoading;
using System;

namespace Mcd.MyStore.WinApp.Core.Common
{
    public class ClassBase
    {
        public void LoadAsyncData(Action work, Action completeWork, bool showLoadingIndicator = true)
        {
            BackgroundWorkerManager.Instance.StartWorker(work, completeWork, showLoadingIndicator);
        }
        public void LoadAsyncData<T>(Func<T> work, BackgroundWorkerManager.BackgroundWorkerResultDelegate<T> completeWork)
        {
            BackgroundWorkerManager.Instance.StartWorker<T>(work, completeWork, true);
        }
        public void LoadAsyncData<T>(Func<T> work, BackgroundWorkerManager.BackgroundWorkerResultDelegate<T> completeWork, bool showLoadingIndicator = true)
        {
            BackgroundWorkerManager.Instance.StartWorker<T>(work, completeWork, showLoadingIndicator);
        }
    }
}
