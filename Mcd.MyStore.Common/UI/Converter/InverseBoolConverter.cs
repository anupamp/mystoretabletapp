﻿using System;
using Windows.UI.Xaml.Data;

namespace Mcd.MyStore.WinApp.Core.UI.Converter
{
    public class InverseBoolConverter : IValueConverter
    {
        public object Convert(object o, Type targetType, object parameter, string language)
        {
            if (!(o is bool))
                return false;

            return !(bool)o;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return Convert(value, targetType, parameter, language);
        }
    }
}
