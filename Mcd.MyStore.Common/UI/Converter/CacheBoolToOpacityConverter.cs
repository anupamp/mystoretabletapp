﻿using System;
using Windows.UI.Xaml.Data;

namespace Mcd.MyStore.WinApp.Core.UI.Converter
{
    public class CacheBoolToOpacityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is bool))
                return 1.0;
            bool v = (bool)value;
            return v ? 0.3 : 1.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}