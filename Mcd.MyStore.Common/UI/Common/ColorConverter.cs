﻿using System.Globalization;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Core.UI.Common
{
    public class ColorConverter
    {
        public static SolidColorBrush ColorToBrush(string color)
        {
            color = color.Replace("#", "");
            if (color.Length == 6)
            {
                return new SolidColorBrush(ColorHelper.FromArgb(255,
                    byte.Parse(color.Substring(0, 2), NumberStyles.HexNumber),
                    byte.Parse(color.Substring(2, 2), NumberStyles.HexNumber),
                    byte.Parse(color.Substring(4, 2), NumberStyles.HexNumber)));
            }
            else
            {
                return null;
            }
        }
    }
}
