﻿using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using System;
using System.Collections.Generic;

namespace Mcd.MyStore.WinApp.Core.MVVM
{
    public abstract class ModuleViewModelBase : ViewModelBase, INavigationModule, INotificationModule
    {
        private List<NotificationArgs<NotificationApplicationType>> _unReceiveNotificationList = new List<NotificationArgs<NotificationApplicationType>>();

        private bool _moduleIsVisible = false;
        public bool ModuleIsVisible
        {
            get { return _moduleIsVisible; }
            set { SetValue(ref _moduleIsVisible, value); }
        }

        public string CurrentTimeLogString
        {
            get
            {
                return string.Format("[{0}]", DateTime.Now.ToString("HH:mm:ss"));
            }
        }

        public void ModuleShowingInternal(NavigationArgs args)
        {
            ModuleIsVisible = true;
            ModuleShowing(new NavigationArgs()
            {
                Parameter = args.Parameter,
                Notifications = _unReceiveNotificationList.Count > 0 ? _unReceiveNotificationList : null
            });
            _unReceiveNotificationList.Clear();
        }

        #region Prop - IsAutoReloadEnabled
        private bool _isAutoReloadEnabled = false;
        public bool IsAutoReloadEnabled
        {
            get { return _isAutoReloadEnabled; }
            set { _isAutoReloadEnabled = value; }
        }
        #endregion


        public void ModuleHidingInternal(NavigationHideCloseArgs args)
        {
            ModuleHiding(args);
            if (!args.Cancel)
            {
                ModuleIsVisible = false;
            }
        }

        public void ApplicationClosingInternal(NavigationHideCloseArgs args)
        {
            ApplicationClosing(args);
        }

        public abstract void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification);
        public abstract void ModuleHiding(NavigationHideCloseArgs args);
        public abstract void ApplicationClosing(NavigationHideCloseArgs args);
        public abstract void ModuleShowing(NavigationArgs navigationArgs);


        #region Notification stuff
        public void SubscribeNotification(NotificationApplicationType type)
        {
            NotificationManager.Instance.SubscribeNotification(this, type);
        }

        public void UnsubscribeNotification(NotificationApplicationType type)
        {
            NotificationManager.Instance.UnsubscribeNotification(this, type);
        }

        public void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            if (_moduleIsVisible)
            {
                ReceiveNotification(notification);
            }
            else
            {
                _unReceiveNotificationList.Add(notification);
            }
        }
        #endregion

        public virtual void TriggerDataLoading()
        { }
    }
}
