﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.WinApp.Core.Common;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Core.MVVM
{
    public abstract class ViewModelBase : ClassBase, INotifyPropertyChanged
    {
        #region Member - DebugVisibility
        public Visibility DebugVisibility
        {
            get { return (SettingsManager.Instance.DebugMode ? Visibility.Visible : Visibility.Collapsed); }
        }
        #endregion

        public LanguageManager Translation
        {
            get { return LanguageManager.Instance; }
        }

        private LanguageManager _moduleLangaugeManager = null;
        public LanguageManager RelTranslation
        {
            get
            {
                if (_moduleLangaugeManager != null)
                {
                    return _moduleLangaugeManager;
                }
                else if (!String.IsNullOrEmpty(TranslationPrefix))
                {
                    _moduleLangaugeManager = new ModuleLanguageManager(TranslationPrefix);
                    return _moduleLangaugeManager;
                }
                return LanguageManager.Instance;
            }
        }

        public virtual string TranslationPrefix
        {
            get { return null; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetValue<T>(ref T storage, T value, [CallerMemberName] String propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            Notify(propertyName);
            return true;
        }

        protected void Notify([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void RunOnUI(Action uiAction, bool waitForExecution = false)
        {
            BackgroundWorkerManager.RunOnUI(uiAction, waitForExecution);
        }

        public void ShowLoadingIndicator()
        {
            try
            {
                RunOnUI(() =>
                {
                    SettingsManager.IncrementItemsToProcess();
                });
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
        }

        public void HideLoadingIndicator()
        {
            try
            {
                RunOnUI(() =>
                {
                    SettingsManager.DecrementItemsToProcess();
                });
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
        }

        public class ModuleLanguageManager : LanguageManager
        {
            private string _prefix;

            public ModuleLanguageManager(string prefix)
            {
                _prefix = prefix;
            }

            public string this[string key]
            {
                get
                {
                    return Instance[_prefix + key];
                }
            }
        }
    }
}
