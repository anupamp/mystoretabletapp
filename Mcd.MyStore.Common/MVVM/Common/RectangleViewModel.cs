﻿using Mcd.MyStore.GeneralReportingService.Common.Dto.ConfigurableOrderPoints;
using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Core.MVVM.Common
{
    public class RectangleViewModel : ViewModelBase
    {
        #region Member - Left
        private double _left;
        public double Left
        {
            get { return _left; }
            set { SetValue(ref _left, value); }
        }
        #endregion

        #region Member - Top
        private double _top;
        public double Top
        {
            get { return _top; }
            set { SetValue(ref _top, value); }
        }
        #endregion

        #region Member - Width
        private double _width = 10;
        public double Width
        {
            get { return _width; }
            set { SetValue(ref _width, value); }
        }
        #endregion

        #region Member - Height
        private double _height = 10;
        public double Height
        {
            get { return _height; }
            set { SetValue(ref _height, value); }
        }
        #endregion

        public void Update(ConfigurableOrderPointsRectangleDto data)
        {
            Left = data.Left;
            Top = data.Top;
            Width = data.Width;
            Height = data.Height;
        }
    }
}
