﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Core.MVVM.Common
{
    public sealed class AutoDataTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item == null)
            {
                return null;
            }

            object key = item.GetType().Name;
            return FindResource(key, container as FrameworkElement) as DataTemplate;
        }

        private static object FindResource(object key, FrameworkElement element)
        {
            if (element == null)
            {
                return null;
            }

            object value;
            if (element.Resources.TryGetValue(key, out value))
            {
                return value;
            }

            return FindResource(key, VisualTreeHelper.GetParent(element) as FrameworkElement);
        }
    }
}
