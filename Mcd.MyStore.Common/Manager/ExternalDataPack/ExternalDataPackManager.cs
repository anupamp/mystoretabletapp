﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Common;
using Mcd.MyStore.WinApp.Core.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Mcd.MyStore.WinApp.Core.Manager.ExternalDataPack
{
    public class ExternalDataPackManager : ClassBase
    {
        #region Singleton
        private static ExternalDataPackManager _instance = null;
        public static ExternalDataPackManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ExternalDataPackManager();
                }
                return _instance;
            }
        }
        #endregion

        private string _externalDataPackFolderName = "ExternalDataPack";
        private string _externalDataPackFilesFolderName = "Data";

        public Task InitManager()
        {
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    StorageFolder roamingFolder = ApplicationData.Current.RoamingFolder;
                    StorageFolder externalFolder = roamingFolder.CreateFolderAsync(_externalDataPackFolderName, CreationCollisionOption.OpenIfExists).AsTask().Result;
                    StorageFolder externalFilesFolder = externalFolder.CreateFolderAsync(_externalDataPackFilesFolderName, CreationCollisionOption.ReplaceExisting).AsTask().Result;

                    var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                    byte[] data = service.GetExternalDataPack("MyStoreApp");
                    WcfConnection.DisconnectWcfService(service);

                    if (data == null)
                        return;

                    Stream zipMemoryStream = new MemoryStream(data);

                    // Create zip archive to access compressed files in memory stream
                    using (ZipArchive zipArchive = new ZipArchive(zipMemoryStream, ZipArchiveMode.Read))
                    {
                        Dictionary<string, StorageFolder> cacheFolders = new Dictionary<string, StorageFolder>();
                        // For each compressed file...
                        foreach (ZipArchiveEntry entry in zipArchive.Entries)
                        {
                            // ... read its uncompressed contents
                            using (Stream entryStream = entry.Open())
                            {
                                byte[] buffer = new byte[entry.Length];
                                entryStream.Read(buffer, 0, buffer.Length);
                                try
                                {
                                    string fullEntryName = entry.FullName.Replace("/", "\\");
                                    if (!String.IsNullOrEmpty(entry.Name))
                                    {
                                        string folderPath = fullEntryName.Substring(0, fullEntryName.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                                        StorageFolder tmpFolder;
                                        if (cacheFolders.TryGetValue(folderPath, out tmpFolder))
                                        {
                                            //Create a file to store the contents
                                            StorageFile uncompressedFile = tmpFolder.CreateFileAsync(entry.Name, CreationCollisionOption.ReplaceExisting).AsTask().Result;
                                            // Store the contents
                                            using (IRandomAccessStream uncompressedFileStream = uncompressedFile.OpenAsync(FileAccessMode.ReadWrite).AsTask().Result)
                                            {
                                                using (Stream outstream = uncompressedFileStream.AsStreamForWrite())
                                                {
                                                    outstream.Write(buffer, 0, buffer.Length);
                                                    outstream.Flush();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StorageFolder folder = externalFilesFolder.CreateFolderAsync(fullEntryName, CreationCollisionOption.OpenIfExists).AsTask().Result;
                                        cacheFolders.Add(fullEntryName, folder);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Write(ex);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write(ex);
                }

            });
        }
    }
}
