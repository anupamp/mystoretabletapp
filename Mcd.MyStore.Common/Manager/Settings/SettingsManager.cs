﻿using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Xaml;


namespace Mcd.MyStore.WinApp.Core.Manager.Settings
{
    public class SettingsManager
    {
        #region Singleton
        private static SettingsManager _instance = null;
        public static SettingsManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingsManager();
                }
                return _instance;
            }
        }
        #endregion

        public bool IsReady { get; set; }



        public string IPAddress { get; set; }

        public bool OrderPointsRefreshPosition { get; set; }

        public bool LoggingEnabled { get; set; }

        public int UpdateTriggerInSecs { get; set; }

        public int LastVisibleDaysCount { get; set; }

        public TimeSpan AutoLogoutTime { get; set; }

        public bool DebugMode { get; set; }

        private int _appServicePort = 9980;
        public int AppServicePort
        {
            get { return _appServicePort; }
            set { _appServicePort = value; }
        }

        private int _operatorId = 0;
        public int OperatorId
        {
            get { return _operatorId; }
            set { _operatorId = value; }
        }

        public string Language { get; set; }

        #region WaitingIndicator

        public string WaitingIndicatorText { get; set; }

        public Visibility WaitingIndicatorVisibility { get; set; }

        private static int _itemsProcessing = 0;

        public static void IncrementItemsToProcess()
        {
            lock (Instance)
            {
                _itemsProcessing += 1;
                if (_itemsProcessing > 0) Instance.WaitingIndicatorVisibility = Visibility.Visible;
                if (OnItemsProcessed != null) OnItemsProcessed(_itemsProcessing > 0);
            }
        }
        public static void DecrementItemsToProcess()
        {
            lock (Instance)
            {
                _itemsProcessing -= 1;
                if (_itemsProcessing == 0) Instance.WaitingIndicatorVisibility = Visibility.Collapsed;
                if (OnItemsProcessed != null) OnItemsProcessed(_itemsProcessing > 0);
            }
        }

        public delegate void ItemsProcessed(bool visible);

        public static event ItemsProcessed OnItemsProcessed;

        #endregion

        public SettingsManager()
        {
            LastVisibleDaysCount = 7;
            UpdateTriggerInSecs = 30;

            WaitingIndicatorVisibility = Visibility.Collapsed;

            IPAddress = getOLC1Host();
        }
        private string getOLC1Host()
        {
            string olc1host;
            try
            {
                var hostnNames = NetworkInformation.GetHostNames();
                var localName = hostnNames.FirstOrDefault(name => name.DisplayName.Contains(".local"));

                olc1host = localName.DisplayName.Replace(".local", "");
            }
            catch (Exception e)
            {
                Logger.Write("getOLC1Host: could not determine hostname via DNS.Exception:" + e.ToString());
                return "localhost";
            }
            // don't use the determination of the OLC1 for Sotec user
            if (olc1host.ToLower().StartsWith("so")) return ("localhost");

            if (olc1host.Length > 1) olc1host = olc1host.Remove(olc1host.Length - 1) + "1";
            return olc1host;
        }


        private async void LoadSettings()
        {
            WaitingIndicatorText = LanguageManager.Instance.GetTranslation("BASIC_PleaseWait");

            try
            {
                StorageFolder roamingFolder = ApplicationData.Current.RoamingFolder;
                StorageFolder settingsFolder = await GetOrCreateFolder(roamingFolder, "MyStore");

                string fileText = await GetTextFromFile(settingsFolder);
                string[] settingLines = fileText.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string currentSetting in settingLines)
                {
                    string[] data = currentSetting.Trim().Split('=');
                    if (data.Length == 2)
                    {
                        string dataText = data[1].Trim();
                        switch (data[0].Trim().ToLower())
                        {
                            case "ip":
                                {
                                    IPAddress = dataText;
                                }
                                break;

                            case "orderpointsrefreshposition":
                                {
                                    bool tmpValue = false;
                                    if (Boolean.TryParse(dataText, out tmpValue))
                                    {
                                        OrderPointsRefreshPosition = tmpValue;
                                    }
                                }
                                break;

                            case "debugmode":
                                {
                                    bool tmpDebugValue = false;
                                    if (Boolean.TryParse(dataText, out tmpDebugValue))
                                    {
                                        DebugMode = tmpDebugValue;
                                    }
                                }
                                break;

                            case "updatetriggerinsecs":
                                {
                                    int tmpUpdateTriggerValue = 0;
                                    if (Int32.TryParse(dataText, out tmpUpdateTriggerValue))
                                    {
                                        UpdateTriggerInSecs = tmpUpdateTriggerValue;
                                    }
                                }
                                break;

                            case "lastvisibledayscount":
                                {
                                    int tmpLastVisibleDaysCount = 0;
                                    if (Int32.TryParse(dataText, out tmpLastVisibleDaysCount))
                                    {
                                        LastVisibleDaysCount = tmpLastVisibleDaysCount;
                                    }
                                }
                                break;

                            case "logging":
                                {
                                    bool tmpLoggingValue = false;
                                    if (Boolean.TryParse(dataText, out tmpLoggingValue))
                                    {
                                        LoggingEnabled = tmpLoggingValue;
                                    }
                                }
                                break;

                            case "AppServicePort":
                                {
                                    int tmpServicePort = 0;
                                    if (Int32.TryParse(dataText, out tmpServicePort) && tmpServicePort > 1024)
                                    {
                                        AppServicePort = tmpServicePort;
                                    }
                                    else
                                    {
                                        AppServicePort = 9980;
                                    }
                                }
                                break;
                            case "operator":
                                {
                                    int tmpOperator = 0;
                                    if (Int32.TryParse(dataText, out tmpOperator))
                                    {
                                        OperatorId = tmpOperator;
                                    }
                                }
                                break;
                            case "language":
                                {
                                    if (string.IsNullOrWhiteSpace(dataText)) dataText = "en-gb";
                                    Language = dataText;
                                }
                                break;
                            case "autologouttime":
                                {
                                    int minutes = 0;
                                    if(Int32.TryParse(dataText, out minutes))
                                    {
                                        AutoLogoutTime = TimeSpan.FromMinutes(minutes);
                                    }
                                    else
                                    {
                                        AutoLogoutTime = TimeSpan.FromMinutes(30);
                                    }

                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex.ToString());
            }
        }

        public Task InitManager()
        {
            return Task.Factory.StartNew(() =>
            {
                IPAddress = getOLC1Host();

                LoadSettings();
            });
        }

        private async Task<StorageFolder> GetOrCreateFolder(StorageFolder storageFolder, string folderName)
        {
            StorageFolder folder = null;

            try
            {
                folder = await storageFolder.GetFolderAsync(folderName);
            }
            catch { }

            if (folder == null)
            {
                folder = await storageFolder.CreateFolderAsync(folderName);
            }

            return folder;
        }

        private async Task<string> GetTextFromFile(StorageFolder storageFolder)
        {
            string result = String.Empty;
            bool areReadOK = false;
            try
            {
                StorageFile currentFile = await storageFolder.GetFileAsync("Settings.txt");
                result = await FileIO.ReadTextAsync(currentFile);

                areReadOK = true;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

            if (!areReadOK)
            {
                try
                {
                    if (String.IsNullOrEmpty(result))
                    {
                        await storageFolder.CreateFileAsync("Settings.txt");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write(ex);
                }
            }


            return result;
        }
    }
}
