﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.Global;
using Mcd.MyStore.SystemMaintenance.Common;
using Mcd.MyStore.SystemMaintenance.Common.Dto;
using Mcd.MyStore.WinApp.Cash.WcfService.Common;
using Mcd.MyStore.WinApp.Core.Common;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus.DataObject;
using Mcd.MyStore.WinApp.SystemMaintenance.WcfService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Core.Manager.SystemStatus
{
    public class SystemStatusManager : ClassBase, INotificationModule
    {
        #region Static Member - Instance

        public static readonly SystemStatusManager Instance = new SystemStatusManager();

        // avoid beforefieldinit
        static SystemStatusManager() { }

        #endregion

        public DateTime CurrentBusinessDay { get; private set; }
        public DateTime PreviousBusinessDay { get; set; }
        public bool PreviousBusinessDayIsClosed { get; set; }


        public DateTime? GlobalUserSelectedDay { get; set; }

        private readonly List<SystemStatusForBusinessDayDto> _systemStatusCache = new List<SystemStatusForBusinessDayDto>();
        public StoreSetupData StoreSetupInformation { get; set; }
        public bool IsServiceConnected { get; set; }

        public async Task InitUpdate()
        {
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceConnected);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceDisconnected);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.ServiceStatusChanged);

            await UpdateStatusDataAsync();
        }

        public async Task UpdateStatusDataAsync()
        {
            SystemStatusDto result = await WcfConnection.ExecuteActionOnInterface((ISystemMaintenanceServiceForApp currentInterface) =>
            {
                return currentInterface.GetServiceSystemStatus();
            });

            if (result != null)
            {
                UpdateSystemData(result);
            }

            #region Load POD / Pos / Queue Types
            var serviceResult = await WcfConnection.ExecuteActionOnInterface((ISystemMaintenanceServiceForApp currentInterface) =>
                {
                    return currentInterface.GetStoreSetupInformation();
                });
            StoreSetupInformation = new StoreSetupData();
            List<int> insertPodIds = new List<int>();
            foreach (var item in serviceResult.PosInformations)
            {
                if (!insertPodIds.Contains(item.PodTypeID))
                {
                    StoreSetupInformation.PodList.Add(new StoreSetupPODType() { ID = item.PodTypeID, Name = item.PodTypeName });
                    insertPodIds.Add(item.PodTypeID);
                }
                StoreSetupInformation.PosList.Add(new StoreSetupPOSInfo() { PodTypeID = item.PodTypeID, PodTypeName = item.PodTypeName, Number = item.PosNumber });
            }
            StoreSetupInformation.QueueList = serviceResult.ActiveQueueNames;
            #endregion
        }

        private void UpdateSystemData(SystemStatusDto systemStatus)
        {
            bool isNewBusinessDay = false;
            if (systemStatus.CurrentBusinessDay.HasValue)
            {
                DateTime newCurrentBusinessDay = systemStatus.CurrentBusinessDay.Value;
                if (newCurrentBusinessDay != CurrentBusinessDay)
                {
                    CurrentBusinessDay = newCurrentBusinessDay;
                    isNewBusinessDay = true;
                }
            }


            if (systemStatus.PreviousBusinessDay.HasValue)
            {
                PreviousBusinessDay = systemStatus.PreviousBusinessDay.Value;
                PreviousBusinessDayIsClosed = systemStatus.PreviousBusinessDayIsClosed;
            }

            Logger.Instance.AddLog(String.Format("Current: {0}, Last: {1}", CurrentBusinessDay, PreviousBusinessDay));
            NotificationManager.Instance.SendNotification(NotificationApplicationType.SystemStatusChanged);

            if (isNewBusinessDay)
            {
                // When the business day is changed send a notification to the modules
                NotificationManager.Instance.SendNotification(NotificationApplicationType.BusinessDayIsChanged);
            }
        }



        /// <summary>
        /// Request the opening and closing time for a businessday
        /// [ATTENTION: Make a request to service in the same thread]
        /// </summary>
        public SystemStatusForBusinessDayDto GetStatusForBusinessDay(DateTime businessDay)
        {
            SystemStatusForBusinessDayDto result = new SystemStatusForBusinessDayDto()
            {
                BusinessDay = businessDay
            };

            // look in previous data (cache)
            lock (_systemStatusCache)
            {
                SystemStatusForBusinessDayDto tmpDto = _systemStatusCache.FirstOrDefault(O => O.BusinessDay == businessDay);
                if (tmpDto != null)
                {
                    return tmpDto;
                }

                try
                {
                    ICashReportsServiceForApp service = WcfConnection.ConnectWcfService<ICashReportsServiceForApp>();
                    result.OpeningTime = service.GetOpeningTimeForBuisnessDate(businessDay);
                    result.ClosingTime = service.GetClosingTimeForBuisnessDate(businessDay);
                    WcfConnection.DisconnectWcfService(service);

                    _systemStatusCache.Add(result);
                    return result;
                }
                catch (Exception ex)
                {
                    Logger.Instance.AddLog(ex.ToString());
                }
            }

            // fallback
            result.OpeningTime = businessDay;
            result.ClosingTime = businessDay.AddDays(1).AddSeconds(-1);
            return result;
        }
        public Task<SystemStatusForBusinessDayDto> GetOpeningAndClosingTimeForBusinessDayTask(DateTime businessDay)
        {
            return Task.Factory.StartNew(() =>
            {
                return GetStatusForBusinessDay(businessDay);
            });

        }

        public void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_AppServiceConnected)
            {
                IsServiceConnected = true;
            }
            else if (notification.Type == NotificationApplicationType.WinApp_AppServiceDisconnected)
            {
                IsServiceConnected = false;
            }
            else
            {
                UpdateStatusDataAsync();
            }
        }
    }
}
