﻿
using System.Collections.Generic;
namespace Mcd.MyStore.WinApp.Core.Manager.SystemStatus.DataObject
{
    public class StoreSetupData
    {
        public List<StoreSetupPODType> PodList { get; set; }
        public List<StoreSetupPOSInfo> PosList { get; set; }

        public List<string> QueueList { get; set; }

        public StoreSetupData()
        {
            PodList = new List<StoreSetupPODType>();
            PosList = new List<StoreSetupPOSInfo>();
            QueueList = new List<string>();
        }
    }
}
