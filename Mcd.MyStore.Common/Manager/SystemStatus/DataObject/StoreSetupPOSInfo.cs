﻿
namespace Mcd.MyStore.WinApp.Core.Manager.SystemStatus.DataObject
{
    public class StoreSetupPOSInfo
    {
        public int PodTypeID { get; set; }
        public string PodTypeName { get; set; }
        public int Number { get; set; }
    }
}
