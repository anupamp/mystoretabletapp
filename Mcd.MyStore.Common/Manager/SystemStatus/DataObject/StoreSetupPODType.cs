﻿
namespace Mcd.MyStore.WinApp.Core.Manager.SystemStatus.DataObject
{
    public class StoreSetupPODType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
