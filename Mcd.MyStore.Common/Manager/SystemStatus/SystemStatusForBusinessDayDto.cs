﻿using System;

namespace Mcd.MyStore.WinApp.Core.Manager.SystemStatus
{
    public class SystemStatusForBusinessDayDto
    {
        public DateTime BusinessDay { get; set; }
        public DateTime OpeningTime { get; set; }
        public DateTime ClosingTime { get; set; }
    }
}
