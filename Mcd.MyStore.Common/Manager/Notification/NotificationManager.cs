﻿using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.Portable.TcpServer;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Core.Manager.Notification
{
    public class NotificationManager
    {
        #region Instance
        private static NotificationManager _instance = null;
        public static NotificationManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NotificationManager();
                }
                return _instance;
            }
        }
        #endregion

        #region Members
        private Dictionary<NotificationApplicationType, List<INotificationModule>> _registeredNotifications = new Dictionary<NotificationApplicationType, List<INotificationModule>>();
        private TcpConnectionManager _tcpClient = null;
        #endregion



        public void SubscribeNotification(INotificationModule module, NotificationApplicationType type)
        {
            if (!_registeredNotifications.ContainsKey(type))
            {
                _registeredNotifications.Add(type, new List<INotificationModule>());
            }
            _registeredNotifications[type].Add(module);
        }

        public void UnsubscribeNotification(INotificationModule module, NotificationApplicationType type)
        {
            if (_registeredNotifications.ContainsKey(type))
            {
                if (_registeredNotifications[type].Contains(module))
                {
                    _registeredNotifications[type].Remove(module);
                }
            }
        }

        public void SendNotification(NotificationApplicationType type, object parameter)
        {
            if (_registeredNotifications.ContainsKey(type))
            {
                var moduleList = _registeredNotifications[type];
                foreach (INotificationModule module in moduleList)
                {
                    module.ReceiveNotificationInternal(new NotificationApplicationTypeArgs() { Type = type, Parameter = parameter });
                }
            }
        }

        public void SendNotification(NotificationApplicationType type)
        {
            SendNotification(type, null);
        }

        public void InitManager()
        {
            _tcpClient = new TcpConnectionManager(new TcpMessageHandler());
            StartNotificationClient();
        }

        private void StartNotificationClient()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    bool? connected = await _tcpClient.Connect(SettingsManager.Instance.IPAddress, SettingsManager.Instance.AppServicePort, new CancellationTokenSource());
                    if (connected.HasValue && connected.Value) break;
                    await Task.Delay(7500);
                }
            });
        }
    }
}
