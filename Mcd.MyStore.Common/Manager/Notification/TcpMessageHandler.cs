﻿using System;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.Portable.TcpServer;
using Mcd.MyStore.Portable.TcpServer.Models;
using Mcd.MyStore.Portable.TcpServer.Protocol;
using Newtonsoft.Json;
using System.Diagnostics;
using Windows.UI.Core;

namespace Mcd.MyStore.WinApp.Core.Manager.Notification
{
    public class TcpMessageHandler : CommunicationContext
    {
        private CoreDispatcher _dispatcher;

        public TcpMessageHandler()
        {
            _dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;
        }

        public string CurrentTimeLogString
        {
            get
            {
                return string.Format("[{0}]", DateTime.Now.ToString("HH:mm:ss"));
            }
        }

        public bool IsConnected { get; set; }

        public override void HandleServerMessage(Connection connection, SimpleMessage message)
        {
            Debug.WriteLine(string.Format(CurrentTimeLogString + " AppService: Got SimpleMessage ({0})", message.SimpleMessageType));

            NotificationApplicationType type = (NotificationApplicationType)(int)message.SimpleMessageType;
            NotificationManager.Instance.SendNotification(type);
        }

        public override void HandleServerMessage(Connection connection, JsonMessage message)
        {
            Debug.WriteLine(CurrentTimeLogString + " AppService: Got JsonMessage");

            var json = JsonConvert.DeserializeObject<JsonNotificationMessage>(message.Data);
            NotificationApplicationType type = (NotificationApplicationType)(int)json.NotificationType;
            NotificationManager.Instance.SendNotification(type, json.Parameter);
        }

        public override void HandleServerMessage(Connection connection, PingRequest request)
        {
            //Sending response is not needed.
        }

        public override void HandleServerMessage(Connection connection, PongResponse message)
        {
            Debug.WriteLine(CurrentTimeLogString + " AppService: Got PongResponse");
        }

        public override void Disconnected()
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_AppServiceDisconnected);
                IsConnected = false;
            });
        }

        public override void Connected()
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_AppServiceConnected);
                IsConnected = true;
            });
        }

        protected override void UnknownMessage(Connection connection, byte[] bytes, CommunicationContextErrors errorType)
        {
            Debug.WriteLine(CurrentTimeLogString + " AppService: No handler for " + (MessageTypes)bytes[0]);
        }

        protected override void Error(Connection connection, byte[] bytes, CommunicationContextErrors errorType)
        {
            Debug.WriteLine(CurrentTimeLogString + " AppService: Error in handle " + (MessageTypes)bytes[0]);
        }
    }
}