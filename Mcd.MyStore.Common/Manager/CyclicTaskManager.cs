﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Core.Manager
{
    public static class CyclicTaskManager
    {
        private static readonly DispatcherTimer DispatcherTimer;
        private static List<CyclicTask> _cyclicTasks;

        public static CyclicTask Register(Action<object> action, TimeSpan repeatTime)
        {
            lock (_cyclicTasks)
            {
                StopTimer();

                CyclicTask task = new CyclicTask(action, repeatTime);

                _cyclicTasks.Add(task);

                StartTimer();

                return task;
            }
        }

        public static CyclicTask Register(Action action, TimeSpan repeatTime)
        {
            lock (_cyclicTasks)
            {
                StopTimer();

                CyclicTask task = new CyclicTask(action, repeatTime);

                _cyclicTasks.Add(task);

                StartTimer();

                return task;
            }
        }

        public static void Unregister(Action<object> action)
        {
            lock (_cyclicTasks)
            {
                StopTimer();

                _cyclicTasks = _cyclicTasks.Where(c => c.ActionWithData != action).ToList();

                StartTimer();
            }
        }

        public static void Unregister(Action action)
        {
            lock (_cyclicTasks)
            {
                StopTimer();

                _cyclicTasks = _cyclicTasks.Where(c => c.Action != action).ToList();

                StartTimer();
            }
        }

        private static void TimerCallback()
        {
            lock (_cyclicTasks)
            {
                foreach (CyclicTask task in _cyclicTasks)
                {
                    if (task.IsActive && DateTime.Now - task.LastExecutionTime > task.RepeatTime)
                    {
                        if (task.ActionWithData != null && task.DataHolder != null)
                        {
                            if (task.RunInOwnThread)
                            {
                                var taskClosure = task;
                                Task.Run(() =>
                                {
                                    taskClosure.ActionWithData(taskClosure.DataHolder);
                                });
                            }
                            else
                            {
                                task.ActionWithData(task.DataHolder);
                            }
                            task.LastExecutionTime = DateTime.Now;
                        }
                        else if (task.Action != null)
                        {
                            if (task.RunInOwnThread)
                            {
                                var taskClosure = task;
                                Task.Run(() =>
                                {
                                    taskClosure.Action();
                                });
                            }
                            else
                            {
                                task.Action();
                            }
                            task.LastExecutionTime = DateTime.Now;
                        }
                    }
                }
            }
        }

        private static void StartTimer()
        {
            if (DispatcherTimer != null)
            {
                DispatcherTimer.Start();
            }
        }
        private static void StopTimer()
        {
            if (DispatcherTimer != null)
            {
                DispatcherTimer.Stop();
            }
        }

        static CyclicTaskManager()
        {
            _cyclicTasks = new List<CyclicTask>();
            DispatcherTimer = new DispatcherTimer();
            DispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            DispatcherTimer.Tick += (sender, o) =>
            {
                TimerCallback();
            };
        }

        public static void InitManager()
        {
            DispatcherTimer.Start();
        }

    }

}