﻿
using System.Collections.Generic;
namespace Mcd.MyStore.WinApp.Core.Manager.Module
{
    public class MenuItemSettings
    {
        public List<MenuItem> MenuItems { get; set; }
    }

    public class MenuItem
    {
        public string Code { get; set; }
        public bool IsInHome { get; set; }
        public bool IsInSide { get; set; }
        public bool IsActive { get; set; }


        public ModuleInfo Module { get; set; }
    }
}
