﻿using System;

namespace Mcd.MyStore.WinApp.Core.Manager.Module
{
    public class ChangeModuleEventArgs : EventArgs
    {
        public ModuleInfo OldModule { get; set; }
        public ModuleInfo NewModule { get; set; }
    }
}
