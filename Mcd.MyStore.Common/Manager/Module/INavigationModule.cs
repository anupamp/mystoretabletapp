﻿using Mcd.MyStore.Common.Manager.Notification;
using System.Collections.Generic;
namespace Mcd.MyStore.WinApp.Core.Manager.Module
{
    public interface INavigationModule
    {
        void ModuleShowingInternal(NavigationArgs args);
        void ModuleHidingInternal(NavigationHideCloseArgs args);
        void ApplicationClosingInternal(NavigationHideCloseArgs args);
    }

    public class NavigationArgs
    {
        public object Parameter { get; set; }
        public List<NotificationArgs<NotificationApplicationType>> Notifications { get; set; }
    }

    public class NavigationHideCloseArgs
    {
        public bool Cancel { get; set; }

        public string NextVisibleModuleCode { get; set; }
    }
}
