﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using System;
using System.Reflection;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Core.Manager.Module
{
    public class ModuleInfo
    {
        public string Code { get; private set; }
        public TypeInfo Type { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }

        private UserControl _loadedControl = null;
        public UserControl LoadedControl
        {
            get
            {
                if (_loadedControl == null)
                {
                    _loadedControl = GetOrCreatePage();
                }
                return _loadedControl;
            }
        }

        public ModuleInfo(MenuModuleAttribute attr, TypeInfo type)
        {
            Code = attr.Code;
            Type = type;

            Title = LanguageManager.Instance.GetTranslation("MODULE_" + Code + "_Title");
            Description = LanguageManager.Instance.GetTranslation("MODULE_" + Code + "_Description");
        }

        private UserControl GetOrCreatePage()
        {
            if (_loadedControl != null)
            {
                return _loadedControl;
            }

            try
            {
                return (UserControl)Activator.CreateInstance(Type.AsType());
            }
            catch (Exception ex)
            {
                Logger.Write("Can't create instance of: " + Code + ". " + ex.ToString());
            }
            return null;
        }
    }
}
