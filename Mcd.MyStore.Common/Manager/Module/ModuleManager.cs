﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Core.Manager.Module
{
    public class ModuleManager
    {
        #region Member - Instance
        private static ModuleManager _instance = new ModuleManager();
        public static ModuleManager Instance
        {
            get
            {
                return _instance;
            }
        }
        #endregion

        private Dictionary<string, ModuleInfo> _availableModules = new Dictionary<string, ModuleInfo>();

        #region Member - CurrentVisibleControl
        private ModuleInfo _currentVisibleControl = null;
        public ModuleInfo CurrentVisibleControl
        {
            get { return _currentVisibleControl; }
        }
        #endregion

        #region Member - MenuSettings
        private MenuItemSettings _menuSettings = null;
        public MenuItemSettings MenuSettings
        {
            get { return _menuSettings; }
        }
        #endregion

        public event EventHandler<ChangeModuleEventArgs> OnChangeModulePage;
        public event EventHandler OnModuleManagerIsReady;

        private DateTime _lastReloadTime = DateTime.Now;
        private bool _reloadTaskIsRunning = false;
        public bool PauseMode { get; set; }

        public ModuleManager()
        {

        }

        public Task InitManager()
        {
            return Task.Factory.StartNew(() =>
            {
                FindAllAvailableModules().Wait();
                _menuSettings = LoadModuleSettings().Result;

                while (OnModuleManagerIsReady == null)
                {
                    Task.Delay(TimeSpan.FromSeconds(1));
                }

                if (OnModuleManagerIsReady != null)
                {
                    OnModuleManagerIsReady(this, null);
                }
            });
        }

        #region ModuleLoading
        private Task FindAllAvailableModules()
        {
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    Assembly assembly = Assembly.Load(new AssemblyName("Mcd.MyStore.WinApp.Modules"));

                    foreach (TypeInfo type in assembly.DefinedTypes.ToList())
                    {
                        List<MenuModuleAttribute> attributeList = type.GetCustomAttributes(typeof(MenuModuleAttribute), false).Select(A => (MenuModuleAttribute)A).ToList();
                        if (attributeList.Count > 0)
                        {
                            _availableModules.Add(attributeList[0].Code, new ModuleInfo(attributeList[0], type));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.AddLog("Cant't load modules: " + ex.ToString());
                }
            });
        }
        #endregion

        #region Navigation
        private ModuleInfo GetModuleInfoByModuleCode(string code)
        {
            ModuleInfo result = null;
            _availableModules.TryGetValue(code, out result);
            return result;
        }

        public bool NavigateToModule(string moduleCode)
        {
            if (!_reloadTaskIsRunning)
            {
                StartReloadTimer();
            }
            return NavigateToModule(moduleCode, null);
        }

        public bool NavigateToModule(string moduleCode, object parameter)
        {
            ModuleInfo navigateToModule = null;

            #region Send ModuleHiding and check if module allow hiding (user can cancel hiding)
            if (_currentVisibleControl != null)
            {
                if (_currentVisibleControl.LoadedControl.DataContext is INavigationModule)
                {
                    INavigationModule oldNavigationModule = (INavigationModule)_currentVisibleControl.LoadedControl.DataContext;
                    NavigationHideCloseArgs args = new NavigationHideCloseArgs();
                    args.NextVisibleModuleCode = moduleCode;
                    oldNavigationModule.ModuleHidingInternal(args);
                    if (args.Cancel)
                    {
                        return false;
                    }
                }
            }
            #endregion

            #region Navigate to new module and set the current page
            RestartReloadTimer();
            if (!String.IsNullOrEmpty(moduleCode))
            {
                navigateToModule = GetModuleInfoByModuleCode(moduleCode);
            }

            if (OnChangeModulePage != null)
            {
                OnChangeModulePage(this, new ChangeModuleEventArgs() { OldModule = _currentVisibleControl, NewModule = navigateToModule });
            }

            _currentVisibleControl = navigateToModule;
            #endregion

            if (navigateToModule != null)
            {
                UserControl navPageObject = navigateToModule.LoadedControl;
                if (navPageObject != null)
                {

                    #region Send ModuleShowing to Page Viewmodel
                    if (navPageObject.DataContext is INavigationModule)
                    {
                        INavigationModule newNavigationModuleViewModel = (INavigationModule)navPageObject.DataContext;
                        newNavigationModuleViewModel.ModuleShowingInternal(new NavigationArgs() { Parameter = parameter });
                    }
                    #endregion

                    return true;
                }
                return false;
            }
            return false;
        }


        public void RestartReloadTimer()
        {
            _lastReloadTime = DateTime.Now;
            Debug.WriteLine("Reset reload timer");
        }

        private async void StartReloadTimer()
        {
            try
            {
                if (_reloadTaskIsRunning)
                {
                    Debug.WriteLine("Don't start timer again");
                    return;
                }

                _reloadTaskIsRunning = true;

                await Task.Run(() =>
                {
                    while (true)
                    {
                        Task.Delay(1000).Wait();

                        if (PauseMode)
                            continue;

                        BackgroundWorkerManager.RunOnUI(() =>
                        {
                            if (_currentVisibleControl == null || _currentVisibleControl.LoadedControl == null || _currentVisibleControl.LoadedControl.DataContext == null)
                                return;

                            if (SystemStatusManager.Instance.GlobalUserSelectedDay != SystemStatusManager.Instance.CurrentBusinessDay)
                                return;

                            ModuleViewModelBase model = (_currentVisibleControl.LoadedControl.DataContext as ModuleViewModelBase);
                            if (model != null && model.IsAutoReloadEnabled)
                            {
                                if (_lastReloadTime.AddSeconds(SettingsManager.Instance.UpdateTriggerInSecs) < DateTime.Now)
                                {
                                    try
                                    {
                                        model.TriggerDataLoading();
                                        _lastReloadTime = DateTime.Now;
                                        Debug.WriteLine("Reload data");
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Write(ex);
                                    }
                                }
                            }
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

            _reloadTaskIsRunning = false;
        }
        #endregion

        #region ModuleSettings
        private async Task<MenuItemSettings> LoadModuleSettings()
        {
            MenuItemSettings menuSettings = null;
            try
            {
                StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/Menu/MenuSettings.dat"));
                string jsonText = await FileIO.ReadTextAsync(file);
                menuSettings = Deserialize<MenuItemSettings>(jsonText);

                foreach (var item in menuSettings.MenuItems)
                {
                    ModuleInfo tmpModuleInfo = null;
                    if (_availableModules.TryGetValue(item.Code, out tmpModuleInfo))
                    {
                        item.Module = tmpModuleInfo;
                    }
                }
                menuSettings.MenuItems = menuSettings.MenuItems.Where(M => M.Module != null).ToList();
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
            return menuSettings;
        }

        private static T Deserialize<T>(string json)
        {
            var bytes = Encoding.Unicode.GetBytes(json);
            using (MemoryStream _Stream = new MemoryStream(bytes))
            {
                var _Serializer = new DataContractJsonSerializer(typeof(T));
                return (T)_Serializer.ReadObject(_Stream);
            }
        }
        #endregion
    }
}
