﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.SystemMaintenance.Common.Dto;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Mcd.MyStore.WinApp.SystemMaintenance.WcfService.Common;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Core.Manager.User
{
    public class UserManager
    {
        #region Singleton - Instance
        private static readonly UserManager _instance = new UserManager();
        public static UserManager Instance
        {
            get { return _instance; }
        }
        #endregion
        public UserManager()
        {
           var autoLogoutManager = AutoLogoutManager.Instance;
        }

        public MyStorePrincipalDto CurrentUser { get; private set; }

        public bool Login(string username, string password)
        {
            MyStorePrincipalDto principal = null;
            bool result = true;
            try
            {
                ISystemMaintenanceServiceForApp maintenanceService = WcfConnection.ConnectWcfService<ISystemMaintenanceServiceForApp>();

                // Check credentials
                principal = maintenanceService.IsUserAuthenticated(new UserCredentialDto { UserName = username, Password = password }, false, string.Empty);
                if (!principal.IsAuthenticated)
                {
                    Logging.Logger.Write("Authentication failed (first try)");

                    principal = maintenanceService.IsUserAuthenticated(new UserCredentialDto { UserName = username, Password = password.ToUpperInvariant() }, false, string.Empty);
                    if (!principal.IsAuthenticated)
                    {
                        Logging.Logger.Write("Authentication failed (second try)");
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                Logging.Logger.Write(ex);
            }

            if (result)
            {
                Logging.Logger.Write("Authenticated!");

                CurrentUser = principal;
                NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_UserLoggedIn);
            }
            else
            {
                Logging.Logger.Write("Authentication failed (final)!");

                CurrentUser = null;
                NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_UserLoginFailed);
            }

            return result;
        }
        
        public void Logout()
        {
            CurrentUser = null;
            NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_UserLoggedOut);
        }
    }
}
