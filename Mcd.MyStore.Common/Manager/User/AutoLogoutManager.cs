﻿using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.System.Threading;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Core.Manager.User
{
    class AutoLogoutManager : INotificationModule
    {
        #region Singleton - Instance
        private static readonly AutoLogoutManager _instance = new AutoLogoutManager();
        public static AutoLogoutManager Instance
        {
            get { return _instance; }
        }
        #endregion

        public AutoLogoutManager()
        {
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_UserLoggedIn);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_UserLoggedOut);
        }

        public void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            switch (notification.Type)
            {
                case NotificationApplicationType.WinApp_UserLoggedIn:
                    StartAutomaticLogoutPeriod();
                    break;
                case NotificationApplicationType.WinApp_UserLoggedOut:
                    StopAutomaticLogoutPeriod();
                    break;
                default:
                    break;
            }
        }
        private ThreadPoolTimer _timer;

        private void StartAutomaticLogoutPeriod()
        {
            if (_timer != null) return;

            _lastActivity = DateTime.UtcNow;
            Window.Current.CoreWindow.PointerPressed += CoreWindow_PointerPressed;

            var logoutDelay = LoadDelayFromConfiguration();
            if (logoutDelay == TimeSpan.Zero)
                return;
            CheckForInactivity(logoutDelay);
        }

        DateTime _lastActivity;
        private void CoreWindow_PointerPressed(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.PointerEventArgs args)
        {
            _lastActivity = DateTime.UtcNow;
        }

        private void StopAutomaticLogoutPeriod()
        {
            if (_timer == null) return;

            Window.Current.CoreWindow.PointerPressed -= CoreWindow_PointerPressed;
            _timer.Cancel();
        }


        private void CheckForInactivity(TimeSpan logoutDelay)
        {
            var inactiveTime = DateTime.UtcNow - _lastActivity;

            if (inactiveTime >= logoutDelay)
            {
                Logger.Instance.AddLog("AutoLogout: User is inactive. Logging out user " + UserManager.Instance.CurrentUser.fullName + " after " + inactiveTime.TotalMinutes + " minutes of inactivity.");
                UserManager.Instance.Logout();
            }
            else
            {
                if (_timer != null)
                    _timer.Cancel();

                var remainingTime = logoutDelay - inactiveTime;
                Logger.Instance.AddLog("AutoLogout: User is active. Check again in " + remainingTime.TotalMinutes + " minutes");
                var syncContext = SynchronizationContext.Current;
                _timer = ThreadPoolTimer.CreateTimer(
                    _ => syncContext.Post(state => CheckForInactivity(logoutDelay), null),
                    remainingTime,
                    _ => _timer = null);
            }
        }

        private TimeSpan LoadDelayFromConfiguration()
        {

            return SettingsManager.Instance.AutoLogoutTime;
        }
    }
}
