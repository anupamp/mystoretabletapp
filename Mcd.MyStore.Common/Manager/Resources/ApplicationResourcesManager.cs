﻿using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Core.Manager.Resources
{
    public class ApplicationResourcesManager
    {
        #region Singleton
        private static ApplicationResourcesManager _instance = null;
        public static ApplicationResourcesManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ApplicationResourcesManager();
                }
                return _instance;
            }
        }
        #endregion

        private Dictionary<string, object> _data = new Dictionary<string, object>();
        public void UpdateResourcesDictionary(ResourceDictionary newData)
        {
            foreach (var dict in newData.MergedDictionaries)
            {
                foreach (var item in dict)
                {
                    if (item.Key.ToString().StartsWith("ServiceEndpoint_"))
                    {
                        _data.Add(item.Key.ToString(), item.Value);
                    }
                }
            }
        }

        public string GetEndpointForService(Type serviceType)
        {
            string host = GetString("ServiceEndpoint_HostAddress");
            if (String.IsNullOrEmpty(host))
            {
                return String.Empty;
            }

            if (!String.IsNullOrEmpty(SettingsManager.Instance.IPAddress))
            {
                host = SettingsManager.Instance.IPAddress;
            }

            //string host = "127.0.0.1";

            string key = "ServiceEndpoint_" + serviceType.Name;
            return String.Format(GetString(key), host);
        }

        public string GetString(string key)
        {
            return (string)GetValue(key, string.Empty);
        }

        public bool GetBool(string key)
        {
            return (bool)GetValue(key, false);
        }

        public object GetValue(string key, object defaultValue)
        {
            try
            {
                object tmpObject = null;
                if (_data.TryGetValue(key, out tmpObject))
                {
                    return tmpObject;
                }
            }
            catch { }
            return defaultValue;
        }
    }
}
