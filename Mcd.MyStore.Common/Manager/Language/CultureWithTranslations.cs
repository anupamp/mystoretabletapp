﻿using System.Collections.Generic;
using System.Globalization;

namespace Mcd.MyStore.WinApp.Core.Manager.Language
{
    public class CultureWithTranslations
    {
        public CultureInfo Culture { get; set; }

        public Dictionary<string, string> Translations { get; set; }

        public CultureWithTranslations(CultureInfo culture, Dictionary<string, string> data = null)
        {
            Culture = culture;
            Translations = data;

            if (Translations == null)
            {
                Translations = new Dictionary<string, string>();
            }
        }
    }
}
