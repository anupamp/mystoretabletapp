﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources.Core;
using Windows.Globalization;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mcd.MyStore.WinApp.Core.Logging;

namespace Mcd.MyStore.WinApp.Core.Manager.Language
{
    public class LanguageManager
    {
        #region Singleton
        private static LanguageManager _instance = null;
        public static LanguageManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LanguageManager();
                }
                return _instance;
            }
        }
        #endregion

        #region Member - CurrentCulture
        private CultureInfo _currentCulture = null;
        public CultureInfo CurrentCulture
        {
            get { return _currentCulture; }
            set
            {
                if (_currentCulture != value)
                {
                    _currentCulture = value;
                    ApplicationLanguages.PrimaryLanguageOverride = _currentCulture.Name;

                    CultureInfo.DefaultThreadCurrentCulture = _currentCulture;
                    CultureInfo.DefaultThreadCurrentUICulture = _currentCulture;

                    ResourceContext.GetForViewIndependentUse().Reset();
                    ResourceContext.GetForCurrentView().Reset();
                    //ResourceManager.Current.DefaultContext.Reset();

                    _currentCultureTranslations = _availableCultureWithTranslations.FirstOrDefault(C => C.Culture.Equals(_currentCulture));

                    if (OnCurrentCultureIsChanged != null)
                    {
                        OnCurrentCultureIsChanged(this, null);
                    }
                }
            }
        }
        #endregion

        #region Member - CurrentCultureTranslations
        private CultureWithTranslations _currentCultureTranslations = new CultureWithTranslations(new CultureInfo("en-gb"), new Dictionary<string, string>
        {
            {"Login_Username", "Username"},
            {"Login_Password", "Password"},
            {"Login_Language", "Language"},
            {"Login_Login", "Login"},
            {"Login_Loading_LoadingTranslations", "Loading Translations..."},
            {"BASIC_PleaseWait", "Please wait..."},
        });
        public CultureWithTranslations CurrentCultureTranslations
        {
            get { return _currentCultureTranslations; }
        }
        #endregion

        private List<CultureWithTranslations> _availableCultureWithTranslations;
        public event EventHandler OnCurrentCultureIsChanged;

        public List<CultureInfo> AvailableCultures
        {
            get
            {
                return _availableCultureWithTranslations.Select(C => C.Culture).ToList();
            }
        }

        public LanguageManager()
        {
            _availableCultureWithTranslations = new List<CultureWithTranslations>();
        }

        public string this[string key]
        {
            get
            {
                #region return key in designer mode
                if (DesignMode.DesignModeEnabled)
                {
                    return "?" + key + "?";
                }
                #endregion

                string result = string.Empty;
                if (CurrentCultureTranslations == null || CurrentCultureTranslations.Translations == null)
                {
                    return key;
                }

                if (CurrentCultureTranslations.Translations.TryGetValue(key, out result))
                {
                    return result;
                }

                return key;
            }
        }

        public string GetTranslation(string key)
        {
            return this[key];
        }

        public async Task InitManager()
        {
            var availableCultures = await GetAvailableCultures();
            foreach (var culture in availableCultures)
            {
                _availableCultureWithTranslations.Add(new CultureWithTranslations(new CultureInfo(culture), await GetTranslations(culture)));
            }
        }

        private async Task<List<string>> GetAvailableCultures()
        {
            using (var httpClient = new HttpClient())
            {
                string endpoint = SettingsManager.Instance.IPAddress;
                string requestUri = string.Format("http://{0}:8889/translations/GetInstalledTranslations/json?Application=MyStoreTabletApp", endpoint);
                string resultJson = await httpClient.GetStringAsync(requestUri);
                JObject jsonObject = JObject.Parse(resultJson);
                bool success = jsonObject.SelectToken("Result.Successful").ToObject<bool>();
                if (!success)
                {
                    Logger.Write("Error while retrieving available cultures:" + resultJson);
                    return null;
                }
                var availableCultures = jsonObject.SelectToken("Result.Data.Entries").ToObject<List<string>>();
                return availableCultures;
            }
        }

        private async Task<Dictionary<string, string>> GetTranslations(string culture)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string endpoint = SettingsManager.Instance.IPAddress;
                    string requestUri = string.Format("http://{0}:8889/translations/GetModuleTranslation/json?Application=MyStoreTabletApp&Culture=" + culture, endpoint);
                    string resultJson = await httpClient.GetStringAsync(requestUri);
                    JObject jsonObject = JObject.Parse(resultJson);
                    bool success = jsonObject.SelectToken("Result.Successful").ToObject<bool>();
                    if (!success)
                    {
                        return null;
                    }

                    var translationServiceEntries = jsonObject.SelectToken("Result.Data.Entries").ToObject<Dictionary<string, List<KeyValuePair<string, string>>>>();

                    Dictionary<string, string> result = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, List<KeyValuePair<string, string>>> modulePair in translationServiceEntries)
                    {
                        foreach (var translationPair in modulePair.Value)
                        {
                            result[translationPair.Key] = translationPair.Value;
                        }
                    }
                    return result;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
