﻿using System;

namespace Mcd.MyStore.WinApp.Core.Manager
{
    public class CyclicTask
    {

        /// <summary>
        /// Task Action
        /// </summary>
        public Action<object> ActionWithData { get; set; }

        public Action Action { get; set; }


        private bool isActive = true;

        public bool IsActive
        {
            get { return isActive; }
            set
            {
                LastExecutionTime = DateTime.Now;
                isActive = value;
            }
        }

        public bool RunInOwnThread { get; set; }

        /// <summary>
        /// Action repeat interval
        /// </summary>
        public TimeSpan RepeatTime { get; set; }

        /// <summary>
        /// Time of last action execution
        /// </summary>
        internal DateTime LastExecutionTime { get; set; }

        public object DataHolder { get; set; }

        public CyclicTask()
        {

        }

        internal CyclicTask(Action<object> action, TimeSpan repeatTime)
        {
            this.ActionWithData = action;
            RepeatTime = repeatTime;
        }

        internal CyclicTask(Action action, TimeSpan repeatTime)
        {
            this.Action = action;
            RepeatTime = repeatTime;
        }

        public void RunNow()
        {
            isActive = true;
            LastExecutionTime = DateTime.MinValue;
        }
    }

}