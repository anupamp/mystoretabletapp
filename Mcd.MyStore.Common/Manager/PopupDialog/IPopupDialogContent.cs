﻿
namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public interface IPopupDialogContent
    {
        PopupDialogContentViewModel ViewModel { get; set; }
    }
}
