﻿
namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public class PopupDialogData
    {
        public delegate void DialogResultHandler(PopupDialogData sender, bool? dialogResult);
        public event DialogResultHandler OnDialogClose;

        public IPopupDialogContent Content { get; set; }


        #region Member - ViewModel
        private PopupDialogContentViewModel _viewModel;
        public PopupDialogContentViewModel ViewModel
        {
            get { return _viewModel; }
            set
            {
                _viewModel = value;
                _viewModel.OnSetDialogResult += OnSetDialogResult;
            }
        }

        private void OnSetDialogResult(PopupDialogContentViewModel sender, bool? dialogResult)
        {
            if (OnDialogClose != null)
            {
                OnDialogClose(this, dialogResult);
            }
        }
        #endregion



    }
}
