﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public abstract class PopupDialogContentViewModel : ViewModelBase
    {

        public delegate void DialogResultHandler(PopupDialogContentViewModel sender, bool? dialogResult);
        public event DialogResultHandler OnSetDialogResult;

        #region Member DialogResult
        private bool? _dialogResult;
        public bool? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (OnSetDialogResult != null)
                {
                    OnSetDialogResult(this, _dialogResult);
                }
            }
        }
        #endregion

        #region Member - TranslateX
        private double _translateX;
        public double TranslateX
        {
            get { return _translateX; }
            set { SetValue(ref _translateX, value); }
        }
        #endregion

        #region Member - TranslateY
        private double _translateY;
        public double TranslateY
        {
            get { return _translateY; }
            set { SetValue(ref _translateY, value); }
        }
        #endregion

        #region Member - AllowDragMove
        private bool _allowDragMove = false;
        public bool AllowDragMove
        {
            get { return _allowDragMove; }
            set { SetValue(ref _allowDragMove, value); }
        }
        #endregion


        public void ManipulationDelta(ContentControl control, FrameworkElement outerElement, ManipulationDeltaRoutedEventArgs e)
        {
            if (_allowDragMove)
            {
                GeneralTransform relTransform = control.TransformToVisual(outerElement);
                Point relPosition = relTransform.TransformPoint(new Point(0, 0));
                Point orgPoint = relPosition;

                relPosition.X += e.Delta.Translation.X;
                relPosition.Y += e.Delta.Translation.Y;

                relPosition.X = Math.Max(relPosition.X, 0);
                relPosition.Y = Math.Max(relPosition.Y, 0);

                relPosition.X = Math.Min(relPosition.X, outerElement.ActualWidth - control.ActualWidth);
                relPosition.Y = Math.Min(relPosition.Y, outerElement.ActualHeight - control.ActualHeight);


                double newX = relPosition.X - orgPoint.X;
                double newY = relPosition.Y - orgPoint.Y;

                TranslateX += newX;
                TranslateY += newY;
            }
        }
    }
}
