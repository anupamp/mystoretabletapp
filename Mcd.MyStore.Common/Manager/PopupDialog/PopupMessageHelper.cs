﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Text;
using Windows.UI.Xaml.Controls;
using Mcd.MyStore.WinApp.Core.Controls.CommonDialog;

namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public class PopupMessageHelper
    {
        public readonly static PopupMessageHelper Instance = new PopupMessageHelper();
        private readonly static PopupDialogManager _manager = new PopupDialogManager();
        public static PopupDialogData ShowQuestionDialog(string caption, string text, Action<bool> completeAction)
        {
            return _manager.ShowQuestionDialog(caption, text, completeAction);
        }

        public static PopupDialogData ShowException(string text)
        {
            var dialogData = PopupDialogManager.CreateDialogData<InformationDialog, InformationDialogViewModel>(new InformationDialogViewModel("Open Close Center", text, true));
            return _manager.ShowDialog(dialogData);
        }

        public static Task ShowExceptionAsync(string text)
        {
            var popupDialogData = ShowException(text);

            return Task.Run(async () =>
            {
                bool fin = false;
                popupDialogData.OnDialogClose += (sender, result) => fin = true;

                while (!fin)
                {
                    await Task.Delay(100);
                }
            });
        }

        public static PopupDialogData ShowMessage(string caption, string text)
        {
            var dialogData = PopupDialogManager.CreateDialogData<InformationDialog, InformationDialogViewModel>(new InformationDialogViewModel(caption, text));
            return _manager.ShowDialog(dialogData);
        }

        public static Task<PopupDialogData> ShowMessageAsync(string caption, string text)
        {
            var popupDialogData = ShowMessage(caption, text);

            return Task.Run(async () =>
            {
                bool fin = false;
                popupDialogData.OnDialogClose += (sender, result) => fin = true;

                while (!fin)
                {
                    await Task.Delay(100);
                }

                return popupDialogData;
            });
        }

        public static PopupDialogData ShowDialog(PopupDialogData dialogData)
        {
            return _manager.ShowDialog(dialogData);
        }

        public static PopupDialogData CreateDialogData<TUserControl, TViewModel>(TViewModel viewModel = null)
            where TUserControl : UserControl, IPopupDialogContent, new()
            where TViewModel : PopupDialogContentViewModel, new()
        {
            return PopupDialogManager.CreateDialogData<TUserControl, TViewModel>(viewModel);
        }

        public static void SetGlobalPopupContainer(IPopupDialogContainer container)
        {
            _manager.Container = container;
        }

        public static void CloseDialog()
        {
            _manager.CloseDialog();
        }

        public static void CloseDialog(PopupDialogData dialogData)
        {
            _manager.CloseDialog(dialogData);
        }
    }
}
