﻿
using Mcd.MyStore.WinApp.Core.Controls.CommonDialog;
using System;
using Windows.UI.Xaml.Controls;
namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public class PopupDialogManager
    {
        public IPopupDialogContainer Container { get; set; }

        public PopupDialogData ShowDialog(PopupDialogData dialogData)
        {
            dialogData.OnDialogClose += (PopupDialogData sender, bool? dialogResult) =>
            {
                Container.RemovePopupDialogData(sender);
            };

            Container.AddPopupDialogData(dialogData);
            return dialogData;
        }

        public void CloseDialog()
        {
            Container.RemovePopupDialogData();
        }

        public void CloseDialog(PopupDialogData dialogData)
        {
            Container.RemovePopupDialogData(dialogData);
        }

        public PopupDialogData ShowQuestionDialog(string caption, string text, Action<bool> userResultAction)
        {
            PopupDialogData dialogData = CreateDialogData<QuestionDialog, QuestionDialogViewModel>(new QuestionDialogViewModel(caption, text));
            dialogData.OnDialogClose += (PopupDialogData sender, bool? dialogResult) =>
            {
                if (userResultAction != null)
                {
                    userResultAction(dialogResult.HasValue && dialogResult.Value);
                }
            };
            ShowDialog(dialogData);
            return dialogData;
        }

        public static PopupDialogData CreateDialogData<TUserControl, TViewModel>(TViewModel viewModel = null)
            where TUserControl : UserControl, IPopupDialogContent, new()
            where TViewModel : PopupDialogContentViewModel, new()
        {
            PopupDialogData result = new PopupDialogData();
            result.Content = new TUserControl();
            if (viewModel != null)
            {
                result.ViewModel = viewModel;
            }
            result.Content.ViewModel = result.ViewModel;



            return result;
        }
    }
}
