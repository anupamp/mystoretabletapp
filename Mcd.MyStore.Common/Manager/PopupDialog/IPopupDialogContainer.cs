﻿
namespace Mcd.MyStore.WinApp.Core.Manager.PopupDialog
{
    public interface IPopupDialogContainer
    {
        void AddPopupDialogData(PopupDialogData dialogData);

        void RemovePopupDialogData();
        void RemovePopupDialogData(PopupDialogData dialogData);
    }
}
