﻿using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;

namespace Mcd.MyStore.WinApp.Core.Logging
{
    public class Logger
    {
        private static object _lockObject = new object();
        private static StorageFile _currentFile = null;
        private static Logger _instance = null;
        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Logger();
                }
                return _instance;
            }
        }

        private List<string> _logMessages = new List<string>();

        public Logger()
        {
            if (SettingsManager.Instance.LoggingEnabled)
            {
                StartTimer();
            }
        }

        private async void StartTimer()
        {
            while (true)
            {
                await Task.Delay(5000);
                await WriteLogInFile();
            }
        }

        public void AddLog(string text)
        {
            if (!SettingsManager.Instance.LoggingEnabled)
            {
                return;
            }

            string message = String.Format("{0} - {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss.fff"), text);
            _logMessages.Add(message);
        }

        public static void Write(string text)
        {
            Instance.AddLog(text);
        }

        public static void Write(Exception ex)
        {
            Instance.AddLog(ex.ToString());
        }

        private async Task WriteLogInFile()
        {
            StorageFolder roamingFolder = ApplicationData.Current.RoamingFolder;
            StorageFolder settingsFolder = await GetOrCreateFolder(roamingFolder, "MyStore");

            _currentFile = null;
            string fileName = DateTime.Now.ToString("yyyyMMdd") + "_Log.txt";
            try
            {
                _currentFile = await settingsFolder.GetFileAsync(fileName);
            }
            catch { }
            try
            {
                if (_currentFile == null)
                {
                    _currentFile = await settingsFolder.CreateFileAsync(fileName, CreationCollisionOption.FailIfExists);
                }

                if (_currentFile != null)
                {
                    string[] lines = new string[_logMessages.Count];
                    _logMessages.CopyTo(lines);
                    _logMessages.Clear();

                    string allText = String.Empty;
                    foreach (var item in lines)
                    {
                        allText += item + "\r\n";
                    }
                    await FileIO.AppendTextAsync(_currentFile, allText);
                }
            }
            catch { }
        }

        private async static Task<StorageFolder> GetOrCreateFolder(StorageFolder storageFolder, string folderName)
        {
            StorageFolder folder = null;

            try
            {
                folder = await storageFolder.GetFolderAsync(folderName);
            }
            catch { }

            if (folder == null)
            {
                folder = await storageFolder.CreateFolderAsync(folderName);
            }

            return folder;
        }
    }
}
