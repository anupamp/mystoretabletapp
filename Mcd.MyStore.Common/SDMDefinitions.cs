﻿namespace Mcd.MyStore.WinApp.Core
{
    public class SDMDefinitions
    {
        public static class OpenCloseLogEntryEventType
        {
            public static readonly string WorkflowStarted = "Workflow_Started";
            public static readonly string WorkflowEnded = "Workflow_Ended";
            public static readonly string WorkflowForceStartMessage = "Workflow_ForceStart";

            public static readonly string PrerequisiteCheckStarted = "PrerequisiteCheck_Started";
            public static readonly string PrerequisiteCheckEnded = "PrerequisiteCheck_Ended";

            public static readonly string WorkstepStarted = "Workstep_Started";
            public static readonly string WorkstepEnded = "Workstep_Ended";

            public static readonly string ItemWorkingDescription = "ItemWorking_Description";

            public static readonly string ItemUserMessage = "ItemUserMessage";
            public static readonly string LegacyScreenMessage = "LegacyScreenMessage";

            public static readonly string ItemCustomUserQuestion = "ItemCustomUserQuestion";
        }

        public class ControlTransactionReasonOrCloseCode
        {
            /// <summary>
            /// Identification for a daily close 
            /// </summary>
            public const string DailyClose = "DAY";
            public static readonly int DailyCloseValue = 0;

            /// <summary>
            /// Identification for a weekly close 
            /// </summary>
            public const string WeeklyClose = "WEEK";
            public static readonly int WeeklyCloseValue = 1;

            /// <summary>
            /// Identification for a monthly close 
            /// </summary>
            public const string MonthlyClose = "MONT";
            public static readonly int MonthlyCloseValue = 2;

            /// <summary>
            /// Identification for a yearly close 
            /// </summary>
            public const string YearlyClose = "YEAR";
            public static readonly int YearlyCloseValue = 3;
        }

        public class OpenCloseLogInformationWorkflowMode
        {
            public static readonly string Normal = "NORMAL";
            public static readonly string Offline = "OFFLINE";
        }

        public enum ItemTransaction_Operator
        {
            StorewideEmployeeMeal = -100
        }
    }
}