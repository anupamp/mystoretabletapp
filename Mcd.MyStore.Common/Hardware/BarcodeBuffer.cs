﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Networking.NetworkOperators;

namespace Mcd.MyStore.WinApp.Core.Hardware
{
    public class BarcodeBuffer
    {
        #region Singleton

        private static readonly BarcodeBuffer _instance = new BarcodeBuffer();
        private readonly Queue<BufferInfo> _buffer = new Queue<BufferInfo>(30);

        static BarcodeBuffer()
        {
        }

        private BarcodeBuffer()
        {
            KeyEventHandlerAsync();
        }

        private void KeyEventHandlerAsync()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    if (_buffer.Count == 0)
                    {
                        await Task.Delay(1);
                        continue;
                    }

                    BufferInfo bufferInfo = _buffer.Peek();
                    if ((DateTime.Now - bufferInfo.AddTime).TotalMilliseconds > 500)
                    {
                        var result = Get();
                        if (Receive != null && !string.IsNullOrWhiteSpace(result))
                            Receive(result);
                    }
                    await Task.Delay(1);
                }
            });
        }

        public static BarcodeBuffer Instance
        {
            get { return _instance; }
        }

        #endregion

        public delegate void InputReceived(string input);
        public event InputReceived Receive;

        public void Add(uint character)
        {
            Debug.WriteLine(character + " - " + (char)character);

            if (character == '\r' || character == '\n') return;
            _buffer.Enqueue(new BufferInfo((char)character));
        }

        public string Get()
        {
            string result = "";
            int count = _buffer.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                if (_buffer.Count == 0) break;
                BufferInfo entry = _buffer.Dequeue();
                result += entry.Character.ToString();
            }

            return result;
        }
    }

    [DebuggerDisplay("{Character}")]
    internal class BufferInfo
    {
        public BufferInfo(char chr)
        {
            AddTime = DateTime.Now;
            Character = chr;
        }

        public DateTime AddTime { get; set; }
        public char Character { get; set; }
    }
}