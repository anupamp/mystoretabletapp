﻿using System;

namespace Mcd.MyStore.WinApp.Core.Attributes
{
    public class MenuModuleAttribute : Attribute
    {
        public string Code { get; set; }

        public MenuModuleAttribute(string code)
        {
            Code = code;
        }
    }
}
