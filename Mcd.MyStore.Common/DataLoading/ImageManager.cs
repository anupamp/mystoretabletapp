﻿using Mcd.MyStore.WinApp.Core.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Core.DataLoading
{
    public class ImageManager
    {
        #region Singleton
        private static ImageManager _instance;
        public static ImageManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ImageManager();
                }
                return _instance;
            }
        }
        #endregion

        private readonly Dictionary<string, BitmapImage> _imageCache = new Dictionary<string, BitmapImage>();

        public async Task<bool> PreloadImages(string[] sourceList)
        {
            foreach (var item in sourceList)
            {
                await GetImage(item);
            }
            return true;
        }

        public async Task<BitmapImage> GetImage(string source)
        {
            try
            {
                StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx://" + source));
                return await GetImage(file);
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        public async Task<BitmapImage> GetImage(StorageFile fileSource)
        {
            try
            {
                string key = fileSource.Path.ToLower();
                if (!_imageCache.ContainsKey(key))
                {
                    using (IRandomAccessStream fileStream = await fileSource.OpenAsync(FileAccessMode.Read))
                    {
                        BitmapImage bitmapImage = new BitmapImage();
                        await bitmapImage.SetSourceAsync(fileStream);
                        if (!_imageCache.ContainsKey(key))
                        {
                            _imageCache.Add(key, bitmapImage);
                        }
                    }
                }

                return _imageCache[key];
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }
    }
}
