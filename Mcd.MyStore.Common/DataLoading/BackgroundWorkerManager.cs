﻿using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Mcd.MyStore.Common.DataLoading
{
    public class BackgroundWorkerManager
    {
        #region Singleton
        private static BackgroundWorkerManager _instance = null;
        public static BackgroundWorkerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new BackgroundWorkerManager();
                }
                return _instance;
            }
        }
        #endregion


        public void StartWorker(Action work, Action completeWork, bool showLoadingIndicator = true)
        {
            StartWorker<object>(
                () =>
                {
                    if (work != null)
                    {

                        work();

                    }
                    return null;
                },
                (object result) =>
                {
                    if (completeWork != null)
                    {
                        completeWork();
                    }
                }, showLoadingIndicator);
        }

        public delegate void BackgroundWorkerResultDelegate<T>(T result);
        public async void StartWorker<T>(Func<T> work, BackgroundWorkerResultDelegate<T> completeWork, bool showLoadingIndicator = true)
        {
            try
            {
                if (showLoadingIndicator)
                {
                    SettingsManager.IncrementItemsToProcess();
                }

                var result = await Task.Run<T>(work);

                if (showLoadingIndicator)
                {
                    SettingsManager.DecrementItemsToProcess();
                }

                if (completeWork != null)
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                    {
                        completeWork(result);
                    });

                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
        }

        public static async void RunOnUI(Action uiAction, bool waitForExecution = false)
        {
            if (waitForExecution)
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    uiAction();
                });
                return;
            }

            CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                uiAction();
            });
        }
    }
}
