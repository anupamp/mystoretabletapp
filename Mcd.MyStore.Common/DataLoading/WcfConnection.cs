﻿using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Resources;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace Mcd.MyStore.Common.DataLoading
{
    public class WcfConnection
    {
        /// <summary>
        /// Connects to the specified WCF service and returns a opened service reference
        /// </summary>
        /// <typeparam name="T">The WCF service.</typeparam>
        /// <param name="serviceEndpoint">The name of the service endpoint.</param>
        /// <returns>Returns a opened service reference.</returns>
        public static T ConnectWcfService<T>()
        {
            T wcfService = default(T);
            try
            {
                string endpoint = ApplicationResourcesManager.Instance.GetEndpointForService(typeof(T));

                //await Window.Current.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                //{
                //    endpoint = ApplicationResourcesManager.Instance.GetEndpointForService(typeof(T));
                //});

                Logger.Write("Connect " + typeof(T).Name + " with endpoint: " + endpoint);
                if (String.IsNullOrEmpty(endpoint))
                {
                    return wcfService;
                }

                Binding binding = new BasicHttpBinding();
                if (endpoint.StartsWith("net.tcp"))
                {
                    if (endpoint.Contains("InventoryService"))
                    {
                        binding = new NetTcpBinding { Security = { Mode = SecurityMode.None }, MaxReceivedMessageSize = Int32.MaxValue, TransferMode = TransferMode.Streamed };
                    }
                    else
                    {
                        binding = new NetTcpBinding { Security = { Mode = SecurityMode.None }, MaxReceivedMessageSize = Int32.MaxValue};
                    }
                }
                ChannelFactory<T> channelFactory = new ChannelFactory<T>(binding, new EndpointAddress(endpoint));
                wcfService = channelFactory.CreateChannel(new EndpointAddress(channelFactory.Endpoint.Address.ToString()));
                ((IClientChannel)wcfService).Open();
            }
            catch (Exception ex)
            {
                string logMessage = "ConnectWcfService encountered an error: " + ex.ToString();
                Logger.Write(logMessage);
            }

            return wcfService;
        }

        public static Task<T2> ExecuteActionOnInterface<T, T2>(Func<T, T2> interfaceAction)
        {
            return Task.Factory.StartNew(() =>
            {
                T2 result = default(T2);
                try
                {
                    T currentInterface = ConnectWcfService<T>();
                    result = interfaceAction(currentInterface);
                    DisconnectWcfService(currentInterface);
                }
                catch (Exception ex)
                {
                    Logger.Write(ex);
                }
                return result;
            });
        }

        /// <summary>
        /// Disconnects a opened WCF service reference
        /// </summary>
        /// <typeparam name="T">The WCF service.</typeparam>
        /// <param name="wcfService">The WCF service reference.</param>
        public static void DisconnectWcfService<T>(T wcfService)
        {
            //Logger.Write("Disconnect " + typeof(T).FullName + "\r\n");

            IClientChannel serviceChannel = wcfService as IClientChannel;

            try
            {
                Logger.Write("Disconnect " + typeof(T).Name);
                if (serviceChannel != null)
                {
                    serviceChannel.Close();
                }
            }
            catch (Exception ex)
            {
                if (serviceChannel != null)
                {
                    serviceChannel.Abort();
                }

                string logMessage = "DisconnectWcfService encountered an error: " + ex.ToString();
                Logger.Write(logMessage);
            }
        }

        public static Task ExecuteActionOnInterface<T>(Action<T> interfaceAction)
        {
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    T currentInterface = ConnectWcfService<T>();
                    interfaceAction(currentInterface);
                    DisconnectWcfService(currentInterface);
                }
                catch (Exception ex)
                {
                    Logger.Write(ex);
                }
            });
        }
    }
}
