﻿using Mcd.MyStore.WinApp.Core.Logging;
using NotificationsExtensions.TileContent;
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Core.HomeTile
{
    public class HomeTileManager
    {
        private static HomeTileManager _instance;
        public static HomeTileManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HomeTileManager();
                }
                return _instance;
            }
        }

        public FrameworkElement MediumTile { get; set; }
        public FrameworkElement WideTile { get; set; }

        private async Task<bool> GenerateImageToFile(string fileName, UIElement uiContent)
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                if (file != null)
                {
                    CachedFileManager.DeferUpdates(file);
                    using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite))
                    {
                        await CaptureToStreamAsync(uiContent, stream, BitmapEncoder.PngEncoderId);
                    }
                    await CachedFileManager.CompleteUpdatesAsync(file);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }

        private async Task<RenderTargetBitmap> CaptureToStreamAsync(UIElement uielement, IRandomAccessStream stream, Guid encoderId)
        {
            var renderTargetBitmap = new RenderTargetBitmap();
            try
            {
                await renderTargetBitmap.RenderAsync(uielement);
                var pixels = await renderTargetBitmap.GetPixelsAsync();
                var logicalDpi = DisplayInformation.GetForCurrentView().LogicalDpi;
                var encoder = await BitmapEncoder.CreateAsync(encoderId, stream);
                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight, (uint)renderTargetBitmap.PixelWidth, (uint)renderTargetBitmap.PixelHeight, logicalDpi, logicalDpi, pixels.ToArray());
                await encoder.FlushAsync();
            }
            catch { }
            return renderTargetBitmap;
        }



        private void SetLiveTileImage(string wideImageFileName, string mediumImageFileUri)
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
            ITileWideImage tileContent = TileContentFactory.CreateTileWideImage();
            var squareTileContent = TileContentFactory.CreateTileSquareImage();
            tileContent.Image.Src = "ms-appdata:///local/" + wideImageFileName;
            squareTileContent.Image.Src = mediumImageFileUri;
            tileContent.SquareContent = squareTileContent;
            tileContent.Branding = TileBranding.None;
            TileNotification MyTileNotification = tileContent.CreateNotification();
            TileUpdateManager.CreateTileUpdaterForApplication().Update(MyTileNotification);
        }

        public async void UpdateTile(byte[] wideBytes, string imageUri)
        {
            string fileName = DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".png";

            bool result1 = await SaveImage("wideTile.png", wideBytes);
            //bool result2 = await GenerateImageToFile("mediumTile.png", MediumTile);

            if (result1)
            {
                SetLiveTileImage("wideTile.png", imageUri);
            }

        }

        private async Task<bool> SaveImage(string fileName, byte[] data)
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                if (file != null)
                {
                    await FileIO.WriteBytesAsync(file, data);
                }
                return true;
            }
            catch(Exception ex)
            {
                Logger.Write(ex);
                return false;
            }
        }

        private async Task<BitmapImage> ByteArrayToBitmapImage(byte[] byteArray)
        {
            var bitmapImage = new BitmapImage();

            var stream = new InMemoryRandomAccessStream();
            await stream.WriteAsync(byteArray.AsBuffer());
            stream.Seek(0);

            bitmapImage.SetSource(stream);
            return bitmapImage;
        }
    }
}
