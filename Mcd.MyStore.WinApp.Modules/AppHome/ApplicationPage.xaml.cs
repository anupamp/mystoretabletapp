﻿using Mcd.MyStore.WinApp.Core.HomeTile;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using NavigationHelper = Mcd.MyStore.WinApp.Core.UI.Common.NavigationHelper;

namespace Mcd.MyStore.WinApp.Modules.AppHome
{
    /// <summary>
    /// Eine Standardseite mit Eigenschaften, die die meisten Anwendungen aufweisen.
    /// </summary>
    public sealed partial class ApplicationPage
    {
        private readonly ApplicationPageViewModel _viewModel = new ApplicationPageViewModel();
        private readonly NavigationHelper _navigationHelper;

        public Double ScreenHeight { get { return Window.Current.Bounds.Height; } }
        public Double ScreenWidth { get { return Window.Current.Bounds.Width; } }


        /// <summary>
        /// NavigationHelper wird auf jeder Seite zur Unterstützung bei der Navigation verwendet und 
        /// Verwaltung der Prozesslebensdauer
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return _navigationHelper; }
        }

        public ApplicationPage()
        {
            InitializeComponent();
            DataContext = _viewModel;

            _navigationHelper = new NavigationHelper(this);

            HomeTileManager.Instance.MediumTile = MediumTile;
            HomeTileManager.Instance.WideTile = WideTile;

            PopupMessageHelper.SetGlobalPopupContainer(PopupDialogContainer);

            Window.Current.VisibilityChanged += AppVisibilityChanged;
        }

        private void AppVisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            ModuleManager.Instance.PauseMode = !e.Visible;
        }


        #region NavigationHelper-Registrierung

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void ConnectivityTextFadeIn_Completed(object sender, object e)
        {
            ConnectivityTextFadeAway.Begin();
        }
    }
}
