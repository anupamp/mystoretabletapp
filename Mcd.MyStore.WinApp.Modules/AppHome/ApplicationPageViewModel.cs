﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.HomeTile;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.Manager.User;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.AppHome.Controls.ViewModel;
using Mcd.MyStore.WinApp.Modules.AppHome.ViewModel;
using Mcd.MyStore.WinApp.Modules.Inventory.Manager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.AppHome
{
    public class ApplicationPageViewModel : ViewModelBase, INotificationModule
    {

        #region Member - HomeMenuViewModel
        private HomeMenuViewModel _homeMenuViewModel = new HomeMenuViewModel();
        public HomeMenuViewModel HomeMenuViewModel
        {
            get { return _homeMenuViewModel; }
        }
        #endregion

        #region Member - SideMenuViewModel
        private SideMenuViewModel _sideMenuViewModel = new SideMenuViewModel();
        public SideMenuViewModel SideMenuViewModel
        {
            get { return _sideMenuViewModel; }
        }
        #endregion


        public DateTime CurrentBusinessDay { get; set; }

        public ObservableCollection<DateSelectViewModel> AvailableDates { get; set; }

        public bool AreAvailableDatesEnabled
        {
            get { return _areAvailableDatesEnabled; }
            set { SetValue(ref _areAvailableDatesEnabled, value); }
        }

        private Uri _connectivityStatus = new Uri("ms-appx:///Assets/CircleRed.png");
        public Uri ConnectivityStatus
        {
            get
            {
                return _connectivityStatus;
            }
            set
            {
                SetValue(ref _connectivityStatus, value);
            }
        }

        private string _connectivityStatusChanged = "Connected";
        public string ConnectivityStatusChanged
        {
            get
            {
                return _connectivityStatusChanged;
            }
            set
            {
                if (SetValue(ref _connectivityStatusChanged, value))
                {
                    if (ConnectivityVisibility != Visibility.Visible)
                    {
                        ConnectivityVisibility = Visibility.Visible;
                    }
                }
            }
        }

        private Visibility _connectivityVisibility = Visibility.Collapsed;
        public Visibility ConnectivityVisibility
        {
            get { return _connectivityVisibility; }
            set { SetValue(ref _connectivityVisibility, value); }
        }

        #region Member SelectedDate
        private DateSelectViewModel _selectedDate;
        public DateSelectViewModel SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if (SetValue<DateSelectViewModel>(ref _selectedDate, value))
                {
                    SystemStatusManager.Instance.GlobalUserSelectedDay = _selectedDate.Date;
                    NotificationManager.Instance.SendNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
                }
            }
        }
        #endregion

        #region Wait Indicator

        public String WaitingIndicatorText
        {
            get { return SettingsManager.Instance.WaitingIndicatorText; }
        }

        public Visibility WaitingIndicatorVisibility
        {
            get { return SettingsManager.Instance.WaitingIndicatorVisibility; }
        }

        #endregion


        #region Member - CurrentVisibleControl
        private UserControl _currentVisibleControl;
        public UserControl CurrentVisibleControl
        {
            get { return _currentVisibleControl; }
            set { SetValue(ref _currentVisibleControl, value); }
        }
        #endregion

        #region Member - ModuleIsVisible
        private bool _moduleIsVisible;
        public bool ModuleIsVisible
        {
            get { return _moduleIsVisible; }
            set { SetValue(ref _moduleIsVisible, value); }
        }
        #endregion

        #region Member - IsAppContentVisibile
        private bool _isAppContentVisibile = false;
        public bool IsAppContentVisibile
        {
            get { return _isAppContentVisibile; }
            set { SetValue(ref _isAppContentVisibile, value); }
        }
        #endregion

        private bool _isUpdateTimerRunning;

        private bool _isFirstLoading = true;
        private bool _areAvailableDatesEnabled;

        public ICommand CloseAppCommand { get; set; }
        public ICommand UserLogoutCommand { get; set; }

        public ApplicationPageViewModel()
        {
            AvailableDates = new ObservableCollection<DateSelectViewModel>();
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.SystemStatusChanged);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_UserLoggedIn);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_UserLoggedOut);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceConnected);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceDisconnected);

            ModuleManager.Instance.OnChangeModulePage += ModuleManagerOnChangeModulePage;
            ModuleManager.Instance.OnModuleManagerIsReady += ModuleManagerOnModuleManagerIsReady;
            SettingsManager.OnItemsProcessed += SettingsManagerOnOnItemsProcessed;

            //StartTimer();
            var inventoryRetryManager = InventoryRetryManager.Instance;

            CloseAppCommand = new RelayCommand(() =>
            {
                Application.Current.Exit();
            });
            UserLogoutCommand = new RelayCommand(() =>
            {
                UserManager.Instance.Logout();
            });
        }

        private void ModuleManagerOnModuleManagerIsReady(object sender, EventArgs e)
        {
            //SystemStatusManager.Instance.InitUpdate();
        }

        private async void LoadFirstModule()
        {
            MenuItem activeModule = ModuleManager.Instance.MenuSettings.MenuItems.FirstOrDefault(M => M.IsActive);
            if (activeModule == null || String.IsNullOrEmpty(activeModule.Code))
            {
                ModuleManager.Instance.NavigateToModule("Dashboard");
            }
            else
            {
                ModuleManager.Instance.NavigateToModule(activeModule.Code);
            }

            await Task.Delay(1500);

            List<MenuItem> homeItems = ModuleManager.Instance.MenuSettings.MenuItems.Where(M => M.IsInHome).ToList();
            _homeMenuViewModel.UpdateMenu(homeItems);

            List<MenuItem> sideItems = ModuleManager.Instance.MenuSettings.MenuItems.Where(M => M.IsInHome).ToList();
            _sideMenuViewModel.UpdateMenu(sideItems);
        }

        private void ModuleManagerOnChangeModulePage(object sender, ChangeModuleEventArgs e)
        {
            // hide side menu if open
            _sideMenuViewModel.SideMenuIsVisible = false;

            if (e.NewModule != null)
            {
                ModuleIsVisible = true;
                CurrentVisibleControl = e.NewModule.LoadedControl;
                _homeMenuViewModel.CurrentVisibleControlTitle = e.NewModule.Title;
            }
            else
            {
                CurrentVisibleControl = null;
                ModuleIsVisible = false;

                // rest title to mystore
                _homeMenuViewModel.CurrentVisibleControlTitle = LanguageManager.Instance.GetTranslation("MODULE_Home_Title");
            }
        }

        private void SettingsManagerOnOnItemsProcessed(bool visible)
        {
            Notify("WaitingIndicatorText");
            Notify("WaitingIndicatorVisibility");
        }


        private async void StartTimer()
        {
            if (_isUpdateTimerRunning)
            {
                return;
            }

            _isUpdateTimerRunning = true;

            while (_isUpdateTimerRunning)
            {
                await Task.Delay(SettingsManager.Instance.UpdateTriggerInSecs * 1000);

                if (!_isFirstLoading)
                {
                    LoadDataForHomeTile();
                }

                _isFirstLoading = false;
            }
        }

        #region HomeTile functions
        private async void LoadDataForHomeTile()
        {
            await Task.Delay(2000);
            LoadAsyncData<WWCHomeTileDto>(LoadDataForHomeTileAsync, LoadDataForHomeTileAsyncFinished);
        }

        private WWCHomeTileDto LoadDataForHomeTileAsync()
        {
            DateTime selectedDate = SystemStatusManager.Instance.CurrentBusinessDay;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                var serviceResult = service.GetHomeTileData(selectedDate);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);

                return serviceResult;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadDataForHomeTileAsyncFinished(WWCHomeTileDto data)
        {
            if (data == null)
            {
                return;
            }

            HomeTileManager.Instance.UpdateTile(data.WideTileBytes, "ms-appx:///Mcd.MyStore.WinApp.MyStoreApp/Assets/MediumTile.png");
        }
        #endregion

        public void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.SystemStatusChanged)
            {
                RunOnUI(() =>
                {
                    UpdateUIWithNewSystemStatusCallback();
                });
            }
            else if (notification.Type == NotificationApplicationType.WinApp_UserLoggedIn)
            {
                StartAppAfterLogin();
            }
            else if (notification.Type == NotificationApplicationType.WinApp_UserLoggedOut)
            {
                IsAppContentVisibile = false;
            }
            else if (notification.Type == NotificationApplicationType.WinApp_AppServiceConnected)
            {
                AreAvailableDatesEnabled = true;
                ConnectivityStatus = new Uri("ms-appx:///Assets/CircleGreen.png");
                ConnectivityStatusChanged = Translation["KEY_BASIC_Online"];
            }
            else if (notification.Type == NotificationApplicationType.WinApp_AppServiceDisconnected)
            {
                AreAvailableDatesEnabled = false;
                ConnectivityStatus = new Uri("ms-appx:///Assets/CircleRed.png");
                ConnectivityStatusChanged = Translation["KEY_BASIC_Offline"];
            }
        }



        private void StartAppAfterLogin()
        {
            IsAppContentVisibile = true;

            if (_isFirstLoading)
            {
                LoadFirstModule();
                _isFirstLoading = false;
            }
        }

        private void UpdateUIWithNewSystemStatusCallback()
        {
            DateTime currentDay = SystemStatusManager.Instance.CurrentBusinessDay;

            DateTime startDate = currentDay.AddDays(-(SettingsManager.Instance.LastVisibleDaysCount));

            bool addedNewDate = false;
            for (DateTime currentDateTime = startDate; currentDateTime <= currentDay; currentDateTime = currentDateTime.AddDays(1))
            {
                if (!AvailableDates.Any(D => D.Date.Equals(currentDateTime)))
                {
                    AvailableDates.Add(new DateSelectViewModel(currentDateTime));
                    addedNewDate = true;
                }
            }

            if (SelectedDate == null || addedNewDate)
            {
                SelectedDate = AvailableDates.Last();
            }
        }

    }
}
