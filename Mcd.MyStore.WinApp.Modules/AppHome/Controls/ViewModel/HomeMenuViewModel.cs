﻿using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.MVVM;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Mcd.MyStore.WinApp.Modules.AppHome.Controls.ViewModel
{
    public class HomeMenuViewModel : ViewModelBase
    {
        #region Member - CurrentVisibleControlTitle
        private string _currentVisibleControlTitle;
        public string CurrentVisibleControlTitle
        {
            get { return _currentVisibleControlTitle; }
            set { SetValue(ref _currentVisibleControlTitle, value); }
        }
        #endregion

        public ObservableCollection<MenuItemViewModel> MenuItems { get; set; }

        public HomeMenuViewModel()
        {
            MenuItems = new ObservableCollection<MenuItemViewModel>();
        }

        public void UpdateMenu(List<MenuItem> menuItems)
        {
            MenuItems.Clear();
            foreach (var item in menuItems)
            {
                MenuItems.Add(new MenuItemViewModel()
                {
                    Code = item.Code,
                    Title = item.Module.Title,
                    Description = item.Module.Description,
                    ImagePath = "ms-appx:///Mcd.MyStore.WinApp.Modules/Images/HomeMenu_" + item.Code + ".png"
                });
            }
        }
    }
}