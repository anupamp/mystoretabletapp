﻿using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Mcd.MyStore.WinApp.Modules.AppHome.Controls.ViewModel
{
    public class SideMenuViewModel : ViewModelBase
    {
        #region Member - SideMenuIsVisible
        private bool _sideMenuIsVisible;
        public bool SideMenuIsVisible
        {
            get { return _sideMenuIsVisible; }
            set { SetValue(ref _sideMenuIsVisible, value); }
        }
        #endregion

        public ObservableCollection<MenuItemViewModel> MenuItems { get; set; }

        public RelayCommand OpenMenuCommand { get; set; }
        public RelayCommand CloseMenuCommand { get; set; }
        public RelayCommand ShowHomeMenuCommand { get; set; }

        public SideMenuViewModel()
        {
            MenuItems = new ObservableCollection<MenuItemViewModel>();

            OpenMenuCommand = new RelayCommand(() =>
            {
                SideMenuIsVisible = true;
            });

            CloseMenuCommand = new RelayCommand(() =>
            {
                SideMenuIsVisible = false;
            });

            ShowHomeMenuCommand = new RelayCommand(() =>
            {
                ModuleManager.Instance.NavigateToModule(null);
            });
        }

        public void UpdateMenu(List<MenuItem> menuItems)
        {
            MenuItems.Clear();
            foreach (var item in menuItems)
            {
                MenuItems.Add(new MenuItemViewModel()
                {
                    Code = item.Code,
                    Title = item.Module.Title,
                    ImagePath = "ms-appx:///Mcd.MyStore.WinApp.Modules/Images/SideMenu_" + item.Code + ".png"
                });
            }
        }
    }
}
