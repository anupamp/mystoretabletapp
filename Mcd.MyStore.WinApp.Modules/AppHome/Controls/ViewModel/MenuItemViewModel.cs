﻿using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.AppHome.Controls.ViewModel
{
    public class MenuItemViewModel : ViewModelBase
    {
        public string Code { get; set; }

        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion

        #region Member - Description
        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetValue(ref _description, value); }
        }
        #endregion

        #region Section Image Handling
        #region Member - ImagePath
        private string _imagePath;
        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                if (SetValue(ref _imagePath, value))
                {
                    _image = new BitmapImage(new Uri(_imagePath));
                    Notify("Image");
                }
            }
        }
        #endregion

        #region Member - Image
        private BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
        }
        #endregion
        #endregion



        public RelayCommand ModuleOpenCommand { get; set; }

        public MenuItemViewModel()
        {
            ModuleOpenCommand = new RelayCommand(() =>
            {
                ModuleManager.Instance.NavigateToModule(Code);
            });
        }
    }
}
