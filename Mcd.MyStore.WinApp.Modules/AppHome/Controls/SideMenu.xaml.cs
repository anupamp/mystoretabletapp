﻿using Mcd.MyStore.WinApp.Modules.AppHome.Controls.ViewModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.AppHome.Controls
{
    public sealed partial class SideMenu : UserControl
    {
        public SideMenu()
        {
            this.InitializeComponent();
        }

        private void OpenTapped(object sender, TappedRoutedEventArgs e)
        {
            SideMenuViewModel model = (DataContext as SideMenuViewModel);
            if (model != null)
            {
                model.OpenMenuCommand.Execute(null);
            }
        }

        private void CloseTapped(object sender, TappedRoutedEventArgs e)
        {
            SideMenuViewModel model = (DataContext as SideMenuViewModel);
            if (model != null)
            {
                model.CloseMenuCommand.Execute(null);
            }
        }
    }
}
