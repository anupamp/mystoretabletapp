﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;

namespace Mcd.MyStore.WinApp.Modules.AppHome.ViewModel
{
    public class DateSelectViewModel : ViewModelBase
    {
        #region Member DateText
        private string _dateText;
        public string DateText
        {
            get { return _dateText; }
            set { SetValue<string>(ref _dateText, value); }
        }
        #endregion

        #region Member Date
        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set { SetValue<DateTime>(ref _date, value); }
        }
        #endregion

        public DateSelectViewModel(DateTime date)
        {
            Date = date;
            DateText = date.ToString("d");
        }
    }
}
