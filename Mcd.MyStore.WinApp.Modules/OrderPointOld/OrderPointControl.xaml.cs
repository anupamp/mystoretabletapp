﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel;
using System;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.OrderPoint
{
    [MenuModuleAttribute("OrderPoints")]
    public sealed partial class OrderPointControl : UserControl
    {
        private OrderPointControlViewModel _viewModel = new OrderPointControlViewModel();

        public OrderPointControl()
        {
            this.InitializeComponent();
            DataContext = _viewModel;
            InitControl();
        }

        private async void InitControl()
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Mcd.MyStore.WinApp.Modules/Images/OrderPoints.png"));
            ImageProperties result = await file.Properties.GetImagePropertiesAsync();
            _viewModel.ImageWidth = result.Width;
        }

        private void StackPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _viewModel.ControlWidth = e.NewSize.Width;
        }

    }


}
