﻿using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.Common;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel
{
    public class OrderPointControlViewModel : ModuleViewModelBase
    {
        #region Member ControlWidth
        private double _controlWidth = 0;
        public double ControlWidth
        {
            get { return _controlWidth; }
            set
            {
                if (_controlWidth != value)
                {
                    _controlWidth = value;
                    UpdateZoneSize();
                }
            }
        }
        #endregion

        #region Member ImageWidth
        private double _imageWidth = 0;
        public double ImageWidth
        {
            get { return _imageWidth; }
            set
            {
                if (_imageWidth != value)
                {
                    _imageWidth = value;
                    UpdateZoneSize();
                }
            }
        }
        #endregion

        public ObservableCollection<OrderItemViewModel> ZoneList { get; set; }

        public OrderPointDetailZoomControlViewModel DetailZoomViewModel { get; set; }

        private bool _isSettingUpdateTimerRunning;

        private WWCOrderPointSettingsContainer _lastSettings = null;

        public OrderPointControlViewModel()
        {
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            ZoneList = new ObservableCollection<OrderItemViewModel>();
            ZoneList.Add(new OrderItemViewModel() { Name = "Fresh", Width = 10 });
            ZoneList.Add(new OrderItemViewModel() { Name = "Together", Width = 10 });
            ZoneList.Add(new OrderItemViewModel() { Name = "Connect", Width = 10 });

            DetailZoomViewModel = new OrderPointDetailZoomControlViewModel();

            LoadSettingOnStartup();

            if (SettingsManager.Instance.OrderPointsRefreshPosition)
            {
                StartTimer();
            }
        }

        private async void LoadSettingOnStartup()
        {
            await Task.Delay(3000);
            LoadSettings();
        }

        private void UpdateZoneSize()
        {
            if (_controlWidth == 0 || _imageWidth == 0)
            {
                return;
            }

            double halfControlWidth = _controlWidth / 2.0;
            double halfImageWidth = _imageWidth / 2.0;
            double imageStartLeft = halfControlWidth - halfImageWidth;

            double firstColumn = Math.Round(imageStartLeft + 230);
            double secondColumn = 195;
            double thirdColumn = _controlWidth - firstColumn - secondColumn;

            if (firstColumn < 0 || thirdColumn < 0)
            {
                return;
            }

            ZoneList[0].Width = (int)firstColumn;
            ZoneList[1].Width = (int)secondColumn;
            ZoneList[2].Width = (int)thirdColumn;
        }

        private async void StartTimer()
        {
            if (_isSettingUpdateTimerRunning)
            {
                return;
            }

            _isSettingUpdateTimerRunning = true;

            while (_isSettingUpdateTimerRunning)
            {
                await Task.Delay(5000);
                LoadSettings();
            }
        }

        private void LoadSettings()
        {
            LoadAsyncData<WWCOrderPointSettingsContainer>(LoadSettingsAsync, LoadSettingsAsyncFinished);
        }

        private WWCOrderPointSettingsContainer LoadSettingsAsync()
        {
            try
            {
                //var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                //WWCOrderPointSettingsContainerDto result = service.GetOrderPointSettings(SettingsManager.Instance.OrderPointsRefreshPosition);

                //WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                //return result.Settings;
                return null;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadSettingsAsyncFinished(WWCOrderPointSettingsContainer result)
        {
            if (result != null)
            {
                _lastSettings = result;
                foreach (var setting in result)
                {
                    OrderPointDetailItemControlViewModel currentViewModel = null;
                    switch (setting.Key)
                    {
                        case "Fresh":
                            currentViewModel = DetailZoomViewModel.FreshDetailViewModel;
                            break;

                        case "Together":
                            currentViewModel = DetailZoomViewModel.TogetherDetailViewModel;
                            break;

                        case "Connect":
                            currentViewModel = DetailZoomViewModel.ConnectDetailViewModel;
                            break;
                    }
                    if (currentViewModel != null)
                    {
                        currentViewModel.InformationList.Clear();

                        foreach (WWCOrderPointSettingsItemDto item in setting.Value)
                        {
                            var newModel = new OrderPointInformationPointControlViewModel()
                            {
                                PosTypeName = setting.Key,
                                Left = item.Left,
                                Top = item.Top,
                                PosID = item.PosID,
                                MaxWidth = item.MaxWidth
                            };

                            #region Set open position
                            switch (item.OpenPosition)
                            {
                                case WWCOrderPointSettingsItemOpenPosition.LeftBottom:
                                    newModel.PopupPosition = OrderPointInformationPopupPositionType.LeftBottom;
                                    break;

                                case WWCOrderPointSettingsItemOpenPosition.LeftTop:
                                    newModel.PopupPosition = OrderPointInformationPopupPositionType.LeftTop;
                                    break;

                                case WWCOrderPointSettingsItemOpenPosition.RightBottom:
                                    newModel.PopupPosition = OrderPointInformationPopupPositionType.RightBottom;
                                    break;

                                case WWCOrderPointSettingsItemOpenPosition.RightTop:
                                    newModel.PopupPosition = OrderPointInformationPopupPositionType.RightTop;
                                    break;
                            }
                            #endregion

                            #region set pos type
                            switch (item.PosType)
                            {
                                case WWCOrderPointSettingsItemType.OrderTaker:
                                    newModel.PosType = OrderPointInformationType.OrderTaker;
                                    break;

                                case WWCOrderPointSettingsItemType.StandingKiosk:
                                    newModel.PosType = OrderPointInformationType.StandingKiosk;
                                    break;

                                case WWCOrderPointSettingsItemType.TabletKiosk:
                                    newModel.PosType = OrderPointInformationType.TabletKiosk;
                                    break;

                                case WWCOrderPointSettingsItemType.TableService:
                                    newModel.PosType = OrderPointInformationType.ServiceTablet;
                                    break;
                            }
                            #endregion

                            currentViewModel.AddPointInformation(newModel);
                        }

                        foreach (var currentItem in currentViewModel.InformationList)
                        {
                            foreach (var searchItem in currentViewModel.InformationList.Where(I => I != currentItem))
                            {
                                if (searchItem.Left == currentItem.Left && searchItem.Top == currentItem.Top)
                                {
                                    currentItem.GroupItemList.Add(searchItem);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void LoadData()
        {
            if (DetailZoomViewModel != null && DetailZoomViewModel.CurrentDetailViewModel != null)
            {
                DetailZoomViewModel.CurrentDetailViewModel.LoadData();
            }
        }


        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }
        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                LoadData();
            }
        }
    }

    public class OrderItemViewModel : ViewModelBase
    {
        #region Member Width
        private int _width;
        public int Width
        {
            get { return _width; }
            set { SetValue<int>(ref _width, value); }
        }
        #endregion

        #region Member Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue<string>(ref _name, value); }
        }
        #endregion

    }
}
