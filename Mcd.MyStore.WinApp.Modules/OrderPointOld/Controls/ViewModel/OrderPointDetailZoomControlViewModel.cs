﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel
{
    public class OrderPointDetailZoomControlViewModel : ViewModelBase
    {
        #region Member ActiveContent
        private UserControl _activeContent;
        public UserControl ActiveContent
        {
            get { return _activeContent; }
            set { SetValue<UserControl>(ref _activeContent, value); }
        }
        #endregion

        #region Member CurrentDetailViewModel
        private OrderPointDetailItemControlViewModel _currentDetailViewModel;
        public OrderPointDetailItemControlViewModel CurrentDetailViewModel
        {
            get { return _currentDetailViewModel; }
            set
            {
                if (SetValue<OrderPointDetailItemControlViewModel>(ref _currentDetailViewModel, value))
                {
                    value.LoadData();
                }
            }
        }
        #endregion

        public UserControl FreshControl { get; set; }
        public OrderPointDetailItemControlViewModel FreshDetailViewModel { get; set; }
        public UserControl TogetherControl { get; set; }
        public OrderPointDetailItemControlViewModel TogetherDetailViewModel { get; set; }
        public UserControl ConnectControl { get; set; }
        public OrderPointDetailItemControlViewModel ConnectDetailViewModel { get; set; }

        public RelayCommand TouchCommand { get; set; }

        public OrderPointDetailZoomControlViewModel()
        {
            FreshControl = new OrderPointDetailFreshControl();
            FreshDetailViewModel = new OrderPointDetailItemControlViewModel();

            TogetherControl = new OrderPointDetailTogetherControl();
            TogetherDetailViewModel = new OrderPointDetailItemControlViewModel();

            ConnectControl = new OrderPointDetailConnectControl();
            ConnectDetailViewModel = new OrderPointDetailItemControlViewModel();

            TouchCommand = new RelayCommand((object parameter) =>
            {
                if (parameter is PointerRoutedEventArgs)
                {
                    PointerRoutedEventArgs eventArgs = (PointerRoutedEventArgs)parameter;
                    Grid source = (Grid)eventArgs.OriginalSource;
                    var positionPoint = eventArgs.GetCurrentPoint(source);

                    foreach (var item in CurrentDetailViewModel.InformationList)
                    {
                        if (positionPoint.Position.X >= item.Left && positionPoint.Position.X < item.Left + 40
                            && positionPoint.Position.Y >= item.Top && positionPoint.Position.Y < item.Top + 40)
                        {
                            item.ToggleOpen();
                        }
                    }
                }
                
                return true;
            });
        }

        public void SwitchContent(string newContentName)
        {
            switch (newContentName)
            {
                case "Fresh":
                    ActiveContent = FreshControl;
                    CurrentDetailViewModel = FreshDetailViewModel;
                    break;

                case "Together":
                    ActiveContent = TogetherControl;
                    CurrentDetailViewModel = TogetherDetailViewModel;
                    break;

                case "Connect":
                    ActiveContent = ConnectControl;
                    CurrentDetailViewModel = ConnectDetailViewModel;
                    break;

                default:
                    ActiveContent = null;
                    break;
            }
        }
    }
}
