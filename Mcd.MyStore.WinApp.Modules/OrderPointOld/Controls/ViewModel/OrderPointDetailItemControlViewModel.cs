﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel
{
    public class OrderPointDetailItemControlViewModel : ViewModelBase
    {
        public ObservableCollection<OrderPointInformationPointControlViewModel> InformationList { get; set; }


        public OrderPointDetailItemControlViewModel()
        {
            InformationList = new ObservableCollection<OrderPointInformationPointControlViewModel>();
        }

        public void LoadData()
        {
            LoadAsyncData<List<OrderPointStatisticsDto>>(LoadInformationDataAsync, LoadInformationDataAsyncFinished);
        }

        public void AddPointInformation(OrderPointInformationPointControlViewModel model)
        {
            InformationList.Add(model);
            model.OnPopupOpen += OnPopupOpen;
        }

        private void OnPopupOpen(object sender, EventArgs e)
        {
            foreach (var item in InformationList)
            {
                if (item != sender)
                {
                    OrderPointInformationPointControlViewModel source = (OrderPointInformationPointControlViewModel)sender;
                    if (!source.GroupItemList.Contains(item))
                    {
                        item.PopupIsOpenedNext = false;
                    }
                }
            }
        }

        public void CloseAllPopups()
        {
            foreach (var item in InformationList)
            {
                item.PopupIsOpenedNext = false;
                item.PopupIsOpened = false;
            }
        }




        private List<OrderPointStatisticsDto> LoadInformationDataAsync()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return null;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                List<int> posIDs = InformationList.Select(I => I.PosID).ToList();
                List<OrderPointStatisticsDto> result = service.GetOrderPointStatistics(selectedDate, selectedDate.AddDays(1).AddMilliseconds(-1), posIDs);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadInformationDataAsyncFinished(List<OrderPointStatisticsDto> result)
        {
            if (result != null)
            {
                foreach (OrderPointInformationPointControlViewModel currentViewModel in InformationList)
                {
                    OrderPointStatisticsDto currentItemResult = result.SingleOrDefault(I => I.PosID == currentViewModel.PosID);
                    if (currentItemResult != null)
                    {
                        currentViewModel.SpeedOfServiceInSecs = (int)Math.Round(currentItemResult.TotalExperienceTimeInSeconds);
                        currentViewModel.TopProductName = currentItemResult.TopProductName;
                        currentViewModel.OrderCount = currentItemResult.Guests;
                    }
                    else
                    {
                        currentViewModel.SpeedOfServiceInSecs = 0;
                        currentViewModel.TopProductName = "n/a";
                        currentViewModel.OrderCount = 0;
                    }
                }
            }
        }


    }
}
