﻿using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.Common;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel
{
    public class OrderPointInformationPointControlViewModel : ViewModelBase
    {
        public List<OrderPointInformationPointControlViewModel> GroupItemList { get; set; }

        #region Member PopupIsOpened
        private bool _popupIsOpened;
        public bool PopupIsOpened
        {
            get { return _popupIsOpened; }
            set { SetValue<bool>(ref _popupIsOpened, value); }
        }
        #endregion

        #region Member PopupIsOpenedNext
        private bool _popupIsOpenedNext;
        public bool PopupIsOpenedNext
        {
            get { return _popupIsOpenedNext; }
            set
            {
                if (SetValue<bool>(ref _popupIsOpenedNext, value) && value)
                {
                    PopupIsOpened = true;
                }
            }
        }
        #endregion

        #region Member Left
        private double _left;
        public double Left
        {
            get { return _left; }
            set { SetValue<double>(ref _left, value); }
        }
        #endregion

        #region Member Top
        private double _top;
        public double Top
        {
            get { return _top; }
            set { SetValue<double>(ref _top, value); }
        }
        #endregion

        #region Member PosID
        private int _posID;
        public int PosID
        {
            get { return _posID; }
            set { SetValue<int>(ref _posID, value); }
        }
        #endregion

        #region Member MaxWidth
        private int _maxWidth = 300;
        public int MaxWidth
        {
            get { return _maxWidth; }
            set
            {
                if (SetValue<int>(ref _maxWidth, value))
                {
                    Notify("PopupHorizontalOffset");
                }
            }
        }
        #endregion

        public int MaxHeight
        {
            get { return 215; }
        }

        #region Member MinPopupHorizontalOffset
        public int MinPopupHorizontalOffset
        {
            get
            {
                switch (_popupPosition)
                {
                    case OrderPointInformationPopupPositionType.LeftBottom:
                    case OrderPointInformationPopupPositionType.LeftTop:
                        return 0;

                    case OrderPointInformationPopupPositionType.RightBottom:
                    case OrderPointInformationPopupPositionType.RightTop:
                    default:
                        return 40;
                }
            }
        }
        #endregion

        #region Member PopupHorizontalOffset
        public int PopupHorizontalOffset
        {
            get
            {
                switch (_popupPosition)
                {
                    case OrderPointInformationPopupPositionType.LeftBottom:
                    case OrderPointInformationPopupPositionType.LeftTop:
                        return -_maxWidth;

                    case OrderPointInformationPopupPositionType.RightBottom:
                    case OrderPointInformationPopupPositionType.RightTop:
                    default:
                        return 40;
                }
            }
        }
        #endregion

        #region Member PopupVerticalOffset
        public int PopupVerticalOffset
        {
            get
            {
                switch (_popupPosition)
                {
                    case OrderPointInformationPopupPositionType.LeftTop:
                    case OrderPointInformationPopupPositionType.RightTop:
                        return -MaxHeight + 40;

                    default:
                        return 0;
                }
            }
        }
        #endregion

        #region Member PopupMargin
        public Thickness PopupMargin
        {
            get
            {
                double left = 0;
                double top = 0;

                switch (_popupPosition)
                {
                    case OrderPointInformationPopupPositionType.LeftTop:
                        left = -_maxWidth;
                        top = -MaxHeight + 40;
                        break;

                    case OrderPointInformationPopupPositionType.LeftBottom:
                        left = -_maxWidth;
                        top = 0;
                        break;

                    case OrderPointInformationPopupPositionType.RightTop:
                        left = 40;
                        top = -MaxHeight + 40;
                        break;

                    case OrderPointInformationPopupPositionType.RightBottom:
                        left = 40;
                        top = 0;
                        break;
                }

                return new Thickness(left, top, 0, 0);
            }
        }
        #endregion

        #region Member PopupPosition
        private OrderPointInformationPopupPositionType _popupPosition = OrderPointInformationPopupPositionType.RightBottom;
        public OrderPointInformationPopupPositionType PopupPosition
        {
            get { return _popupPosition; }
            set
            {
                if (SetValue<OrderPointInformationPopupPositionType>(ref _popupPosition, value))
                {
                    Notify("PopupHorizontalOffset");
                    Notify("PopupMargin");
                    Notify("LeftPopupVisbility");
                    Notify("RightPopupVisbility");
                    Notify("TextVerticalAligment");
                    Notify("TitleVisbilityBottom");
                    Notify("TitleVisbilityTop");
                }
            }
        }
        #endregion

        #region Member PosType
        private OrderPointInformationType _posType = OrderPointInformationType.None;
        public OrderPointInformationType PosType
        {
            get { return _posType; }
            set
            {
                if (SetValue<OrderPointInformationType>(ref _posType, value))
                {
                    UpdatePosImage();
                    PosTitle = LanguageManager.Instance["ORDERPOINTS_PosTitle" + _posType.ToString()];
                }
            }
        }
        #endregion

        public string PosTypeName { get; set; }

        #region Member PosImage
        private BitmapImage _posImage;
        public BitmapImage PosImage
        {
            get { return _posImage; }
            set { SetValue<BitmapImage>(ref _posImage, value); }
        }
        #endregion

        #region Member PosTitle
        private string _posTitle;
        public string PosTitle
        {
            get { return _posTitle; }
            set { SetValue<string>(ref _posTitle, value); }
        }
        #endregion

        #region Member SpeedOfServiceInSecs
        private int _speedOfServiceInSecs;
        public int SpeedOfServiceInSecs
        {
            get { return _speedOfServiceInSecs; }
            set { SetValue<int>(ref _speedOfServiceInSecs, value); }
        }
        #endregion

        #region Member OrderCount
        private int _orderCount;
        public int OrderCount
        {
            get { return _orderCount; }
            set { SetValue<int>(ref _orderCount, value); }
        }
        #endregion

        #region Member TopProductName
        private string _topProductName = "n/a";
        public string TopProductName
        {
            get { return _topProductName; }
            set { SetValue<string>(ref _topProductName, value); }
        }
        #endregion



        public Visibility LeftPopupVisbility
        {
            get
            {
                return (_popupPosition == OrderPointInformationPopupPositionType.LeftBottom || _popupPosition == OrderPointInformationPopupPositionType.LeftTop ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        public Visibility RightPopupVisbility
        {
            get
            {
                return (_popupPosition == OrderPointInformationPopupPositionType.RightBottom || _popupPosition == OrderPointInformationPopupPositionType.RightTop ? Visibility.Visible : Visibility.Collapsed);
            }
        }

        public Visibility TitleVisbilityBottom
        {
            get
            {
                return (_popupPosition == OrderPointInformationPopupPositionType.LeftTop || _popupPosition == OrderPointInformationPopupPositionType.RightTop ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        public Visibility TitleVisbilityTop
        {
            get
            {
                return (_popupPosition == OrderPointInformationPopupPositionType.LeftBottom || _popupPosition == OrderPointInformationPopupPositionType.RightBottom ? Visibility.Visible : Visibility.Collapsed);
            }
        }

        public VerticalAlignment TextVerticalAligment
        {
            get
            {
                return (_popupPosition == OrderPointInformationPopupPositionType.LeftTop || _popupPosition == OrderPointInformationPopupPositionType.RightTop ? VerticalAlignment.Bottom : VerticalAlignment.Top);
            }
        }


        public ICommand PopupToggleCommand { get; set; }
        public event EventHandler OnPopupOpen;

        public OrderPointInformationPointControlViewModel()
        {
            GroupItemList = new List<OrderPointInformationPointControlViewModel>();

            PopupToggleCommand = new RelayCommand(() =>
            {
                //ToggleOpen();
            });
        }

        public void ToggleOpen()
        {
            PopupIsOpenedNext = !PopupIsOpenedNext;
            if (OnPopupOpen != null)
            {
                if (PopupIsOpenedNext)
                {
                    OnPopupOpen(this, null);
                }
            }
        }

        private async void UpdatePosImage()
        {
            switch (_posType)
            {
                case OrderPointInformationType.OrderTaker:
                    PosImage = await ImageManager.Instance.GetImage("/Mcd.MyStore.WinApp.Modules/Images/" + PosTypeName + "_OrderTaker.png");
                    break;

                case OrderPointInformationType.StandingKiosk:
                    PosImage = await ImageManager.Instance.GetImage("/Mcd.MyStore.WinApp.Modules/Images/" + PosTypeName + "_StandingKiosk.png");
                    break;

                case OrderPointInformationType.TabletKiosk:
                    PosImage = await ImageManager.Instance.GetImage("/Mcd.MyStore.WinApp.Modules/Images/" + PosTypeName + "_TabletKiosk.png");
                    break;

                case OrderPointInformationType.ServiceTablet:
                    PosImage = await ImageManager.Instance.GetImage("/Mcd.MyStore.WinApp.Modules/Images/" + PosTypeName + "_ServiceTablet.png");
                    break;
            }
        }
    }
}
