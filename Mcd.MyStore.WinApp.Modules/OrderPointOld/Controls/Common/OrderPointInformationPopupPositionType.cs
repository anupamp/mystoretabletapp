﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.Common
{
    public enum OrderPointInformationPopupPositionType
    {
        LeftTop,
        LeftBottom,
        RightTop,
        RightBottom
    }
}
