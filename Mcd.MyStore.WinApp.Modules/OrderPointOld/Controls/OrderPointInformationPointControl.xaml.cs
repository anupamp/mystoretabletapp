﻿using Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls
{
    public sealed partial class OrderPointInformationPointControl : UserControl
    {
        public OrderPointInformationPointControl()
        {
            this.InitializeComponent();
        }

        private void UserControl_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            //if (DataContext is OrderPointInformationPointControlViewModel)
            //{
            //    OrderPointInformationPointControlViewModel viewModel = (OrderPointInformationPointControlViewModel)DataContext;
            //    viewModel.ToggleOpen();
            //}


            e.Handled = false;
        }
    }
}
