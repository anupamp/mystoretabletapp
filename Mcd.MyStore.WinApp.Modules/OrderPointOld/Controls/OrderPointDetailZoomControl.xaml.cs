﻿using Mcd.MyStore.WinApp.Modules.OrderPoint.Controls.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls
{
    public sealed partial class OrderPointDetailZoomControl : UserControl, ISemanticZoomInformation
    {
        //private OrderPointDetailZoomControlViewModel _viewModel = new OrderPointDetailZoomControlViewModel();
        public OrderPointDetailZoomControl()
        {
            this.InitializeComponent();
            //DataContext = _viewModel;
        }

        public void CompleteViewChange() { }
        public void CompleteViewChangeFrom(SemanticZoomLocation source, SemanticZoomLocation destination) { }
        public void CompleteViewChangeTo(SemanticZoomLocation source, SemanticZoomLocation destination) { }
        public void InitializeViewChange() { }
        public bool IsActiveView { get; set; }
        public bool IsZoomedInView { get; set; }
        public void MakeVisible(SemanticZoomLocation item) { }
        public SemanticZoom SemanticZoomOwner { get; set; }

        public void StartViewChangeFrom(SemanticZoomLocation source, SemanticZoomLocation destination) 
        {
            OrderPointDetailZoomControlViewModel controlViewModel = (OrderPointDetailZoomControlViewModel)DataContext;
            if (controlViewModel == null)
            {
                return;
            }
            if (controlViewModel.CurrentDetailViewModel != null && controlViewModel.CurrentDetailViewModel is OrderPointDetailItemControlViewModel)
            {
                OrderPointDetailItemControlViewModel viewModel = (OrderPointDetailItemControlViewModel)controlViewModel.CurrentDetailViewModel;
                if (viewModel != null)
                {
                    viewModel.CloseAllPopups();
                }
            }
        }
        public void StartViewChangeTo(SemanticZoomLocation source, SemanticZoomLocation destination)
        {
            if (source.Item is OrderItemViewModel)
            {
                OrderItemViewModel itemViewModel = (OrderItemViewModel)source.Item;
                OrderPointDetailZoomControlViewModel controlViewModel = (OrderPointDetailZoomControlViewModel)DataContext;
                controlViewModel.SwitchContent(itemViewModel.Name);
            }
        }
    }
}
