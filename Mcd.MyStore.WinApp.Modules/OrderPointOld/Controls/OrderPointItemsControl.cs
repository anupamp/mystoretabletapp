﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Controls
{
    public class OrderPointItemsControl : ItemsControl
    {
        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);
            FrameworkElement source = element as FrameworkElement;
            source.SetBinding(Canvas.TopProperty, new Binding { Path = new PropertyPath("Top") });
            source.SetBinding(Canvas.LeftProperty, new Binding { Path = new PropertyPath("Left") });
        } 
    }
}
