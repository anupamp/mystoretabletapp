﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Report.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report
{
    [MenuModuleAttribute("OrderPointReport")]
    public sealed partial class OrderPointReport : UserControl
    {
        private OrderPointReportViewModel _viewModel;
        public OrderPointReport()
        {
            this.InitializeComponent();
            _viewModel = new OrderPointReportViewModel();
            DataContext = _viewModel;
        }

        private void OrderPointReport_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _viewModel.UpdateLayerSize(InsertContainer.ActualWidth, InsertContainer.ActualHeight);
        }

        private void OrderPointsEditorActiveItemControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            UserControl control = (UserControl)sender;
            OrderPointEditorActiveItemViewModel activeViewModel = (OrderPointEditorActiveItemViewModel)control.DataContext;

            if (activeViewModel is ItemTypeAreaViewModel)
            {
                string areaName = activeViewModel.TemplateDto.AreaKey;
                _viewModel.SetViewToArea(areaName);

                e.Handled = true;
            }
        }
    }
}
