﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto.ConfigurableOrderPoints;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.GeneralReportingService.Common.Dto.OrderPoint;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report.ViewModel
{
    public class OrderPointReportViewModel : ModuleViewModelBase
    {
        #region Not used base stuff
        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }
        #endregion

        #region Member - OverviewLayer
        private LayerViewModel _overviewLayer;
        public LayerViewModel OverviewLayer
        {
            get { return _overviewLayer; }
            set { SetValue(ref _overviewLayer, value); }
        }
        #endregion

        #region Member - OverviewIsVisible
        private bool _overviewIsVisible = true;
        public bool OverviewIsVisible
        {
            get { return _overviewIsVisible; }
            set { SetValue(ref _overviewIsVisible, value); }
        }
        #endregion

        #region Member - DetailViewModel
        private DetailInfoControlViewModel _detailViewModel = new DetailInfoControlViewModel();
        public DetailInfoControlViewModel DetailViewModel
        {
            get { return _detailViewModel; }
            set { SetValue(ref _detailViewModel, value); }
        }
        #endregion

        public ObservableCollection<LayerViewModel> LayerItems { get; set; }

        private Size _wrapperInsertContainerSize = Size.Empty;
        private OrderPointEditorManager _manager = OrderPointEditorManager.Instance;
        private List<OrderPointItemTemplateViewModel> _allTemplateItems = new List<OrderPointItemTemplateViewModel>();
        private bool _isDataLoaded = false;

        public ICommand ShowOverviewCommand { get; set; }

        public OrderPointReportViewModel()
        {
            IsAutoReloadEnabled = true;
            SubscribeNotification(NotificationApplicationType.WinApp_OrderPointEditorSettingsSaved);
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);

            LayerItems = new ObservableCollection<LayerViewModel>();
            OrderPointEditorManager.Instance.OnTemplateSettingsChanged += ManagerOnTemplateSettingsChanged;

            ShowOverviewCommand = new RelayCommand(() =>
            {
                OverviewIsVisible = true;
            });
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OrderPointEditorSettingsSaved)
            {
                LoadLayersFromService();
            }
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadOrderPointDataFromService();
            }
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            if (!OrderPointEditorManager.Instance.InitIfNeeded() && _allTemplateItems.Count == 0)
            {
                ManagerOnTemplateSettingsChanged(this, null);
            }
            else if (navigationArgs.Notifications != null
                && navigationArgs.Notifications.Any(N => N.Type == NotificationApplicationType.WinApp_OrderPointEditorSettingsSaved)
                && _isDataLoaded)
            {
                LoadLayersFromService();
            }
        }

        private async void ManagerOnTemplateSettingsChanged(object sender, System.EventArgs e)
        {
            foreach (OrderPointTemplateItem currentTemplate in OrderPointEditorManager.Instance.ItemTemplates)
            {
                OrderPointItemTemplateViewModel newModel = new OrderPointItemTemplateViewModel(currentTemplate);

                if (!String.IsNullOrEmpty(currentTemplate.Background))
                {
                    newModel.BackgroundColor = SolidColorBrushExtensions.ConvertFromHex(currentTemplate.Background);
                }

                if (!String.IsNullOrEmpty(currentTemplate.AreaKey))
                {
                    newModel.TemplateText = currentTemplate.AreaKey;
                }

                if (!String.IsNullOrEmpty(currentTemplate.LocalSotrageFolderPath) && !String.IsNullOrEmpty(currentTemplate.Icon))
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(currentTemplate.LocalSotrageFolderPath);
                    newModel.TemplateImage = await ImageManager.Instance.GetImage(await folder.GetFileAsync(currentTemplate.Icon));
                }

                _allTemplateItems.Add(newModel);
            }

            LoadLayersFromService();
        }

        private async void LoadLayersFromService()
        {
            ShowLoadingIndicator();
            OrderPointLayerSettingsDto layerSettingsDto = await Task.Run(() =>
            {
                OrderPointLayerSettingsDto result = null;
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                result = service.GetAllLayerSettings();
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            });

            #region Update UI Data
            _detailViewModel.ClearItems();
            LayerItems.Clear();


            Size containerSize = new Size(_wrapperInsertContainerSize.Width, _wrapperInsertContainerSize.Height);

            Size detailSize = new Size((_detailViewModel.ControlWidth.Width / 2.0) - 180, _detailViewModel.ControlWidth.Height - 80);

            int layerCount = layerSettingsDto.LayerItems.Count;
            for (int i = 0; i < layerCount; i++)
            {
                OrderPointLayerItemDto currentLayer = layerSettingsDto.LayerItems[i];

                bool isDetailLayer = false;
                LayerViewModel layerViewModel = null;
                if (i == 0) // overview layer
                {
                    OverviewLayer = new LayerViewModel()
                    {
                        LayerID = "Overview",
                        Width = containerSize.Width,
                        Height = containerSize.Height
                    };
                    LayerItems.Add(_overviewLayer);
                    layerViewModel = _overviewLayer;
                }
                else
                {
                    isDetailLayer = true;
                    LayerViewModel detailLayer = new LayerViewModel()
                    {
                        LayerID = currentLayer.LayerID,
                        Width = detailSize.Width,
                        Height = detailSize.Height
                    };
                    LayerItems.Add(detailLayer);
                    layerViewModel = detailLayer;
                }

                if (layerViewModel != null)
                {
                    foreach (var currentContent in currentLayer.ContentItems)
                    {
                        OrderPointItemTemplateViewModel templateViewModel = _allTemplateItems.SingleOrDefault(T => T.TemplateDto.IDKey.Equals(currentContent.ContentKey, StringComparison.CurrentCultureIgnoreCase));
                        if (templateViewModel != null)
                        {
                            OrderPointEditorActiveItemViewModel newActiveViewModel = _manager.CreateActiveItemViewModelFromTemplate(templateViewModel.TemplateDto, (isDetailLayer ? detailSize : containerSize));
                            //newActiveViewModel.IsHitTestVisible = true;
                            newActiveViewModel.IsReportMode = true;

                            if (newActiveViewModel is ItemTypeAreaViewModel)
                            {
                                ItemTypeAreaViewModel areaModel = (ItemTypeAreaViewModel)newActiveViewModel;
                                areaModel.IsVisible = true;
                                areaModel.BackgroundColor = new SolidColorBrush(Colors.Transparent);
                                areaModel.AreaName = String.Empty;
                                areaModel.BorderWidth = new Thickness(0);
                            }

                            if (newActiveViewModel is ItemTypeImageButtonViewModelBase)
                            {
                                ((ItemTypeImageButtonViewModelBase)newActiveViewModel).IsActive = true;
                            }

                            double positionX = currentContent.Left / currentLayer.ContainerWidth;
                            double positionY = currentContent.Top / currentLayer.ContainerHeight;

                            double sizeX = currentContent.Width / currentLayer.ContainerWidth;
                            double sizeY = currentContent.Height / currentLayer.ContainerHeight;

                            newActiveViewModel.Left = layerViewModel.Width * positionX;
                            newActiveViewModel.Top = layerViewModel.Height * positionY;
                            newActiveViewModel.Width = layerViewModel.Width * sizeX;
                            newActiveViewModel.Height = layerViewModel.Height * sizeY;
                            newActiveViewModel.UserSettings = currentContent.UserSettings;
                            layerViewModel.ActiveItems.Add(newActiveViewModel);
                        }
                    }

                    if (isDetailLayer)
                    {
                        _detailViewModel.AddDetailViewModel(layerViewModel);
                    }
                }
            }
            #endregion

            HideLoadingIndicator();

            LoadOrderPointDataFromService();
            _isDataLoaded = true;
        }

        private async void LoadOrderPointDataFromService()
        {
            ShowLoadingIndicator();
            ConfigurableOrderPointsDataDto dataDto = await Task.Run(() =>
            {
                ConfigurableOrderPointsDataDto result = null;
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                result = service.GetOrderPointDataForEditorSettings(SystemStatusManager.Instance.GlobalUserSelectedDay.Value);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            });

            foreach (var area in dataDto.Areas)
            {
                DetailInfoItemViewModel detailModel = _detailViewModel.DetailViewModels.FirstOrDefault(D => D.UniqueIDText.Equals(area.UniqueIDText, StringComparison.CurrentCultureIgnoreCase));
                if (detailModel != null)
                {
                    foreach (var item in area.ItemValues)
                    {
                        string keyLower = item.UniqueIDText.ToLower();

                        List<DetailInfoItemDataValueViewModel> selectedList = null;
                        if (!detailModel.AllDataValues.TryGetValue(keyLower, out selectedList))
                        {
                            selectedList = new List<DetailInfoItemDataValueViewModel>();
                            detailModel.AllDataValues.Add(keyLower, selectedList);
                        }
                        else
                        {
                            selectedList.Clear();
                        }

                        foreach (var dataItem in item.DataValues)
                        {
                            selectedList.Add(new DetailInfoItemDataValueViewModel()
                            {
                                TextKey = "KEY_ConfigurableOrderPoints_DataItemTitle_" + dataItem.Key,
                                ValueFormat = dataItem.ValueFormat,
                                Value = dataItem.Value,
                            });
                        }

                    }

                    detailModel.UpdateVisibleData();
                }
            }

            HideLoadingIndicator();
            ModuleManager.Instance.RestartReloadTimer();
        }

        internal void UpdateLayerSize(double width, double height)
        {
            _wrapperInsertContainerSize = new Size(width, height);
            if (_overviewLayer != null)
            {
                _overviewLayer.Width = _wrapperInsertContainerSize.Width;
                _overviewLayer.Height = _wrapperInsertContainerSize.Height;
            }
        }

        internal void SetViewToArea(string areaName)
        {
            _detailViewModel.SetViewToArea(areaName);
            OverviewIsVisible = false;
        }

        public override void TriggerDataLoading()
        {
            LoadOrderPointDataFromService();
        }
    }
}
