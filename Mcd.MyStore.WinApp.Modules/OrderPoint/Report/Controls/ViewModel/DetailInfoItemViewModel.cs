﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls.ViewModel
{
    public class DetailInfoItemViewModel : ViewModelBase
    {
        public string UniqueIDText { get; set; }

        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion

        #region Member - SelectedItem
        private ItemTypeImageButtonViewModelBase _selectedItem = null;
        public ItemTypeImageButtonViewModelBase SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem == value)
                    return;

                if (value != null)
                {
                    foreach (var item in MapLayer.ActiveItems)
                    {
                        if (item is ItemTypeImageButtonViewModelBase)
                        {
                            ItemTypeImageButtonViewModelBase itemBase = (ItemTypeImageButtonViewModelBase)item;
                            itemBase.IsActive = false;
                        }
                    }
                }

                _selectedItem = value;
                if (_selectedItem != null)
                {
                    _selectedItem.IsActive = true;
                }

                Notify("SelectedItemTitle");
                UpdateVisibleData();
            }
        }
        #endregion

        #region Member - SelectedItemTitle
        public string SelectedItemTitle
        {
            get
            {
                if (_selectedItem != null)
                {
                    if (_selectedItem is ItemTypeImageButtonRegisterViewModel)
                    {
                        int posNumber = ((ItemTypeImageButtonRegisterViewModel)_selectedItem).PosNumber;
                        return String.Format(Translation["OrderPointReport_Items_Register"], posNumber);
                    }
                    else if (_selectedItem is ItemTypeImageButtonQueueViewModel)
                    {
                        string queueName = ((ItemTypeImageButtonQueueViewModel)_selectedItem).QueueName;
                        string[] splittedQueueName = queueName.Split(new string[] { ":S" }, StringSplitOptions.RemoveEmptyEntries);

                        if (splittedQueueName.Length == 2)
                        {
                            string queueType = splittedQueueName[0];
                            string lineNumber = splittedQueueName[1];

                            if (queueType.StartsWith("drkdes", StringComparison.CurrentCultureIgnoreCase))
                            {
                                return String.Format(Translation["OrderPointReport_Items_DRKDES_Line"], lineNumber);
                            }
                            else
                            {
                                return String.Format(Translation["OrderPointReport_Items_MFY_Line"], lineNumber);
                            }
                        }

                        return ((ItemTypeImageButtonQueueViewModel)_selectedItem).QueueName;
                    }
                }
                return "All " + _title;
            }
        }
        #endregion

        #region Member - MapLayer
        private LayerViewModel _mapLayer;
        public LayerViewModel MapLayer
        {
            get { return _mapLayer; }
            set { SetValue(ref _mapLayer, value); }
        }
        #endregion

        public Dictionary<string, List<DetailInfoItemDataValueViewModel>> AllDataValues { get; set; }
        public ObservableCollection<DetailInfoItemDataValueViewModel> DataValues { get; set; }

        public ICommand SelectAllItemsCommand { get; set; }

        public DetailInfoItemViewModel()
        {
            AllDataValues = new Dictionary<string, List<DetailInfoItemDataValueViewModel>>();
            DataValues = new ObservableCollection<DetailInfoItemDataValueViewModel>();

            SelectAllItemsCommand = new RelayCommand(() =>
            {
                SelectAllItems();
            });
        }


        //public void AddInfoItem(ConfigurableOrderPointsInfoItemDto detailItem)
        //{
        //    string tmpString = null;

        //    MapDetailIconViewModel iconModel = new MapDetailIconViewModel();
        //    if (detailItem.TryGetDataAsString("Type", out tmpString))
        //    {
        //        switch (tmpString)
        //        {
        //            case "IconWithRightText":
        //                iconModel = new MapDetailIconWithRightTextViewModel();
        //                break;
        //        }
        //    }

        //    iconModel.UniqueIDText = detailItem.UniqueIDText;

        //    if (detailItem.UniqueID > 0)
        //    {
        //        iconModel.UniqueIDText = detailItem.UniqueID.ToString();
        //    }

        //    iconModel.OuterRect.Update(detailItem.TouchRect);
        //    iconModel.InnerRect.Update(detailItem.InfoRect);
        //    iconModel.Text = detailItem.UniqueIDText;
        //    iconModel.OnTapped += IconModelOnTapped;

        //    bool tmpBool = false;
        //    if (detailItem.TryGetDataAsBool("TextIsOnTop", out tmpBool))
        //    {
        //        iconModel.TextIsOnTop = tmpBool;
        //    }


        //    if (detailItem.TryGetDataAsString("TextTranslationKey", out tmpString))
        //    {
        //        iconModel.Text = LanguageManager.Instance.GetTranslation(tmpString);
        //    }

        //    if (detailItem.TryGetDataAsString("ImagePath", out tmpString))
        //    {
        //        iconModel.NormalImage = tmpString;
        //    }

        //    if (detailItem.TryGetDataAsString("ActiveImagePath", out tmpString))
        //    {
        //        iconModel.ActiveImage = tmpString;
        //    }



        //    MapItems.Add(iconModel);
        //}

        //private void IconModelOnTapped(object sender, TappedRoutedEventArgs e)
        //{
        //    MapDetailIconViewModel activeModel = (MapDetailIconViewModel)sender;

        //    var otherItems = MapItems.Where(M => M != activeModel).ToList();
        //    foreach (var item in otherItems)
        //    {
        //        item.IsActive = false;
        //    }
        //    activeModel.IsActive = true;

        //    SelectedItem = activeModel;

        //    e.Handled = true;
        //}

        public void SelectAllItems()
        {
            foreach (var item in MapLayer.ActiveItems)
            {
                if (item is ItemTypeImageButtonViewModelBase)
                {
                    ItemTypeImageButtonViewModelBase itemBase = (ItemTypeImageButtonViewModelBase)item;
                    itemBase.IsActive = true;
                }
            }
            SelectedItem = null;
        }


        internal void UpdateVisibleData()
        {
            string key = "all";

            if (_selectedItem is ItemTypeImageButtonRegisterViewModel)
            {
                key = ((ItemTypeImageButtonRegisterViewModel)_selectedItem).PosNumber.ToString();
            }
            else if (_selectedItem is ItemTypeImageButtonQueueViewModel)
            {
                key = ((ItemTypeImageButtonQueueViewModel)_selectedItem).QueueName.ToLower();
            }

            DataValues.Clear();

            List<DetailInfoItemDataValueViewModel> selectedList = null;
            if (AllDataValues.TryGetValue(key, out selectedList))
            {
                foreach (var item in selectedList)
                {
                    DataValues.Add(item);
                }
            }
        }
    }
}
