﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.Foundation;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls.ViewModel
{
    public class DetailInfoControlViewModel : ViewModelBase
    {
        public static event EventHandler OnActiveIndexChanged;

        #region Member - CurrentActiveIndex
        private int _currentActiveIndex = 0;
        public int CurrentActiveIndex
        {
            get { return _currentActiveIndex; }
            set
            {
                if (SetValue(ref _currentActiveIndex, value))
                {
                    SelectPage(_currentActiveIndex);
                    CurrentScrollPositionIndex = _currentActiveIndex;
                }
            }
        }
        #endregion

        #region Member - CurrentScrollPositionIndex
        private int _currentScrollPositionIndex = 0;
        public int CurrentScrollPositionIndex
        {
            get { return _currentScrollPositionIndex; }
            set
            {
                if (SetValue(ref _currentScrollPositionIndex, value))
                {
                    SelectPage(_currentScrollPositionIndex);
                }
            }
        }
        #endregion


        #region Member - ControlWidth
        private Size _controlWidth;
        public Size ControlWidth
        {
            get { return _controlWidth; }
            set { SetValue(ref _controlWidth, value); }
        }
        #endregion

        public ObservableCollection<DetailInfoItemViewModel> DetailViewModels { get; set; }

        private ScrollPageItemViewModel _lastPageItem = null;
        public ObservableCollection<ScrollPageItemViewModel> PageItems { get; set; }

        public DetailInfoControlViewModel()
        {
            DetailViewModels = new ObservableCollection<DetailInfoItemViewModel>();
            PageItems = new ObservableCollection<ScrollPageItemViewModel>();
        }

        public void ClearItems()
        {
            _lastPageItem = null;
            DetailViewModels.Clear();
            PageItems.Clear();
        }

        public void ShowIndex(int index)
        {
            CurrentActiveIndex = index;

            if (OnActiveIndexChanged != null)
            {
                OnActiveIndexChanged(_currentActiveIndex, null);
            }
        }
        public void SelectPage(int index)
        {
            if (_lastPageItem != null)
            {
                _lastPageItem.IsActive = false;
            }

            if (index >= PageItems.Count)
            {
                index = 0;
            }

            _lastPageItem = PageItems[index];
            _lastPageItem.IsActive = true;
            _currentActiveIndex = index;
        }

        internal void AddDetailViewModel(LayerViewModel layerViewModel)
        {
            DetailInfoItemViewModel detailModel = new DetailInfoItemViewModel();
            detailModel.UniqueIDText = layerViewModel.LayerID;
            detailModel.Title = layerViewModel.LayerID;
            detailModel.MapLayer = layerViewModel;

            DetailViewModels.Add(detailModel);

            PageItems.Add(new ScrollPageItemViewModel());
            if (_currentScrollPositionIndex > 0)
            {
                if (PageItems.Count > _currentScrollPositionIndex)
                {
                    _lastPageItem = PageItems[_currentScrollPositionIndex];
                    _lastPageItem.IsActive = true;
                }
            }
            else if (PageItems.Count == 1)
            {
                SelectPage(_currentActiveIndex);
            }
        }

        internal void SetSelectedItem(ItemTypeImageButtonViewModelBase activeViewModel)
        {
            DetailViewModels[_currentActiveIndex].SelectedItem = activeViewModel;

        }

        internal void SetViewToArea(string areaName)
        {
            DetailInfoItemViewModel foundedArea = DetailViewModels.FirstOrDefault(D => D.UniqueIDText.Equals(areaName));
            if (foundedArea != null)
            {
                ShowIndex(DetailViewModels.IndexOf(foundedArea));
            }
        }
    }
}
