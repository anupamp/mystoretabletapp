﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls.ViewModel
{
    public class DetailInfoItemDataValueViewModel : ViewModelBase
    {
        #region Member - TextKey
        private string _textKey;
        public string TextKey
        {
            get { return _textKey; }
            set
            {
                if (SetValue(ref _textKey, value))
                {
                    Notify("TitleText");
                }
            }
        }
        #endregion

        #region Member - TitleText
        public string TitleText
        {
            get { return Translation[_textKey]; }
        }
        #endregion


        #region Member - Value
        private object _value;
        public object Value
        {
            get { return _value; }
            set { SetValue(ref _value, value); }
        }
        #endregion

        #region Member - ValueFormat
        private string _valueFormat = "0";
        public string ValueFormat
        {
            get { return _valueFormat; }
            set
            {
                if (SetValue(ref _valueFormat, value))
                {
                    Notify("ValueWithFormat");
                }
            }
        }
        #endregion


        #region Member - ValueWithFormat
        public string ValueWithFormat
        {
            get
            {
                if (Value == null)
                {
                    return String.Empty;
                }

                try
                {
                    if (_value is int)
                    {
                        return ((int)_value).ToString(_valueFormat);
                    }
                    else if (_value is decimal)
                    {
                        return ((decimal)_value).ToString(_valueFormat);
                    }
                    else if (_value is double)
                    {
                        return ((double)_value).ToString(_valueFormat);
                    }
                    else if (_value is DateTime)
                    {
                        return ((DateTime)_value).ToString(_valueFormat);
                    }
                }
                catch { }

                return _value==null ? "" : _value.ToString();
            }
        }
        #endregion


    }
}
