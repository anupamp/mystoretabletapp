﻿using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Report.Controls
{
    public sealed partial class DetailInfoControl : UserControl
    {
        private ScrollViewer _cachedScrollViewer = null;
        private DetailInfoControlViewModel _cachedViewModel = null;

        public DetailInfoControl()
        {
            this.InitializeComponent();

            DetailInfoControlViewModel.OnActiveIndexChanged += DetailInfoControlOnActiveIndexChanged;
        }

        private ScrollViewer GetCurrentScrollViewer()
        {
            if (_cachedScrollViewer == null)
            {
                _cachedScrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;
            }
            return _cachedScrollViewer;
        }
        private DetailInfoControlViewModel GetCurrentViewModel()
        {
            if (_cachedViewModel == null)
            {
                _cachedViewModel = (DetailInfoControlViewModel)DataContext;
            }
            return _cachedViewModel;
        }

        private void DetailInfoControlOnActiveIndexChanged(object sender, EventArgs args)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            GetCurrentScrollViewer().ChangeView(viewModel.ControlWidth.Width * (int)sender, null, null, true);
        }

        private void Container_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            viewModel.ControlWidth = e.NewSize;
        }

        private void ContainerScroller_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {

        }

        private void ContainerScroller_ViewChanging(object sender, ScrollViewerViewChangingEventArgs e)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            ScrollViewer scroller = GetCurrentScrollViewer();

            int index = (int)Math.Round(e.NextView.HorizontalOffset / viewModel.ControlWidth.Width);
            viewModel.CurrentScrollPositionIndex = index;
        }

        private void DetailInfoControlActiveItemControl_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            //if (!_manipulationStarted)
            //{


            //if (_viewModel.SelectedActiveItem != viewModel)
            //{
            //    _viewModel.SelectedActiveItem = viewModel;
            //    ToolGridVisibilityRelatedToElement(control);
            //    _viewModel.SelectedLayer.SaveLayerOnService();
            //}
            //else
            //{
            //    _viewModel.SelectedActiveItem = null;
            //    ToolGridVisibilityRelatedToElement(null);
            //}
            //}
        }

        private void OrderPointsEditorActiveItemControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            UserControl control = (UserControl)sender;
            OrderPointEditorActiveItemViewModel activeViewModel = (OrderPointEditorActiveItemViewModel)control.DataContext;

            if (activeViewModel is ItemTypeImageButtonViewModelBase)
            {
                DetailInfoControlViewModel viewModel = GetCurrentViewModel();
                viewModel.SetSelectedItem((ItemTypeImageButtonViewModelBase)activeViewModel);

                e.Handled = true;
            }


        }

    }
}
