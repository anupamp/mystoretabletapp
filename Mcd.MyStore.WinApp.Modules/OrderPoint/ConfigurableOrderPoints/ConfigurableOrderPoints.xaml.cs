﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints
{
    [MenuModuleAttribute("ConfOrderPoints")]
    public sealed partial class ConfigurableOrderPoints : UserControl
    {
        public ConfigurableOrderPoints()
        {
            this.InitializeComponent();
            DataContext = new ConfigurableOrderPointsViewModel();
        }
    }
}
