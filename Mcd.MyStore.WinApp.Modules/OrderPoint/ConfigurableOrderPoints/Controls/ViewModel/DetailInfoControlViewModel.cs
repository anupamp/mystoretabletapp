﻿using Mcd.MyStore.GeneralReportingService.Common.Dto.ConfigurableOrderPoints;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel.Items;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel
{
    public class DetailInfoControlViewModel : ViewModelBase
    {
        public static event EventHandler OnActiveIndexChanged;

        #region Member - CurrentActiveIndex
        private int _currentActiveIndex = 0;
        public int CurrentActiveIndex
        {
            get { return _currentActiveIndex; }
            set
            {
                if (SetValue(ref _currentActiveIndex, value))
                {
                    SelectPage(_currentActiveIndex);
                    CurrentScrollPositionIndex = _currentActiveIndex;
                }
            }
        }
        #endregion

        #region Member - CurrentScrollPositionIndex
        private int _currentScrollPositionIndex = 0;
        public int CurrentScrollPositionIndex
        {
            get { return _currentScrollPositionIndex; }
            set
            {
                if (SetValue(ref _currentScrollPositionIndex, value))
                {
                    SelectPage(_currentScrollPositionIndex);
                }
            }
        }
        #endregion


        #region Member - ControlWidth
        private double _controlWidth;
        public double ControlWidth
        {
            get { return _controlWidth; }
            set { SetValue(ref _controlWidth, value); }
        }
        #endregion

        public ObservableCollection<DetailInfoItemViewModel> DetailViewModels { get; set; }

        private ScrollPageItemViewModel _lastPageItem = null;
        public ObservableCollection<ScrollPageItemViewModel> PageItems { get; set; }

        public DetailInfoControlViewModel()
        {
            DetailViewModels = new ObservableCollection<DetailInfoItemViewModel>();
            PageItems = new ObservableCollection<ScrollPageItemViewModel>();
        }

        public void ClearItems()
        {
            _lastPageItem = null;
            DetailViewModels.Clear();
            PageItems.Clear();
        }

        public void ShowIndex(int index)
        {
            CurrentActiveIndex = index;

            if (OnActiveIndexChanged != null)
            {
                OnActiveIndexChanged(_currentActiveIndex, null);
            }
        }

        public void AddDetailViewModel(MapOverviewItemViewModel overviewItemModel, ConfigurableOrderPointsOverviewItemDto area)
        {
            DetailInfoItemViewModel detailModel = new DetailInfoItemViewModel();
            detailModel.UniqueIDText = area.UniqueIDText;
            detailModel.Title = overviewItemModel.Text;
            detailModel.BackgroundImagePath = "ms-appdata:///roaming/" + area.ImagePath;


            if (area.ImageMargin != null)
            {
                detailModel.BackgroundMargin = new Thickness((double)area.ImageMargin.Left, (double)area.ImageMargin.Top, (double)area.ImageMargin.Right, (double)area.ImageMargin.Bottom);
            }

            DetailViewModels.Add(detailModel);

            foreach (var detailItem in area.DetailItems)
            {
                detailModel.AddInfoItem(detailItem);

            }

            PageItems.Add(new ScrollPageItemViewModel());
            if (PageItems.Count == 1)
            {
                SelectPage(_currentActiveIndex);
            }
        }

        public void SelectPage(int index)
        {
            if (_lastPageItem != null)
            {
                _lastPageItem.IsActive = false;
            }

            if (index >= PageItems.Count)
            {
                index = 0;
            }

            _lastPageItem = PageItems[index];
            _lastPageItem.IsActive = true;
        }
    }
}
