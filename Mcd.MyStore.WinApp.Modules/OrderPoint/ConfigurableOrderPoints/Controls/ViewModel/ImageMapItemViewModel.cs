﻿using Mcd.MyStore.WinApp.Core.MVVM.Common;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Windows.Input;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel
{
    public class ImageMapItemViewModel : ViewModelBase
    {
        public string UniqueIDText { get; set; }

        #region Member - OuterRect
        private RectangleViewModel _outerRect = new RectangleViewModel();
        public RectangleViewModel OuterRect
        {
            get { return _outerRect; }
        }
        #endregion

        #region Member - InnerRect
        private RectangleViewModel _innerRect = new RectangleViewModel();
        public RectangleViewModel InnerRect
        {
            get { return _innerRect; }
        }
        #endregion

        public ICommand TappedCommand { get; set; }
        public event EventHandler<TappedRoutedEventArgs> OnTapped;

        public ImageMapItemViewModel()
        {
            TappedCommand = new RelayCommand(delegate(object e)
                {
                    return InternalTapped((TappedRoutedEventArgs)e);
                }
            );
        }


        protected virtual object InternalTapped(TappedRoutedEventArgs parameter)
        {
            SendOnTapped(parameter);
            return parameter;
        }

        public void SendOnTapped(TappedRoutedEventArgs parameter)
        {
            if (OnTapped != null)
            {
                OnTapped(this, parameter);
            }
        }
    }
}
