﻿using Mcd.MyStore.GeneralReportingService.Common.Dto.ConfigurableOrderPoints;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel.Items;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel
{
    public class DetailInfoItemViewModel : ViewModelBase
    {
        public string UniqueIDText { get; set; }

        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion

        #region Member - BackgroundImagePath
        private string _backgroundImagePath;
        public string BackgroundImagePath
        {
            get { return _backgroundImagePath; }
            set { SetValue(ref _backgroundImagePath, value); }
        }
        #endregion

        #region Member - BackgroundMargin
        private Thickness _backgroundMargin;
        public Thickness BackgroundMargin
        {
            get { return _backgroundMargin; }
            set { SetValue(ref _backgroundMargin, value); }
        }
        #endregion

        #region Member - SelectedItem
        private MapDetailIconViewModel _selectedItem = null;
        public MapDetailIconViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (SetValue(ref _selectedItem, value))
                {
                    Notify("SelectedItemTitle");
                    UpdateVisibleData();
                }
            }
        }
        #endregion

        #region Member - SelectedItemTitle
        public string SelectedItemTitle
        {
            get { return (_selectedItem != null ? _selectedItem.Text : "All " + _title); }
        }
        #endregion


        #region Member - IsBackgroundHitTestEnabled
        public bool IsBackgroundHitTestEnabled
        {
            get { return false; }
        }
        #endregion

        public ObservableCollection<MapDetailIconViewModel> MapItems { get; set; }

        public Dictionary<string, List<DetailInfoItemDataValueViewModel>> AllDataValues { get; set; }
        public ObservableCollection<DetailInfoItemDataValueViewModel> DataValues { get; set; }

        public bool AllItemsAreActive
        {
            get { return !MapItems.Any(M => !M.IsActive); }
        }

        public ICommand SelectAllItemsCommand { get; set; }

        public DetailInfoItemViewModel()
        {
            BackgroundMargin = new Thickness();
            AllDataValues = new Dictionary<string, List<DetailInfoItemDataValueViewModel>>();
            DataValues = new ObservableCollection<DetailInfoItemDataValueViewModel>();
            MapItems = new ObservableCollection<MapDetailIconViewModel>();

            SelectAllItemsCommand = new RelayCommand(() =>
            {
                SelectAllItems();
            });
        }


        public void AddInfoItem(ConfigurableOrderPointsInfoItemDto detailItem)
        {
            string tmpString = null;

            MapDetailIconViewModel iconModel = new MapDetailIconViewModel();
            if (detailItem.TryGetDataAsString("Type", out tmpString))
            {
                switch (tmpString)
                {
                    case "IconWithRightText":
                        iconModel = new MapDetailIconWithRightTextViewModel();
                        break;
                }
            }

            iconModel.UniqueIDText = detailItem.UniqueIDText;

            if (detailItem.UniqueID > 0)
            {
                iconModel.UniqueIDText = detailItem.UniqueID.ToString();
            }

            iconModel.OuterRect.Update(detailItem.TouchRect);
            iconModel.InnerRect.Update(detailItem.InfoRect);
            iconModel.Text = detailItem.UniqueIDText;
            iconModel.OnTapped += IconModelOnTapped;

            bool tmpBool = false;
            if (detailItem.TryGetDataAsBool("TextIsOnTop", out tmpBool))
            {
                iconModel.TextIsOnTop = tmpBool;
            }


            if (detailItem.TryGetDataAsString("TextTranslationKey", out tmpString))
            {
                iconModel.Text = LanguageManager.Instance.GetTranslation(tmpString);
            }

            if (detailItem.TryGetDataAsString("ImagePath", out tmpString))
            {
                iconModel.NormalImage = tmpString;
            }

            if (detailItem.TryGetDataAsString("ActiveImagePath", out tmpString))
            {
                iconModel.ActiveImage = tmpString;
            }



            MapItems.Add(iconModel);
        }

        private void IconModelOnTapped(object sender, TappedRoutedEventArgs e)
        {
            MapDetailIconViewModel activeModel = (MapDetailIconViewModel)sender;

            var otherItems = MapItems.Where(M => M != activeModel).ToList();
            foreach (var item in otherItems)
            {
                item.IsActive = false;
            }
            activeModel.IsActive = true;

            SelectedItem = activeModel;

            e.Handled = true;
        }

        public void SelectAllItems()
        {
            foreach (var item in MapItems)
            {
                item.IsActive = true;
            }
            SelectedItem = null;
        }


        internal void UpdateVisibleData()
        {
            string key = (_selectedItem == null ? "all" : _selectedItem.UniqueIDText.ToLower());

            DataValues.Clear();

            List<DetailInfoItemDataValueViewModel> selectedList = null;
            if (AllDataValues.TryGetValue(key, out selectedList))
            {
                foreach (var item in selectedList)
                {
                    DataValues.Add(item);
                }
            }
        }
    }
}
