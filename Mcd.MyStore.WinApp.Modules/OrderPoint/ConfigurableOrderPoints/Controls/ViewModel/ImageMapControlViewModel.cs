﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System.Collections.ObjectModel;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel
{
    public class ImageMapControlViewModel : ViewModelBase
    {
        #region Member - BackgroundImagePath
        private string _backgroundImagePath;
        public string BackgroundImagePath
        {
            get { return _backgroundImagePath; }
            set { SetValue(ref _backgroundImagePath, value); }
        }
        #endregion

        #region Member - BackgroundMargin
        private Thickness _backgroundMargin;
        public Thickness BackgroundMargin
        {
            get { return _backgroundMargin; }
            set { SetValue(ref _backgroundMargin, value); }
        }
        #endregion

        #region Member - OverviewIsVisible
        private bool _overviewIsVisible = true;
        public bool OverviewIsVisible
        {
            get { return _overviewIsVisible; }
            set
            {
                if (SetValue(ref _overviewIsVisible, value))
                {
                    Notify("HitTestEnabled");
                }
            }
        }
        #endregion

        #region Member - HitTestEnabled
        public bool HitTestEnabled
        {
            get { return OverviewIsVisible; }
        }
        #endregion

        #region Member - BackgroundColor
        public SolidColorBrush BackgroundColor
        {
            get { return new SolidColorBrush(Colors.White); }
        }
        #endregion



        public ObservableCollection<ImageMapItemViewModel> MapItems { get; set; }

        public ImageMapControlViewModel()
        {
            MapItems = new ObservableCollection<ImageMapItemViewModel>();
        }
    }
}
