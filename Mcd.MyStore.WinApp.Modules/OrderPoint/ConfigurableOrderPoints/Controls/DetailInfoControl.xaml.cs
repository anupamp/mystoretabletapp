﻿using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls
{
    public sealed partial class DetailInfoControl : UserControl
    {
        private ScrollViewer _cachedScrollViewer = null;
        private DetailInfoControlViewModel _cachedViewModel = null;

        public DetailInfoControl()
        {
            this.InitializeComponent();

            DetailInfoControlViewModel.OnActiveIndexChanged += DetailInfoControlOnActiveIndexChanged;
        }

        private ScrollViewer GetCurrentScrollViewer()
        {
            if (_cachedScrollViewer == null)
            {
                _cachedScrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;
            }
            return _cachedScrollViewer;
        }
        private DetailInfoControlViewModel GetCurrentViewModel()
        {
            if (_cachedViewModel == null)
            {
                _cachedViewModel = (DetailInfoControlViewModel)DataContext;
            }
            return _cachedViewModel;
        }

        private void DetailInfoControlOnActiveIndexChanged(object sender, EventArgs args)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            GetCurrentScrollViewer().ChangeView(viewModel.ControlWidth * (int)sender, null, null, true);
        }

        private void Container_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            viewModel.ControlWidth = e.NewSize.Width;
        }

        private void ContainerScroller_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {

        }

        private void ContainerScroller_ViewChanging(object sender, ScrollViewerViewChangingEventArgs e)
        {
            DetailInfoControlViewModel viewModel = GetCurrentViewModel();
            ScrollViewer scroller = GetCurrentScrollViewer();

            int index = (int)Math.Round(e.NextView.HorizontalOffset / viewModel.ControlWidth);
            viewModel.CurrentScrollPositionIndex = index;
        }
    }
}
