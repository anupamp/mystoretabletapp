﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto.ConfigurableOrderPoints;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel;
using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel
{
    public class ConfigurableOrderPointsViewModel : ModuleViewModelBase
    {
        #region Member - MapViewModel
        private ImageMapControlViewModel _mapViewModel = new ImageMapControlViewModel();
        public ImageMapControlViewModel MapViewModel
        {
            get { return _mapViewModel; }
            set { SetValue(ref _mapViewModel, value); }
        }
        #endregion

        #region Member - DetailViewModel
        private DetailInfoControlViewModel _detailViewModel = new DetailInfoControlViewModel();
        public DetailInfoControlViewModel DetailViewModel
        {
            get { return _detailViewModel; }
            set { SetValue(ref _detailViewModel, value); }
        }
        #endregion

        public ICommand ReloadSettingsCommand { get; set; }
        public ICommand ShowOverviewCommand { get; set; }

        public ConfigurableOrderPointsViewModel()
        {
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);

            ReloadSettingsCommand = new RelayCommand(() =>
            {
                LoadSettings();
            });

            ShowOverviewCommand = new RelayCommand(() =>
            {
                _mapViewModel.OverviewIsVisible = true;
            });
        }

        private void OnOverviewItemTapped(object sender, TappedRoutedEventArgs e)
        {
            MapOverviewItemViewModel model = (MapOverviewItemViewModel)sender;

            int index = _mapViewModel.MapItems.IndexOf(model);

            _detailViewModel.ShowIndex(index);

            _mapViewModel.OverviewIsVisible = false;

        }

        #region Loading settings
        private void LoadSettings()
        {
            LoadAsyncData<ConfigurableOrderPointsSettingsDto>(LoadSettingsAsync, LoadSettingsAsyncFinished);
        }

        private ConfigurableOrderPointsSettingsDto LoadSettingsAsync()
        {
            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                ConfigurableOrderPointsSettingsDto result = service.GetOrderPointSettings(true);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadSettingsAsyncFinished(ConfigurableOrderPointsSettingsDto data)
        {
            _mapViewModel.MapItems.Clear();
            _detailViewModel.ClearItems();

            if (data == null)
            {
                return;
            }

            _mapViewModel.BackgroundImagePath = "ms-appdata:///roaming/" + data.ImagePath;

            if (data.ImageMargin != null)
            {
                _mapViewModel.BackgroundMargin = new Thickness((double)data.ImageMargin.Left, (double)data.ImageMargin.Top, (double)data.ImageMargin.Right, (double)data.ImageMargin.Bottom);
            }

            foreach (var area in data.OverviewItems)
            {
                MapOverviewItemViewModel overviewItemModel = new MapOverviewItemViewModel();
                overviewItemModel.UniqueIDText = area.UniqueIDText;
                overviewItemModel.OuterRect.Update(area.OverviewItem.TouchRect);
                overviewItemModel.InnerRect.Update(area.OverviewItem.InfoRect);
                overviewItemModel.OnTapped += OnOverviewItemTapped;

                if (String.IsNullOrEmpty(overviewItemModel.UniqueIDText))
                {
                    continue;
                }

                string tmpColor = null;
                if (area.OverviewItem.TryGetDataAsString("BackgroundColor", out tmpColor))
                {
                    overviewItemModel.BackgroundColor = SolidColorBrushExtensions.ConvertFromHex(tmpColor);
                }


                string tmpTranslationKey = null;
                if (area.OverviewItem.TryGetDataAsString("TextTranslationKey", out tmpTranslationKey))
                {
                    overviewItemModel.Text = LanguageManager.Instance.GetTranslation(tmpTranslationKey);
                }


                _mapViewModel.MapItems.Add(overviewItemModel);

                _detailViewModel.AddDetailViewModel(overviewItemModel, area);
            }

            LoadData();
        }
        #endregion

        #region Loading data
        private void LoadData()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return;

            LoadAsyncData(LoadDataAsync, LoadDataAsyncFinished);
        }

        private ConfigurableOrderPointsDataDto LoadDataAsync()
        {
            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                ConfigurableOrderPointsDataDto result = service.GetOrderPointData(SystemStatusManager.Instance.GlobalUserSelectedDay.Value);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadDataAsyncFinished(ConfigurableOrderPointsDataDto data)
        {
            foreach (var area in data.Areas)
            {
                DetailInfoItemViewModel detailModel = _detailViewModel.DetailViewModels.FirstOrDefault(D => D.UniqueIDText.Equals(area.UniqueIDText, StringComparison.CurrentCultureIgnoreCase));
                if (detailModel != null)
                {
                    foreach (var item in area.ItemValues)
                    {
                        string keyLower = item.UniqueIDText.ToLower();

                        List<DetailInfoItemDataValueViewModel> selectedList = null;
                        if (!detailModel.AllDataValues.TryGetValue(keyLower, out selectedList))
                        {
                            selectedList = new List<DetailInfoItemDataValueViewModel>();
                            detailModel.AllDataValues.Add(keyLower, selectedList);
                        }
                        else
                        {
                            selectedList.Clear();
                        }

                        foreach (var dataItem in item.DataValues)
                        {
                            selectedList.Add(new DetailInfoItemDataValueViewModel()
                            {
                                TextKey = "KEY_ConfigurableOrderPoints_DataItemTitle_" + dataItem.Key,
                                ValueFormat = dataItem.ValueFormat,
                                Value = dataItem.Value,
                            });
                        }

                    }

                    detailModel.UpdateVisibleData();
                }
            }
        }
        #endregion

        #region common module stuff
        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            LoadData();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadSettings();
        }
        #endregion
    }
}
