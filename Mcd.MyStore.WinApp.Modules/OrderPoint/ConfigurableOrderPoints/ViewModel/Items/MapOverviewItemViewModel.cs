﻿using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel.Items
{
    public class MapOverviewItemViewModel : ImageMapItemViewModel
    {
        #region Member - BackgroundColor
        private SolidColorBrush _backgroundColor = new SolidColorBrush(Colors.Purple);
        public SolidColorBrush BackgroundColor
        {
            get { return _backgroundColor; }
            set { SetValue(ref _backgroundColor, value); }
        }
        #endregion

        #region Member - Text
        private string _text;
        public string Text
        {
            get { return _text; }
            set { SetValue(ref _text, value); }
        }
        #endregion


    }
}
