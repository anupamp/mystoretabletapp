﻿using Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.Controls.ViewModel;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Modules.ConfigurableOrderPoints.ViewModel.Items
{
    public class MapDetailIconViewModel : ImageMapItemViewModel
    {
        #region Member - Text
        private string _text;
        public string Text
        {
            get { return _text; }
            set { SetValue(ref _text, value); }
        }
        #endregion

        #region Member - NormalImage
        private string _normalImage;
        public string NormalImage
        {
            get { return _normalImage; }
            set { SetValue(ref _normalImage, "ms-appdata:///roaming/" + value); }
        }
        #endregion

        #region Member - ActiveImage
        private string _activeImage;
        public string ActiveImage
        {
            get { return _activeImage; }
            set { SetValue(ref _activeImage, "ms-appdata:///roaming/" + value); }
        }
        #endregion

        #region Member - IsActive
        private bool _isActive = true;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (SetValue(ref _isActive, value))
                {
                    Notify("ActiveImageVisibility");
                }
            }
        }
        #endregion

        #region Member - ActiveImageVisibility
        public Visibility ActiveImageVisibility
        {
            get { return (_isActive ? Visibility.Visible : Visibility.Collapsed); }
        }
        #endregion




        #region Text positioning (top or bottom)
        #region Member - TextIsOnTop
        private bool _textIsOnTop = false;
        public bool TextIsOnTop
        {
            get { return _textIsOnTop; }
            set
            {
                if (SetValue(ref _textIsOnTop, value))
                {
                    Notify("TextTopVisibility");
                    Notify("TextBottomVisibility");
                }
            }
        }
        #endregion
        #region Member - TextTopVisibility
        public Visibility TextTopVisibility
        {
            get { return (_textIsOnTop ? Visibility.Visible : Visibility.Collapsed); }
        }
        #endregion
        #region Member - TextBottomVisibility
        public Visibility TextBottomVisibility
        {
            get { return (_textIsOnTop ? Visibility.Collapsed : Visibility.Visible); }
        }
        #endregion
        #endregion




    }
}
