﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel
{
    public class CategoryItemViewModel : ViewModelBase
    {
        #region Member - Key
        private string _key;
        public string Key
        {
            get { return _key; }
            set
            {
                if (SetValue(ref _key, value))
                {
                    Notify("Title");
                }
            }
        }
        #endregion

        #region Member - Title
        public string Title
        {
            get { return Translation["OrderPointEditor_Category_" + _key]; }
        }
        #endregion

        #region Member - Visibility
        private Visibility _visibility;
        public Visibility Visibility
        {
            get { return _visibility; }
            set { SetValue(ref _visibility, value); }
        }
        #endregion

        #region Member - IsActive
        private bool _isActive = true;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (SetValue(ref _isActive, value))
                {
                    Visibility = (_isActive ? Visibility.Visible : Visibility.Collapsed);
                }
            }
        }
        #endregion


    }
}
