﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.GeneralReportingService.Common.Dto.OrderPoint;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using System.Collections.ObjectModel;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel
{
    public class LayerViewModel : ViewModelBase
    {
        #region Member - LayerID
        private string _layerID;
        public string LayerID
        {
            get { return _layerID; }
            set
            {
                SetValue(ref _layerID, value);
            }
        }
        #endregion

        #region Member - Width
        private double _width = 100;
        public double Width
        {
            get { return _width; }
            set { SetValue(ref _width, value); }
        }
        #endregion

        #region Member - Height
        private double _height;
        public double Height
        {
            get { return _height; }
            set { SetValue(ref _height, value); }
        }
        #endregion



        public ObservableCollection<OrderPointEditorActiveItemViewModel> ActiveItems { get; set; }

        public LayerViewModel()
        {
            ActiveItems = new ObservableCollection<OrderPointEditorActiveItemViewModel>();
        }

        public void SaveLayerOnService()
        {
            OrderPointLayerItemDto layerDto = new OrderPointLayerItemDto();
            layerDto.LayerID = LayerID;
            layerDto.ContainerWidth = Width;
            layerDto.ContainerHeight = Height;

            foreach (OrderPointEditorActiveItemViewModel item in ActiveItems)
            {
                layerDto.ContentItems.Add(new OrderPointLayerContentItemDto()
                {
                    ContentKey = item.TemplateDto.IDKey,
                    Left = item.Left,
                    Top = item.Top,
                    Width = item.Width,
                    Height = item.Height,
                    UserSettings = item.UserSettings
                });
            }

            LoadAsyncData(() =>
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                service.UpdateLayerSettings(layerDto);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
            }, null, false);
        }
    }
}
