﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.GeneralReportingService.Common.Dto.OrderPoint;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel
{
    public class OrderPointEditorViewModel : ModuleViewModelBase
    {
        private List<OrderPointItemTemplateViewModel> _allTemplateItems = new List<OrderPointItemTemplateViewModel>();
        public ObservableCollection<OrderPointItemTemplateViewModel> TemplateItems { get; set; }
        public ObservableCollection<LayerViewModel> LayerItems { get; set; }
        private LayerViewModel _overviewLayer = null;
        private OrderPointEditorManager _manager = OrderPointEditorManager.Instance;
        private Size _wrapperInsertContainerSize = Size.Empty;

        #region Member - SelectedLayer
        private LayerViewModel _selectedLayer;
        public LayerViewModel SelectedLayer
        {
            get { return _selectedLayer; }
            set
            {
                SetValue(ref _selectedLayer, value);

                SelectedActiveItem = null;
                if (_areasCategory != null)
                {
                    if (_selectedLayer == _overviewLayer)
                    {
                        _areasCategory.IsActive = true;
                    }
                    else
                    {
                        if (SelectedCategoryItem == _areasCategory)
                        {
                            SelectedCategoryItem = CategoryList.FirstOrDefault();
                        }

                        _areasCategory.IsActive = false;
                    }
                }
                UpdateAreasVisibilityOnOverview(_selectedCategoryItem == _areasCategory);
            }
        }
        #endregion


        #region Member - SelectedTemplateItem
        private OrderPointItemTemplateViewModel _selectedTemplateItem;
        public OrderPointItemTemplateViewModel SelectedTemplateItem
        {
            get { return _selectedTemplateItem; }
            set { SetValue(ref _selectedTemplateItem, value); }
        }
        #endregion

        #region Member - SelectedActiveItem
        public event EventHandler OnSelectedActiveItemChanged;
        private OrderPointEditorActiveItemViewModel _selectedActiveItem;
        public OrderPointEditorActiveItemViewModel SelectedActiveItem
        {
            get { return _selectedActiveItem; }
            set
            {
                if (_selectedActiveItem != null && _selectedActiveItem != value && _selectedActiveItem is ItemTypeImageButtonViewModelBase)
                {
                    (_selectedActiveItem as ItemTypeImageButtonViewModelBase).IsActive = false;
                }

                if (SetValue(ref _selectedActiveItem, value))
                {
                    if (value != null && _selectedActiveItem != null && _selectedActiveItem is ItemTypeImageButtonViewModelBase)
                    {
                        (_selectedActiveItem as ItemTypeImageButtonViewModelBase).IsActive = true;
                    }
                }

                if (OnSelectedActiveItemChanged != null)
                {
                    OnSelectedActiveItemChanged(this, null);
                }
            }
        }
        #endregion

        private CategoryItemViewModel _areasCategory;
        public ObservableCollection<CategoryItemViewModel> CategoryList { get; set; }
        #region Member - SelectedCategoryItem
        private CategoryItemViewModel _selectedCategoryItem;
        public CategoryItemViewModel SelectedCategoryItem
        {
            get { return _selectedCategoryItem; }
            set
            {
                if (SetValue(ref _selectedCategoryItem, value))
                {
                    SelectedActiveItem = null;

                    UpdateTemplateList();

                    UpdateAreasVisibilityOnOverview(_selectedCategoryItem == _areasCategory);
                }
            }
        }
        #endregion

        #region Member - InsertColumnSpan
        private int _insertColumnSpan = 2;
        public int InsertColumnSpan
        {
            get { return _insertColumnSpan; }
            set { SetValue(ref _insertColumnSpan, value); }
        }
        #endregion

        public OrderPointEditorViewModel()
        {
            CategoryList = new ObservableCollection<CategoryItemViewModel>();
            TemplateItems = new ObservableCollection<OrderPointItemTemplateViewModel>();
            LayerItems = new ObservableCollection<LayerViewModel>();



            OrderPointEditorManager.Instance.OnTemplateSettingsChanged += ManagerOnTemplateSettingsChanged;

            LayerItems.Clear();

            _overviewLayer = new LayerViewModel()
            {
                LayerID = "Overview",
                Width = 100,
                Height = 100
            };
            _overviewLayer.ActiveItems.CollectionChanged += OverviewActiveItems_CollectionChanged;
            LayerItems.Add(_overviewLayer);
            SelectedLayer = _overviewLayer;
        }

        private void OverviewActiveItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null && e.NewItems.Count > 0)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is ItemTypeAreaViewModel)
                    {
                        string layerName = ((ItemTypeAreaViewModel)item).AreaName;

                        LayerViewModel layer = LayerItems.FirstOrDefault(L => L.LayerID.Equals(layerName, StringComparison.CurrentCultureIgnoreCase));
                        if (layer == null)
                        {
                            LayerItems.Add(new LayerViewModel()
                            {
                                LayerID = ((ItemTypeAreaViewModel)item).AreaName,
                                Width = _wrapperInsertContainerSize.Width / 2.0,
                                Height = _wrapperInsertContainerSize.Height
                            });
                        }
                    }
                }
            }

            if (e.OldItems != null && e.OldItems.Count > 0)
            {
                foreach (var item in e.OldItems)
                {
                    if (item is ItemTypeAreaViewModel)
                    {
                        string layerName = ((ItemTypeAreaViewModel)item).AreaName;

                        List<OrderPointEditorActiveItemViewModel> itemViewModels = _overviewLayer.ActiveItems
                            .Where(I => I is ItemTypeAreaViewModel && ((ItemTypeAreaViewModel)I).AreaName.Equals(layerName, StringComparison.CurrentCultureIgnoreCase))
                            .ToList();
                        if (itemViewModels == null || itemViewModels.Count == 0)
                        {
                            LayerViewModel layer = LayerItems.FirstOrDefault(L => L.LayerID.Equals(layerName, StringComparison.CurrentCultureIgnoreCase));
                            if (layer != null)
                            {
                                layer.ActiveItems.Clear();
                                layer.SaveLayerOnService();

                                LayerItems.Remove(layer);
                            }
                        }
                    }
                }
            }
        }

        private async void ManagerOnTemplateSettingsChanged(object sender, System.EventArgs e)
        {
            foreach (OrderPointTemplateItem currentTemplate in OrderPointEditorManager.Instance.ItemTemplates)
            {
                OrderPointItemTemplateViewModel newModel = new OrderPointItemTemplateViewModel(currentTemplate);

                if (!String.IsNullOrEmpty(currentTemplate.Background))
                {
                    newModel.BackgroundColor = SolidColorBrushExtensions.ConvertFromHex(currentTemplate.Background);
                }

                if (!String.IsNullOrEmpty(currentTemplate.AreaKey))
                {
                    newModel.TemplateText = currentTemplate.AreaKey;
                }

                if (!String.IsNullOrEmpty(currentTemplate.LocalSotrageFolderPath) && !String.IsNullOrEmpty(currentTemplate.Icon))
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(currentTemplate.LocalSotrageFolderPath);
                    newModel.TemplateImage = await ImageManager.Instance.GetImage(await folder.GetFileAsync(currentTemplate.Icon));
                }

                _allTemplateItems.Add(newModel);
            }

            List<string> categories = OrderPointEditorManager.Instance.ItemTemplates.Select(I => I.Category).Distinct().ToList();
            foreach (string category in categories)
            {
                CategoryItemViewModel categoryItemViewModel = new CategoryItemViewModel()
                {
                    Key = category
                };

                if (category.Equals("areas", StringComparison.CurrentCultureIgnoreCase))
                {
                    _areasCategory = categoryItemViewModel;
                }

                CategoryList.Add(categoryItemViewModel);
            }

            if (CategoryList.Count > 0)
            {
                SelectedCategoryItem = CategoryList.FirstOrDefault();
            }

            LoadLayersFromService();
        }

        private void UpdateTemplateList()
        {
            TemplateItems.Clear();
            var filteredResult = _allTemplateItems.Where(T => T.TemplateDto.Category.Equals(_selectedCategoryItem.Key, StringComparison.CurrentCultureIgnoreCase)).ToList();
            foreach (var item in filteredResult)
            {
                TemplateItems.Add(item);
            }
        }

        private async void LoadLayersFromService()
        {
            OrderPointLayerSettingsDto layerSettingsDto = await Task.Run(() =>
            {
                OrderPointLayerSettingsDto result = null;
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                result = service.GetAllLayerSettings();
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            });

            foreach (var currentLayer in layerSettingsDto.LayerItems)
            {
                LayerViewModel layerViewModel = LayerItems.SingleOrDefault(L => L.LayerID.Equals(currentLayer.LayerID, StringComparison.CurrentCultureIgnoreCase));
                if (layerViewModel != null)
                {
                    foreach (var currentContent in currentLayer.ContentItems)
                    {
                        OrderPointItemTemplateViewModel templateViewModel = _allTemplateItems.SingleOrDefault(T => T.TemplateDto.IDKey.Equals(currentContent.ContentKey, StringComparison.CurrentCultureIgnoreCase));
                        if (templateViewModel != null)
                        {
                            OrderPointEditorActiveItemViewModel newActiveViewModel = _manager.CreateActiveItemViewModelFromTemplate(templateViewModel.TemplateDto);

                            //double positionWidthRatio = layerViewModel.Width / currentLayer.ContainerWidth;
                            //double positionHeightRatio = layerViewModel.Height / currentLayer.ContainerHeight;

                            double positionX = currentContent.Left / currentLayer.ContainerWidth;
                            double positionY = currentContent.Top / currentLayer.ContainerHeight;

                            double sizeX = currentContent.Width / currentLayer.ContainerWidth;
                            double sizeY = currentContent.Height / currentLayer.ContainerHeight;

                            //double ratio = Math.Max(positionWidthRatio, positionHeightRatio);

                            newActiveViewModel.Left = layerViewModel.Width * positionX;// *positionWidthRatio;
                            newActiveViewModel.Top = layerViewModel.Height * positionY; // *positionHeightRatio;
                            newActiveViewModel.Width = layerViewModel.Width * sizeX;
                            newActiveViewModel.Height = layerViewModel.Height * sizeY;
                            newActiveViewModel.UserSettings = currentContent.UserSettings;
                            layerViewModel.ActiveItems.Add(newActiveViewModel);
                        }
                    }
                }
            }
        }


        #region Module base stuff
        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {

        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {

        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {

        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            if (!OrderPointEditorManager.Instance.InitIfNeeded() && _allTemplateItems.Count == 0)
            {
                ManagerOnTemplateSettingsChanged(this, null);
            }
        }
        #endregion

        internal OrderPointEditorActiveItemViewModel CreateNewActiveItemViewModel()
        {
            OrderPointEditorActiveItemViewModel newInsertModel = _manager.CreateActiveItemViewModelFromTemplate(SelectedTemplateItem.TemplateDto);
            if (newInsertModel == null)
                return null;

            if (SelectedTemplateItem.TemplateDto.ContentTemplate.IsBackgroundContent)
            {
                // find last background content item
                var lastBackgroundContent = SelectedLayer.ActiveItems.LastOrDefault(I => I.TemplateDto.ContentTemplate.IsBackgroundContent);

                // find index of last background content
                int insertIndex = (lastBackgroundContent != null ? SelectedLayer.ActiveItems.IndexOf(lastBackgroundContent) + 1 : 0);

                SelectedLayer.ActiveItems.Insert(insertIndex, newInsertModel);
            }
            else if (SelectedTemplateItem.TemplateDto.ContentTemplate.Type == OrderPointTemplateItemContentType.Area)
            {
                newInsertModel.IsVisible = true;
                SelectedLayer.ActiveItems.Add(newInsertModel);
            }
            else
            {
                // find first Area content item
                var firstAreaContent = SelectedLayer.ActiveItems.FirstOrDefault(I => I.TemplateDto.ContentTemplate.Type == OrderPointTemplateItemContentType.Area);

                // find index of first Area content
                int insertIndex = (firstAreaContent != null ? SelectedLayer.ActiveItems.IndexOf(firstAreaContent) : SelectedLayer.ActiveItems.Count);

                SelectedLayer.ActiveItems.Insert(insertIndex, newInsertModel);
            }

            return newInsertModel;
        }

        private void UpdateAreasVisibilityOnOverview(bool itemsAreVisible)
        {
            if (_selectedLayer == _overviewLayer)
            {
                List<OrderPointEditorActiveItemViewModel> orderPointItemList = _overviewLayer.ActiveItems.Where(I => I.TemplateDto.ContentTemplate.Type == OrderPointTemplateItemContentType.Area).ToList();
                foreach (var item in orderPointItemList)
                {
                    item.IsVisible = itemsAreVisible;
                }
            }
        }

        internal void UpdateLayerSize(double width, double height)
        {
            _wrapperInsertContainerSize = new Size(width, height);
            _overviewLayer.Width = _wrapperInsertContainerSize.Width;
            _overviewLayer.Height = _wrapperInsertContainerSize.Height;
        }
    }
}
