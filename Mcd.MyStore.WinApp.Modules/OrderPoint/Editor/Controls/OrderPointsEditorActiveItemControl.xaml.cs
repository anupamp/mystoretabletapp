﻿using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.Controls
{
    public sealed partial class OrderPointsEditorActiveItemControl : UserControl
    {
        private OrderPointEditorActiveItemViewModel _viewModel = null;

        public OrderPointsEditorActiveItemControl()
        {
            this.InitializeComponent();
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            if (_viewModel == null || _viewModel.Control == null)
            {
                OrderPointEditorActiveItemViewModel model = (OrderPointEditorActiveItemViewModel)DataContext;
                _viewModel = model;
                _viewModel.Control = this;
            }

            return base.MeasureOverride(availableSize); ;
        }

        private void Grid_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!_viewModel.IsReportMode)
            {
                _viewModel.UpdateWithManipulation(e);
            }
        }
    }
}
