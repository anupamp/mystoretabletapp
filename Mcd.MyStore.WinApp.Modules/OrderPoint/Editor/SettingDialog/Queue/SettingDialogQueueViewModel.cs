﻿using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Queue
{
    public class SettingDialogQueueViewModel : PopupDialogContentViewModel
    {
        #region Member - ItemViewModel
        private ItemTypeImageButtonQueueViewModel _itemViewModel;
        public ItemTypeImageButtonQueueViewModel ItemViewModel
        {
            get { return _itemViewModel; }
            set
            {
                if (SetValue(ref _itemViewModel, value))
                {
                    UpdateSettings();
                }
            }
        }
        #endregion


        public ObservableCollection<SettingDialogTypeImageViewModel> QueueImageList { get; set; }

        #region Member - SelectedQueueImage
        private SettingDialogTypeImageViewModel _selectedQueueImage;
        public SettingDialogTypeImageViewModel SelectedQueueImage
        {
            get { return _selectedQueueImage; }
            set
            {
                if (value == null)
                    return;

                if (SetValue(ref _selectedQueueImage, value))
                {
                    ItemViewModel.TypeKey = _selectedQueueImage.TypeKey;
                }
            }
        }
        #endregion

        public ObservableCollection<SettingDialogQueueNameItemViewModel> QueueList { get; set; }

        #region Member - SelectedQueue
        private SettingDialogQueueNameItemViewModel _selectedQueue;
        public SettingDialogQueueNameItemViewModel SelectedQueue
        {
            get { return _selectedQueue; }
            set
            {
                if (value == null)
                    return;

                if (SetValue(ref _selectedQueue, value))
                {
                    ItemViewModel.QueueName = _selectedQueue.Name;
                }
            }
        }
        #endregion


        public ICommand CloseCommand { get; set; }

        public ICommand TestCommand { get; set; }
        public SettingDialogQueueViewModel()
        {
            QueueImageList = new ObservableCollection<SettingDialogTypeImageViewModel>();
            QueueList = new ObservableCollection<SettingDialogQueueNameItemViewModel>();
            AllowDragMove = true;




            CloseCommand = new RelayCommand(() =>
            {
                DialogResult = false;
            });

            TestCommand = new RelayCommand(() =>
            {
                ItemViewModel.TypeKey = "DT";
            });

        }

        private async void UpdateSettings()
        {
            ItemTypeImageButtonQueueSettingObject settingObject = (ItemViewModel.TemplateDto.SettingDataObject as ItemTypeImageButtonQueueSettingObject);
            if (settingObject != null)
            {
                foreach (var item in settingObject.ImageHolderList)
                {
                    SettingDialogTypeImageViewModel itemModel = new SettingDialogTypeImageViewModel();
                    itemModel.TypeKey = item.Type;
                    itemModel.Image = await ImageManager.Instance.GetImage(await item.Folder.GetFileAsync(item.ActiveImagePath));

                    QueueImageList.Add(itemModel);
                }

                object tmpPODType = null;
                if (_itemViewModel.UserSettings.TryGetValue("QueueImageTypeKey", out tmpPODType))
                {
                    SelectedQueueImage = QueueImageList.FirstOrDefault(P => P.TypeKey.Equals((string)tmpPODType, StringComparison.CurrentCultureIgnoreCase));
                }

                if (SelectedQueueImage == null)
                {
                    SelectedQueueImage = QueueImageList.FirstOrDefault();
                }
            }


            foreach (var currentQueue in SystemStatusManager.Instance.StoreSetupInformation.QueueList)
            {
                QueueList.Add(new SettingDialogQueueNameItemViewModel()
                {
                    Name = currentQueue,
                    Title = currentQueue
                });
            }

            object tmpQueueName = null;
            if (_itemViewModel.UserSettings.TryGetValue("QueueName", out tmpQueueName))
            {
                SelectedQueue = QueueList.FirstOrDefault(P => P.Name == (string)tmpQueueName);
            }

            if (SelectedQueue == null)
            {
                SelectedQueue = QueueList.FirstOrDefault();
            }
        }
    }
}
