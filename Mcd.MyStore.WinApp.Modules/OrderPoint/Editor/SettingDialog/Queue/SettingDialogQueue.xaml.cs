﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Queue
{
    public sealed partial class SettingDialogQueue : UserControl, IPopupDialogContent
    {
        public SettingDialogQueue()
        {
            this.InitializeComponent();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
    }
}
