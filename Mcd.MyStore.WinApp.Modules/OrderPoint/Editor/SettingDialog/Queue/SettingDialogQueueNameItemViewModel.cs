﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Queue
{
    public class SettingDialogQueueNameItemViewModel : ViewModelBase
    {
        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }
        #endregion


        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion
    }
}
