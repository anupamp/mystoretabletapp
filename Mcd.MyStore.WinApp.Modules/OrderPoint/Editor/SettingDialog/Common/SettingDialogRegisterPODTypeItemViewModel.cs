﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common
{
    public class SettingDialogTypeImageViewModel : ViewModelBase
    {
        public string TypeKey { get; set; }

        #region Member - Image
        private BitmapImage _image;
        public BitmapImage Image
        {
            get { return _image; }
            set { SetValue(ref _image, value); }
        }
        #endregion

    }
}
