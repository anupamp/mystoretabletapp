﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common
{
    public class SettingDialogRegisterPOSNumberItemViewModel : ViewModelBase
    {
        #region Member - PosNumber
        private int _posNumber;
        public int PosNumber
        {
            get { return _posNumber; }
            set
            {
                SetValue(ref _posNumber, value);
            }
        }
        #endregion

        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion


    }
}
