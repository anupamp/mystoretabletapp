﻿using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common
{
    public class SettingDialogRegisterViewModel : PopupDialogContentViewModel
    {
        #region Member - ItemViewModel
        private ItemTypeImageButtonRegisterViewModel _itemViewModel;
        public ItemTypeImageButtonRegisterViewModel ItemViewModel
        {
            get { return _itemViewModel; }
            set
            {
                if (SetValue(ref _itemViewModel, value))
                {
                    UpdateSettings();
                }
            }
        }
        #endregion


        public ObservableCollection<SettingDialogTypeImageViewModel> PODTypeList { get; set; }

        #region Member - SelectedPODType
        private SettingDialogTypeImageViewModel _selectedPODType;
        public SettingDialogTypeImageViewModel SelectedPODType
        {
            get { return _selectedPODType; }
            set
            {
                if (value == null)
                    return;

                if (SetValue(ref _selectedPODType, value))
                {
                    ItemViewModel.TypeKey = _selectedPODType.TypeKey;
                }
            }
        }
        #endregion

        public ObservableCollection<SettingDialogRegisterPOSNumberItemViewModel> POSNumberList { get; set; }

        #region Member - SelectedPOSNumber
        private SettingDialogRegisterPOSNumberItemViewModel _selectedPOSNumber;
        public SettingDialogRegisterPOSNumberItemViewModel SelectedPOSNumber
        {
            get { return _selectedPOSNumber; }
            set
            {
                if (value == null)
                    return;

                if (SetValue(ref _selectedPOSNumber, value))
                {
                    ItemViewModel.PosNumber = _selectedPOSNumber.PosNumber;
                }
            }
        }
        #endregion


        public ICommand CloseCommand { get; set; }

        public ICommand TestCommand { get; set; }
        public SettingDialogRegisterViewModel()
        {
            PODTypeList = new ObservableCollection<SettingDialogTypeImageViewModel>();
            POSNumberList = new ObservableCollection<SettingDialogRegisterPOSNumberItemViewModel>();
            AllowDragMove = true;




            CloseCommand = new RelayCommand(() =>
            {
                DialogResult = false;
            });

            TestCommand = new RelayCommand(() =>
            {
                ItemViewModel.TypeKey = "DT";
            });

        }

        private async void UpdateSettings()
        {
            ItemTypeImageButtonRegisterSettingObject settingObject = (ItemViewModel.TemplateDto.SettingDataObject as ItemTypeImageButtonRegisterSettingObject);
            if (settingObject != null)
            {
                foreach (var item in settingObject.ImageHolderList)
                {
                    SettingDialogTypeImageViewModel itemModel = new SettingDialogTypeImageViewModel();
                    itemModel.TypeKey = item.Type;
                    itemModel.Image = await ImageManager.Instance.GetImage(await item.Folder.GetFileAsync(item.ActiveImagePath));

                    PODTypeList.Add(itemModel);
                }

                object tmpPODType = null;
                if (_itemViewModel.UserSettings.TryGetValue("PodTypeKey", out tmpPODType))
                {
                    SelectedPODType = PODTypeList.FirstOrDefault(P => P.TypeKey.Equals((string)tmpPODType, StringComparison.CurrentCultureIgnoreCase));
                }

                if (SelectedPODType == null)
                {
                    SelectedPODType = PODTypeList.FirstOrDefault();
                }


            }

            foreach (var currentPos in SystemStatusManager.Instance.StoreSetupInformation.PosList)
            {
                POSNumberList.Add(new SettingDialogRegisterPOSNumberItemViewModel()
                {
                    PosNumber = currentPos.Number,
                    Title = currentPos.Number.ToString() + " (" + currentPos.PodTypeName + ")"
                });
            }

            object tmpPosNumber = null;
            if (_itemViewModel.UserSettings.TryGetValue("PosNumber", out tmpPosNumber))
            {
                SelectedPOSNumber = POSNumberList.FirstOrDefault(P => P.PosNumber == Convert.ToInt32(tmpPosNumber));
            }

            if (SelectedPOSNumber == null)
            {
                SelectedPOSNumber = POSNumberList.FirstOrDefault();
            }
        }
    }
}
