﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common
{
    public sealed partial class SettingDialogRegister : UserControl, IPopupDialogContent
    {
        public SettingDialogRegister()
        {
            this.InitializeComponent();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
    }
}
