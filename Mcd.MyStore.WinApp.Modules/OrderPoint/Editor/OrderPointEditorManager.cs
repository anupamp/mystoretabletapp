﻿
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor
{
    public class OrderPointEditorManager
    {
        #region Instance

        public static readonly OrderPointEditorManager Instance = new OrderPointEditorManager();

        #endregion

        public UserControl EditorContainer { get; set; }


        #region Member - InsertContainer

        public Grid InsertContainer { get; set; }

        #endregion

        #region Member - InsertContainerPoint
        private Point _insertContainerPoint;
        public Point InsertContainerPoint
        {
            get { return _insertContainerPoint; }
            set { _insertContainerPoint = value; }
        }
        #endregion




        #region Method - UpdateTranslate
        public void UpdateTranslate(object elementObject, Point position, bool setFix = false)
        {
            UpdateTranslate(elementObject, position.X, position.Y, setFix);
        }
        public void UpdateTranslate(object elementObject, double x, double y, bool setFix = false)
        {
            var element = elementObject as UIElement;
            if (element != null)
            {
                CompositeTransform composite = element.RenderTransform as CompositeTransform;
                if (composite == null) return;

                if (setFix)
                {
                    composite.TranslateX = x;
                    composite.TranslateY = y;
                }
                else
                {
                    composite.TranslateX += x;
                    composite.TranslateY += y;
                }
            }
        }

        #endregion

        #region Method - GetPositionInEditor
        public Point GetPositionInEditor(object elementObject)
        {
            var element = elementObject as UIElement;
            return element != null ? GetPositionInEditor(element) : new Point();
        }
        public Point GetPositionInEditor(UIElement element)
        {
            return element.TransformToVisual(EditorContainer).TransformPoint(new Point());
        }
        #endregion


        public OrderPointEditorManager()
        {
            InsertContainer = null;
        }

        public event EventHandler OnTemplateSettingsChanged;
        public OrderPointTemplateList ItemTemplates { get; set; }

        public bool InitIfNeeded()
        {
            if (ItemTemplates == null)
            {
                LoadTemplates();
                return true;
            }
            return false;
        }

        private async void LoadTemplates()
        {
            await Task.Run(() =>
            {
                StorageFolder roamingFolder = ApplicationData.Current.RoamingFolder;
                StorageFolder templateFolder = roamingFolder.CreateFolderAsync("ExternalDataPack\\Data\\OrderPointTemplates", CreationCollisionOption.OpenIfExists).AsTask().Result;

                ItemTemplates = GetTemplatesFromSotrage(templateFolder).Result;

                if (ItemTemplates == null || ItemTemplates.Count == 0)
                {
                    ItemTemplates = GetTemplatesFromSotrage(templateFolder).Result;
                }
            });


            List<string> colors = typeof(Colors).GetRuntimeProperties().Select(c => ((Windows.UI.Color)c.GetValue(null)))
                .Select(c => "#22" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2")).ToList();

            int index = 10;
            foreach (var currentPOD in SystemStatusManager.Instance.StoreSetupInformation.PodList.OrderBy(P => P.ID))
            {
                ItemTemplates.Add(new OrderPointTemplateItem()
                {
                    Category = "Areas",
                    IDKey = "Areas_" + currentPOD.Name,
                    AreaKey = currentPOD.Name,
                    Background = colors[index],
                    ContentTemplate = new OrderPointTemplateItemContent()
                    {
                        Type = OrderPointTemplateItemContentType.Area,
                        OrgWidth = 150,
                        OrgHeight = 150,
                        ContainerWidth = 1400,
                        Resizeable = true,
                        IsFreeResizeMode = true
                    }
                });

                index++;
            }

            index = 35;
            foreach (var currentProduction in SystemStatusManager.Instance.StoreSetupInformation.QueueList.Select(Q => Q.Substring(0, Q.IndexOf(":"))).Distinct().ToList())
            {
                ItemTemplates.Add(new OrderPointTemplateItem()
                {
                    Category = "Areas",
                    IDKey = "Areas_" + currentProduction,
                    AreaKey = currentProduction,
                    Background = colors[index],
                    ContentTemplate = new OrderPointTemplateItemContent()
                    {
                        Type = OrderPointTemplateItemContentType.Area,
                        OrgWidth = 150,
                        OrgHeight = 150,
                        ContainerWidth = 1400,
                        Resizeable = true,
                        IsFreeResizeMode = true
                    }
                });

                index++;
            }



            if (OnTemplateSettingsChanged != null)
            {
                OnTemplateSettingsChanged(this, null);
            }
        }

        private async Task<OrderPointTemplateList> GetTemplatesFromSotrage(StorageFolder storageFolder)
        {
            OrderPointTemplateList result = new OrderPointTemplateList();
            try
            {
                IReadOnlyList<StorageFolder> folders = await storageFolder.GetFoldersAsync();
                foreach (var currentFolder in folders)
                {
                    StorageFile infoFile = await currentFolder.GetFileAsync("Info.json");
                    if (infoFile != null)
                    {
                        string infoText = await FileIO.ReadTextAsync(infoFile);
                        if (!String.IsNullOrEmpty(infoText))
                        {
                            OrderPointTemplateItem templateItem = await Task.Run(() => JsonConvert.DeserializeObject<OrderPointTemplateItem>(infoText));
                            templateItem.IDKey = currentFolder.Name;
                            templateItem.LocalSotrageFolderPath = currentFolder.Path;

                            switch (templateItem.ContentTemplate.Type)
                            {
                                case OrderPointTemplateItemContentType.ImageButtonRegister:
                                    templateItem.SettingDataObject = await ItemTypeImageButtonRegisterSettingObject.CreateFromTemplate(templateItem);
                                    break;

                                case OrderPointTemplateItemContentType.ImageButtonQueue:
                                    templateItem.SettingDataObject = await ItemTypeImageButtonQueueSettingObject.CreateFromTemplate(templateItem);
                                    break;
                            }

                            result.Add(templateItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

            return result;
        }





        public Rect GetTransformBounds(FrameworkElement of, bool relatedToEditor = true)
        {
            GeneralTransform transform;
            if (relatedToEditor)
            {
                transform = of.TransformToVisual(EditorContainer);
            }
            else
            {
                transform = of.TransformToVisual(InsertContainer);
            }

            return transform.TransformBounds(new Rect(0, 0, of.ActualWidth, of.ActualHeight));
        }





        public Point GetPositionInInsertContainer(Point absolutePoint)
        {
            return new Point(absolutePoint.X - _insertContainerPoint.X, absolutePoint.Y - _insertContainerPoint.Y);
        }

        public Point GetValidTranslatePoint(UIElement element, Point translate)
        {
            Point position = GetPositionInEditor(element);
            position.X += translate.X;
            position.Y += translate.Y;


            position.X = Math.Max(position.X, 0);
            position.Y = Math.Max(position.Y, 0);

            Grid grid = (Grid)element;

            if (position.X + grid.ActualWidth > _insertContainerPoint.X + InsertContainer.ActualWidth)
            {
                position.X = _insertContainerPoint.X + InsertContainer.ActualWidth - grid.ActualWidth;
            }
            if (position.Y + grid.ActualHeight > _insertContainerPoint.Y + InsertContainer.ActualHeight)
            {
                position.Y = _insertContainerPoint.Y + InsertContainer.ActualHeight - grid.ActualHeight;
            }
            return position;
        }



        internal Rect GetValidPointForInsertModel(Rect newPosition)
        {
            double left = newPosition.Left;
            double top = newPosition.Top;
            double width = newPosition.Width;
            double height = newPosition.Height;

            left = Math.Max(left, 0);
            top = Math.Max(top, 0);


            if (left + width > InsertContainer.ActualWidth)
            {
                left = InsertContainer.ActualWidth - width;
            }
            if (top + height > InsertContainer.ActualHeight)
            {
                top = InsertContainer.ActualHeight - height;
            }

            return new Rect(left, top, width, height);
        }


        internal OrderPointEditorActiveItemViewModel CreateActiveItemViewModelFromTemplate(OrderPointTemplateItem template, Size? containerSize = null)
        {
            if (template.ContentTemplate == null)
                return null;

            switch (template.ContentTemplate.Type)
            {
                case OrderPointTemplateItemContentType.Area:
                    return new ItemTypeAreaViewModel(template, containerSize);

                case OrderPointTemplateItemContentType.Image:
                    return new ItemTypeImageViewModel(template, containerSize);

                case OrderPointTemplateItemContentType.ImageButtonRegister:
                    return new ItemTypeImageButtonRegisterViewModel(template, containerSize);

                case OrderPointTemplateItemContentType.ImageButtonQueue:
                    return new ItemTypeImageButtonQueueViewModel(template, containerSize);

                default:
                    return new OrderPointEditorActiveItemViewModel(template, containerSize);
            }
        }
    }
}
