﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Common;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.SettingDialog.Queue;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.ViewModel;
using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Editor
{
    [MenuModuleAttribute("OrderPointEditor")]
    public sealed partial class OrderPointEditor : UserControl
    {
        private bool _dragViewIsVisible = false;
        private OrderPointEditorActiveItemViewModel _newInsertModel = null;
        private OrderPointEditorManager _manager = OrderPointEditorManager.Instance;

        private OrderPointEditorViewModel _viewModel = new OrderPointEditorViewModel();
        private readonly PopupDialogManager _settingPopupManager = new PopupDialogManager();

        public OrderPointEditor()
        {
            this.InitializeComponent();
            DataContext = _viewModel;
            _settingPopupManager.Container = SettingPopupContainer;
            Loaded += OrderPointsEditor_Loaded;
            _viewModel.OnSelectedActiveItemChanged += SelectedActiveItemChanged;
        }

        void OrderPointsEditor_Loaded(object sender, RoutedEventArgs e)
        {
            _manager.EditorContainer = EditorContainer;
            _manager.InsertContainer = InsertContainer;
        }

        private void EditorContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _manager.InsertContainerPoint = InsertContainer.TransformToVisual(EditorContainer).TransformPoint(new Point());
            _viewModel.UpdateLayerSize(WrapperInsertContainer.ActualWidth, WrapperInsertContainer.ActualHeight);
        }

        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            _viewModel.SelectedTemplateItem = (OrderPointItemTemplateViewModel)((Grid)sender).DataContext;
            Point validPoint = _manager.GetPositionInEditor(sender);
            _manager.UpdateTranslate(DragGrid, validPoint, true);

            _dragViewIsVisible = true;
            DragGrid.Opacity = 1;

            _viewModel.SelectedActiveItem = null;
            ToolGridVisibilityRelatedToElement(null);
        }

        private void SelectedActiveItemChanged(object sender, EventArgs e)
        {
            if (_viewModel.SelectedActiveItem == null)
            {
                ToolGridVisibilityRelatedToElement(null);
            }
        }



        private void Grid_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            UIElement senderElement = (UIElement)sender;

            _manager.UpdateTranslate(senderElement, e.Delta.Translation.X, e.Delta.Translation.Y);

            Point absolutePosition = _manager.GetPositionInEditor(senderElement);
            Point insertPosition = _manager.GetPositionInInsertContainer(absolutePosition);

            _manager.UpdateTranslate(DragGrid, absolutePosition, true);

            if (_newInsertModel == null)
            {
                if (insertPosition.X >= 0 && _dragViewIsVisible)
                {
                    _dragViewIsVisible = false;
                    if (DragGrid.Opacity > 0)
                    {
                        DragGrid.Animate("Opacity", null, 0, 200);
                    }

                    if (_newInsertModel == null)
                    {
                        _newInsertModel = _viewModel.CreateNewActiveItemViewModel();
                    }
                }
            }

            if (insertPosition.X < 0 && !_dragViewIsVisible)
            {
                _dragViewIsVisible = true;
                if (DragGrid.Opacity < 1)
                {
                    DragGrid.Animate("Opacity", null, 1, 200);
                }

                if (_newInsertModel != null)
                {
                    _viewModel.SelectedLayer.ActiveItems.Remove(_newInsertModel);
                    _newInsertModel = null;
                }
            }

            if (_newInsertModel != null)
            {
                if (_newInsertModel.Control != null && _newInsertModel.Left > 0)
                {
                    _newInsertModel.UpdateWithManipulation(e);
                }
                else
                {
                    double x = insertPosition.X + (DragGrid.ActualWidth / 2) - (_newInsertModel.Width / 2);
                    double y = insertPosition.Y + (DragGrid.ActualHeight / 2) - (_newInsertModel.Height / 2);

                    _newInsertModel.Left = x;
                    _newInsertModel.Top = y;
                }
            }

        }



        private void Grid_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            // reset translate
            _manager.UpdateTranslate(sender, 0, 0, true);

            // deselect insert model
            if (_newInsertModel != null)
            {
                _newInsertModel.UpdateWithManipulation(null);
                _newInsertModel = null;
            }

            // hide DragGrid
            DragGrid.Opacity = 0;
            _dragViewIsVisible = false;
        }

        private void OrderPointsEditorActiveItemControl_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            //if (!_manipulationStarted)
            {
                UserControl control = (UserControl)sender;
                OrderPointEditorActiveItemViewModel viewModel = (OrderPointEditorActiveItemViewModel)control.DataContext;


                //if (_viewModel.SelectedActiveItem != viewModel)
                {
                    _viewModel.SelectedActiveItem = viewModel;
                    ToolGridVisibilityRelatedToElement(control);
                    _viewModel.SelectedLayer.SaveLayerOnService();
                }
                //else
                //{
                //    _viewModel.SelectedActiveItem = null;
                //    ToolGridVisibilityRelatedToElement(null);
                //}
            }
        }

        private void OrderPointsEditorActiveItemControl_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            ToolGridVisibilityRelatedToElement(null);
        }

        private void OrderPointsEditorActiveItemControl_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            //OrderPointsEditorActiveItemControl activeItemControl = (OrderPointsEditorActiveItemControl)sender;
            //OrderPointEditorActiveItemViewModel viewModel = (OrderPointEditorActiveItemViewModel)activeItemControl.DataContext;

            //_viewModel.SelectedActiveItem = viewModel;
            //_viewModel.SelectedLayer.SaveLayerOnService();
            //ToolGridVisibilityRelatedToElement(activeItemControl);
        }



        private void SettingActiveItem_Click(object sender, RoutedEventArgs e)
        {
            if (_viewModel.SelectedActiveItem == null)
                return;

            Type type = _viewModel.SelectedActiveItem.GetType();
            if (type == typeof(ItemTypeImageButtonRegisterViewModel))
            {
                SettingDialogRegisterViewModel viewModel = new SettingDialogRegisterViewModel();
                viewModel.ItemViewModel = (ItemTypeImageButtonRegisterViewModel)_viewModel.SelectedActiveItem;

                PopupDialogData dialogData = PopupDialogManager.CreateDialogData<SettingDialogRegister, SettingDialogRegisterViewModel>(viewModel);
                dialogData.OnDialogClose += (PopupDialogData senderDialogData, bool? dialogResult) =>
                {
                    _viewModel.SelectedLayer.SaveLayerOnService();
                };
                _settingPopupManager.ShowDialog(dialogData);
            }
            if (type == typeof(ItemTypeImageButtonQueueViewModel))
            {
                SettingDialogQueueViewModel viewModel = new SettingDialogQueueViewModel();
                viewModel.ItemViewModel = (ItemTypeImageButtonQueueViewModel)_viewModel.SelectedActiveItem;

                PopupDialogData dialogData = PopupDialogManager.CreateDialogData<SettingDialogQueue, SettingDialogQueueViewModel>(viewModel);
                dialogData.OnDialogClose += (PopupDialogData senderDialogData, bool? dialogResult) =>
                {
                    _viewModel.SelectedLayer.SaveLayerOnService();
                };
                _settingPopupManager.ShowDialog(dialogData);
            }
        }

        private void RemoveActiveItem_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.SelectedLayer.ActiveItems.Remove(_viewModel.SelectedActiveItem);
            ToolGridVisibilityRelatedToElement(null);
            _viewModel.SelectedLayer.SaveLayerOnService();
        }

        private void ResizeActiveItem_Click(object sender, RoutedEventArgs e)
        {

            //_viewModel.ActiveItems.Remove(_viewModel.SelectedActiveItem);
            //ToolGridVisibilityRelatedToElement(null);
        }


        private void ToolGridVisibilityRelatedToElement(FrameworkElement element)
        {
            CompositeTransform transform = (CompositeTransform)ToolGrid.RenderTransform;

            if (element != null)
            {
                Point absolutePosition = _manager.GetPositionInEditor(element);

                Rect rect = _manager.GetTransformBounds(element);

                transform.TranslateX = rect.Left - 35;
                transform.TranslateY = rect.Top - 35;

                ToolGrid.Width = rect.Width + 70;
                ToolGrid.Height = rect.Height + 70;

                ToolGrid.Animate("Opacity", null, 1, 200);

                ToolGridSettingButton.Visibility = (_viewModel.SelectedActiveItem.HasSettings ? Visibility.Visible : Visibility.Collapsed);
                ToolGridResizeButton.Visibility = (_viewModel.SelectedActiveItem.Resizeable ? Visibility.Visible : Visibility.Collapsed);
            }
            else
            {
                ToolGrid.Animate("Opacity", null, 0, 100);
                //await Task.Delay(150);
                //transform.TranslateX = -1000;
                //transform.TranslateY = -1000;
            }
        }

        private void Resize_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!_viewModel.SelectedActiveItem.Resizeable)
                return;

            if (_viewModel.SelectedActiveItem.IsFreeResizeMode)
            {
                double newWidth = _viewModel.SelectedActiveItem.Width + e.Delta.Translation.X;
                double newHeight = _viewModel.SelectedActiveItem.Height + e.Delta.Translation.Y;

                _viewModel.SelectedActiveItem.Width = Math.Max(newWidth, 50);
                _viewModel.SelectedActiveItem.Height = Math.Max(newHeight, 50);
            }
            else
            {
                int orgWidth = _viewModel.SelectedActiveItem.TemplateDto.ContentTemplate.DefaultWidth;
                double newWidth = _viewModel.SelectedActiveItem.Width + e.Delta.Translation.X;
                double ratio = newWidth / (double)orgWidth;

                ratio = Math.Max(Math.Min(ratio, 2), 0.5);


                //Debug.WriteLine(ratio);
                _viewModel.SelectedActiveItem.Width = _viewModel.SelectedActiveItem.TemplateDto.ContentTemplate.DefaultWidth * ratio;
                _viewModel.SelectedActiveItem.Height = _viewModel.SelectedActiveItem.TemplateDto.ContentTemplate.DefaultHeight * ratio;
            }

            ToolGridVisibilityRelatedToElement(_viewModel.SelectedActiveItem.Control);
        }

        private void Resize_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            _viewModel.SelectedLayer.SaveLayerOnService();
        }


    }
}
