﻿using System.Collections.Generic;
using Windows.Storage;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject
{
    public class ItemTypeImageButtonSettingObjectBase
    {
        public List<ImageHolder> ImageHolderList { get; set; }

        public ItemTypeImageButtonSettingObjectBase()
        {
            ImageHolderList = new List<ImageHolder>();
        }

        public class ImageHolder
        {
            public StorageFolder Folder { get; set; }
            public string Type { get; set; }
            public string NormalImagePath { get; set; }
            public string ActiveImagePath { get; set; }
        }
    }
}
