﻿using System;
using System.Threading.Tasks;
using Windows.Storage;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject
{
    public class ItemTypeImageButtonRegisterSettingObject : ItemTypeImageButtonSettingObjectBase
    {
        public static async Task<ItemTypeImageButtonRegisterSettingObject> CreateFromTemplate(OrderPointTemplateItem template)
        {
            ItemTypeImageButtonRegisterSettingObject result = new ItemTypeImageButtonRegisterSettingObject();

            dynamic data = null;
            StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(template.LocalSotrageFolderPath);
            if (template.SettingData.TryGetValue("PODTypes", out data))
            {
                foreach (var settingItem in data)
                {
                    try
                    {
                        ItemTypeImageButtonSettingObjectBase.ImageHolder newHolder = new ItemTypeImageButtonSettingObjectBase.ImageHolder();
                        newHolder.Type = settingItem.Type;
                        newHolder.NormalImagePath = settingItem.NormalImage;
                        newHolder.ActiveImagePath = settingItem.ActiveImage;
                        newHolder.Folder = folder;
                        result.ImageHolderList.Add(newHolder);
                    }
                    catch
                    {
                        // ignore corrupt configuration 
                    }
                }
            }

            return result;
        }


    }
}
