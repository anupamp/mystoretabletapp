﻿using System;
using System.Threading.Tasks;
using Windows.Storage;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject
{
    public class ItemTypeImageButtonQueueSettingObject : ItemTypeImageButtonSettingObjectBase
    {
        public static async Task<ItemTypeImageButtonQueueSettingObject> CreateFromTemplate(OrderPointTemplateItem template)
        {
            ItemTypeImageButtonQueueSettingObject result = new ItemTypeImageButtonQueueSettingObject();

            dynamic data = null;
            StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(template.LocalSotrageFolderPath);
            if (template.SettingData.TryGetValue("QueueTypes", out data))
            {
                foreach (var settingItem in data)
                {
                    try
                    {
                        ItemTypeImageButtonSettingObjectBase.ImageHolder newHolder = new ItemTypeImageButtonSettingObjectBase.ImageHolder();
                        newHolder.Type = settingItem.Type;
                        newHolder.NormalImagePath = settingItem.NormalImage;
                        newHolder.ActiveImagePath = settingItem.ActiveImage;
                        newHolder.Folder = folder;
                        result.ImageHolderList.Add(newHolder);
                    }
                    catch
                    {
                        // ignore corrupt configuration 
                    }
                }
            }

            return result;
        }
    }
}
