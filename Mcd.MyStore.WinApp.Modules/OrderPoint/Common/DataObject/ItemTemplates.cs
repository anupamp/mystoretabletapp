﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject
{
    public class OrderPointTemplateList : List<OrderPointTemplateItem>
    {
    }

    [DataContract]
    public class OrderPointTemplateItem
    {
        public string LocalSotrageFolderPath { get; set; }

        [DataMember]
        public string IDKey { get; set; }

        [DataMember]
        public string Icon { get; set; }

        public string AreaKey { get; set; }

        [DataMember]
        public string Background { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember(Name = "Template")]
        public OrderPointTemplateItemContent ContentTemplate { get; set; }

        [DataMember]
        public Dictionary<string, object> SettingData { get; set; }

        public object SettingDataObject { get; set; }
    }

    [DataContract]
    public class OrderPointTemplateItemContent
    {
        [DataMember]
        public OrderPointTemplateItemContentType Type { get; set; }

        [DataMember]
        public int Left { get; set; }

        [DataMember]
        public int Top { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public int DefaultWidth { get; set; }
        public int DefaultHeight { get; set; }

        [DataMember(Name = "Width")]
        public int OrgWidth { get; set; }

        [DataMember(Name = "Height")]
        public int OrgHeight { get; set; }

        [DataMember]
        public int ContainerWidth { get; set; }

        [DataMember]
        public int ContainerHeight { get; set; }

        [DataMember]
        public bool Resizeable { get; set; }

        [DataMember]
        public bool IsFreeResizeMode { get; set; }

        [DataMember]
        public bool IsBackgroundContent { get; set; }

        [DataMember]
        public double Rotation { get; set; }

        [DataMember]
        public Dictionary<string, string> Data { get; set; }

        [DataMember]
        public List<OrderPointTemplateItemContent> Childs { get; set; }

        public double SizeRatio { get; set; }

        public void RecalculateSizeRatio(double currentContainerWidth, double currentContainerHeight)
        {
            double widthRatio = (ContainerWidth > 1 ? currentContainerWidth / (double)ContainerWidth : 100);
            double heightRatio = (ContainerHeight > 1 ? currentContainerHeight / (double)ContainerHeight : 100);

            double currentRatio = Math.Min(widthRatio, heightRatio);

            SizeRatio = (currentRatio < 50 ? currentRatio : 1);

            DefaultWidth = (int)Math.Round(OrgWidth * SizeRatio);
            DefaultHeight = (int)Math.Round(OrgHeight * SizeRatio);

            Width = DefaultWidth;
            Height = DefaultHeight;
        }
    }

    [DataContract]
    public enum OrderPointTemplateItemContentType
    {
        Area,
        Image,
        ImageButtonRegister,
        ImageButtonQueue
    }
}
