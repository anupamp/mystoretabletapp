﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel
{
    public class OrderPointItemTemplateViewModel : ViewModelBase
    {
        #region Member - BackgroundColor
        private SolidColorBrush _backgroundColor = new SolidColorBrush(Colors.Transparent);
        public SolidColorBrush BackgroundColor
        {
            get { return _backgroundColor; }
            set { SetValue(ref _backgroundColor, value); }
        }
        #endregion

        #region Member - TemplateImage
        private BitmapImage _templateImage;
        public BitmapImage TemplateImage
        {
            get { return _templateImage; }
            set { SetValue(ref _templateImage, value); }
        }
        #endregion

        #region Member - TemplateText
        private string _templateText;
        public string TemplateText
        {
            get { return _templateText; }
            set { SetValue(ref _templateText, value); }
        }
        #endregion


        #region Member - TemplateDto
        private OrderPointTemplateItem _templateDto;
        public OrderPointTemplateItem TemplateDto
        {
            get { return _templateDto; }
            set { SetValue(ref _templateDto, value); }
        }
        #endregion



        public OrderPointItemTemplateViewModel()
        {
        }

        public OrderPointItemTemplateViewModel(OrderPointTemplateItem currentTemplate)
        {
            _templateDto = currentTemplate;
        }

    }
}
