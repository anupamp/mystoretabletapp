﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Editor.Controls;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel
{
    public class OrderPointEditorActiveItemViewModel : ViewModelBase
    {
        public OrderPointsEditorActiveItemControl Control { get; set; }

        #region Member - TemplateDto
        protected OrderPointTemplateItem _templateDto;
        public OrderPointTemplateItem TemplateDto
        {
            get { return _templateDto; }
            set { SetValue(ref _templateDto, value); }
        }
        #endregion

        #region Member - Left
        private double _left;
        public double Left
        {
            get { return _left; }
            set { SetValue(ref _left, value); }
        }
        #endregion

        #region Member - Top
        private double _top;
        public double Top
        {
            get { return _top; }
            set { SetValue(ref _top, value); }
        }
        #endregion

        #region Member - Width
        private double _width = 10;
        public double Width
        {
            get { return _width; }
            set { SetValue(ref _width, value); }
        }
        #endregion

        #region Member - Height
        private double _height = 10;
        public double Height
        {
            get { return _height; }
            set { SetValue(ref _height, value); }
        }
        #endregion

        #region Member - Resizeable
        private bool _resizeable;
        public bool Resizeable
        {
            get { return _resizeable; }
            set { SetValue(ref _resizeable, value); }
        }
        #endregion

        #region Member - IsFreeResizeMode
        private bool _isFreeResizeMode;
        public bool IsFreeResizeMode
        {
            get { return _isFreeResizeMode; }
            set { SetValue(ref _isFreeResizeMode, value); }
        }
        #endregion


        #region Member - Rotation
        private double _rotation;
        public double Rotation
        {
            get { return _rotation; }
            set { SetValue(ref _rotation, value); }
        }
        #endregion

        #region Member - HasSettings
        private bool _hasSettings;
        public bool HasSettings
        {
            get { return _hasSettings; }
            set { SetValue(ref _hasSettings, value); }
        }
        #endregion

        #region Member - IsVisible
        private bool _isVisible = true;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                SetValue(ref _isVisible, value);
                Notify("IsHitTestVisible");
            }
        }
        #endregion

        #region Member - IsHitTestVisible
        private bool _isHitTestVisible = true;
        public bool IsHitTestVisible
        {
            get { return _isHitTestVisible && _isVisible; }
            set { SetValue(ref _isHitTestVisible, value); }
        }
        #endregion

        #region Member - UserSettings
        protected Dictionary<string, object> _userSettings = new Dictionary<string, object>();
        public Dictionary<string, object> UserSettings
        {
            get { return _userSettings; }
            set
            {
                if (value != null)
                {
                    _userSettings = value;
                    ApplyUserSettings();
                }
            }
        }
        #endregion


        public bool IsReportMode { get; set; }

        public OrderPointEditorActiveItemViewModel(OrderPointTemplateItem templateDto, Size? containerSize = null)
        {
            _templateDto = templateDto;

            if (containerSize.HasValue)
            {
                templateDto.ContentTemplate.RecalculateSizeRatio(containerSize.Value.Width, containerSize.Value.Height);
            }
            else
            {
                templateDto.ContentTemplate.RecalculateSizeRatio(OrderPointEditorManager.Instance.InsertContainer.ActualWidth, OrderPointEditorManager.Instance.InsertContainer.ActualHeight);
            }

            Width = templateDto.ContentTemplate.Width;
            Height = templateDto.ContentTemplate.Height;
            Resizeable = templateDto.ContentTemplate.Resizeable;
            IsFreeResizeMode = templateDto.ContentTemplate.IsFreeResizeMode;
            Rotation = templateDto.ContentTemplate.Rotation;
        }

        public void UpdateWithManipulation(ManipulationDeltaRoutedEventArgs e)
        {
            Rect currentPosition = OrderPointEditorManager.Instance.GetTransformBounds(Control, false);

            double leftDiff = Left - currentPosition.Left;
            double topDiff = Top - currentPosition.Top;

            Rect newPosition = currentPosition;
            if (e != null)
            {
                newPosition = new Rect(currentPosition.Left + e.Delta.Translation.X, currentPosition.Top + e.Delta.Translation.Y, currentPosition.Width, currentPosition.Height);
            }

            newPosition = OrderPointEditorManager.Instance.GetValidPointForInsertModel(newPosition);

            Left = newPosition.X + leftDiff;
            Top = newPosition.Y + topDiff;
        }

        public virtual void ApplyUserSettings() { }

    }
}
