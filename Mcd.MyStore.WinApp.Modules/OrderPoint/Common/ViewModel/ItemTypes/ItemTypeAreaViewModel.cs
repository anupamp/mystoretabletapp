﻿
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes
{
    public class ItemTypeAreaViewModel : OrderPointEditorActiveItemViewModel
    {
        #region Member - BackgroundColor
        private SolidColorBrush _backgroundColor;
        public SolidColorBrush BackgroundColor
        {
            get { return _backgroundColor; }
            set { SetValue(ref _backgroundColor, value); }
        }
        #endregion

        #region Member - AreaName
        private string _areaName;
        public string AreaName
        {
            get { return _areaName; }
            set { SetValue(ref _areaName, value); }
        }
        #endregion

        #region Member - BorderWidth
        private Thickness _borderWidth = new Thickness(1);
        public Thickness BorderWidth
        {
            get { return _borderWidth; }
            set { SetValue(ref _borderWidth, value); }
        }
        #endregion


        public ItemTypeAreaViewModel(OrderPointTemplateItem templateDto, Size? containerSize = null)
            : base(templateDto, containerSize)
        {
            BackgroundColor = SolidColorBrushExtensions.ConvertFromHex(templateDto.Background);
            AreaName = templateDto.AreaKey;
            IsVisible = false;
        }
    }
}
