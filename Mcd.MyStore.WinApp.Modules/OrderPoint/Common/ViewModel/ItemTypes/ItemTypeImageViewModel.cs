﻿using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using System;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes
{
    public class ItemTypeImageViewModel : OrderPointEditorActiveItemViewModel
    {
        private BitmapImage _normalImage = null;

        #region Member - CurrentImage
        public BitmapImage CurrentImage
        {
            get { return _normalImage; }
        }
        #endregion

        public ItemTypeImageViewModel(OrderPointTemplateItem templateDto, Size? containerSize = null)
            : base(templateDto, containerSize)
        {
            InitModel();
        }

        private async void InitModel()
        {
            StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(_templateDto.LocalSotrageFolderPath);

            string normalImageName = null;
            if (_templateDto.ContentTemplate.Data.TryGetValue("ImageNormal", out normalImageName))
            {
                _normalImage = await ImageManager.Instance.GetImage(await folder.GetFileAsync(normalImageName));
            }

            Notify("CurrentImage");
        }
    }
}
