﻿using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus.DataObject;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using System;
using System.Linq;
using Windows.Foundation;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes
{
    public class ItemTypeImageButtonRegisterViewModel : ItemTypeImageButtonViewModelBase
    {
        #region Member - PosNumber
        private int _posNumber;
        public int PosNumber
        {
            get { return _posNumber; }
            set
            {
                if (SetValue(ref _posNumber, value))
                {
                    UpdateUserSettings();
                }
            }
        }
        #endregion


        public ItemTypeImageButtonRegisterViewModel(OrderPointTemplateItem templateDto, Size? containerSize = null)
            : base(templateDto, containerSize)
        {
            StoreSetupPOSInfo posInfo = SystemStatusManager.Instance.StoreSetupInformation.PosList.FirstOrDefault();
            if (posInfo != null)
            {
                PosNumber = posInfo.Number;
            }
        }

        protected override void UpdateUserSettings()
        {
            UserSettings["PodTypeKey"] = TypeKey;
            UserSettings["PosNumber"] = PosNumber;
        }

        public override void ApplyUserSettings()
        {
            if (UserSettings != null)
            {
                object tmpTypeKey = null;
                if (UserSettings.TryGetValue("PodTypeKey", out tmpTypeKey))
                {
                    _typeKey = (string)tmpTypeKey;
                    ChangeImages();
                }

                object tmpPosNumber = null;
                if (UserSettings.TryGetValue("PosNumber", out tmpPosNumber))
                {
                    _posNumber = Convert.ToInt32(tmpPosNumber);
                }
            }
        }
    }
}
