﻿using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using System;
using System.Linq;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes
{
    public class ItemTypeImageButtonViewModelBase : OrderPointEditorActiveItemViewModel
    {
        private BitmapImage _normalImage = null;
        private BitmapImage _activeImage = null;

        #region Member - IsActive
        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (SetValue(ref _isActive, value))
                {
                    Notify("CurrentImage");
                }
            }
        }
        #endregion


        #region Member - CurrentImage
        public BitmapImage CurrentImage
        {
            get { return (_isActive ? _activeImage : _normalImage); }
        }
        #endregion

        #region Member - TypeKey
        protected string _typeKey = null;
        public string TypeKey
        {
            get { return _typeKey; }
            set
            {
                if (SetValue(ref _typeKey, value))
                {
                    ChangeImages();
                    UpdateUserSettings();
                }
            }
        }
        #endregion

        public ItemTypeImageButtonViewModelBase(OrderPointTemplateItem templateDto, Size? containerSize = null)
            : base(templateDto, containerSize)
        {
            InitModel();
        }

        private async void InitModel()
        {
            StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(_templateDto.LocalSotrageFolderPath);
            ItemTypeImageButtonSettingObjectBase settingObject = (_templateDto.SettingDataObject as ItemTypeImageButtonSettingObjectBase);
            if (settingObject != null && settingObject.ImageHolderList.Count > 0)
            {
                if (String.IsNullOrEmpty(TypeKey))
                {
                    TypeKey = settingObject.ImageHolderList.FirstOrDefault().Type;
                }
                HasSettings = true;
            }
        }

        protected async void ChangeImages()
        {
            ItemTypeImageButtonSettingObjectBase settingObject = (_templateDto.SettingDataObject as ItemTypeImageButtonSettingObjectBase);
            if (settingObject != null)
            {
                var imageHolder = settingObject.ImageHolderList.FirstOrDefault(I => I.Type.Equals(_typeKey, StringComparison.CurrentCultureIgnoreCase));
                if (imageHolder == null)
                    return;

                _normalImage = await ImageManager.Instance.GetImage(await imageHolder.Folder.GetFileAsync(imageHolder.NormalImagePath));
                _activeImage = await ImageManager.Instance.GetImage(await imageHolder.Folder.GetFileAsync(imageHolder.ActiveImagePath));

                Notify("CurrentImage");
            }
        }

        protected virtual void UpdateUserSettings()
        {
        }
    }
}
