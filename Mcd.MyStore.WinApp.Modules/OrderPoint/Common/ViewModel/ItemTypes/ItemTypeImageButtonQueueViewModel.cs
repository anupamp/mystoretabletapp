﻿using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Modules.OrderPoint.Common.DataObject;
using System.Linq;
using Windows.Foundation;

namespace Mcd.MyStore.WinApp.Modules.OrderPoint.Common.ViewModel.ItemTypes
{
    public class ItemTypeImageButtonQueueViewModel : ItemTypeImageButtonViewModelBase
    {
        #region Member - QueueName
        private string _queueName;
        public string QueueName
        {
            get { return _queueName; }
            set
            {
                if (SetValue(ref _queueName, value))
                {
                    UpdateUserSettings();
                }
            }
        }
        #endregion

        public ItemTypeImageButtonQueueViewModel(OrderPointTemplateItem templateDto, Size? containerSize = null)
            : base(templateDto, containerSize)
        {
            QueueName = SystemStatusManager.Instance.StoreSetupInformation.QueueList.FirstOrDefault();
        }

        protected override void UpdateUserSettings()
        {
            UserSettings["QueueImageTypeKey"] = TypeKey;
            UserSettings["QueueName"] = QueueName;
        }

        public override void ApplyUserSettings()
        {
            if (UserSettings != null)
            {
                object tmpTypeKey = null;
                if (UserSettings.TryGetValue("QueueImageTypeKey", out tmpTypeKey))
                {
                    _typeKey = (string)tmpTypeKey;
                    ChangeImages();
                }

                object tmpQueueName = null;
                if (UserSettings.TryGetValue("QueueName", out tmpQueueName))
                {
                    _queueName = (string)tmpQueueName;
                }
            }
        }
    }
}
