﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.GuestCountVsProduction.ViewModel;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.GuestCountVsProduction
{
    [MenuModuleAttribute("GuestCountVsProduction")]
    public sealed partial class GuestCountVsProductionControl : UserControl
    {
        public GuestCountVsProductionControl()
        {
            this.InitializeComponent();
            DataContext = new GuestCountVsProductionControlViewModel();
        }
    }
}
