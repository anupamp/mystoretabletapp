﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.SpeedOfService.Common.DataContracts;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.GuestCountVsProduction.ViewModel
{
    public class GuestCountVsProductionControlViewModel : ModuleViewModelBase
    {
        public ObservableCollection<ProductChartItemViewModel> ProductionDataItems { get; set; }
        public ObservableCollection<ProductChartItemViewModel> GuestCountDataItems { get; set; }

        #region Member LegendName1
        private string _legendName1;
        public string LegendName1
        {
            get { return _legendName1; }
            set { SetValue<string>(ref _legendName1, value); }
        }
        #endregion
        #region Member LegendName2
        private string _legendName2;
        public string LegendName2
        {
            get { return _legendName2; }
            set { SetValue<string>(ref _legendName2, value); }
        }
        #endregion

        public GuestCountVsProductionControlViewModel()
        {
            IsAutoReloadEnabled = true;
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            ProductionDataItems = new ObservableCollection<ProductChartItemViewModel>();
            GuestCountDataItems = new ObservableCollection<ProductChartItemViewModel>();
            LoadEmptyInitData();
        }

        private async void LoadEmptyInitData()
        {
            LegendName1 = "DRKDES";
            LegendName2 = "MFY";

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            SystemStatusForBusinessDayDto statusDto = await SystemStatusManager.Instance.GetOpeningAndClosingTimeForBusinessDayTask(selectedDate);
            if (statusDto == null)
                return;

            for (DateTime i = statusDto.OpeningTime; i < statusDto.ClosingTime; i = i.AddHours(1))
            {

                #region Create empty Chart - ProductionDataItems

                ProductChartItemViewModel newItem = new ProductChartItemViewModel()
                {
                    ID = i,
                    Title = i.ToString("HH")
                };
                newItem.AddDataValue(new ProductChartDataItemViewModel()
                {
                    Value = 0,
                    FillColor = (SolidColorBrush)Application.Current.Resources["GuestCountColor1Brush"]
                });
                newItem.AddDataValue(new ProductChartDataItemViewModel()
                {
                    Value = 0,
                    FillColor = (SolidColorBrush)Application.Current.Resources["GuestCountColor2Brush"]
                });

                ProductionDataItems.Add(newItem);
                #endregion

                #region Create empty Chart - GuestCountDataItems
                ProductChartItemViewModel newItem2 = new ProductChartItemViewModel()
                {
                    ID = i,
                    Title = i.ToString("HH")
                };
                newItem2.AddDataValue(new ProductChartDataItemViewModel()
                {
                    Value = 0,
                    FillColor = (SolidColorBrush)Application.Current.Resources["CrewHoursColor1Brush"]
                });

                GuestCountDataItems.Add(newItem2);
                #endregion

            }
        }

        public void LoadData()
        {
            LoadAsyncData<WWCBeverageWidgetData>(LoadDataAsync, LoadDataAsyncFinished);
        }

        private WWCBeverageWidgetData LoadDataAsync()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return null;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                var result = service.GetBeverageWidgetData(selectedDate, AppReportPODEnum.All);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }


        public void LoadDataAsyncFinished(WWCBeverageWidgetData result)
        {
            if (result == null)
                return;

            if (result.DrinkSegments == null)
                return;

            LoadGuestCountData();

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            foreach (DRKDESLineTimingSegment item in result.DrinkSegments)
            {
                int productionLine1 = item.SandwichCountsPerDRKDESLine.Sum(L => L.ItemCount);

                var currentItem = ProductionDataItems.SingleOrDefault(I => (DateTime)I.ID == item.Start);
                if (currentItem != null)
                {
                    currentItem.DataValues[0].Value = productionLine1;
                }
            }

            foreach (MfyLineTimingSegment item in result.MFYSegments)
            {
                Dictionary<PointOfDistribution, int> values = (Dictionary<PointOfDistribution, int>)item.SandwichCountsPerMfyLine;

                int productionLine2 = values.Sum(V => V.Value);

                var currentItem = ProductionDataItems.SingleOrDefault(I => (DateTime)I.ID == item.Start);
                if (currentItem != null)
                {
                    currentItem.DataValues[1].Value = productionLine2;
                }
            }

            ModuleManager.Instance.RestartReloadTimer();
        }


        public void LoadGuestCountData()
        {
            LoadAsyncData<HourlySalesWidgetData>(LoadGuestCountDataAsync, LoadGuestCountDataAsyncFinished);
        }

        private HourlySalesWidgetData LoadGuestCountDataAsync()
        {
            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                var result = service.GetProjectedVsActualWWC(selectedDate);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadGuestCountDataAsyncFinished(HourlySalesWidgetData result)
        {
            if (result == null)
                return;

            if (result.HourlyMetrics == null)
                return;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            foreach (var currentHourlyItem in result.HourlyMetrics)
            {
                //if (currentHour < SystemStatusManager.Instance.BeginHourForCurrentBusinessDay || currentHour > SystemStatusManager.Instance.EndHourForCurrentBusinessDay)
                //{
                //    continue;
                //}

                int guestCount = currentHourlyItem.Value.ActualGuests;
                var currentItem = GuestCountDataItems.SingleOrDefault(I => (DateTime)I.ID == currentHourlyItem.Key);
                if (currentItem != null)
                {
                    currentItem.DataValues[0].Value = guestCount;
                }

            }
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadData();
            }
        }

        public override void TriggerDataLoading()
        {
            LoadData();
        }
    }
}
