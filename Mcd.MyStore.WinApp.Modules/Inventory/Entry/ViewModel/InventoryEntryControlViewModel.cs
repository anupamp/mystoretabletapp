﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.InventoryService.Common.DataContracts;
using Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEngine;
using Mcd.MyStore.InventoryService.Common.DataContracts.InventoryEntry;
using Mcd.MyStore.InventoryService.Common.Definitions;
using Mcd.MyStore.InventoryService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Hardware;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.Inventory.Entry.DataObject;
using Mcd.MyStore.WinApp.Modules.Inventory.Manager;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class InventoryEntryControlViewModel : ModuleViewModelBase
    {
        public override string TranslationPrefix
        {
            get
            {
                return "InventoryEntry_";
            }
        }

        public ObservableCollection<ListItemViewModel> ListItems { get; set; }
        public ObservableCollection<SelectedItemLocationViewModel> SelectedGroupedItems { get; set; }
        public ObservableCollection<WarehouseItemViewModel> WarehouseItemList { get; set; }
        //public ObservableCollection<PeriodItemViewModel> PeriodItemList { get; set; }

        #region Member - SelectedListItem
        private ListItemViewModel _selectedListItem;
        public ListItemViewModel SelectedListItem
        {
            get { return _selectedListItem; }
            set
            {
                if (_selectedListItem != null)
                {
                    _selectedListItem.IsActive = false;
                }

                if (value != null && value.IsGroupHeader)
                {
                    if (value.IsGroupOpen)
                    {
                        value.IsGroupOpen = false;

                        var items = ListItems.Where(I => !I.IsGroupHeader && I.McDPOSRefIDAI == value.McDPOSRefIDAI).ToList();
                        foreach (var item in items)
                        {
                            item.IsEnabled = false;
                        }

                        _selectedListItem = null;
                        Notify("SelectedListItem");
                    }
                    else
                    {
                        value.IsGroupOpen = true;

                        var items = ListItems.Where(I => !I.IsGroupHeader && I.McDPOSRefIDAI == value.McDPOSRefIDAI).ToList();
                        foreach (var item in items)
                        {
                            item.IsEnabled = true;
                        }

                        _selectedListItem = ListItems.FirstOrDefault(I => I.IsEnabled && !I.IsGroupHeader && I.McDPOSRefIDAI == value.McDPOSRefIDAI);
                        Notify("SelectedListItem");
                    }
                }
                else
                {
                    SetValue(ref _selectedListItem, value);
                }

                if (_selectedListItem != null)
                {
                    _selectedListItem.IsActive = true;

                    RunOnUI(async () =>
                    {
                        await Task.Delay(10);
                        InventoryItemList.ScrollIntoView(_selectedListItem);
                    });

                    UpdateSelectedInput();
                }
                Notify("IsInputRowActive");
            }
        }
        #endregion

        #region Member - SelectedWarehouseItem
        private WarehouseItemViewModel _selectedWarehouseItem;
        public WarehouseItemViewModel SelectedWarehouseItem
        {
            get { return _selectedWarehouseItem; }
            set
            {
                if (SetValue(ref _selectedWarehouseItem, value))
                {
                    IsOverviewModeActive = (_selectedWarehouseItem == WarehouseItemList.FirstOrDefault());

                    _currentPage = 1;
                    Notify("CurrentPage");

                    int refID = -1;
                    int refIDAI = -1;
                    if (_selectedListItem != null)
                    {
                        refID = _selectedListItem.McDPOSRefID;
                        refIDAI = _selectedListItem.McDPOSRefIDAI;
                    }

                    _lastAllFilteredItems = GenerateFilterCountingListItems();

                    if (refID < 0 || !SelectItemInList(refID))
                    {
                        UpdateListItems(() =>
                        {

                            SelectedListItem = ListItems.FirstOrDefault(I => I.IsEnabled && !I.IsGroupHeader);
                        });
                    }
                }
            }
        }
        #endregion

        #region Member - IsShowOnlyUncountedItemsChecked
        private bool _isShowOnlyUncountedItemsChecked;
        public bool IsShowOnlyUncountedItemsChecked
        {
            get { return _isShowOnlyUncountedItemsChecked; }
            set
            {
                if (SetValue(ref _isShowOnlyUncountedItemsChecked, value))
                {
                    UpdateListItems();
                }
            }
        }
        #endregion

        #region Member - ItemHeight
        private int _itemHeight = 43;
        public int ItemHeight
        {
            get { return _itemHeight; }
            set { SetValue(ref _itemHeight, value); }
        }
        #endregion

        #region Member - ItemListCount
        private int _itemListCount = 15;
        public int ItemListCount
        {
            get { return _itemListCount; }
            set
            {
                if (SetValue(ref _itemListCount, value))
                {
                    RecreateViewModels();
                    UpdateListItems();
                }
            }
        }
        #endregion

        #region Member - SelectedItemInput
        private SelectedItemInputViewModel _selectedItemInput = new SelectedItemInputViewModel();
        public SelectedItemInputViewModel SelectedItemInput
        {
            get { return _selectedItemInput; }
            set
            {
                SetValue(ref _selectedItemInput, value);
            }
        }
        #endregion

        #region Member - CurrentPage
        private int _currentPage = 1;
        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                if (SetValue(ref _currentPage, value))
                {
                    UpdateListItems(() =>
                    {
                        SelectedListItem = ListItems.FirstOrDefault();
                    });
                }
            }
        }
        #endregion

        #region Member - MaxPage
        private int _maxPage;
        public int MaxPage
        {
            get { return _maxPage; }
            set { SetValue(ref _maxPage, value); }
        }
        #endregion

        #region Member - LoadingDate
        private DateTime _loadingDate;
        public DateTime LoadingDate
        {
            get { return _loadingDate; }
            set { SetValue(ref _loadingDate, value); }
        }
        #endregion

        public string _usedCountingLists;

        public string UsedCountingLists
        {
            get
            {
                return _usedCountingLists;
            }
            set
            {
                SetValue(ref _usedCountingLists, value);
            }
        }

        #region Member - IsPseudoAssemblyItemMode
        private bool _isPseudoAssemblyItemMode;
        public bool IsPseudoAssemblyItemMode
        {
            get { return _isPseudoAssemblyItemMode; }
            set { SetValue(ref _isPseudoAssemblyItemMode, value, "IsPseudoAssemblyItemMode"); }
        }
        #endregion

        #region Member - IsSelectionModeActive
        private bool _isSelectionModeActive;
        public bool IsSelectionModeActive
        {
            get { return _isSelectionModeActive; }
            set
            {
                if (SetValue(ref _isSelectionModeActive, value))
                {
                    Notify("DisabledOpacityForSelectionModeActive");
                }
            }
        }
        #endregion

        #region Member - DisabledOpacityForSelectionModeActive
        public double DisabledOpacityForSelectionModeActive
        {
            get { return (_isSelectionModeActive ? 0.5 : 1); }
        }
        #endregion

        #region Member - IsInputRowActive
        public bool IsInputRowActive
        {
            get { return (_selectedListItem != null); }
        }
        #endregion

        #region Member - IsOverviewModeActive
        private bool _isOverviewModeActive = true;
        public bool IsOverviewModeActive
        {
            get { return _isOverviewModeActive; }
            set
            {
                if (SetValue(ref _isOverviewModeActive, value))
                {
                    AreExpectedValuesVisible = _isOverviewModeActive;
                }
            }
        }
        #endregion

        #region Member - AreExpectedValuesVisible
        private bool _areExpectedValuesVisible = true;
        public bool AreExpectedValuesVisible
        {
            get { return _areExpectedValuesVisible; }
            set
            {
                if (SetValue(ref _areExpectedValuesVisible, value))
                {
                    Notify("InputRowWidth");
                    Notify("DecimalKeyboardHeight");
                }
            }
        }
        #endregion

        #region Member - InputRowWidth
        private GridLength _inputRowWidthNormal = new GridLength(6, GridUnitType.Star);
        private GridLength _inputRowWidthExtend = new GridLength(8, GridUnitType.Star);
        public GridLength InputRowWidth
        {
            get
            {
                return (_areExpectedValuesVisible ? _inputRowWidthNormal : _inputRowWidthExtend);
            }
        }
        #endregion

        #region Member - DecimalKeyboardHeight
        private GridLength _decimalKeyboardHeightNormal = new GridLength(4, GridUnitType.Star);
        private GridLength _decimalKeyboardHeightExtend = new GridLength(6, GridUnitType.Star);
        public GridLength DecimalKeyboardHeight
        {
            get
            {
                return (_areExpectedValuesVisible ? _decimalKeyboardHeightNormal : _decimalKeyboardHeightExtend);
            }
        }
        #endregion

        public ICommand PagingCommand { get; set; }
        public ICommand SelectPrevItemCommand { get; set; }
        public ICommand SelectNextItemCommand { get; set; }
        public ICommand SelectionModeCommand { get; set; }
        public ICommand OpenWarehouseWithItem { get; set; }

        public ICommand ListItemTappedCommand { get; set; }

        public decimal RAGLevelWarning { get; set; }
        public decimal RAGLevelCritical { get; set; }

        public ListView CountingList { get; set; }
        public ListView InventoryItemList { get; set; }

        private CountingListContainerDto _lastResponse;
        private InventoryEntrySettingsDto _lastSettings;
        private readonly CoreDispatcher _dispatcher;
        private readonly InventoryRetryManager _retryManager;
        private List<FilterListItemObject> _lastAllFilteredItems;

        private InventoryStatusDto _requestedInventory;

        private DispatcherTimer _nextUpdateTimer = new DispatcherTimer();

        public InventoryEntryControlViewModel()
        {
            ListItems = new ObservableCollection<ListItemViewModel>();
            SelectedGroupedItems = new ObservableCollection<SelectedItemLocationViewModel>();
            WarehouseItemList = new ObservableCollection<WarehouseItemViewModel>();
            _selectedItemInput.InventryMainViewModel = this;
            _selectedItemInput.OnItemIsCounted += ItemIsCountedEvent;

            ConfigValueUpdater();

            PagingCommand = new RelayCommand(PagingCommandExecuted);
            BarcodeBuffer.Instance.Receive += OnReceiveCharacters;
            _dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;

            SelectNextItemCommand = new RelayCommand(SelectNextItemInList);
            SelectPrevItemCommand = new RelayCommand(SelectPrevItemInList);

            SelectionModeCommand = new RelayCommand(SelectionModeClick);

            OpenWarehouseWithItem = new RelayCommand(OpenWarehouseWithItemClick);

            ListItemTappedCommand = new RelayCommand(ListItemTappedCommandClick);

            _retryManager = InventoryRetryManager.Instance;
            _retryManager.OnItemRemovedFromCache += RetryManagerOnItemRemovedFromCache;

            RecreateViewModels();

            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            SubscribeNotification(NotificationApplicationType.BusinessDayIsChanged);
            SubscribeNotification(NotificationApplicationType.OpenClose_ControlCenter_RequestBusinessDayUpdate);
        }

        #region Command methods
        private object ListItemTappedCommandClick(object parameter)
        {
            SelectedListItem = (parameter as ListItemViewModel);
            return true;
        }

        private object OpenWarehouseWithItemClick(object parameter)
        {
            SelectedItemLocationViewModel viewModel = (parameter as SelectedItemLocationViewModel);
            if (viewModel == null)
            {
                return true;
            }

            SelectItemInList(viewModel.McdPOSRefID, viewModel.WarehouseID);

            return true;
        }

        private void SelectPrevItemInList()
        {
            // select prev item in the list
            int index = ListItems.IndexOf(_selectedListItem) - 1;
            if (index > -1)
            {
                SelectedListItem = ListItems[index];
            }
            else if (_currentPage > 1)
            {
                SelectedListItem = ListItems.LastOrDefault();
                CurrentPage--;
            }
        }
        private void SelectNextItemInList()
        {
            // select next item in the list
            int index = ListItems.IndexOf(_selectedListItem) + 1;
            if (index < _itemListCount)
            {
                if (ListItems[index].IsEnabled)
                {
                    //detect next possible item
                    ListItemViewModel model = ListItems[index];
                    if (model.IsGroupHeader && model.IsGroupOpen)
                    {
                        SelectedListItem = ListItems[index + 1];
                    }
                    else
                    {
                        SelectedListItem = model;
                    }
                }
            }
            else if (_currentPage < _maxPage)
            {
                CurrentPage++;
            }
        }

        private object SelectionModeClick(object parameter)
        {
            string command = (parameter as string);
            if (String.IsNullOrEmpty(command))
            {
                return true;
            }

            _selectedItemInput.CountCaseItem.OldCountValue = 0;
            _selectedItemInput.CountSlaveItem.OldCountValue = 0;
            _selectedItemInput.CountSingleItem.OldCountValue = 0;

            switch (command)
            {
                case "override":
                    _selectedItemInput.CountCaseItem.IsOverrideMode = true;
                    break;

                case "addition":
                    _selectedItemInput.IsAdditionMode = true;

                    _selectedItemInput.CountCaseItem.OldCountValue = _selectedItemInput.CountCaseItem.CountValue;
                    _selectedItemInput.CountSlaveItem.OldCountValue = _selectedItemInput.CountSlaveItem.CountValue;
                    _selectedItemInput.CountSingleItem.OldCountValue = _selectedItemInput.CountSingleItem.CountValue;
                    _selectedItemInput.CountCaseItem.CountValue = 0;
                    _selectedItemInput.CountSlaveItem.CountValue = 0;
                    _selectedItemInput.CountSingleItem.CountValue = 0;
                    break;

                case "delete":
                    _selectedItemInput.CountCaseItem.CountValue = 0;
                    _selectedItemInput.CountSlaveItem.CountValue = 0;
                    _selectedItemInput.CountSingleItem.CountValue = 0;
                    break;
            }

            IsSelectionModeActive = false;

            return true;
        }

        private object PagingCommandExecuted(object parameter)
        {
            var s = parameter as string;
            if (s == null) return true;
            string command = s;

            switch (command)
            {
                case "first":
                    CurrentPage = 1;
                    break;

                case "previous":
                    if (_currentPage > 1)
                    {
                        CurrentPage--;
                    }
                    break;

                case "next":
                    if (_currentPage < _maxPage)
                    {
                        CurrentPage++;
                    }
                    break;

                case "last":
                    CurrentPage = MaxPage;
                    break;
            }

            return true;
        }
        #endregion


        private bool SelectItemInList(int mcdPOSRefID, int? warehouseID = null)
        {
            if (warehouseID.HasValue)
            {
                SelectedWarehouseItem = WarehouseItemList.FirstOrDefault(W => W.ID == warehouseID.Value);
            }

            if (_lastAllFilteredItems == null)
            {
                return false;
            }

            List<CountingListItemDto> foundedItems = _lastAllFilteredItems.Select(F => F.Items.FirstOrDefault(I => I.McDPOSRefID == mcdPOSRefID || I.McDPOSRefIDAI == mcdPOSRefID || (I.CaseUPCEANNumber ?? "").Trim() == mcdPOSRefID.ToString())).Where(S => S != null).ToList();
            CountingListItemDto listItem = foundedItems.FirstOrDefault();

            if (listItem == null)
            {
                return false;
            }

            FilterListItemObject groupItem = null;
            if (IsPseudoAssemblyItemMode)
            {
                groupItem = _lastAllFilteredItems.FirstOrDefault(F => F.McDPOSRefIDAI == listItem.McDPOSRefIDAI);
                int itemIndex = _lastAllFilteredItems.IndexOf(groupItem);
                _currentPage = ((int)Math.Floor(itemIndex / (double)ItemListCount)) + 1;
            }
            else if (_lastAllFilteredItems.Count > 0)
            {
                int itemIndex = _lastAllFilteredItems.First().Items.IndexOf(listItem);
                _currentPage = ((int)Math.Floor(itemIndex / (double)ItemListCount)) + 1;
            }
            Notify("CurrentPage");

            UpdateListItems(() =>
            {
                ListItemViewModel foundItem = ListItems.FirstOrDefault(x => x.McDPOSRefID == listItem.McDPOSRefID);
                if (foundItem == null && groupItem != null)
                {
                    foundItem = ListItems.FirstOrDefault(x => x.McDPOSRefIDAI == groupItem.McDPOSRefIDAI);
                }

                SelectedListItem = foundItem;
            });

            return true;
        }
        private async void RetryManagerOnItemRemovedFromCache(CountingListItemDto item)
        {
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (item == null) return;
                var i1 = ListItems.FirstOrDefault(x => x.McDPOSRefID == item.McDPOSRefID && x.WarehouseID == item.LocationID);
                if (i1 != null)
                {
                    i1.IsLocal = false;
                }

                if (_lastResponse != null && _lastResponse.Items != null)
                {
                    var i2 = _lastResponse.Items.FirstOrDefault(x => x.McDPOSRefID == item.McDPOSRefID && x.LocationID == item.LocationID);
                    if (i2 != null)
                    {
                        i2.IsLocal = false;
                    }
                }
            });
        }




        private async void OnReceiveCharacters(string input)
        {
            int intValue;
            Int32.TryParse(input, out intValue);

            Logger.Instance.AddLog("BarcodeScanner: receive - " + input);

            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                try
                {
                    SelectItemInList(intValue, null);
                }
                catch (Exception e)
                {
                    Logger.Write(e);
                }
            });
        }

        private void ItemIsCountedEvent(object sender, EventArgs e)
        {
            SelectedItemInputUnitCountViewModel caseModel = _selectedItemInput.CountCaseItem;
            SelectedItemInputUnitCountViewModel slaveModel = _selectedItemInput.CountSlaveItem;
            SelectedItemInputUnitCountViewModel singleModel = _selectedItemInput.CountSingleItem;

            // update the counted values to the list item viewmodel
            _selectedListItem.CaseUnitCount = caseModel.CountValue + caseModel.OldCountValue;
            if (_selectedListItem.HasSlaveUnit.HasValue && _selectedListItem.HasSlaveUnit.Value)
            {
                _selectedListItem.SubUnitCount = slaveModel.CountValue + slaveModel.OldCountValue;
            }
            _selectedListItem.SingleUnitCount = singleModel.CountValue + singleModel.OldCountValue;
            _selectedListItem.IsCounted = true;
            _selectedListItem.RecalculateTotalUnitCount();
            _selectedListItem.IsLocal = true;

            if (_lastResponse != null && _lastResponse.Items != null)
            {
                var countingListItem = _lastResponse.Items.FirstOrDefault(x => x.McDPOSRefID == _selectedListItem.McDPOSRefID && x.LocationID == _selectedListItem.WarehouseID);
                countingListItem.CaseUnitCount = _selectedListItem.CaseUnitCount;
                countingListItem.SlaveUnitCount = _selectedListItem.SubUnitCount;
                countingListItem.UnitCount = _selectedListItem.SingleUnitCount;
                countingListItem.IsCounted = _selectedListItem.IsCounted;
                SaveLocal(countingListItem);
            }

            SelectNextItemInList();
        }

        private void SaveLocal(CountingListItemDto viewModel)
        {
            viewModel.IsLocal = true;
            _retryManager.StoreInventoryCountItem(viewModel);
        }



        #region Base Stuff
        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
          {
              if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged || notification.Type == NotificationApplicationType.BusinessDayIsChanged || notification.Type == NotificationApplicationType.OpenClose_ControlCenter_RequestBusinessDayUpdate)
              {
                  LoadSettings();

              }
          });
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
            Window.Current.CoreWindow.CharacterReceived -= CoreWindowOnCharacterReceived;
            StopValueUpdater();
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadSettings();
            Window.Current.CoreWindow.CharacterReceived += CoreWindowOnCharacterReceived;
        }
        #endregion



        private async void LoadSettings()
        {
            var lastSettings = await WcfConnection.ExecuteActionOnInterface((IInventoryServiceWinApp currentInterface) =>
            {
                return currentInterface.LoadInventoryEntrySettings();
            });


            if (lastSettings != null)
            {
                _lastSettings = lastSettings;
            }

            if (_lastSettings != null)
            {
                LoadingDate = _lastSettings.LastOpenBusinessDay.Value;
                IsPseudoAssemblyItemMode = _lastSettings.IsPseudoAssemblyItemMode;

                RAGLevelWarning = _lastSettings.RAGLevelWarning;
                RAGLevelCritical = _lastSettings.RAGLevelCritical;
                _requestedInventory = await WcfConnection.ExecuteActionOnInterface((IInventoryServiceWinApp currentInterface) =>
                {
                    return currentInterface.GetInventoryStatus(LoadingDate);
                });

                _nextUpdateTimer.Interval = new TimeSpan(0, 0, _lastSettings.RefreshDataIntervalInSeconds);
            }

            LoadData();
        }

        private async void LoadData()
        {
            StopValueUpdater();

            ShowLoadingIndicator();

            _currentPage = 1;
            Notify("CurrentPage");

            await UpdateLastResponseFromService();

            WarehouseItemList.Clear();
            WarehouseItemList.Add(new WarehouseItemViewModel()
            {
                ID = -1,
                Title = "All Warehouses"
            });

            if (_lastResponse != null && _lastResponse.Warehouses != null)
            {
                foreach (var item in _lastResponse.Warehouses)
                {
                    WarehouseItemList.Add(new WarehouseItemViewModel()
                    {
                        ID = item.ID,
                        Title = item.Description
                    });
                }
                _selectedWarehouseItem = WarehouseItemList.FirstOrDefault();
                Notify("SelectedWarehouseItem");

                UpdateListItems(() =>
                {
                    SelectedListItem = ListItems.FirstOrDefault();
                });

                UsedCountingLists = GetCountingListDescription(_lastResponse);
            }
            else
            {
                UsedCountingLists = string.Empty;
            }

            HideLoadingIndicator();

            StartValueUpdater();
        }

        private string GetCountingListDescription(CountingListContainerDto _lastResponse)
        {
            if (_lastResponse == null || _lastResponse.CountingLists == null)
                return "";

            return String.Join(",", _lastResponse.CountingLists.Select(x => GetTranslatedCountingListName(x.CountingListName)));
        }

        internal string GetTranslatedCountingListName(string listName)
        {
            if (_lastSettings == null)
                return string.Empty;

            if (listName == _lastSettings.DefaultCountingListForDailyItems)
            {
                return Translation["KEY_CountingListEditor_DefaultDailyList"];
            }
            else if (listName == _lastSettings.DefaultCountingListForWeeklyItems)
            {
                return Translation["KEY_CountingListEditor_DefaultWeeklyList"];
            }
            else if (listName == _lastSettings.DefaultCountingListForMonthlyItems)
            {
                return Translation["KEY_CountingListEditor_DefaultMonthlyList"];
            }
            return listName;
        }


        private async Task UpdateLastResponseFromService()
        {
            var lastResult = await WcfConnection.ExecuteActionOnInterface((IInventoryServiceWinApp currentInterface) =>
            {
                return currentInterface.LoadInventoryCountOverviewForDynamicList(LoadingDate, true, _requestedInventory.DynamicModeCountTypeNeeded == CountingListType.Full);
            });
            if (lastResult != null)
            {
                _lastResponse = lastResult;
            }
        }

        private void UpdateSelectedInput()
        {
            if (_selectedListItem == null)
                return;

            SelectedGroupedItems.Clear();

            _selectedItemInput.IsCounted = _selectedListItem.IsCounted;

            if (_isOverviewModeActive)
            {
                if (_lastResponse != null && _lastResponse.Items != null)
                {
                    int mcdPosRefID = _selectedListItem.McDPOSRefID;
                    List<CountingListItemDto> selectedItems = null;

                    if (IsPseudoAssemblyItemMode)
                    {
                        selectedItems = _lastResponse.Items.Where(I => I.McDPOSRefIDAI == mcdPosRefID).ToList();
                    }
                    else
                    {
                        selectedItems = _lastResponse.Items.Where(I => I.McDPOSRefID == mcdPosRefID).ToList();
                    }

                    if (selectedItems != null)
                    {
                        var warehouseGroupItem = selectedItems.GroupBy(I => I.LocationID).Select(K => new
                        {
                            WarehouseID = K.Key.GetValueOrDefault(-1),
                            McDPOSRefID = K.First().McDPOSRefID
                        }).ToList();

                        foreach (var item in warehouseGroupItem)
                        {
                            SelectedItemLocationViewModel newItem = new SelectedItemLocationViewModel();

                            WarehouseItemDto warehouseItem = _lastResponse.Warehouses.FirstOrDefault(W => W.ID == item.WarehouseID);
                            if (warehouseItem != null)
                            {
                                newItem.McdPOSRefID = item.McDPOSRefID;
                                newItem.WarehouseID = item.WarehouseID;
                                newItem.WarehouseName = warehouseItem.Description;
                                SelectedGroupedItems.Add(newItem);
                            }
                        }
                    }
                }

                IsSelectionModeActive = true;
            }
            else
            {
                IsSelectionModeActive = _selectedItemInput.IsCounted;
            }

            _selectedItemInput.IsAdditionMode = false;

            _selectedItemInput.NumberText = _selectedListItem.ItemNumberText;
            _selectedItemInput.Name = _selectedListItem.Name;
            _selectedItemInput.HasSlaveUnit = _selectedListItem.HasSlaveUnit;

            _selectedItemInput.CountCaseItem.OldCountValue = 0;
            _selectedItemInput.CountSlaveItem.OldCountValue = 0;
            _selectedItemInput.CountSingleItem.OldCountValue = 0;

            _selectedItemInput.CountCaseItem.CountValue = _selectedListItem.CaseUnitCount;
            _selectedItemInput.CountSlaveItem.CountValue = _selectedListItem.SubUnitCount;
            _selectedItemInput.CountSingleItem.CountValue = _selectedListItem.SingleUnitCount;

            _selectedItemInput.CountCaseItem.UnitsInCase = _selectedListItem.UnitsPerCase;
            _selectedItemInput.CountCaseItem.IsEnabled = !_isOverviewModeActive;

            _selectedItemInput.CountSlaveItem.UnitsInCase = _selectedListItem.UnitsPerSubCase;
            _selectedItemInput.CountSlaveItem.IsEnabled = _selectedListItem.HasSlaveUnit.GetValueOrDefault(false) && !_isOverviewModeActive;

            _selectedItemInput.CountSingleItem.IsEnabled = !_isOverviewModeActive;
            _selectedItemInput.CountSingleItem.ShowUnitsPerCase = false;
            _selectedItemInput.CountSingleItem.IsDecimalAllowed = true;

            _selectedItemInput.TotalUnitsCount = _selectedItemInput.UnitCountList.Sum(U => U.TotalUnits);
            _selectedItemInput.ExpectedUnitsCount = _selectedListItem.ExpectedTotalUnitCount;
            _selectedItemInput.DifferenceUnitsCount = _selectedItemInput.TotalUnitsCount - _selectedItemInput.ExpectedUnitsCount;
        }

        private void RecreateViewModels()
        {
            _currentPage = 1;
            ListItems.Clear();
            for (int i = 0; i < _itemListCount * 2; i++)
            {
                ListItems.Add(new ListItemViewModel()
                {
                    InventryMainViewModel = this
                });
            }
        }

        private async void UpdateListItems(Action afterAction = null, bool updateOnlyValues = false)
        {
            if (_lastResponse == null || _lastResponse.Items == null)
                return;

            #region Cache
            var cache = await _retryManager.GetCountingCacheAsync();
            foreach (var cacheItem in cache)
            {
                var countingListItemDto = _lastResponse.Items.FirstOrDefault(x => x.McDPOSRefID == cacheItem.McDPOSRefID && x.LocationID == cacheItem.LocationID);
                if (countingListItemDto == null) continue;
                countingListItemDto.CaseUnitCount = cacheItem.CaseUnitCount;
                countingListItemDto.SlaveUnitCount = cacheItem.SlaveUnitCount;
                countingListItemDto.UnitCount = cacheItem.UnitCount;
                countingListItemDto.IsCounted = cacheItem.IsCounted;
                countingListItemDto.IsLocal = true;
            }
            #endregion

            List<FilterListItemObject> filterItems = GenerateFilterCountingListItems();
            _lastAllFilteredItems = filterItems;

            int skipCount = (_currentPage - 1) * _itemListCount;

            List<CountingListItemDto> items = new List<CountingListItemDto>();
            if (IsPseudoAssemblyItemMode)
            {
                MaxPage = (int)Math.Ceiling((double)filterItems.Count / (double)_itemListCount);

                List<FilterListItemObject> pageGroupedData = filterItems.Skip(skipCount).Take(_itemListCount).ToList();
                foreach (var item in pageGroupedData)
                {
                    if (item.McDPOSRefIDAI > 0 && item.Items.Count > 1)
                    {
                        items.Add(new CountingListItemDto()
                        {
                            IsGroupHeader = true,
                            McDPOSRefID = item.McDPOSRefIDAI,
                            McDPOSRefIDAI = item.McDPOSRefIDAI,
                            Description = item.DescriptionAI,
                            IsCounted = item.Items.All(I => I.IsCounted)
                        });
                    }

                    items.AddRange(item.Items);
                }
            }
            else
            {
                if (filterItems.Count == 0)
                {
                    MaxPage = 1;
                }
                else
                {
                    FilterListItemObject firstGroup = filterItems.First();
                    MaxPage = (int)Math.Ceiling((double)firstGroup.Items.Count / (double)_itemListCount);

                    var pageItems = firstGroup.Items.Skip(skipCount).Take(_itemListCount).ToList();
                    items.AddRange(pageItems);
                }
            }

            int index = 0;
            foreach (var viewModel in ListItems)
            {
                if (!updateOnlyValues)
                {
                    viewModel.IsActive = false;
                    viewModel.IsGroupOpen = true;
                }

                if (index < items.Count)
                {
                    if (!updateOnlyValues)
                    {
                        viewModel.IsEnabled = true;
                    }

                    CountingListItemDto item = items[index];
                    int rawCode = (int)Math.Floor(item.McDPOSRefID / 1000M);
                    int packCode = item.McDPOSRefID % 1000;
                    viewModel.McDPOSRefIDAI = item.McDPOSRefIDAI;

                    if (IsPseudoAssemblyItemMode && IsOverviewModeActive)
                    {
                        viewModel.McDPOSRefID = item.McDPOSRefIDAI;
                        viewModel.Name = item.DescriptionAI;
                    }
                    else
                    {
                        viewModel.McDPOSRefID = item.McDPOSRefID;
                        viewModel.Name = item.Description;
                    }

                    viewModel.IsGroupHeader = item.IsGroupHeader;
                    viewModel.RawItemCode = rawCode;
                    viewModel.PackageCode = packCode;
                    viewModel.UnitsPerCase = item.UnitsPerCase;
                    viewModel.CaseUnitCount = item.CaseUnitCount.GetValueOrDefault(0);
                    viewModel.UnitsPerSubCase = (item.SlaveUnitsPerCase > 0 ? (item.UnitsPerCase / item.SlaveUnitsPerCase) : 0);
                    viewModel.HasSlaveUnit = (item.SlaveUnitsPerCase > 0);
                    viewModel.SubUnitCount = item.SlaveUnitCount.GetValueOrDefault(0);
                    viewModel.SingleUnitCount = item.UnitCount.GetValueOrDefault(0);
                    viewModel.ExpectedTotalUnitCount = item.ExpectedUnitCount.GetValueOrDefault(0);
                    viewModel.IsCounted = item.IsCounted;
                    viewModel.WarehouseID = item.LocationID;
                    viewModel.IsLocal = item.IsLocal;
                    viewModel.CaseUPCEANNumber = item.CaseUPCEANNumber;

                    viewModel.RecalculateTotalUnitCount();
                }
                else
                {
                    viewModel.IsEnabled = false;
                    viewModel.McDPOSRefID = 0;
                    viewModel.McDPOSRefIDAI = 0;
                    viewModel.Name = String.Empty;
                }

                index++;
            }

            if (!updateOnlyValues)
            {
                UpdateSelectedInput();

                if (afterAction != null)
                {
                    afterAction();
                }
            }
        }

        private List<FilterListItemObject> GenerateFilterCountingListItems()
        {
            if (_lastResponse == null || _lastResponse.Items == null)
                return null;
            
            // Removed sorting order here in the tablet app to be the same as in MyStore orders */            
           List<CountingListItemDto> filteredData = _lastResponse.Items.ToList();            

            if (_selectedWarehouseItem != null && _selectedWarehouseItem.ID > -1)
            {
                filteredData = filteredData.Where(D => D.LocationID == _selectedWarehouseItem.ID).ToList();                
            }            

            if (_isOverviewModeActive)
            {
                if (IsPseudoAssemblyItemMode)
                {
                    filteredData = filteredData.GroupBy(I => I.McDPOSRefIDAI).Select(I => new CountingListItemDto()
                    {
                        McDPOSRefID = I.First().McDPOSRefID,
                        McDPOSRefIDAI = I.First().McDPOSRefIDAI,
                        Description = I.First().Description,
                        DescriptionAI = I.First().DescriptionAI,
                        UnitsPerCase = I.First().UnitsPerCase,
                        CaseUnitCount = I.Sum(E => E.CaseUnitCount.GetValueOrDefault(0)),
                        SlaveUnitsPerCase = I.First().SlaveUnitsPerCase,
                        SlaveUnitCount = I.Sum(E => E.SlaveUnitCount),
                        UnitCount = I.Sum(E => E.UnitCount),
                        ExpectedUnitCount = I.First().ExpectedUnitCount,
                        IsCounted = I.All(E => E.IsCounted),
                        LocationID = -1
                    }).ToList();
                }
                else
                {
                    filteredData = filteredData.GroupBy(I => I.McDPOSRefID).Select(I => new CountingListItemDto()
                    {
                        McDPOSRefID = I.First().McDPOSRefID,
                        McDPOSRefIDAI = I.First().McDPOSRefIDAI,
                        Description = I.First().Description,
                        UnitsPerCase = I.First().UnitsPerCase,
                        CaseUnitCount = I.Sum(E => E.CaseUnitCount.GetValueOrDefault(0)),
                        SlaveUnitsPerCase = I.First().SlaveUnitsPerCase,
                        SlaveUnitCount = I.Sum(E => E.SlaveUnitCount),
                        UnitCount = I.Sum(E => E.UnitCount),
                        ExpectedUnitCount = I.First().ExpectedUnitCount,
                        IsCounted = I.All(E => E.IsCounted),
                        LocationID = -1
                    }).ToList();
                }
            }
            
            if (_isShowOnlyUncountedItemsChecked)
            {
                filteredData = filteredData.Where(D => !D.IsCounted).ToList();
            }
            


            if (IsPseudoAssemblyItemMode)
            {
                return filteredData.GroupBy(F => F.McDPOSRefIDAI).Select(I => new FilterListItemObject()
                {
                    McDPOSRefIDAI = I.Key,
                    DescriptionAI = I.First().DescriptionAI,
                    Items = I.ToList()
                }).ToList();
            }
            else
            {
                List<FilterListItemObject> result = new List<FilterListItemObject>();
                result.Add(new FilterListItemObject()
                {
                    McDPOSRefIDAI = -1,
                    Items = filteredData
                });
                return result;
            }
        }

        private void CoreWindowOnCharacterReceived(CoreWindow sender, CharacterReceivedEventArgs args)
        {
            if (!args.Handled) return;
            BarcodeBuffer.Instance.Add(args.KeyCode);
        }

        private void ConfigValueUpdater()
        {
            _nextUpdateTimer.Tick += async (object sender, object e) =>
            {
                if (SystemStatusManager.Instance.IsServiceConnected)
                {
                    await UpdateLastResponseFromService();
                    Debug.WriteLine("Updated Data");

                    UpdateListItems(null, true);
                    Debug.WriteLine("Updated Items");
                }
            };
        }

        private void StartValueUpdater()
        {
            if (_nextUpdateTimer.IsEnabled)
                return;

            if (_nextUpdateTimer.Interval.TotalSeconds == 0)
                return;

            _nextUpdateTimer.Start();
            Debug.WriteLine("Start Timer");
        }

        private void StopValueUpdater()
        {
            _nextUpdateTimer.Stop();
            Debug.WriteLine("Stop Timer");
        }
    }
}
