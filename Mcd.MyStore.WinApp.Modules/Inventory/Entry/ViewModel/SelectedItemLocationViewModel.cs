﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class SelectedItemLocationViewModel : ViewModelBase
    {
        #region Member - McdPOSRefID
        private int _mcdPOSRefID;
        public int McdPOSRefID
        {
            get { return _mcdPOSRefID; }
            set { SetValue(ref _mcdPOSRefID, value); }
        }
        #endregion

        #region Member - WarehouseID
        private int _warehouseID;
        public int WarehouseID
        {
            get { return _warehouseID; }
            set { SetValue(ref _warehouseID, value); }
        }
        #endregion


        #region Member - WarehouseName
        private string _warehouseName;
        public string WarehouseName
        {
            get { return _warehouseName; }
            set { SetValue(ref _warehouseName, value); }
        }
        #endregion

    }
}
