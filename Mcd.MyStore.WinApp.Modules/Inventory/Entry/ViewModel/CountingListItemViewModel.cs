﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class CountingListItemViewModel : ViewModelBase
    {
        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion

        public string Name { get; set; }
    }
}
