﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class WarehouseItemViewModel : ViewModelBase
    {
        #region Member - Title
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }
        #endregion

        #region Member - ID
        private int _id;
        public int ID
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }
        #endregion

    }
}
