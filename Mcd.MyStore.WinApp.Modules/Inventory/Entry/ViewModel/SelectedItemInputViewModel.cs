﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class SelectedItemInputViewModel : ViewModelBase
    {
        #region Member - NumberText
        private string _numberText;
        public string NumberText
        {
            get { return _numberText; }
            set
            {
                if (SetValue(ref _numberText, value))
                {
                    SelectedUnitCountItem = UnitCountList.FirstOrDefault();
                }
            }
        }
        #endregion

        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }
        #endregion

        #region Member - TotalUnitsCount
        private decimal _totalUnitsCount;
        public decimal TotalUnitsCount
        {
            get { return _totalUnitsCount; }
            set { SetValue(ref _totalUnitsCount, value); }
        }
        #endregion

        #region Member - ExpectedUnitsCount
        private decimal _expectedUnitsCount;
        public decimal ExpectedUnitsCount
        {
            get { return _expectedUnitsCount; }
            set { SetValue(ref _expectedUnitsCount, value); }
        }
        #endregion

        #region Member - DifferenceUnitsCount
        private decimal _differenceUnitsCount;
        public decimal DifferenceUnitsCount
        {
            get { return _differenceUnitsCount; }
            set { SetValue(ref _differenceUnitsCount, value); }
        }
        #endregion

        #region Member - HasSlaveUnit
        private bool? _hasSlaveUnit = null;
        public bool? HasSlaveUnit
        {
            get { return _hasSlaveUnit; }
            set
            {
                if (SetValue(ref _hasSlaveUnit, value))
                {
                    SlaveUnitOpacity = (_hasSlaveUnit.HasValue && _hasSlaveUnit.Value ? 1 : 0.4);
                }
            }
        }
        #endregion

        #region Member - SlaveUnitOpacity
        private double _slaveUnitOpacity = 1;
        public double SlaveUnitOpacity
        {
            get { return _slaveUnitOpacity; }
            set { SetValue(ref _slaveUnitOpacity, value); }
        }
        #endregion

        #region Member - IsCounted
        private bool _isCounted;
        public bool IsCounted
        {
            get { return _isCounted; }
            set { SetValue(ref _isCounted, value); }
        }
        #endregion

        #region Member - IsAdditionMode
        private bool _isAdditionMode = false;
        public bool IsAdditionMode
        {
            get { return _isAdditionMode; }
            set { SetValue(ref _isAdditionMode, value); }
        }
        #endregion

        public ObservableCollection<SelectedItemInputUnitCountViewModel> UnitCountList { get; set; }

        public readonly SelectedItemInputUnitCountViewModel CountCaseItem = new SelectedItemInputUnitCountViewModel() { CountType = SelectedItemInputUnitCountType.Case };
        public readonly SelectedItemInputUnitCountViewModel CountSlaveItem = new SelectedItemInputUnitCountViewModel() { CountType = SelectedItemInputUnitCountType.Slave };
        public readonly SelectedItemInputUnitCountViewModel CountSingleItem = new SelectedItemInputUnitCountViewModel() { CountType = SelectedItemInputUnitCountType.Single };

        #region Member - SelectedUnitCountItem
        private SelectedItemInputUnitCountViewModel _selectedUnitCountItem;
        public SelectedItemInputUnitCountViewModel SelectedUnitCountItem
        {
            get { return _selectedUnitCountItem; }
            set
            {
                if (_selectedUnitCountItem != null)
                {
                    _selectedUnitCountItem.IsOverrideMode = false;
                }

                if (SetValue(ref _selectedUnitCountItem, value))
                {
                    _selectedUnitCountItem.IsOverrideMode = true;
                }
            }
        }
        #endregion

        #region Member - InventryMainViewModel
        private InventoryEntryControlViewModel _inventryMainViewModel;
        public InventoryEntryControlViewModel InventryMainViewModel
        {
            get { return _inventryMainViewModel; }
            set
            {
                if (SetValue(ref _inventryMainViewModel, value))
                {
                    foreach (var item in UnitCountList)
                    {
                        item.InventryMainViewModel = _inventryMainViewModel;
                    }
                }
            }
        }
        #endregion

        public ICommand TypeSelectionCommand { get; set; }
        public ICommand ClickKeyboardCommand { get; set; }
        public ICommand CountedSelectNextItemCommand { get; set; }

        public event EventHandler OnItemIsCounted;

        public override string TranslationPrefix
        {
            get
            {
                return "InventoryEntry_";
            }
        }

        public SelectedItemInputViewModel()
        {
            UnitCountList = new ObservableCollection<SelectedItemInputUnitCountViewModel>();
            CountCaseItem.OnInputChanged += OnCountInputChanged;
            CountSlaveItem.OnInputChanged += OnCountInputChanged;
            CountSingleItem.OnInputChanged += OnCountInputChanged;
            UnitCountList.Add(CountCaseItem);
            UnitCountList.Add(CountSlaveItem);
            UnitCountList.Add(CountSingleItem);

            TypeSelectionCommand = new RelayCommand((object parameter) =>
            {
                if (parameter is string)
                {
                    string selection = ((string)parameter).ToLower();

                    switch (selection)
                    {
                        case "case":
                            SelectedUnitCountItem = CountCaseItem;
                            break;

                        case "slave":
                            SelectedUnitCountItem = CountSlaveItem;
                            break;

                        case "single":
                            SelectedUnitCountItem = CountSingleItem;
                            break;
                    }

                }

                return true;
            });

            ClickKeyboardCommand = new RelayCommand((object parameter) =>
            {
                if (SelectedUnitCountItem.IsOverrideMode)
                {
                    SelectedUnitCountItem.CountValueText = String.Empty;
                    SelectedUnitCountItem.IsOverrideMode = false;
                }

                string decimalSeperator = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
                string oldValue = SelectedUnitCountItem.CountValueText;
                bool hasDecimal = (!String.IsNullOrEmpty(oldValue) && oldValue.IndexOf(decimalSeperator) >= 0);
                bool hasMaxLengh = oldValue.Length >= 7;

                if (parameter is string)
                {
                    string inputText = ((string)parameter).ToLower();
                    switch (inputText)
                    {
                        case "clr":
                            SelectedUnitCountItem.CountValueText = String.Empty;
                            break;

                        case "decimal":
                            if (!hasMaxLengh && !hasDecimal && !SelectedUnitCountItem.IsOverrideMode)
                            {
                                SelectedUnitCountItem.CountValueText += decimalSeperator;
                            }
                            break;

                        default:
                            if (!hasMaxLengh)
                            {
                                SelectedUnitCountItem.CountValueText += inputText;
                            }
                            break;
                    }
                }

                return true;
            });

            CountedSelectNextItemCommand = new RelayCommand(() =>
            {
                if (OnItemIsCounted != null)
                {
                    OnItemIsCounted(this, null);
                }
            });
        }

        private void OnCountInputChanged(object sender, EventArgs e)
        {
            TotalUnitsCount = UnitCountList.Sum(U => U.TotalUnits);
            DifferenceUnitsCount = TotalUnitsCount - ExpectedUnitsCount;
        }


    }

    public enum SelectedItemInputUnitCountType
    {
        Case,
        Slave,
        Single
    }
}
