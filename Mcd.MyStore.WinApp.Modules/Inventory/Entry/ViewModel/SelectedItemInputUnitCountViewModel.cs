﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class SelectedItemInputUnitCountViewModel : ViewModelBase
    {
        public SelectedItemInputUnitCountType CountType { get; set; }

        public string Title
        {
            get { return Translation["InventoryEntry_CountType" + CountType.ToString()]; }
        }

        #region Member - CountValueText
        private string _countValueText;
        public string CountValueText
        {
            get
            {
                if (String.IsNullOrEmpty(_countValueText))
                {
                    return "0";
                }

                return _countValueText;
            }
            set
            {
                string clearInput = value.TrimStart('0');
                if (SetValue(ref _countValueText, clearInput))
                {
                    if (String.IsNullOrEmpty(_countValueText))
                    {
                        _countValue = 0;
                    }
                    else
                    {
                        decimal tmpValue = 0;
                        if (Decimal.TryParse(_countValueText, out tmpValue))
                        {
                            _countValue = tmpValue;
                        }
                    }

                    Notify("CountValue");
                    Notify("TotalUnits");

                    if (OnInputChanged != null)
                    {
                        OnInputChanged(this, null);
                    }
                }
            }
        }
        #endregion

        #region Member - CountValue
        private decimal _countValue;
        public decimal CountValue
        {
            get { return _countValue; }
            set
            {
                if (SetValue(ref _countValue, value))
                {
                    CountValueText = _countValue.ToString("###,###,##0.##");
                    Notify("TotalUnits");
                }
            }
        }
        #endregion

        #region Member - OldCountValueText
        private string _oldCountValueText;
        public string OldCountValueText
        {
            get { return _oldCountValueText; }
            set { SetValue(ref _oldCountValueText, value); }
        }
        #endregion


        #region Member - OldCountValue
        private decimal _oldCountValue;
        public decimal OldCountValue
        {
            get { return _oldCountValue; }
            set
            {
                if (SetValue(ref _oldCountValue, value))
                {
                    OldCountValueText = _oldCountValue.ToString("###,###,##0.##");
                    Notify("TotalUnits");
                }
            }
        }
        #endregion

        #region Member - UnitsInCase
        private decimal _unitsInCase;
        public decimal UnitsInCase
        {
            get { return _unitsInCase; }
            set
            {
                if (SetValue(ref _unitsInCase, value))
                {
                    Notify("TotalUnits");
                }
            }
        }
        #endregion

        #region Member - IsOverrideMode
        private bool _isOverrideMode;
        public bool IsOverrideMode
        {
            get { return _isOverrideMode; }
            set { SetValue(ref _isOverrideMode, value); }
        }
        #endregion

        #region Member - IsEnabled
        private bool? _isEnabled = null;
        public bool? IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                if (SetValue(ref _isEnabled, value))
                {
                    ListViewItem item = (ListViewItem)InventryMainViewModel.CountingList.ContainerFromItem(this);
                    if (_isEnabled != null)
                    {
                        item.IsEnabled = _isEnabled.Value;
                    }
                    else
                    {
                        item.IsEnabled = true;
                    }
                    SlaveUnitOpacity = (_isEnabled.HasValue && _isEnabled.Value ? 1 : 0.4);
                }
            }
        }
        #endregion

        #region Member - SlaveUnitOpacity
        private double _slaveUnitOpacity = 1;
        public double SlaveUnitOpacity
        {
            get { return _slaveUnitOpacity; }
            set { SetValue(ref _slaveUnitOpacity, value); }
        }
        #endregion

        #region Member - ShowUnitsPerCase
        private bool _showUnitsPerCase = true;
        public bool ShowUnitsPerCase
        {
            get { return _showUnitsPerCase; }
            set { SetValue(ref _showUnitsPerCase, value); }
        }
        #endregion

        #region Member - IsDecimalAllowed
        private bool _isDecimalAllowed = false;
        public bool IsDecimalAllowed
        {
            get { return _isDecimalAllowed; }
            set
            {
                if (SetValue(ref _isDecimalAllowed, value))
                {
                    Notify("DecimalAllowedOpacity");
                }
            }
        }
        #endregion

        #region Member - DecimalAllowedOpacity
        public double DecimalAllowedOpacity
        {
            get { return (_isDecimalAllowed ? 1 : 0.4); }
        }
        #endregion


        public decimal TotalUnits
        {
            get { return (_unitsInCase > 0 ? ((_oldCountValue + _countValue) * _unitsInCase) : (_oldCountValue + _countValue)); }
        }

        public event EventHandler OnInputChanged;

        public InventoryEntryControlViewModel InventryMainViewModel { get; set; }
    }
}
