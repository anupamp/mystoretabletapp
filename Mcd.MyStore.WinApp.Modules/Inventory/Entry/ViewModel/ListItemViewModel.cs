﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel
{
    public class ListItemViewModel : ViewModelBase
    {
        public InventoryEntryControlViewModel InventryMainViewModel { get; set; }

        #region Member - RawItemCode
        private int _rawItemCode;
        public int RawItemCode
        {
            get { return _rawItemCode; }
            set
            {
                if (SetValue(ref _rawItemCode, value))
                {
                    Notify("ItemNumberText");
                }
            }
        }
        #endregion

        #region Member - PackageCode
        private int _packageCode;
        public int PackageCode
        {
            get { return _packageCode; }
            set
            {
                if (SetValue(ref _packageCode, value))
                {
                    Notify("ItemNumberText");
                }
            }
        }
        #endregion

        #region Member - McDPOSRefID
        private int _mcDPOSRefID;
        public int McDPOSRefID
        {
            get { return _mcDPOSRefID; }
            set
            {
                if (SetValue(ref _mcDPOSRefID, value))
                {
                    Notify("ItemNumberText");
                }
            }
        }
        #endregion

        #region Member - McDPOSRefIDAI
        private int _mcDPOSRefIDAI;
        public int McDPOSRefIDAI
        {
            get { return _mcDPOSRefIDAI; }
            set { SetValue(ref _mcDPOSRefIDAI, value); }
        }
        #endregion

        #region Member - IsGroupHeader
        private bool _isGroupHeader;
        public bool IsGroupHeader
        {
            get { return _isGroupHeader; }
            set
            {
                if (SetValue(ref _isGroupHeader, value))
                {
                    Notify("ItemNumberText");
                }
            }
        }
        #endregion

        #region Member - IsGroupOpen
        private bool _isGroupOpen = true;
        public bool IsGroupOpen
        {
            get { return _isGroupOpen; }
            set { SetValue(ref _isGroupOpen, value); }
        }
        #endregion


        #region Member - ItemNumberText
        public string ItemNumberText
        {
            get
            {
                if (_isGroupHeader)
                {
                    return _mcDPOSRefIDAI.ToString("0");
                }
                else if (InventryMainViewModel.IsPseudoAssemblyItemMode && InventryMainViewModel.IsOverviewModeActive)
                {
                    return _mcDPOSRefID.ToString("0");
                }
                else
                {
                    return _mcDPOSRefID.ToString("#######/000");
                }
            }
        }
        #endregion

        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }
        #endregion

        #region Members - Case Stuff
        #region Member - UnitsPerCase
        private decimal _unitsPerCase;
        public decimal UnitsPerCase
        {
            get { return _unitsPerCase; }
            set { SetValue(ref _unitsPerCase, value); }
        }
        #endregion

        #region Member - CaseUnitCount
        private decimal _caseUnitCount;
        public decimal CaseUnitCount
        {
            get { return _caseUnitCount; }
            set { SetValue(ref _caseUnitCount, value); }
        }
        #endregion
        #endregion

        #region Members - SubCase Stuff
        #region Member - UnitsPerSubCase
        private decimal _unitsPerSubCase;
        public decimal UnitsPerSubCase
        {
            get { return _unitsPerSubCase; }
            set { SetValue(ref _unitsPerSubCase, value); }
        }
        #endregion

        #region Member - HasSlaveUnit
        private bool? _hasSlaveUnit;
        public bool? HasSlaveUnit
        {
            get { return _hasSlaveUnit; }
            set { SetValue(ref _hasSlaveUnit, value); }
        }
        #endregion



        #region Member - SubUnitCount
        private decimal _subUnitCount;
        public decimal SubUnitCount
        {
            get { return _subUnitCount; }
            set { SetValue(ref _subUnitCount, value); }
        }
        #endregion
        #endregion

        #region Member - SingleUnitCount
        private decimal _singleUnitCount;
        public decimal SingleUnitCount
        {
            get { return _singleUnitCount; }
            set { SetValue(ref _singleUnitCount, value); }
        }
        #endregion

        #region Member - TotalUnitCount
        private decimal _totalUnitCount;
        public decimal TotalUnitCount
        {
            get { return _totalUnitCount; }
            set
            {
                if (SetValue(ref _totalUnitCount, value))
                {
                }
            }
        }
        #endregion

        #region Member - ExpectedTotalUnitCount
        private decimal _expectedTotalUnitCount;
        public decimal ExpectedTotalUnitCount
        {
            get { return _expectedTotalUnitCount; }
            set
            {
                if (SetValue(ref _expectedTotalUnitCount, value))
                {
                    RecalculateStatusRAG();
                }
            }
        }
        #endregion

        #region Member - IsCounted
        private bool _isCounted;
        public bool IsCounted
        {
            get { return _isCounted; }
            set
            {
                if (SetValue(ref _isCounted, value))
                {
                    RecalculateStatusRAG();
                }
            }
        }
        #endregion

        #region Member - IsEnabled
        private bool _isEnabled = false;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                SetValue(ref _isEnabled, value);

                ListViewItem item = (ListViewItem)InventryMainViewModel.InventoryItemList.ContainerFromItem(this);
                item.IsEnabled = _isEnabled;
                item.Visibility = (_isEnabled ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        #endregion

        #region Member - WarehouseID

        private int? _warehouseID;

        public int? WarehouseID
        {
            get { return _warehouseID; }
            set { SetValue(ref _warehouseID, value); }
        }

        #endregion

        #region Member - IsLocal

        private bool _isLocal;

        public bool IsLocal
        {
            get { return _isLocal; }
            set { SetValue(ref _isLocal, value); }
        }

        #endregion

        #region Member - CaseUPCEANNumber

        private string _caseUpceanNumber;

        public string CaseUPCEANNumber
        {
            get { return _caseUpceanNumber; }
            set { _caseUpceanNumber = value; }
        }

        #endregion

        #region Member - StatusRAGImage
        private BitmapImage _statusRAGOkImage = new BitmapImage(new Uri("ms-appx:///Mcd.MyStore.WinApp.Modules/Images/check_green.png"));
        private BitmapImage _statusRAGWarningImage = new BitmapImage(new Uri("ms-appx:///Mcd.MyStore.WinApp.Modules/Images/check_orange.png"));
        private BitmapImage _statusRAGCriticalImage = new BitmapImage(new Uri("ms-appx:///Mcd.MyStore.WinApp.Modules/Images/check_red.png"));
        public BitmapImage StatusRAGImage
        {
            get
            {
                switch (_currentStatusRAG)
                {
                    default:
                    case StatusRAG.None:
                        return null;
                    case StatusRAG.Ok:
                        return _statusRAGOkImage;
                    case StatusRAG.Warning:
                        return _statusRAGWarningImage;
                    case StatusRAG.Critical:
                        return _statusRAGCriticalImage;
                }
            }
        }
        #endregion

        #region Member - CurrentStatusRAG
        private StatusRAG _currentStatusRAG;
        public StatusRAG CurrentStatusRAG
        {
            get { return _currentStatusRAG; }
            set
            {
                if (SetValue(ref _currentStatusRAG, value))
                {
                    Notify("StatusRAGImage");
                }
            }
        }
        #endregion

        #region Member - IsActive
        private bool _isActive = false;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetValue(ref _isActive, value); }
        }
        #endregion


        #region Methods

        public void RecalculateTotalUnitCount()
        {
            TotalUnitCount = (UnitsPerCase * CaseUnitCount) +
                             (UnitsPerSubCase * SubUnitCount) +
                             (SingleUnitCount);

            RecalculateStatusRAG();
        }

        public void RecalculateStatusRAG()
        {
            if (!_isCounted || _expectedTotalUnitCount == 0 || !InventryMainViewModel.IsOverviewModeActive)
            {
                CurrentStatusRAG = StatusRAG.None;
                return;
            }

            decimal calculation = Math.Abs(1 - (_totalUnitCount / _expectedTotalUnitCount));
            if (calculation < InventryMainViewModel.RAGLevelWarning)
            {
                CurrentStatusRAG = StatusRAG.Ok;
            }
            else if (calculation < InventryMainViewModel.RAGLevelCritical)
            {
                CurrentStatusRAG = StatusRAG.Warning;
            }
            else
            {
                CurrentStatusRAG = StatusRAG.Critical;
            }
        }

        #endregion

        public enum StatusRAG
        {
            None,
            Ok,
            Warning,
            Critical
        }
    }
}
