﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry
{
    [MenuModuleAttribute("InventoryEntry")]
    public sealed partial class InventoryEntryControl : UserControl
    {
        InventoryEntryControlViewModel _viewModel = new InventoryEntryControlViewModel();

        public InventoryEntryControl()
        {
            this.InitializeComponent();
            DataContext = _viewModel;
            _viewModel.CountingList = CountingList;
            _viewModel.InventoryItemList = ItemListView;
        }

        private void ItemListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //_viewModel.ItemListCount = (int)Math.Floor(e.NewSize.Height / (double)_viewModel.ItemHeight);
        }
    }
}
