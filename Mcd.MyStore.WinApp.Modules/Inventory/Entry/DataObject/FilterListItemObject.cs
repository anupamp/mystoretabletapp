﻿
using Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEngine;
using System.Collections.Generic;
namespace Mcd.MyStore.WinApp.Modules.Inventory.Entry.DataObject
{
    public class FilterListItemObject
    {
        public int McDPOSRefIDAI { get; set; }
        public string DescriptionAI { get; set; }
        public List<CountingListItemDto> Items { get; set; }
    }
}
