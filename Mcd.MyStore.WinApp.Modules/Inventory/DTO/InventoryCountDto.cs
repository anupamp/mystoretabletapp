﻿using System;
using Mcd.MyStore.WinApp.Modules.Inventory.Entry.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.Inventory.DTO
{
    public class InventoryCountDto
    {
        public bool IsCounted { get; set; }
        public int McDPOSRefID { get; set; }
        public decimal CaseUnitCount { get; set; }
        public decimal SubUnitCount { get; set; }
        public decimal SingleUnitCount { get; set; }
        public DateTime BusinessDayOfInventory { get; set; }

        public static implicit operator InventoryCountDto(ListItemViewModel viewModel)
        {
            return new InventoryCountDto
            {
                IsCounted = viewModel.IsCounted,
                McDPOSRefID = viewModel.McDPOSRefID,
                CaseUnitCount = viewModel.CaseUnitCount,
                SubUnitCount = viewModel.SubUnitCount,
                SingleUnitCount = viewModel.SingleUnitCount
            };
        }
    }
}