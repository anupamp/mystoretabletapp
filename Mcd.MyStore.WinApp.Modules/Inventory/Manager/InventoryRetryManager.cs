﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEngine;
using Mcd.MyStore.InventoryService.Common.DataContracts.InventoryEntry;
using Mcd.MyStore.InventoryService.Common.Definitions;
using Mcd.MyStore.InventoryService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Search;
using Windows.Storage.Streams;

namespace Mcd.MyStore.WinApp.Modules.Inventory.Manager
{
    public class InventoryRetryManager : INotificationModule
    {
        private const int ChunkSize = 20;
        private static readonly CancellationTokenSource Cts = new CancellationTokenSource();
        private int _onlineOfflineTimeoutInMilliseconds = 1 * 60 * 1000; //1 Minute

        public delegate void ItemRemovedFromCache(CountingListItemDto item);
        public event ItemRemovedFromCache OnItemRemovedFromCache;

        public async void StoreInventoryCountItem(CountingListItemDto viewModel)
        {
            if (viewModel == null) return;

            CountingListItemCacheDto storeModel = CountingListItemCacheDto.FromCountingListItemDto(viewModel);
            storeModel.Timestamp = DateTime.Now;

            string fileName = storeModel.McDPOSRefID + "_" + storeModel.LocationID + ".json";
            string json = JsonConvert.SerializeObject(storeModel);

            var storeFolder = await GetStoreFolderAsync();

            StorageFile file = await storeFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(file, json, UnicodeEncoding.Utf8);
        }

        public async void DeleteInventoryCountItem(CountingListItemDto viewModel)
        {
            try
            {
                string fileName = viewModel.McDPOSRefID + "_" + viewModel.LocationID + ".json";
                var storeFolder = await GetStoreFolderAsync();
                var file = await storeFolder.GetFileAsync(fileName);

                string text = await FileIO.ReadTextAsync(file);
                CountingListItemCacheDto dto = JsonConvert.DeserializeObject<CountingListItemCacheDto>(text);

                await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
                if (OnItemRemovedFromCache != null)
                {
                    OnItemRemovedFromCache(dto);
                }
            }
            catch { }
        }

        public async Task<bool> HasCacheAsync()
        {
            StorageFolder baseFolder = ApplicationData.Current.RoamingFolder;
            StorageFolder inventoryFolder = await baseFolder.CreateFolderAsync("Inventory", CreationCollisionOption.OpenIfExists);
            StorageFolder offlineFolder = await inventoryFolder.CreateFolderAsync("Offline", CreationCollisionOption.OpenIfExists);
            var files = await offlineFolder.GetFilesAsync(CommonFileQuery.OrderByName);
            return files.Count > 0;
        }

        public async Task<List<CountingListItemCacheDto>> GetCountingCacheAsync()
        {
            StorageFolder baseFolder = ApplicationData.Current.RoamingFolder;
            StorageFolder inventoryFolder = await baseFolder.CreateFolderAsync("Inventory", CreationCollisionOption.OpenIfExists);
            StorageFolder offlineFolder = await inventoryFolder.CreateFolderAsync("Offline", CreationCollisionOption.OpenIfExists);
            var files = await offlineFolder.GetFilesAsync(CommonFileQuery.OrderByName);

            var inventoryCountObjectBuffer = new List<CountingListItemCacheDto>();

            for (int index = 0; index < files.Count; index++)
            {
                StorageFile storageFile = files[index];
                if (!storageFile.Name.EndsWith(".json")) continue;

                //Read bytes
                string text = await FileIO.ReadTextAsync(storageFile);

                //convert json
                CountingListItemCacheDto dto = JsonConvert.DeserializeObject<CountingListItemCacheDto>(text);

                //collect data
                inventoryCountObjectBuffer.Add(dto);
            }

            return inventoryCountObjectBuffer;
        }

        private void RunWorkerAsync()
        {
            Task.Run(async () =>
            {
                DateTime nextRun = DateTime.MinValue;
                int cachedTimeout = _onlineOfflineTimeoutInMilliseconds;
                while (true)
                {
                    if (Cts.Token.IsCancellationRequested)
                        break;

                    if (!await HasCacheAsync())
                    {
                        await Task.Delay(1000);
                        continue;
                    }

                    if (cachedTimeout != _onlineOfflineTimeoutInMilliseconds)
                    {
                        cachedTimeout = _onlineOfflineTimeoutInMilliseconds;
                        nextRun = DateTime.Now.AddMilliseconds(_onlineOfflineTimeoutInMilliseconds);
                    }

                    if (DateTime.Now < nextRun)
                    {
                        await Task.Delay(1000);
                        continue;
                    }

                    RetryInternal();
                    while (_readInternalRunning)
                    {
                        await Task.Delay(1000);
                    }

                    nextRun = DateTime.Now.AddMilliseconds(_onlineOfflineTimeoutInMilliseconds);
                }
            }, Cts.Token);
        }

        private async void RetryInternal()
        {
            lock (_readWriteLock)
            {
                if (_readInternalRunning) return;
                _readInternalRunning = true;
            }

            try
            {
                var source = await GetCountingCacheAsync();
                if (source == null || !source.Any())
                {
                    return;
                }

                var settings = await GetInventoryEntrySettings();

                if (settings == null)
                    return;

                var chunks = ChunkList(source); // chunk the data in small parts

                foreach (var chunk in chunks)
                {
                    var chunkClosure = chunk;
                    await WcfConnection.ExecuteActionOnInterface((IInventoryServiceWinApp currentInterface) =>
                    {
                        CountingListContainerDto container = new CountingListContainerDto
                        {
                            Items = chunkClosure.Select(CountingListItemDto.FromCountingListItemCacheDto).ToList(),
                            Name = "Inventory Entry Count on MyStore App", // name? wtf => wichtig
                            IsCompleted = source.All(x => x.IsCounted)
                        };
                        bool success = currentInterface.SaveCountingListItemsForCurrentBusinessDay(container, settings.LastOpenBusinessDay.Value);

                        if (success)
                        {
                            foreach (var countingListItemCacheDto in chunkClosure)
                            {
                                DeleteInventoryCountItem(countingListItemCacheDto);
                            }
                        }

                        return success;
                    });
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                _readInternalRunning = false;
            }
        }

        private async Task<InventoryEntrySettingsDto> GetInventoryEntrySettings()
        {
            return await WcfConnection.ExecuteActionOnInterface((IInventoryServiceWinApp currentInterface) =>
                currentInterface.LoadInventoryEntrySettings());
        }

        private async Task<StorageFolder> GetStoreFolderAsync()
        {
            StorageFolder baseFolder = ApplicationData.Current.RoamingFolder;
            StorageFolder inventoryFolder = await baseFolder.CreateFolderAsync("Inventory", CreationCollisionOption.OpenIfExists);
            StorageFolder offlineFolder = await inventoryFolder.CreateFolderAsync("Offline", CreationCollisionOption.OpenIfExists);
            return offlineFolder;
        }

        private List<List<CountingListItemCacheDto>> ChunkList(List<CountingListItemCacheDto> source)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x }) // create indexed list
                .GroupBy(x => x.Index / ChunkSize) // group 20 items per chunk
                .Select(x => x.Select(v => v.Value).ToList()) // select values without index
                .ToList(); // create List of chunks
        }

        private CountingListType StringToCountingType(string countingMode, bool legacyCountingMode)
        {
            if (legacyCountingMode)
            {
                if (countingMode == "day") return CountingListType.DailyLegacy;
                if (countingMode == "week") return CountingListType.WeeklyLegacy;
                if (countingMode == "month") return CountingListType.MonthlyLegacy;
            }
            else
            {
                if (countingMode == "day") return CountingListType.Daily;
                if (countingMode == "full") return CountingListType.Full;
            }
            return CountingListType.DailyLegacy;
        }

        #region Member - ReadWriteLock

        private readonly object _readWriteLock = new object();
        private bool _readInternalRunning;

        #endregion

        #region Singleton

        private static readonly InventoryRetryManager _instance = new InventoryRetryManager();

        public static InventoryRetryManager Instance
        {
            get { return _instance; }
        }

        private InventoryRetryManager()
        {
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceConnected);
            NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.WinApp_AppServiceDisconnected);
        }

        public void Init()
        {
            RunWorkerAsync();
        }

        #endregion

        public void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            switch (notification.Type)
            {
                case NotificationApplicationType.WinApp_AppServiceConnected:
                    _onlineOfflineTimeoutInMilliseconds = 10000;
                    break;
                case NotificationApplicationType.WinApp_AppServiceDisconnected:
                    _onlineOfflineTimeoutInMilliseconds = 1 * 60 * 1000;
                    break;
            }
        }
    }
}