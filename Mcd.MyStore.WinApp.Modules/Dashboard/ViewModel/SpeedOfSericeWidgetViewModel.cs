﻿using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel
{
    public class SpeedOfSericeWidgetViewModel : ViewModelBase
    {
        #region MEMBERS

        #region REACTION
        private int _reactionThreshold = 0;
        public int ReactionThreshold
        {
            get { return _reactionThreshold; }
            set { SetValue<int>(ref _reactionThreshold, value); }
        }

        private int _reactionValue = 0;
        public int ReactionValue
        {
            get { return _reactionValue; }
            set { SetValue<int>(ref _reactionValue, value); }
        }
        #endregion

        #region ASSEMBLE
        private int _assembleThreshold;
        public int AssembleThreshold
        {
            get { return _assembleThreshold; }
            set { SetValue<int>(ref _assembleThreshold, value); }
        }

        private int _assembleValue;
        public int AssembleValue
        {
            get { return _assembleValue; }
            set { SetValue<int>(ref _assembleValue, value); }
        }
        #endregion

        #region CounterOrder
        private int _counterOrderThreshold;
        public int CounterOrderThreshold
        {
            get { return _counterOrderThreshold; }
            set { SetValue<int>(ref _counterOrderThreshold, value); }
        }

        private int _counterOrderValue;
        public int CounterOrderValue
        {
            get { return _counterOrderValue; }
            set { SetValue<int>(ref _counterOrderValue, value); }
        }
        #endregion

        #region CounterCash
        private int _counterCashThreshold;
        public int CounterCashThreshold
        {
            get { return _counterCashThreshold; }
            set { SetValue<int>(ref _counterCashThreshold, value); }
        }

        private int _counterCashValue;
        public int CounterCashValue
        {
            get { return _counterCashValue; }
            set { SetValue<int>(ref _counterCashValue, value); }
        }
        #endregion

        #region CounterPresent
        private int _counterPresentThreshold;
        public int CounterPresentThreshold
        {
            get { return _counterPresentThreshold; }
            set { SetValue<int>(ref _counterPresentThreshold, value); }
        }

        private int _counterPresentValue;
        public int CounterPresentValue
        {
            get { return _counterPresentValue; }
            set { SetValue<int>(ref _counterPresentValue, value); }
        }
        #endregion

        #region Total
        private int _totalThreshold;
        public int TotalThreshold
        {
            get { return _totalThreshold; }
            set { SetValue<int>(ref _totalThreshold, value); }
        }
        private int _totalValue;
        public int TotalValue
        {
            get { return _totalValue; }
            set { SetValue<int>(ref _totalValue, value); }
        }
        #endregion

        #endregion

        public void UpdateData(SpeedOfServiceWidgetData result)
        {
            if (result == null)
            {
                return;
            }

            ReactionThreshold = result.ReactionThreshold;
            ReactionValue = result.FCReactionTime + result.DTReactionTime;

            AssembleThreshold = result.AssemblyThreshold;
            AssembleValue = result.FCAssemblyTime + result.DTAssemblyTime;

            CounterOrderThreshold = result.FCOrderThreshold;
            CounterOrderValue = result.FCOrderTime;

            CounterCashThreshold = result.FCCashThreshold;
            CounterCashValue = result.FCCashTime;

            CounterPresentThreshold = result.FCPresentationThreshold;
            CounterPresentValue = result.FCPresentationTime;

            TotalThreshold = result.FCTotalThreshold;
            TotalValue = result.FCTotalTime;
        }

    }
}
