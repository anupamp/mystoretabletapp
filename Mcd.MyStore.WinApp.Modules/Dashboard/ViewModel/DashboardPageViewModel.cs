﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel
{
    public class DashboardPageViewModel : ModuleViewModelBase
    {
        #region Member - HourlyGuestWidgetViewModel
        private readonly HourlyWidgetViewModel _hourlyWidgetViewModel = new HourlyWidgetViewModel();
        public HourlyWidgetViewModel HourlyGuestWidgetViewModel
        {
            get { return _hourlyWidgetViewModel; }
        }
        #endregion

        #region Member - GuestChartWidgetViewModel
        private readonly ChartWidgetViewModel _chartWidgetViewModel = new ChartWidgetViewModel();
        public ChartWidgetViewModel GuestChartWidgetViewModel
        {
            get { return _chartWidgetViewModel; }
        }
        #endregion

        #region Member - SpeedOfServiceWidgetViewModel
        private readonly SpeedOfSericeWidgetViewModel _speedOfServiceWidgetViewModel = new SpeedOfSericeWidgetViewModel();
        public SpeedOfSericeWidgetViewModel SpeedOfServiceWidgetViewModel
        {
            get { return _speedOfServiceWidgetViewModel; }
        }
        #endregion

        #region Member - BeverageProductionWidgetViewModel
        private readonly BeverageProductionWidgetViewModel _beverageProductionWidgetViewModel = new BeverageProductionWidgetViewModel();
        public BeverageProductionWidgetViewModel BeverageProductionWidgetViewModel
        {
            get { return _beverageProductionWidgetViewModel; }
        }
        #endregion

        public DashboardPageViewModel()
        {
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            IsAutoReloadEnabled = true;
        }

        #region Load Data
        public async void LoadData()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return;

            ShowLoadingIndicator();

            DailyDashboardDataResponse data = await Task.Run(() =>
            {
                DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

                try
                {
                    var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                    Logger.Write(String.Format("Start DailyDashboard - LoadDataAsync with date: {0}", selectedDate));
                    var serviceResult = service.GetDailyDashboardData(new DailyDashboardDataRequest
                    {
                        ReportDate = selectedDate,
                        RequestDate = selectedDate,
                        PODType = DailyDashboardPODType.Storewide,
                        IncludeSalesWidgetData = true,
                        IncludeHourlySalesWidgetData = true,
                        HourlySalesWidgetDataIntervalInMinutes = 15,
                        IncludeSpeedOfServiceWidgetData = true,
                        IncludeBeverageData = true,
                        IncludeMfyData = true,
                    });

                    Logger.Write(String.Format("Finished DailyDashboard - LoadDataAsync with date: {0}", selectedDate));
                    WcfConnection.DisconnectWcfService(service);
                    return serviceResult;
                }
                catch (Exception ex)
                {
                    Logger.Write(ex);
                    return null;
                }
            });

            LoadDataAsyncFinished(data);

            HideLoadingIndicator();
        }

        private void LoadDataAsyncFinished(DailyDashboardDataResponse result)
        {
            if (result == null)
            {
                return;
            }

            try
            {
                if (result.SalesWidgetData != null)
                {
                    _chartWidgetViewModel.UpdateData(result.SalesWidgetData);
                }

                if (result.HourlySalesWidgetData != null)
                {
                    _hourlyWidgetViewModel.UpdateData(result.HourlySalesWidgetData);
                }
#if !WINDOWS_APP
                if (result.SpeedOfServiceWidgetData != null)
                {
                    _speedOfServiceWidgetViewModel.UpdateData(result.SpeedOfServiceWidgetData);
                }
#endif
                if (result.BeverageCountData != null)
                {
                    _beverageProductionWidgetViewModel.UpdateData(result.BeverageCountData);
                }
            }
            catch (Exception e)
            {
                Logger.Instance.AddLog("Error when initializing widget:" + e.ToString());
                Logger.Write(e);
            }

            ModuleManager.Instance.RestartReloadTimer();
        }
        #endregion

        public override void TriggerDataLoading()
        {
            LoadData();
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args) { }

        public override void ApplicationClosing(NavigationHideCloseArgs args) { }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadData();
            }
        }
    }
}
