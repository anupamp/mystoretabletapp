﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel.Chart;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel
{
    public class ChartWidgetViewModel : ViewModelBase
    {
        private int _currentActiveType;
        public int CurrentActiveType
        {
            get { return _currentActiveType; }
            set
            {
                int tmpValue = _currentActiveType;
                if (SetValue(ref _currentActiveType, value))
                {
                    SelectCarouselItem();
                }
            }
        }

        private ChartItemViewModel _salesViewModel = new ChartItemViewModel(DailyDashboardReportSalesDataType.Sales);
        private ChartItemViewModel _guestViewModel = new ChartItemViewModel(DailyDashboardReportSalesDataType.GuestCount);
        private ChartItemViewModel _avgViewModel = new ChartItemViewModel(DailyDashboardReportSalesDataType.AvgCheck);

        #region Member - ItemViewModels
        private List<ChartItemViewModel> _itemViewModels;
        public List<ChartItemViewModel> ItemViewModels
        {
            get
            {
                if (_itemViewModels == null)
                {
                    _itemViewModels = new List<ChartItemViewModel>() { _salesViewModel, _guestViewModel, _avgViewModel };
                }
                return _itemViewModels;
            }
        }
        #endregion

        #region Member - ControlWidth
        private double _controlWidth;

        public double ControlWidth
        {
            get { return _controlWidth; }
            set { SetValue(ref _controlWidth, value); }
        }
        #endregion

        public ICommand NextCommand { get; set; }
        public ICommand PrevCommand { get; set; }
        public ObservableCollection<ScrollPageItemViewModel> CarouselPageItems { get; set; }

        public ChartWidgetViewModel()
        {
            CarouselPageItems = new ObservableCollection<ScrollPageItemViewModel>(new[]
            {
                new ScrollPageItemViewModel{IsActive = true}, 
                new ScrollPageItemViewModel(),             
                new ScrollPageItemViewModel()
            });
            NextCommand = new RelayCommand(() =>
            {
                if (OnSelectedActiveType != null)
                {
                    OnSelectedActiveType(1, null);
                }
            });

            PrevCommand = new RelayCommand(() =>
            {
                if (OnSelectedActiveType != null)
                {
                    OnSelectedActiveType(-1, null);
                }
            });
        }

        private void SelectCarouselItem()
        {
            var currentSelectedItem = CarouselPageItems.FirstOrDefault(x => x.IsActive);
            currentSelectedItem.IsActive = false;
            CarouselPageItems[CurrentActiveType].IsActive = true;
        }

        public void UpdateData(SalesWidgetData salesData)
        {
            _salesViewModel.Update(salesData);
            _guestViewModel.Update(salesData);
            _avgViewModel.Update(salesData);

            //#region Sales Widget

            //{
            //    decimal salesFreshCount = Math.Round(salesData.KioskTotalMetric, 2);
            //    decimal salesTogetherCount = Math.Round(salesData.DTTotalMetric, 2);
            //    decimal SalesConnectCount = Math.Round(salesData.FCTotalMetric, 2);

            //    _salesFreshPoint.Value = Math.Max((double)salesFreshCount, 0.0001);
            //    _salesTogetherPoint.Value = Math.Max((double)salesTogetherCount, 0.0001);
            //    _salesConnectPoint.Value = Math.Max((double)SalesConnectCount, 0.0001);

            //    //GuestCountStoreWideSales = (int)(salesFreshCount + salesTogetherCount + SalesConnectCount);
            //}

            //#endregion

            //#region Guests Widget

            //{
            //    decimal guestsFreshCount = Math.Round(guestsData.KioskTotalMetric, 2);
            //    decimal guestsTogetherCount = Math.Round(guestsData.DTTotalMetric, 2);
            //    decimal guestsConnectCount = Math.Round(guestsData.FCTotalMetric, 2);

            //    _guestsFreshPoint.Value = Math.Max((double)guestsFreshCount, 0.0001);
            //    _guestsTogetherPoint.Value = Math.Max((double)guestsTogetherCount, 0.0001);
            //    _guestsConnectPoint.Value = Math.Max((double)guestsConnectCount, 0.0001);

            //    //GuestCountStoreWideGuests = (int)(guestsFreshCount + guestsTogetherCount + guestsConnectCount);
            //}

            //#endregion

            //#region AvgChq Widget

            //{
            //    decimal avgChqFreshCount = Math.Round(avgChqData.KioskTotalMetric, 2);
            //    decimal avgChqTogetherCount = Math.Round(avgChqData.DTTotalMetric, 2);
            //    decimal avgChqConnectCount = Math.Round(avgChqData.FCTotalMetric, 2);

            //    _avgChqFreshPoint.Value = Math.Max((double)avgChqFreshCount, 0.0001);
            //    _avgChqTogetherPoint.Value = Math.Max((double)avgChqTogetherCount, 0.0001);
            //    _avgChqConnectPoint.Value = Math.Max((double)avgChqConnectCount, 0.0001);

            //    //GuestCountStoreWideAvgChq = (int)(avgChqFreshCount + avgChqTogetherCount + avgChqConnectCount);
            //}

            //#endregion

        }

        public static event EventHandler OnSelectedActiveType;
    }
}
