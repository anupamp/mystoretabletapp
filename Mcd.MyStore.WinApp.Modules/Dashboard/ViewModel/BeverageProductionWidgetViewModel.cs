﻿using DevExpress.Data;
using DevExpress.UI.Xaml.Charts;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Dashboard.View;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Mcd.MyStore.SpeedOfService.Common.DataContracts;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel
{
    public class BeverageProductionWidgetViewModel : ViewModelBase
    {
        private DateTime _lastDate;

        #region Member - DrinkModel
        private readonly BeverageProductionChartViewModel _drinkProductionLines = new BeverageProductionChartViewModel();
        public BeverageProductionChartViewModel DrinkModel
        {
            get { return _drinkProductionLines; }
        }
        #endregion

        #region Member - MfyModel
        private readonly BeverageProductionChartViewModel _mfyProductionLines = new BeverageProductionChartViewModel();
        public BeverageProductionChartViewModel MfyModel
        {
            get { return _mfyProductionLines; }
        }
        #endregion


        #region Member IsProductionLine1Checked
        private bool _isProductionLine1Checked = true;
        public bool IsProductionLine1Checked
        {
            get { return _isProductionLine1Checked; }
            set
            {
                if (SetValue<bool>(ref _isProductionLine1Checked, value))
                {
                    ChangeChart();
                }
                Notify("ProductionLine1Visibility");
                Notify("ProductionLine2Visibility");
            }
        }



        public Visibility ProductionLine1Visibility
        {
            get { return IsProductionLine1Checked ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility ProductionLine2Visibility
        {
            get { return !IsProductionLine1Checked ? Visibility.Visible : Visibility.Collapsed; }
        }
        #endregion

        public DailyDashboardBeverageMode Mode { get; set; }

        public ObservableCollection<LabelDescription> DrinkChartLabels { get; set; }
        public ObservableCollection<LabelDescription> MfyChartLabels { get; set; }

        public BeverageProductionWidgetViewModel()
        {
            DrinkChartLabels = new BaseObservableCollection<LabelDescription>();
            MfyChartLabels = new BaseObservableCollection<LabelDescription>();
        }

        public async void UpdateData(WWCBeverageWidgetData result)
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return;

            try
            {
                int drinkCount = result.DrinkSegments.Where(D => D.SandwichCountsPerDRKDESLine.Count > 0)
                    .DefaultIfEmpty(new DRKDESLineTimingSegment())
                    .Max(D => D.SandwichCountsPerDRKDESLine.Count);
                int mfyCount = result.MFYSegments.Where(D => D.SandwichCountsPerMfyLine.Count > 0)
                    .DefaultIfEmpty(new MfyLineTimingSegment())
                    .Max(D => D.SandwichCountsPerMfyLine.Count);


                if (_lastDate != SystemStatusManager.Instance.GlobalUserSelectedDay.Value)
                {
                    #region Init Data
                    _drinkProductionLines.Queues.Clear();
                    _mfyProductionLines.Queues.Clear();

                    for (int i = 1; i <= drinkCount; i++)
                    {
                        _drinkProductionLines.Queues.Add(i, new DataPointCollection());
                    }

                    for (int i = 1; i <= mfyCount; i++)
                    {
                        _mfyProductionLines.Queues.Add(i, new DataPointCollection());
                    }

                    await GenerateEmptyData(_drinkProductionLines);
                    await GenerateEmptyData(_mfyProductionLines);
                    #endregion

                    _lastDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;
                }

                #region DRINK
                if (result.DrinkSegments != null && drinkCount > 0)
                {
                    foreach (DRKDESLineTimingSegment item in result.DrinkSegments)
                    {
                        int queueID = 0;
                        foreach (var queue in item.SandwichCountsPerDRKDESLine)
                        {
                            queueID++;

                            DataPoint point = _drinkProductionLines.Queues[queueID].Points.FirstOrDefault(P => ((DateTime)P.Argument).Equals(item.Start));
                            if (point != null)
                            {
                                point.Value = queue.ItemCount;
                            }
                        }
                    }
                }
                #endregion

                #region MFY
                if (result.MFYSegments != null && mfyCount > 0)
                {
                    foreach (MfyLineTimingSegment item in result.MFYSegments)
                    {
                        int queueID = 0;
                        foreach (var queue in item.SandwichCountsPerMfyLine)
                        {
                            queueID++;

                            DataPoint point = _mfyProductionLines.Queues[queueID].Points.FirstOrDefault(P => ((DateTime)P.Argument).Equals(item.Start));
                            if (point != null)
                            {
                                point.Value = queue.Value;
                            }
                        }
                    }
                }
                #endregion

                ChangeChart();
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
        }

        private Task GenerateEmptyData(BeverageProductionChartViewModel model)
        {
            return Task.Factory.StartNew(() =>
            {
                DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

                SystemStatusForBusinessDayDto statusDto = SystemStatusManager.Instance.GetStatusForBusinessDay(selectedDate);
                if (statusDto == null)
                    return;

                DateTime start = statusDto.OpeningTime;
                DateTime end = statusDto.ClosingTime;
                for (DateTime i = start; i < end; i = i.AddHours(1))
                {
                    foreach (var item in model.Queues)
                    {
                        item.Value.Points.Add(new DataPoint { Argument = i, Value = 0 });
                    }
                }
            });
        }

        private void ChangeChart()
        {
            var drinkChart = (CartesianChart)BeverageProductionWidget.Instance.FindName("drinkChart");
            if (drinkChart == null) return;
            drinkChart.Series.Clear();

            var mfyChart = (CartesianChart)BeverageProductionWidget.Instance.FindName("mfyChart");
            if (mfyChart == null) return;
            mfyChart.Series.Clear();

            foreach (var queue in _drinkProductionLines.Queues)
            {
                drinkChart.Series.Add(new Series
                {
                    DisplayName = queue.Key.ToString(),
                    Data = queue.Value,
                    View = new LineSeriesView
                    {
                        ShowMarkers = true,
                        DataChangingAnimationMode = DataChangingAnimationMode.AnimateEntireSeries,
                        ShowToolTipOnSelectItem = true,
                        IsDoubleTapEnabled = false,
                        ToolTipPointPattern = "{A:t} - {V}"
                    }
                });
            }

            foreach (var queue in _mfyProductionLines.Queues)
            {
                mfyChart.Series.Add(new Series
                {
                    DisplayName = queue.Key.ToString(),
                    Data = queue.Value,
                    View = new LineSeriesView
                    {
                        ShowMarkers = true,
                        DataChangingAnimationMode = DataChangingAnimationMode.AnimateEntireSeries,
                        ShowToolTipOnSelectItem = true,
                        IsDoubleTapEnabled = false,
                        ToolTipPointPattern = "{A:t} - {V}"
                    }
                });
            }


            var colors = new[]
              {
                  new SolidColorBrush(Color.FromArgb(255,71,0,255)),
                  new SolidColorBrush(Color.FromArgb(255,0,219,44)),
                  new SolidColorBrush(Color.FromArgb(255,0,155,255)),
                  new SolidColorBrush(Color.FromArgb(255,255,174,69)),
                  new SolidColorBrush(Color.FromArgb(255,0,255,179)),
                  new SolidColorBrush(Color.FromArgb(255,255,51,51)),
              };

            DrinkChartLabels.Clear();
            for (int index = 0; index < drinkChart.Series.Count; index++)
            {
                var serie = drinkChart.Series[index];
                DrinkChartLabels.Add(new LabelDescription { QueueID = serie.DisplayName, FillColorBrush = colors[index] });
            }

            MfyChartLabels.Clear();
            for (int index = 0; index < mfyChart.Series.Count; index++)
            {
                var serie = mfyChart.Series[index];
                MfyChartLabels.Add(new LabelDescription { QueueID = serie.DisplayName, FillColorBrush = colors[index] });
            }
        }
    }
}