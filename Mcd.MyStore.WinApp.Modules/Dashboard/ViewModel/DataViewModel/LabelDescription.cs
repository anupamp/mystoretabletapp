﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel
{
    public class LabelDescription : ViewModelBase
    {
        public Brush FillColorBrush { get; set; }

        #region Member - QueueID
        private string _queueID;

        public string QueueID
        {
            get { return _queueID; }
            set
            {
                if (SetValue(ref _queueID, value))
                {
                    Text = String.Format(Translation["DASHBOARD_BeverageProductionQueueLine"], _queueID);
                }
            }
        }
        #endregion


        #region Member - Text
        private string _text;
        public string Text
        {
            get { return _text; }
            set { SetValue(ref _text, value); }
        }
        #endregion

    }
}