﻿using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel.Hourly;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.Hourly
{
    public class HourlyItemViewModel : ViewModelBase
    {
        public ObservableCollection<HourlyTopItemViewModel> LeftGuestList { get; set; }
        public ObservableCollection<HourlyTopItemViewModel> RightGuestList { get; set; }

        private string _decimalFormat = "0";

        #region Member - CountDailyTotal
        private string _countDailyTotal;
        public string CountDailyTotal
        {
            get { return _countDailyTotal; }
            set { SetValue(ref _countDailyTotal, value); }
        }
        #endregion

        #region Member - TotalVisbility
        private Visibility _totalVisbility = Visibility.Visible;
        public Visibility TotalVisbility
        {
            get { return _totalVisbility; }
            set { SetValue(ref _totalVisbility, value); }
        }
        #endregion


        #region Member - ActualTitleText
        public String ActualTitleText
        {
            get
            {
                switch (_dataType)
                {
                    case DailyDashboardReportSalesDataType.Sales:
                        return Translation["DASHBOARD_HourlyTitleSales"];

                    case DailyDashboardReportSalesDataType.GuestCount:
                        return Translation["DASHBOARD_HourlyTitleGuest"];

                    case DailyDashboardReportSalesDataType.AvgCheck:
                        return Translation["DASHBOARD_HourlyTitleAvg"];
                }
                return String.Empty;
            }
        }
        #endregion

        #region Member - ActualModeText
        public String ActualModeText
        {
            get
            {
                switch (_dataType)
                {
                    case DailyDashboardReportSalesDataType.Sales:
                        return Translation["DASHBOARD_ChartTitleSales"];

                    case DailyDashboardReportSalesDataType.GuestCount:
                        return Translation["DASHBOARD_ChartTitleGuests"];

                    case DailyDashboardReportSalesDataType.AvgCheck:
                        return Translation["DASHBOARD_ChartTitleAvgCheck"];
                }
                return String.Empty;
            }
        }
        #endregion

        #region Member - DataType
        private DailyDashboardReportSalesDataType _dataType = DailyDashboardReportSalesDataType.Sales;
        public DailyDashboardReportSalesDataType DataType
        {
            get { return _dataType; }
            set
            {
                if (SetValue(ref _dataType, value))
                {
                    Notify("ActualModeText");
                    Notify("ActualTitleText");
                }
            }
        }
        #endregion

        #region Member SelectedTimeItem
        private HourlyTopItemViewModel _selectedTimeItem;
        public HourlyTopItemViewModel SelectedTimeItem
        {
            get { return _selectedTimeItem; }
            set
            {
                if (_selectedTimeItem != value)
                {
                    if (_selectedTimeItem != null)
                    {
                        _selectedTimeItem.IsOpened = false;
                    }
                    _selectedTimeItem = value;
                }
                Notify();
            }
        }
        #endregion

        public HourlyItemViewModel(DailyDashboardReportSalesDataType dataType)
        {
            DataType = dataType;

            switch (_dataType)
            {
                case DailyDashboardReportSalesDataType.AvgCheck:
                case DailyDashboardReportSalesDataType.Sales:
                    _decimalFormat = "N2";
                    break;
            }

            LeftGuestList = new ObservableCollection<HourlyTopItemViewModel>();
            RightGuestList = new ObservableCollection<HourlyTopItemViewModel>();

            for (int i = 0; i < 20; i++)
            {
                LeftGuestList.Add(CreateTimeItem());
                RightGuestList.Add(CreateTimeItem());
            }
        }

        private void CreateMissingViewModels(SystemStatusForBusinessDayDto statusDto)
        {

            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
            {
                return;
            }

            int businessDayLength = (int)(statusDto.ClosingTime - statusDto.OpeningTime).TotalHours;
            int halfTimeline = (int)Math.Ceiling(businessDayLength / 2.0M);

            int leftCount = LeftGuestList.Count;

            for (int i = 0; i < leftCount; i++)
            {
                LeftGuestList[i].IsOpened = false;
                LeftGuestList[i].ItemVisibility = (i < halfTimeline ? Visibility.Visible : Visibility.Collapsed);
            }

            int rightMaxCount = (businessDayLength - halfTimeline);
            int rightCount = RightGuestList.Count;
            for (int i = 0; i < rightCount; i++)
            {
                RightGuestList[i].IsOpened = false;
                RightGuestList[i].ItemVisibility = (i < rightMaxCount ? Visibility.Visible : Visibility.Collapsed);
            }

        }

        private void OnIsOpenTapped(object sender, EventArgs e)
        {
            SelectedTimeItem = (HourlyTopItemViewModel)sender;
        }

        private HourlyTopItemViewModel CreateTimeItem()
        {


            HourlyTopItemViewModel result = new HourlyTopItemViewModel();
            result.Time = DateTime.Now.Date;
            result.ItemVisibility = Visibility.Collapsed;
            result.OnIsOpened += OnIsOpenTapped;
            result.DecimalFormat = _decimalFormat;

            result.Childs.Clear();
            for (int i = 0; i < 60; i += 15)
            {
                result.Childs.Add(new HourlyGuestItemViewModel
                {
                    Time = result.Time.AddMinutes(i),
                    ActualValue = 0,
                    ProjectedValue = 0,
                    DecimalFormat = _decimalFormat
                });
            }

            return result;
        }

        public async void UpdateData(HourlySalesWidgetData result)
        {
            if (result == null || !SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
            {
                return;
            }

            Dictionary<DateTime, HourlySalesWidgetEntry> hourlyGuestCountData = result.HourlyMetrics.Where(D => D.Key.Minute == 0)
                .ToDictionary(D => D.Key, D => new HourlySalesWidgetEntry()
                {
                    ActualSales = result.HourlyMetrics.Where(H => H.Key.Hour == D.Key.Hour).Sum(S => S.Value.ActualSales),
                    ProjectedSales = result.HourlyMetrics.Where(H => H.Key.Hour == D.Key.Hour).Sum(S => S.Value.ProjectedSales),
                    ActualGuests = result.HourlyMetrics.Where(H => H.Key.Hour == D.Key.Hour).Sum(S => S.Value.ActualGuests),
                    ProjectedGuests = result.HourlyMetrics.Where(H => H.Key.Hour == D.Key.Hour).Sum(S => S.Value.ProjectedGuests)
                });
            int dailyGuestCount = hourlyGuestCountData.Sum(H => H.Value.ActualGuests);


            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            SystemStatusForBusinessDayDto statusDto = await SystemStatusManager.Instance.GetOpeningAndClosingTimeForBusinessDayTask(selectedDate);
            if (statusDto == null)
                return;

            CreateMissingViewModels(statusDto);

            decimal totalCount = 0;
            int hourIndex = 0;
            int visibleLeftItems = LeftGuestList.Count(L => L.ItemVisibility == Visibility.Visible);
            int visibleRightItems = RightGuestList.Count(R => R.ItemVisibility == Visibility.Visible);
            List<HourlyTopItemViewModel> usesItems = new List<HourlyTopItemViewModel>();
            HourlyTopItemViewModel lastFoundItem = null;
            foreach (var currentHourlyItem in result.HourlyMetrics)
            {
                DateTime time = currentHourlyItem.Key;
                //if (time.Hour >= SystemStatusManager.Instance.EndHourForCurrentBusinessDay)
                //    continue;

                //if (time.Hour < SystemStatusManager.Instance.BeginHourForCurrentBusinessDay)
                //    continue;

                if (time.Minute == 0)
                {
                    if (hourIndex < visibleLeftItems)
                    {
                        lastFoundItem = LeftGuestList[hourIndex];
                    }
                    else if (hourIndex < visibleLeftItems + visibleRightItems)
                    {
                        lastFoundItem = RightGuestList[hourIndex - visibleLeftItems];
                    }
                    else
                    {
                        lastFoundItem = null;
                    }

                    if (lastFoundItem != null)
                    {
                        lastFoundItem.Time = time;
                        hourIndex++;
                        usesItems.Add(lastFoundItem);
                    }
                }



                decimal actualCount = 0;
                decimal projectedCount = 0;

                switch (_dataType)
                {
                    case DailyDashboardReportSalesDataType.Sales:
                        actualCount = currentHourlyItem.Value.ActualSales;
                        projectedCount = currentHourlyItem.Value.ProjectedSales;
                        break;
                    case DailyDashboardReportSalesDataType.GuestCount:
                        actualCount = currentHourlyItem.Value.ActualGuests;
                        projectedCount = currentHourlyItem.Value.ProjectedGuests;
                        break;
                    case DailyDashboardReportSalesDataType.AvgCheck:
                        actualCount = currentHourlyItem.Value.ActualAverageCheck;
                        projectedCount = currentHourlyItem.Value.ProjectedAverageCheck;
                        break;
                }

                totalCount += actualCount;

                if (lastFoundItem != null)
                {
                    int subIndex = (int)Math.Ceiling(time.Minute / 15M);

                    //foundItem.IsOpened = false;
                    HourlyGuestItemViewModel foundChildItem = lastFoundItem.Childs[subIndex];
                    foundChildItem.ActualValue = actualCount;
                    foundChildItem.ProjectedValue = projectedCount;
                    foundChildItem.Time = time;

                    if (_dataType == DailyDashboardReportSalesDataType.AvgCheck)
                    {
                        HourlySalesWidgetEntry hourlyGuestCount = null;
                        if (hourlyGuestCountData.TryGetValue(time, out hourlyGuestCount))
                        {
                            if (hourlyGuestCount.ActualGuests > 0)
                                lastFoundItem.SumActualValue = ((decimal)hourlyGuestCount.ActualSales / (decimal)hourlyGuestCount.ActualGuests);
                            if (hourlyGuestCount.ProjectedGuests > 0)
                                lastFoundItem.SumProjectedValue = ((decimal)hourlyGuestCount.ProjectedSales / (decimal)hourlyGuestCount.ProjectedGuests);
                        }
                    }
                    else
                    {
                        lastFoundItem.SumActualValue = lastFoundItem.Childs.Sum(x => x.ActualValue);
                        lastFoundItem.SumProjectedValue = lastFoundItem.Childs.Sum(x => x.ProjectedValue);
                    }

                }
            }

            if (dailyGuestCount > 0)
            {
                //calculate dailyCount
                if (_dataType == DailyDashboardReportSalesDataType.AvgCheck)
                {
                    CountDailyTotal = (result.HourlyMetrics.Sum(H => H.Value.ActualSales) / (decimal)dailyGuestCount).ToString(_decimalFormat);
                }
                else
                {
                    CountDailyTotal = totalCount.ToString(_decimalFormat);
                }
            }
        }
    }
}
