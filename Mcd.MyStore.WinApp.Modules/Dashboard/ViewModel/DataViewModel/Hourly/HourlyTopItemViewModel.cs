﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel.Hourly
{
    public class HourlyTopItemViewModel : HourlyGuestItemViewModel
    {

        #region Member - SumActualValue
        private decimal _sumActualValue;
        public decimal SumActualValue
        {
            get { return _sumActualValue; }
            set
            {

                if (SetValue(ref _sumActualValue, value))
                {
                    Notify("SumActualValueText");
                }
            }
        }
        public string SumActualValueText
        {
            get { return _sumActualValue.ToString(DecimalFormat); }
        }
        #endregion

        #region Member - SumProjectedValue
        private decimal _sumProjectedValue;
        public decimal SumProjectedValue
        {
            get { return _sumProjectedValue; }
            set
            {
                if (SetValue(ref _sumProjectedValue, value))
                {
                    Notify("SumProjectedValueText");
                }
            }
        }
        public string SumProjectedValueText
        {
            get { return _sumProjectedValue.ToString(DecimalFormat); }
        }
        #endregion

        #region Member Height
        public int Height
        {
            get { return (IsOpened ? 140 : 27); }
        }
        #endregion

        public event EventHandler OnIsOpened;

        #region Member IsOpened
        private bool _isOpened;
        public bool IsOpened
        {
            get { return _isOpened; }
            set
            {
                if (SetValue<bool>(ref _isOpened, value))
                {
                    Notify("Height");
                    Notify("PlusVisbility");
                    Notify("MinusVisbility");
                    if (_isOpened && OnIsOpened != null)
                    {
                        OnIsOpened(this, null);
                    }
                }
            }
        }
        #endregion

        #region ItemVisibility
        private Visibility _itemVisibility;

        public Visibility ItemVisibility
        {
            get { return _itemVisibility; }
            set { SetValue(ref _itemVisibility, value); }
        }
        #endregion


        #region Member PlusVisbility
        public Visibility PlusVisbility
        {
            get { return IsOpened ? Visibility.Collapsed : Visibility.Visible; }
        }
        #endregion

        #region Member MinusVisbility
        public Visibility MinusVisbility
        {
            get { return !IsOpened ? Visibility.Collapsed : Visibility.Visible; }
        }
        #endregion

        public RelayCommand ToggleOpenCommand { get; set; }


        public ObservableCollection<HourlyGuestItemViewModel> Childs { get; set; }

        public HourlyTopItemViewModel()
        {
            Childs = new ObservableCollection<HourlyGuestItemViewModel>();
            ToggleOpenCommand = new RelayCommand(() =>
            {
                IsOpened = !IsOpened;
            });
        }

    }

    public class HourlyGuestItemViewModel : ViewModelBase
    {
        #region Member - Time
        private DateTime _time = DateTime.MinValue;
        public DateTime Time
        {
            get { return _time; }
            set
            {
                SetValue<DateTime>(ref _time, value);
                Notify("TimeText");
            }
        }
        #endregion

        public string TimeText
        {
            get { return _time.ToString("HH:mm"); }
        }

        #region Member - DecimalFormat
        private string _decimalFormat = "N2";
        public string DecimalFormat
        {
            get { return _decimalFormat; }
            set { SetValue(ref _decimalFormat, value); }
        }
        #endregion

        #region Member - ActualValue
        private decimal _actualValue = 0;
        public decimal ActualValue
        {
            get { return _actualValue; }
            set
            {
                if (SetValue(ref _actualValue, value))
                {
                    Notify("ActualValueFormatted");
                }
            }
        }
        #endregion
        #region Member - ActualValueFormatted
        public string ActualValueFormatted
        {
            get { return ActualValue.ToString(DecimalFormat); }
        }
        #endregion

        #region Member - ProjectedValue
        private decimal _projectedValue = 0;
        public decimal ProjectedValue
        {
            get { return _projectedValue; }
            set
            {
                if (SetValue(ref _projectedValue, value))
                {
                    Notify("ProjectedValueFormatted");
                }
            }
        }
        #endregion
        #region Member - ProjectedValueFormatted
        public string ProjectedValueFormatted
        {
            get { return ProjectedValue.ToString(DecimalFormat); }
        }
        #endregion
    }
}
