﻿using DevExpress.UI.Xaml.Charts;
using Mcd.MyStore.WinApp.Core.MVVM;
using System.Collections.Generic;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel
{
    public class BeverageProductionChartViewModel : ViewModelBase
    {
        public Dictionary<int, DataPointCollection> Queues { get; set; }

        public BeverageProductionChartViewModel()
        {
            Queues = new Dictionary<int, DataPointCollection>();
        }
    }
}
