﻿using DevExpress.UI.Xaml.Charts;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel.Chart
{
    public class ChartItemViewModel : ViewModelBase
    {
        #region Member - DecimalFormat
        private string _decimalFormat = "0";
        public string DecimalFormat
        {
            get { return _decimalFormat; }
        }
        #endregion

        public DataPointCollection ChartItems { get; set; }

        #region Member - DataType
        private DailyDashboardReportSalesDataType _dataType = DailyDashboardReportSalesDataType.Sales;
        public DailyDashboardReportSalesDataType DataType
        {
            get { return _dataType; }
            set
            {
                if (SetValue(ref _dataType, value))
                {
                    Notify("ActualTitleText");
                }
            }
        }
        #endregion

        #region Member - ActualTitleText
        public String ActualTitleText
        {
            get
            {
                switch (_dataType)
                {
                    case DailyDashboardReportSalesDataType.Sales:
                        return Translation["DASHBOARD_ChartTitleSales"];

                    case DailyDashboardReportSalesDataType.GuestCount:
                        return Translation["DASHBOARD_ChartTitleGuests"];

                    case DailyDashboardReportSalesDataType.AvgCheck:
                        return Translation["DASHBOARD_ChartTitleAvgCheck"];
                }
                return String.Empty;
            }
        }
        #endregion

        #region Member - StoreWideValue
        private decimal _storeWideValue;
        public decimal StoreWideValue
        {
            get { return _storeWideValue; }
            set
            {
                if (SetValue(ref _storeWideValue, value))
                {
                    Notify("StoreWideValueText");
                }
            }
        }
        #endregion

        #region Member - StoreWideValueText
        public string StoreWideValueText
        {
            get { return _storeWideValue.ToString(_decimalFormat); }
        }
        #endregion

        #region Member - AnimationMode
        private DataChangingAnimationMode _animationMode = DataChangingAnimationMode.Disabled;
        public DataChangingAnimationMode AnimationMode
        {
            get { return _animationMode; }
            set { SetValue(ref _animationMode, value); }
        }
        #endregion


        public ChartItemViewModel(DailyDashboardReportSalesDataType dataType)
        {
            DataType = dataType;

            switch (_dataType)
            {
                case DailyDashboardReportSalesDataType.AvgCheck:
                case DailyDashboardReportSalesDataType.Sales:
                    _decimalFormat = "N2";
                    break;
            }

            ChartItems = new DataPointCollection();
            ChartItems.Add(new DataPoint(LanguageManager.Instance["BASIC_NO_DATA"], 0.0001));
        }

        public void Update(SalesWidgetData data)
        {
            AnimationMode = DataChangingAnimationMode.AnimateEntireSeries;
            ChartItems.Clear();

            foreach (var item in data.PODWiseData)
            {
                switch (_dataType)
                {
                    case DailyDashboardReportSalesDataType.Sales:
                        if (item.Sales == 0.0M) break;
                        ChartItems.Add(new DataPoint(item.PODName, (double)item.Sales));
                        break;
                    case DailyDashboardReportSalesDataType.GuestCount:
                        if (item.GuestCount == 0.0M) break;
                        ChartItems.Add(new DataPoint(item.PODName, (double)item.GuestCount));
                        break;
                    case DailyDashboardReportSalesDataType.AvgCheck:
                        if (item.AverageCheck == 0.0M) break;
                        ChartItems.Add(new DataPoint(item.PODName, (double)item.AverageCheck));
                        break;
                }
            }

            switch (_dataType)
            {
                case DailyDashboardReportSalesDataType.Sales:
                    StoreWideValue = data.StorewideSales;
                    break;
                case DailyDashboardReportSalesDataType.GuestCount:
                    StoreWideValue = data.StorewideGuestCount;
                    break;
                case DailyDashboardReportSalesDataType.AvgCheck:
                    StoreWideValue = data.StorewideAverageCheck;
                    break;
            }
        }

    }
}
