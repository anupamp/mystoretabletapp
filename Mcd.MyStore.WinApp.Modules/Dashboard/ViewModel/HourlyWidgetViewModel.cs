﻿using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.Hourly;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel
{
    public class HourlyWidgetViewModel : ViewModelBase
    {
        private int _currentActiveType;
        public int CurrentActiveType
        {
            get { return _currentActiveType; }
            set
            {
                int tmpValue = _currentActiveType;
                if (SetValue(ref _currentActiveType, value))
                {
                    SelectCarouselItem();
                }
            }
        }

        private HourlyItemViewModel _salesViewModel = new HourlyItemViewModel(DailyDashboardReportSalesDataType.Sales);
        private HourlyItemViewModel _guestViewModel = new HourlyItemViewModel(DailyDashboardReportSalesDataType.GuestCount);
        private HourlyItemViewModel _avgViewModel = new HourlyItemViewModel(DailyDashboardReportSalesDataType.AvgCheck);


        #region Member - ItemViewModels
        private List<HourlyItemViewModel> _itemViewModels;
        public List<HourlyItemViewModel> ItemViewModels
        {
            get
            {
                if (_itemViewModels == null)
                {
                    _itemViewModels = new List<HourlyItemViewModel>() { _salesViewModel, _guestViewModel, _avgViewModel };
                }
                return _itemViewModels;
            }
        }
        #endregion

        #region Member - ControlWidth
        private double _controlWidth;
        public double ControlWidth
        {
            get { return _controlWidth; }
            set { SetValue(ref _controlWidth, value); }
        }
        #endregion

        public ICommand NextCommand { get; set; }
        public ICommand PrevCommand { get; set; }
        public ObservableCollection<ScrollPageItemViewModel> CarouselPageItems { get; set; }

        public static event EventHandler OnSelectedActiveType;

        public HourlyWidgetViewModel()
        {
            CarouselPageItems = new ObservableCollection<ScrollPageItemViewModel>(new[]
            {
                new ScrollPageItemViewModel{IsActive = true}, 
                new ScrollPageItemViewModel(),
                new ScrollPageItemViewModel()
            });
            NextCommand = new RelayCommand(() =>
            {
                OnSelectedActiveType(1, null);
            });

            PrevCommand = new RelayCommand(() =>
            {
                OnSelectedActiveType(-1, null);
            });
        }

        private void SelectCarouselItem()
        {
            var currentSelectedItem = CarouselPageItems.FirstOrDefault(x => x.IsActive);
            currentSelectedItem.IsActive = false;
            CarouselPageItems[CurrentActiveType].IsActive = true;
        }

        public void UpdateData(HourlySalesWidgetData result)
        {
            _salesViewModel.UpdateData(result);
            _guestViewModel.UpdateData(result);
            _avgViewModel.UpdateData(result);
        }


    }
}
