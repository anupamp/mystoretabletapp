﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.Dashboard.Controls
{
    public sealed partial class HourlyItemGuestControl : UserControl
    {
        public HourlyItemGuestControl()
        {
            this.InitializeComponent();

            Tapped += HourlyItemGuestControl_Tapped;
        }

        private void HourlyItemGuestControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //ScrollViewer scroller = GetVisualParent<ScrollViewer>(this);
            //scroller.ChangeView(null, 1000, null);
        }

        public T GetVisualParent<T>(object childObject) where T : UIElement
        {
            DependencyObject child = childObject as DependencyObject;
            // iteratively traverse the visual tree
            while ((child != null) && !(child is T))
            {
                child = VisualTreeHelper.GetParent(child);
            }
            return child as T;
        }
    }
}
