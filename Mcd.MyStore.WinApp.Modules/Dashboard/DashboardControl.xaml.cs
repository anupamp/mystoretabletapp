﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.Dashboard
{
    [MenuModuleAttribute("Dashboard")]
    public sealed partial class DashboardControl : UserControl
    {
        private DashboardPageViewModel _viewModel = null;

        public DashboardControl()
        {
            this.InitializeComponent();
            _viewModel = new DashboardPageViewModel();
            DataContext = _viewModel;
        }
    }
}
