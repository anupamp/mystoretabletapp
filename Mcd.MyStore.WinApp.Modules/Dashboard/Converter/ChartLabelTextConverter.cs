﻿using System;
using Windows.UI.Xaml.Data;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.Converter
{
    public class ChartLabelTextConverter : IValueConverter
    {
        bool _firstPart = true;
        public bool FirstPart
        {
            get { return _firstPart; }
            set { _firstPart = value; }
        }
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string requestObject = (string)value;
            string[] data = requestObject.Split('#');

            if (data.Length != 2)
            {
                return String.Empty;
            }

            if (!_firstPart)
            {
                decimal tmpValue;
                if (decimal.TryParse(data[1], out tmpValue))
                {
                    if (tmpValue % 1 == 0)
                    {
                        return tmpValue.ToString("N0");
                    }
                    return tmpValue.ToString("N2");
                }
            }

            return data[0];
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
