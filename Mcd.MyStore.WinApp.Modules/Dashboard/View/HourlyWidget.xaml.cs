﻿using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel;
using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel.DataViewModel.Hourly;
using System;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.View
{
    public sealed partial class HourlyWidget
    {
        public HourlyWidget()
        {
            InitializeComponent();
            HourlyWidgetViewModel.OnSelectedActiveType += InstanceOnOnDataTypeChanged;
        }

        private void InstanceOnOnDataTypeChanged(object sender, EventArgs args)
        {
            HourlyWidgetViewModel viewModel = (HourlyWidgetViewModel)DataContext;
            ScrollViewer scroller = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;

            int oldIndex = viewModel.CurrentActiveType;
            int newIndex = viewModel.CurrentActiveType + (int)sender;
            newIndex = newIndex % 3;

            if (Math.Abs(oldIndex - newIndex) == 1)
            {
                scroller.ChangeView(viewModel.ControlWidth * newIndex, null, null);
            }
        }

        private async void LeftGuestList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView listView = (ListView)sender;

            listView.SelectedItem = null;

            if (e.AddedItems.Count == 0)
            {
                return;
            }

            // wait for animation is finished
            await Task.Delay(150);

            try
            {
                #region calculate new scroll position
                HourlyTopItemViewModel item = (HourlyTopItemViewModel)e.AddedItems[0];
                var scrollViewer = listView.GetFirstDescendantOfType<ScrollViewer>();
                var listViewItem = (FrameworkElement)listView.ContainerFromItem(item);

                // Calculations relative to the ScrollViewer within the ListView
                if (item.IsOpened)
                {
                    var topLeft = listViewItem.TransformToVisual(listView).TransformPoint(new Point()).Y;
                    int openHeight = item.Height;

                    if (topLeft + openHeight >= listView.ActualHeight)
                    {
                        double diff = topLeft + openHeight - listView.ActualHeight;
                        var currentOffset = scrollViewer.VerticalOffset;
                        scrollViewer.ChangeView(null, currentOffset + diff, null);
                    }
                }
                #endregion
            }
            catch { }
        }

        public T GetVisualParent<T>(object childObject) where T : FrameworkElement
        {
            DependencyObject child = childObject as DependencyObject;
            // iteratively traverse the visual tree
            while ((child != null) && !(child is T))
            {
                child = VisualTreeHelper.GetParent(child);
            }
            return child as T;
        }

        private void Container_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            HourlyWidgetViewModel viewModel = (HourlyWidgetViewModel)DataContext;
            viewModel.ControlWidth = e.NewSize.Width;
        }

        private void ContainerScroller_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (!e.IsIntermediate)
            {
                HourlyWidgetViewModel viewModel = (HourlyWidgetViewModel)DataContext;
                ScrollViewer scroller = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;

                viewModel.CurrentActiveType = (int)Math.Ceiling(scroller.HorizontalOffset / viewModel.ControlWidth);
            }
        }

    }
}
