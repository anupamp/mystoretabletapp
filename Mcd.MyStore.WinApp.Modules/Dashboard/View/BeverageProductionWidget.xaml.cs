﻿using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.View
{
    public sealed partial class BeverageProductionWidget : UserControl
    {
        public static BeverageProductionWidget Instance { get; set; }

        public BeverageProductionWidget()
        {
            Instance = this;
            this.InitializeComponent();
        }
    }
}
