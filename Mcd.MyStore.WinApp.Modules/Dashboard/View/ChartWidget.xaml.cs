﻿using Mcd.MyStore.WinApp.Modules.Dashboard.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.Dashboard.View
{
    public sealed partial class ChartWidget
    {
        public ChartWidget()
        {
            InitializeComponent();

            ChartWidgetViewModel.OnSelectedActiveType += InstanceOnOnDataTypeChanged;
        }

        private void InstanceOnOnDataTypeChanged(object sender, EventArgs args)
        {
            ChartWidgetViewModel viewModel = (ChartWidgetViewModel)DataContext;
            ScrollViewer scroller = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;

            int oldIndex = viewModel.CurrentActiveType;
            int newIndex = viewModel.CurrentActiveType + (int)sender;
            newIndex = newIndex % 3;

            if (Math.Abs(oldIndex - newIndex) == 1)
            {
                scroller.ChangeView(viewModel.ControlWidth * newIndex, null, null);
            }
        }

        private void Container_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ChartWidgetViewModel viewModel = (ChartWidgetViewModel)DataContext;
            viewModel.ControlWidth = e.NewSize.Width;
        }

        private void ContainerScroller_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            if (!e.IsIntermediate)
            {
                ChartWidgetViewModel viewModel = (ChartWidgetViewModel)DataContext;
                ScrollViewer scroller = (ScrollViewer)VisualTreeHelper.GetChild(Container, 0) as ScrollViewer;

                viewModel.CurrentActiveType = (int)Math.Ceiling(scroller.HorizontalOffset / viewModel.ControlWidth);
            }
        }
    }
}
