﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter
{
    [MenuModule("ControlCenter")]
    public sealed partial class ControlCenterPage
    {
        public ControlCenterPage()
        {
            InitializeComponent();
            DataContext = ViewModel = new ControlCenterPageViewModel();
        }

        public ControlCenterPageViewModel ViewModel { get; set; }
    }
}
