﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter
{
    public class ControlCenterTemplateSelector : DataTemplateSelector
    {
        public DataTemplate UserMessageTemplate { get; set; }
        public DataTemplate WorkflowTemplate { get; set; }
        public DataTemplate CheckTemplate { get; set; }
        public DataTemplate LegacyScreenTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item is UserMessageViewModel)
                return UserMessageTemplate;
            if (item is OpenCloseWorkflowModel)
                return WorkflowTemplate;
            if (item is OpenCloseCheckViewModel)
                return CheckTemplate;
            if (item is LegacyScreenOutputMessageViewModel)
                return LegacyScreenTemplate;
            return null;
        }
    }
}