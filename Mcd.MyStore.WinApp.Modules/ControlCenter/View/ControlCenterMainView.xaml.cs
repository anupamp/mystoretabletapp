﻿using System;
using System.Linq;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.View
{
    public sealed partial class ControlCenterMainView
    {
        public ControlCenterMainView()
        {
            InitializeComponent();
        }

        public ControlCenterPageViewModel ViewModel { get { return DataContext as ControlCenterPageViewModel; } }

        private void LeftControl_OnLayoutUpdated(object sender, object e)
        {
            var detailGrids = this.GetDescendantsOfType<Grid>().Where(x => x.Name == "DetailsGrid").ToList();
            foreach (var detailGrid in detailGrids)
            {
                var leftControl = detailGrid.GetDescendantsOfType<ItemsControl>().FirstOrDefault(x => x.Name == "LeftControl");
                var rightControl = detailGrid.GetDescendantsOfType<ScrollViewer>().FirstOrDefault(x => x.Name == "RightControl");

                leftControl.UpdateLayout();
                rightControl.Height = leftControl.ActualHeight;
            }
        }

        private void LegacyScreenFullScreenPreviewMouseDown(object sender, PointerRoutedEventArgs e)
        {
            ViewModel.LargeLegacyScreenVisible = Visibility.Collapsed;
            if (ViewModel.CurrentActiveWorkflowItem != null)
            {
                ViewModel.CurrentActiveWorkflowItem.ShowLegacyScreenPreview = true;
            }
        }

        private void LegacyScrollViewerMouseDown(object sender, PointerRoutedEventArgs e)
        {
            ViewModel.LargeLegacyScreenVisible = ViewModel.LargeLegacyScreenVisible == Visibility.Visible
                ? Visibility.Collapsed : Visibility.Visible;
            ViewModel.CurrentActiveWorkflowItem.ShowLegacyScreenPreview = ViewModel.LargeLegacyScreenVisible == Visibility.Collapsed;
        }
    }
}
