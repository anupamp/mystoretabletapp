﻿

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.View
{
    /// <summary>
    /// Interaction logic for WeatherAndCommentDialog.xaml
    /// </summary>
    public partial class OverridePrerequisiteCheckDialog : IPopupDialogContent
    {
        public OverridePrerequisiteCheckDialog()
        {
            InitializeComponent();
            UsernameInput.Focus(FocusState.Keyboard);
        }

        private void Password_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var context = DataContext as OverridePrerequisiteCheckDialogViewModel;
            context.PasswordInput = (sender as PasswordBox).Password;
        }

        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox s = (sender as TextBox);
            if (s != null && s.Text.Length == 3)
            {
                PasswordBox.Focus(FocusState.Keyboard);
            }
        }

        public PopupDialogContentViewModel ViewModel { get; set; }
    }
}
