﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.View
{
    public partial class WeatherAndCommentDialog : IPopupDialogContent
    {
        public WeatherAndCommentDialog()
        {
            InitializeComponent();
            DataContext = WeatherViewModel = new WeatherAndCommentDialogViewModel();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
        public WeatherAndCommentDialogViewModel WeatherViewModel
        {
            get { return ViewModel as WeatherAndCommentDialogViewModel; }
            set { ViewModel = value; }
        }

        private void SelectAllInTextBox(object sender, RoutedEventArgs e)
        {
            TextBox senderTextBox = sender as TextBox;
            if (senderTextBox == null) return;

            senderTextBox.SelectAll();
        }

        private void WeatherTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                WeatherViewModel.WeatherText = textBox.Text;
            }
        }

        private void CommentTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null)
            {
                WeatherViewModel.CommentText = textBox.Text;
            }
        }
    }
}
