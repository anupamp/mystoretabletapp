﻿using Windows.UI.Xaml;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.View
{
    /// <summary>
    /// Interaction logic for ConfirmUserInputDialog.xaml
    /// </summary>
    public partial class ConfirmUserInputDialog : IPopupDialogContent
    {
        public ConfirmUserInputDialog()
        {
            InitializeComponent();
            DataContext = ViewModel = new ConfirmUserInputDialogViewModel();
        }

        public PopupDialogContentViewModel ViewModel
        {
            set { DataContext = value; }
            get { return (PopupDialogContentViewModel)DataContext; }
        }
    }
}
