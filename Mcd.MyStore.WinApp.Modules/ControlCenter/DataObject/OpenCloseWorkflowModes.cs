﻿using System;
using Mcd.MyStore.OpenCloseService.Common.Dto;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.DataObject
{
    public class OpenCloseWorkflowModes
    {
        public String DisplayName { get; set; }
        public OpenCloseWorkflowModeType Mode { get; set; }
    }
}
