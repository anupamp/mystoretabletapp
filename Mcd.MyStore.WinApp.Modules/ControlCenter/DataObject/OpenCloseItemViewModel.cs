﻿using System;
using System.Windows.Input;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using DevExpress.Mvvm;
using Mcd.MyStore.OpenCloseService.Common.Dto;
using Mcd.MyStore.WinApp.Core.UI.Common;
using ViewModelBase = Mcd.MyStore.WinApp.Core.MVVM.ViewModelBase;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject
{
    public class OpenCloseItemViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public string TypeName { get; set; }

        #region Member - DisplayName
        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetValue(ref _displayName, value, "DisplayName"); }
        }
        #endregion

        #region Member - Description
        private UserMessageViewModel _description;
        public UserMessageViewModel Description
        {
            get { return _description; }
            set
            {
                if (SetValue(ref _description, value, "Description"))
                {
                    DetailExpanderVisbility = IsSuccessful || _description == null || String.IsNullOrEmpty(_description.MessageText) ? Visibility.Collapsed : Visibility.Visible;
                }
            }
        }
        #endregion

        #region Member - IsSuccessful
        private bool _isSuccessful;
        public bool IsSuccessful
        {
            get { return _isSuccessful; }
            set
            {
                if (SetValue(ref _isSuccessful, value, "IsSuccessful"))
                {
                    Notify("StatusColor");
                    DetailExpanderVisbility = IsSuccessful || _description == null || String.IsNullOrEmpty(_description.MessageText) ? Visibility.Collapsed : Visibility.Visible;
                }
            }
        }
        #endregion

        #region Member - RunAtLeastOnce

        private bool _runAtLeastOnce;
        public bool RunAtLeastOnce
        {
            get { return _runAtLeastOnce; }
            set { SetValue(ref _runAtLeastOnce, value, "RunAtLeastOnce"); }
        }

        #endregion

        #region Member - DetailExpanderVisbility
        private Visibility _detailExpanderVisbility = Visibility.Collapsed;
        public Visibility DetailExpanderVisbility
        {
            get { return _detailExpanderVisbility; }
            set { SetValue(ref _detailExpanderVisbility, value, "DetailExpanderVisbility"); }
        }
        #endregion

        #region Member - DetailVisibility
        private Visibility _detailVisibility = Visibility.Collapsed;
        public Visibility DetailVisibility
        {
            get { return _detailVisibility; }
            set
            {
                if (SetValue(ref _detailVisibility, value, "DetailVisibility"))
                {
                    Notify("ExpandImage");
                }
            }
        }
        #endregion

        #region Member - ExpandImage
        private BitmapImage _collapsedImage = new BitmapImage(new Uri("ms-appx:///Assets/detail_circle_with_plus.png"));
        private BitmapImage _expandImage = new BitmapImage(new Uri("ms-appx:///Assets/detail_circle_with_minus.png"));
        public BitmapImage ExpandImage
        {
            get
            {
                return (_detailVisibility == Visibility.Collapsed ? _collapsedImage : _expandImage);
            }
        }
        #endregion

        #region Member - StatusColor
        public SolidColorBrush StatusColor
        {
            get
            {
                if (_isSuccessful)
                {
                    return new SolidColorBrush(Color.FromArgb(255, 0, 172, 124));
                }
                return new SolidColorBrush(Color.FromArgb(255, 239, 63, 88));
            }
        }
        #endregion

        #region CriticalCheckFailed

        public bool CriticalCheckFailed
        {
            get
            {
                return AllowManagerOverride
                    && (Importance == PrerequisiteImportance.Critical || Importance == PrerequisiteImportance.CriticalWithStop)
                    && !IsSuccessful
                    && RunAtLeastOnce;
            }
        }

        #endregion

        #region Member - AllowManagerOverride

        private bool _allowManagerOverride;
        public bool AllowManagerOverride
        {
            get { return _allowManagerOverride; }
            set
            {
                if (SetValue(ref _allowManagerOverride, value, "AllowManagerOverride"))
                {
                    Notify("StatusColor");
                    Notify("StatusTextVisibility");
                    Notify("StatusCursor");
                    Notify("CriticalCheckFailed");
                }
            }
        }

        #endregion

        #region Member - Importance

        public PrerequisiteImportance Importance { get; set; }

        #endregion

        #region Member - MessageOnFailed

        public string MessageOnFailed { get; set; }

        #endregion


        #region Member - IsRunning
        private bool _isRunning;

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                if (SetValue(ref _isRunning, value, "IsRunning"))
                {
                    Notify("RunningStatusVisibility");
                }
            }
        }
        #endregion

        #region Member - IsCritical

        private bool _isCritical;
        public bool IsCritical
        {
            get { return _isCritical; }
            set
            {
                if (SetValue(ref _isCritical, value, "IsCritical"))
                {
                    //
                }
            }
        }

        #endregion


        #region Member - RunningStatusVisibility
        public Visibility RunningStatusVisibility
        {
            get { return (_isRunning ? Visibility.Visible : Visibility.Collapsed); }
        }
        #endregion




        public ICommand ToggleDetailVisibilityCommand { get; set; }

        public OpenCloseItemViewModel()
        {
            ToggleDetailVisibilityCommand = new RelayCommand(ToggleDetailVisibilityClick);
        }

        public void SetDetailVisbility(bool showDetails)
        {
            DetailVisibility = (showDetails && _description != null && !String.IsNullOrEmpty(_description.MessageText) ? Visibility.Visible : Visibility.Collapsed);
        }

        private void ToggleDetailVisibilityClick()
        {
            if (_detailExpanderVisbility != Visibility.Visible)
                return;

            DetailVisibility = (_detailVisibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed);
        }
    }
}
