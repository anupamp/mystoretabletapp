﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.View;
using Mcd.MyStore.OpenCloseService.Common.Dto;
using Mcd.MyStore.OpenCloseService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.ControlCenter.View;
using Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject
{
    public class OpenCloseWorkflowModel : ViewModelBase
    {
        #region All Members
        public ControlCenterPageViewModel ControlCenterViewModel { get; set; }

        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value, "Name"); }
        }
        #endregion

        #region Member - DisplayName
        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetValue(ref _displayName, value, "DisplayName"); }
        }
        #endregion

        #region Member - WorkflowStatus

        private WorkflowStatusType _workflowStatus = WorkflowStatusType.NotReady;
        public WorkflowStatusType WorkflowStatus
        {
            get { return _workflowStatus; }
            set
            {
                if (SetValue(ref _workflowStatus, value, "WorkflowStatus"))
                {
                    Notify("WorkflowStatusText");
                    Notify("StatusColor");
                    Notify("ProgressVisibility");
                    Notify("StartButtonVisibility");
                    Notify("OverrideButtonVisibility");
                    Notify("WeatherButtonVisibility");
                    Notify("ConfirmedClose");
                }
            }
        }
        public string WorkflowStatusText
        {
            get { return Translation["KEY_OpenClose_WorkflowStatus_" + _workflowStatus.ToString()]; }
        }
        #endregion

        #region Member - ProgressVisibility
        public Visibility ProgressVisibility
        {
            get
            {
                return (_workflowStatus == WorkflowStatusType.Working || _workflowStatus == WorkflowStatusType.Finished ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        #endregion

        #region Member - StartButtonVisibility
        public Visibility StartButtonVisibility
        {
            get
            {
                return (_workflowStatus == WorkflowStatusType.Ready ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        #endregion

        #region Member - WorkstepsProgress
        private double _workstepsProgress;
        public double WorkstepsProgress
        {
            get { return _workstepsProgress; }
            set
            {
                if (SetValue(ref _workstepsProgress, value, "WorkstepsProgress"))
                {
                    Notify("CurrentWorkstepText");
                }
            }
        }
        #endregion

        #region Member - CurrentWorkstepName
        private string _currentWorkstepName;
        public string CurrentWorkstepName
        {
            get { return _currentWorkstepName; }
            set
            {
                if (SetValue(ref _currentWorkstepName, value, "CurrentWorkstepName"))
                {
                    Notify("CurrentWorkstepText");
                }
            }
        }
        #endregion

        #region Member - CurrentWorkstepText
        public string CurrentWorkstepText
        {
            get
            {
                if (_workstepsProgress >= 100)
                {
                    return Translation["KEY_OpenCloseControlCenter_WorkstepsFinished"];
                }
                if (String.IsNullOrEmpty(_currentWorkstepName))
                {
                    return Translation["KEY_OpenCloseControlCenter_WorkstepsNotRunning"];
                }
                return _currentWorkstepName + " " + Translation["KEY_OpenCloseControlCenter_CurrentWorkstepInProgress"];
            }
        }
        #endregion

        #region Member - StatusColor
        public SolidColorBrush StatusColor
        {
            get
            {
                switch (_workflowStatus)
                {
                    case WorkflowStatusType.NotReady:
                        return new SolidColorBrush(Color.FromArgb(255, 239, 63, 88));

                    case WorkflowStatusType.Ready:
                        return new SolidColorBrush(Color.FromArgb(255, 249, 165, 84));

                    case WorkflowStatusType.Working:
                        return new SolidColorBrush(Color.FromArgb(255, 255, 211, 0));

                    case WorkflowStatusType.Finished:
                        return new SolidColorBrush(Color.FromArgb(255, 0, 172, 124));
                }
                return new SolidColorBrush(Color.FromArgb(255, 211, 211, 211));
            }
        }
        #endregion

        #region Member - DetailVisibility
        private Visibility _detailVisibility = Visibility.Collapsed;
        public Visibility DetailVisibility
        {
            get { return _detailVisibility; }
            set
            {
                if (SetValue(ref _detailVisibility, value, "DetailVisibility"))
                {
                    Notify("ExpandImage");
                }
            }
        }
        #endregion

        #region Member - ExpandImage
        private BitmapImage _collapsedImage = new BitmapImage(new Uri("ms-appx:///Assets/circle_with_plus.png"));
        private BitmapImage _expandImage = new BitmapImage(new Uri("ms-appx:///Assets/circle_with_minus.png"));
        public BitmapImage ExpandImage
        {
            get
            {
                return (_detailVisibility == Visibility.Collapsed ? _collapsedImage : _expandImage);
            }
        }
        #endregion

        #region Member - StartButton

        private bool _startButtonIsEnabled = false;
        public bool StartButtonIsEnabled
        {
            get { return _startButtonIsEnabled; }
            set
            {
                SetValue(ref _startButtonIsEnabled, value, "StartButtonIsEnabled");
            }
        }
        #endregion

        #region Members - Weather Members
        #region Member - ShowAskForWeatherAndComment
        private bool _showAskForWeatherAndComment;
        public bool ShowAskForWeatherAndComment
        {
            get { return _showAskForWeatherAndComment; }
            set
            {
                if (SetValue(ref _showAskForWeatherAndComment, value, "ShowAskForWeatherAndComment"))
                {
                    Notify("WeatherButtonVisibility");
                }
            }
        }
        #endregion

        #region Member - AskForWeatherAndCommentIsEnabled
        private bool _askForWeatherAndCommentIsEnabled;
        public bool AskForWeatherAndCommentIsEnabled
        {
            get { return _askForWeatherAndCommentIsEnabled; }
            set { SetValue(ref _askForWeatherAndCommentIsEnabled, value, "AskForWeatherAndCommentIsEnabled"); }
        }
        #endregion

        #region Member - CloseWeatherComment
        private string _closeWeatherComment;
        public string CloseWeatherComment
        {
            get { return _closeWeatherComment; }
            set { SetValue(ref _closeWeatherComment, value, "CloseWeatherComment"); }
        }
        #endregion
        #region Member - CloseUserComment
        private string _closeUserComment;
        public string CloseUserComment
        {
            get { return _closeUserComment; }
            set { SetValue(ref _closeUserComment, value, "CloseUserComment"); }
        }
        #endregion

        #region Member - WeatherButtonVisibility
        public Visibility WeatherButtonVisibility
        {
            get
            {
                return (_showAskForWeatherAndComment && (StartButtonVisibility == Visibility.Visible) ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        #endregion
        #endregion

        #region Members - UserConfirmation
        #region Member - AskForCloseConfirmation
        private bool _askForCloseConfirmation;
        public bool AskForCloseConfirmation
        {
            get { return _askForCloseConfirmation; }
            set
            {
                if (SetValue(ref _askForCloseConfirmation, value, "AskForCloseConfirmation"))
                {
                    Notify("ConfirmUserInputButtonVisibility");
                }
            }
        }
        #endregion

        #region Member - IsNextRunningWorkflow

        private bool _isNextRunningWorkflow;
        public bool IsNextRunningWorkflow
        {
            get { return _isNextRunningWorkflow; }
            set
            {
                if (SetValue(ref _isNextRunningWorkflow, value, "IsNextRunningWorkflow"))
                {
                    Notify("ConfirmUserInputButtonVisibility");
                }
            }
        }

        #endregion

        #region Member - ConfirmUserInputButtonVisibility
        public Visibility ConfirmUserInputButtonVisibility
        {
            get
            {
                return (_askForCloseConfirmation && _isNextRunningWorkflow ? Visibility.Visible : Visibility.Collapsed);
            }
        }
        #endregion
        #endregion

        #region Member - ConfirmedClose

        private bool _confirmedClose;
        public bool ConfirmedClose
        {
            get { return _confirmedClose; }
            set
            {
                if (SetValue(ref _confirmedClose, value, "ConfirmedClose"))
                {
                    Notify("OverrideButtonVisibility");
                }
            }
        }

        #endregion

        #region Member - Reset Button

        private bool _resetButtonIsEnabled;
        public bool ResetButtonIsEnabled
        {
            get { return _resetButtonIsEnabled; }
            set { SetValue(ref _resetButtonIsEnabled, value, "ResetButtonIsEnabled"); }
        }

        public Visibility IsRestartButtonVisible { get; set; }

        #endregion

        #region Member - WorkingBusinessDay
        private DateTime _workingBusinessDay;
        private bool _settingsLocked;

        public DateTime WorkingBusinessDay
        {
            get { return _workingBusinessDay; }
            set { SetValue(ref _workingBusinessDay, value, "WorkingBusinessDay"); }
        }
        #endregion

        #region Member - Locked

        public bool SettingsLocked
        {
            get { return _settingsLocked; }
            set { _settingsLocked = value; Notify("SettingsLocked"); }
        }

        #endregion

        #region Member - Order

        private int _order;

        public Int32 Order
        {
            get { return _order; }
            set { SetValue(ref _order, value, "Order"); }
        }

        #endregion

        #region Member - AutoStart allowed
        private bool _isAutostartAllowed = true;
        public bool IsAutostartAllowed
        {
            get { return _isAutostartAllowed; }
            set { SetValue(ref _isAutostartAllowed, value, "IsAutostartAllowed"); }
        }
        #endregion

        #region Member - WorkflowStarted
        private bool _workflowStarted;
        private bool _showLegacyScreenPreview = true;
        private LegacyScreenOutputMessageViewModel _workstepLegacyScreen;

        public bool WorkflowStarted
        {
            get { return _workflowStarted; }
            set { SetValue(ref _workflowStarted, value, "WorkflowStarted"); }
        }
        #endregion

        #endregion

        #region Commands
        public ICommand ToggleDetailVisibilityCommand { get; set; }
        public ICommand StartWorkflowCommand { get; set; }
        public ICommand OverridePrerequisiteCheckCommand { get; set; }
        public ICommand RestartWorkflowCommand { get; set; }
        public ICommand OpenWeatherDialogCommand { get; set; }
        public ICommand OpenConfirmUserInputDialogCommand { get; set; }
        #endregion

        public ObservableCollection<OpenCloseCheckViewModel> CheckItems { get; private set; }
        public ObservableCollection<OpenCloseWorkstepViewModel> WorkstepItems { get; private set; }
        public ObservableCollection<UserMessageViewModel> WorkstepUserMessages { get; private set; }

        public LegacyScreenOutputMessageViewModel WorkstepLegacyScreen
        {
            get { return _workstepLegacyScreen; }
            set { SetValue(ref _workstepLegacyScreen, value); }
        }

        public Boolean ShowLegacyScreenPreview
        {
            get { return _showLegacyScreenPreview; }
            set { SetValue(ref _showLegacyScreenPreview, value); }
        }

        #region Member - Workflow

        private OpenCloseWorkflowStatusDto _workflow;

        public OpenCloseWorkflowStatusDto Workflow
        {
            get { return _workflow; }
            set { SetValue(ref _workflow, value); }
        }

        #endregion

        #region Member - OverrideButton

        private bool _overrideButtonIsEnabled = true;
        public bool OverrideButtonIsEnabled
        {
            get { return _overrideButtonIsEnabled; }
            set
            {
                SetValue(ref _overrideButtonIsEnabled, value);
            }
        }

        #endregion

        #region Member - OverrideButtonVisibility

        public Visibility OverrideButtonVisibility
        {
            get
            {
                return Visibility.Collapsed; // On Hold
                //return Workflow != null
                //    && (Workflow.WorkflowStatus == WorkflowStatusType.NotReady || Workflow.WorkflowStatus == WorkflowStatusType.Ready)
                //    && Workflow.ConfirmedClose
                //    && CheckItems.Any(x => x.CriticalCheckFailed) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion

        public OpenCloseWorkflowModel()
        {
            CheckItems = new ObservableCollection<OpenCloseCheckViewModel>();
            WorkstepItems = new ObservableCollection<OpenCloseWorkstepViewModel>();
            WorkstepUserMessages = new ObservableCollection<UserMessageViewModel>();
            ToggleDetailVisibilityCommand = new RelayCommand(ToggleDetailVisibilityClick);
            StartWorkflowCommand = new RelayCommand(StartWorkflowCommandClick);
            OverridePrerequisiteCheckCommand = new RelayCommand(OverridePrerequisiteCheckCommandClick);
            RestartWorkflowCommand = new RelayCommand(RestartWorkflowCommandClick);
            OpenWeatherDialogCommand = new RelayCommand(OpenWeatherDialogCommandClick);
            OpenConfirmUserInputDialogCommand = new RelayCommand(OpenConfirmUserInputDialogCommandClick);

            IsRestartButtonVisible = (SettingsManager.Instance.DebugMode ? Visibility.Visible : Visibility.Collapsed);
            
            CheckItems.CollectionChanged += CheckItemsOnCollectionChanged;
        }

        private void CheckItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            try
            {
                if (e.OldItems != null)
                {
                    foreach (OpenCloseCheckViewModel check in e.OldItems)
                    {
                        check.PropertyChanged -= CheckOnPropertyChanged;
                    }
                }
                foreach (OpenCloseCheckViewModel check in CheckItems)
                {
                    check.PropertyChanged += CheckOnPropertyChanged;
                }
            }
            catch (Exception exception)
            {
                //LoggingHelper.LogException("Error in OpenCloseWorkflowModel.CheckItemsOnCollectionChanged", LogEventId.Error, exception);
            }
            finally
            {
                Notify("OverrideButtonVisibility");
            }
        }

        private void CheckOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == "RunningStatusVisibility")
            //{
            Notify("OverrideButtonVisibility");
            //}
        }

        #region Command Handler
        private void ToggleDetailVisibilityClick()
        {
            DetailVisibility = (_detailVisibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed);
        }

        public void SetDetailVisibility(bool visible)
        {
            DetailVisibility = (visible ? Visibility.Visible : Visibility.Collapsed);
        }

        public void StartWorkflowCommandClick()
        {
            if (!StartButtonIsEnabled)
                return;

            DetailVisibility = Visibility.Visible;

            ForceStartWorkflow();
        }

        public void OverridePrerequisiteCheckCommandClick()
        {
            if (!OverrideButtonIsEnabled)
                return;

            var check = CheckItems.FirstOrDefault(x => x.CriticalCheckFailed);
            ControlCenterViewModel.ManagerOverridePrerequisiteCheck(this, check);
        }

        private void OpenWeatherDialogCommandClick()
        {
            WeatherAndCommentDialogViewModel dialogModel = new WeatherAndCommentDialogViewModel();
            dialogModel.WeatherText = CloseWeatherComment;
            dialogModel.CommentText = CloseUserComment;

            PopupDialogData dialogData = PopupDialogManager.CreateDialogData<WeatherAndCommentDialog, WeatherAndCommentDialogViewModel>(dialogModel);
            dialogData.OnDialogClose += (PopupDialogData sender, bool? dialogResult) =>
            {
                if (dialogResult.GetValueOrDefault())
                {
                    ControlCenterViewModel.SaveWeatherAndCommentInput(WorkingBusinessDay, dialogModel.WeatherText, dialogModel.CommentText);
                }
                PopupMessageHelper.CloseDialog(sender);
            };
            PopupMessageHelper.ShowDialog(dialogData);
        }

        private async void OpenConfirmUserInputDialogCommandClick()
        {
            ConfirmUserInputDialogViewModel dialogModel = new ConfirmUserInputDialogViewModel();
            dialogModel.NextBusinessDayText = ControlCenterViewModel.NextBusinessDay.ToString("d");
            dialogModel.CloseTypeText = ControlCenterViewModel.CurrentDayCloseMode.DisplayName;

            if (ControlCenterViewModel.NextBusinessDay <= ControlCenterViewModel.CurrentBusinessDay)
            {
                await PopupMessageHelper.ShowMessageAsync("Open Close Center", Translation["KEY_OpenClose_NextBusinessDayMustBeInFuture"]);
                return;
            }

            if (ControlCenterViewModel.OpenCloseMaxAllowedDaysToSkip.Value != int.MaxValue)
            {
                DateTime lastPossibleDate = ControlCenterViewModel.CurrentBusinessDay.AddDays(ControlCenterViewModel.OpenCloseMaxAllowedDaysToSkip.Value);
                if (ControlCenterViewModel.NextBusinessDay > lastPossibleDate)
                {
                    await PopupMessageHelper.ShowMessageAsync("Open Close Center", string.Format(Translation["KEY_OpenClose_NextBusinessDayIsTooFarInFuture"], lastPossibleDate.ToString("d")));
                    return;
                }
            }

            PopupDialogData dialogData = PopupDialogManager.CreateDialogData<ConfirmUserInputDialog, ConfirmUserInputDialogViewModel>(dialogModel);
            dialogData.OnDialogClose += (PopupDialogData sender, bool? dialogResult) =>
            {
                if (dialogResult.HasValue && dialogResult.Value)
                {
                    if (!dialogModel.BusinessDayConfirmed || !dialogModel.CloseModeConfirmed)
                    {
                        return;
                    }

                    AskForCloseConfirmation = false;
                    ControlCenterViewModel.ConfirmSettingsAsync();
                }
            };
            PopupMessageHelper.ShowDialog(dialogData);
        }
        #endregion

        private void ForceStartWorkflow()
        {
            StartButtonIsEnabled = false;
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                openCloseService.StartWorkflowByName(_name, SettingsManager.Instance.OperatorId, WorkingBusinessDay, ControlCenterViewModel.NextBusinessDay);
                WcfConnection.DisconnectWcfService(openCloseService);
                return true;
            }, null, false);
        }

        private void RestartWorkflowCommandClick()
        {
            IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
            openCloseService.ResetWorkflow(WorkingBusinessDay, Name);
            WcfConnection.DisconnectWcfService(openCloseService);

            WorkstepUserMessages.Clear();
        }
    }
}
