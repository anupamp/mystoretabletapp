﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject
{
    public class UserMessageViewModel : ViewModelBase
    {
        #region Member - MessageText
        private string _messageText;
        public string MessageText
        {
            get { return _messageText; }
            set { SetValue(ref _messageText, value, "MessageText"); }
        }
        #endregion

        #region Member - MessageColor
        private SolidColorBrush _messageColor = new SolidColorBrush(Colors.Black);
        public SolidColorBrush MessageColor
        {
            get { return _messageColor; }
            set { SetValue(ref _messageColor, value, "MessageColor"); }
        }
        #endregion

        public UserMessageViewModel()
        {
        }

        public UserMessageViewModel(string textData, DateTime date)
        {
            try
            {
                if (String.IsNullOrEmpty(textData))
                    return;

                string[] messageData = textData.Split(new string[] { ";#;" }, StringSplitOptions.RemoveEmptyEntries);
                if (messageData.Length == 1)
                {
                    string translatableMessage = messageData[0];
                    if (translatableMessage.StartsWith("KEY_")) translatableMessage = Translation[translatableMessage];

                    MessageText = translatableMessage;
                }
                else if (messageData.Length == 2)
                {
                    string translatableMessage = messageData[0];
                    if (translatableMessage.StartsWith("KEY_")) translatableMessage = Translation[translatableMessage];

                    string[] parameterData = messageData[1].Split(new string[] { ";!;" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int index = 0; index < parameterData.Length; index++)
                    {
                        var s = parameterData[index];
                        if (string.IsNullOrWhiteSpace(s)) continue;
                        if (!s.StartsWith("KEY_")) continue;
                        parameterData[index] = Translation[s];
                    }

                    MessageText = String.Format(translatableMessage, parameterData);
                }

                if (MessageText != null)
                {
                    MessageText = "[" + date.ToString("G") + "] " + MessageText;
                }
            }
            catch (Exception e)
            {
                MessageText = Translation["KEY_OpenCloseControlCenter_Error_UserMessageViewModel"] + textData;
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as UserMessageViewModel;
            if (item == null) return false;

            return item.MessageText == MessageText && item.MessageColor == MessageColor;
        }

        public override int GetHashCode()
        {
            return MessageText.GetHashCode() ^ MessageColor.GetHashCode();
        }
    }
}
