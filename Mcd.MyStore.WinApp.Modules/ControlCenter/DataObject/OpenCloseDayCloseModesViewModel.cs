﻿using System;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.DataObject
{
    public class OpenCloseDayCloseModesViewModel
    {
        public String DisplayName { get; set; }
        public String Value { get; set; }
    }
}