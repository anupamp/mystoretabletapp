﻿using System;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject
{
    public class OpenCloseCheckViewModel : OpenCloseItemViewModel
    {
        #region Member - LinkModuleToolTipText
        private string _linkModuleToolTipText;
        public string LinkModuleToolTipText
        {
            get { return _linkModuleToolTipText; }
            set { SetValue(ref _linkModuleToolTipText, value, "LinkModuleToolTipText"); }
        }
        #endregion

        #region Member - Workflow

        public OpenCloseWorkflowModel Workflow { get; set; }

        #endregion

        public DateTime WorkingDateTime { get; set; }
    }
}
