﻿using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.LegacyScreenService.Common;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.WinApp.Core.Manager.Notification;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel
{
    public class LegacyScreenOutputMessageViewModel : ViewModelBase, INotificationModule
    {
        private static readonly object DialogLock = new object();
        private PopupDialogData _currentDialog;
        private ObservableCollection<string> _messages;

        public LegacyScreenOutputMessageViewModel()
        {
            Messages = new ObservableCollection<String>(Enumerable.Repeat("".PadRight(80, ' '), 25));

            GetInitialScreenData(screenData =>
            {
                for (int i = 0; i < screenData.Count; i++)
                {
                    Messages[i] = screenData[i];
                }

                NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.OpenClose_LegacyScreen_ScreenUpdate);
                NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.OpenClose_LegacyScreen_MyStoreNeedsYouMessage);
                NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.OpenClose_LegacyScreen_DisposeMessage);
                NotificationManager.Instance.SubscribeNotification(this, NotificationApplicationType.OpenClose_ControlCenter_UserInputAnswered);
            });
        }

        public ObservableCollection<String> Messages
        {
            get { return _messages; }
            set { SetValue(ref _messages, value); }
        }

        private void GetInitialScreenData(BackgroundWorkerManager.BackgroundWorkerResultDelegate<List<string>> callback)
        {
            LoadAsyncData(() =>
            {
                var legacyScreenService = WcfConnection.ConnectWcfService<ILegacyScreenService>();
                var currentScreen = legacyScreenService.GetCurrentScreen();
                WcfConnection.DisconnectWcfService(legacyScreenService);

                return currentScreen;
            }, callback);
        }

        public async void ReceiveNotificationInternal(NotificationArgs<NotificationApplicationType> notification)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                switch (notification.Type)
                {
                    case NotificationApplicationType.OpenClose_LegacyScreen_ScreenUpdate:
                        {
                            var param = notification.Parameter as Dictionary<string, string>;
                            if (param == null) return;

                            foreach (var key in param.Keys)
                            {
                                int keyInt;
                                if (Int32.TryParse(key, out keyInt) && Messages.Count > keyInt)
                                {
                                    Messages[keyInt] = param[key];
                                }
                            }
                        }
                        break;
                    case NotificationApplicationType.OpenClose_LegacyScreen_MyStoreNeedsYouMessage:
                        {
                            lock (DialogLock)
                            {
                                if (_currentDialog != null)
                                {
                                    PopupMessageHelper.CloseDialog(_currentDialog);
                                    _currentDialog = null;
                                }
                                PopupMessageHelper.CloseDialog();
                                _currentDialog = PopupMessageHelper.ShowMessage("Open Close Center", Translation["KEY_OpenClose_MyStoreNeedsYou"]);
                            }
                        }
                        break;
                    case NotificationApplicationType.OpenClose_ControlCenter_UserInputAnswered:
                    case NotificationApplicationType.OpenClose_LegacyScreen_DisposeMessage:
                        {
                            lock (DialogLock)
                            {
                                if (_currentDialog != null)
                                {
                                    PopupMessageHelper.CloseDialog(_currentDialog);
                                }
                            }
                        }
                        break;
                }
            });
        }
    }

}