﻿using System;
using System.Windows.Input;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.UI.Common;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel
{
    public class WeatherAndCommentDialogViewModel : PopupDialogContentViewModel
    {
        public WeatherAndCommentDialogViewModel()
        {
            SaveCommand = new RelayCommand(SaveCommandExecute);
            CancelCommand = new RelayCommand(CancelCommandExecute);
        }

        private void SaveCommandExecute()
        {
            DialogResult = true;
        }

        private void CancelCommandExecute()
        {
            DialogResult = false;
        }

        #region Member - WeatherText
        private string _weatherText;
        public string WeatherText
        {
            get { return _weatherText; }
            set
            {
                if (SetValue(ref _weatherText, value))
                {
                    SaveEnabled = !String.IsNullOrWhiteSpace(WeatherText) && !String.IsNullOrWhiteSpace(CommentText);
                }
            }
        }
        #endregion

        #region Member - CommentText
        private string _commentText;
        private bool _saveEnabled;

        public string CommentText
        {
            get { return _commentText; }
            set
            {
                if (SetValue(ref _commentText, value))
                {
                    SaveEnabled = !String.IsNullOrWhiteSpace(WeatherText) && !String.IsNullOrWhiteSpace(CommentText);
                }
            }
        }
        #endregion

        #region SaveEnabled

        public bool SaveEnabled
        {
            get { return _saveEnabled; }
            set { SetValue(ref _saveEnabled, value); }
        }

        #endregion

        public ICommand SaveCommand { get; set; }
        public ICommand CancelCommand { get; set; }
    }
}
