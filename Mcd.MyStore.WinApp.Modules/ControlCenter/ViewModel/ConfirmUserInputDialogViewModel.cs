﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel
{
    public class ConfirmUserInputDialogViewModel : PopupDialogContentViewModel
    {
        #region Constructor

        public ConfirmUserInputDialogViewModel()
        {
            SaveCommand = new RelayCommand(SaveCommandExecute);
            CancelCommand = new RelayCommand(CancelCommandExecute);
        }

        #endregion

        #region Command implementations

        private void SaveCommandExecute()
        {
            DialogResult = true;
        }

        private void CancelCommandExecute()
        {
            DialogResult = false;
        }

        #endregion

        #region Member - Save Command

        public ICommand SaveCommand { get; set; }

        #endregion

        #region Member - Cancel Command

        public ICommand CancelCommand { get; set; }

        #endregion

        #region Member - BusinessDayConfirmed
        private bool _businessDayConfirmed;
        public bool BusinessDayConfirmed
        {
            get { return _businessDayConfirmed; }
            set
            {
                if (SetValue(ref _businessDayConfirmed, value))
                {
                    SaveEnabled = BusinessDayConfirmed && CloseModeConfirmed;
                }
            }
        }
        #endregion

        #region Member - CloseModeConfirmed
        private bool _closeModeConfirmed;
        public bool CloseModeConfirmed
        {
            get { return _closeModeConfirmed; }
            set
            {
                if (SetValue(ref _closeModeConfirmed, value))
                {
                    SaveEnabled = BusinessDayConfirmed && CloseModeConfirmed;
                }
            }
        }
        #endregion

        #region Member - NextBusinessDayText
        private string _nextBusinessDayText;
        public string NextBusinessDayText
        {
            get { return _nextBusinessDayText; }
            set { SetValue(ref _nextBusinessDayText, value); }
        }
        #endregion

        #region Member - CloseTypeText
        private string _closeTypeText;
        public string CloseTypeText
        {
            get { return _closeTypeText; }
            set { SetValue(ref _closeTypeText, value); }
        }
        #endregion

        #region SaveEnabled

        private bool _saveEnabled;
        public bool SaveEnabled
        {
            get { return _saveEnabled; }
            set { SetValue(ref _saveEnabled, value); }
        }

        #endregion
    }
}
