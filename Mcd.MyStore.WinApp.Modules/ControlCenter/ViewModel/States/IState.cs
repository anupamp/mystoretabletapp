﻿namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States
{
    public interface IState
    {
        string Name { get; set; }
        bool? IsSuccessful { get; set; }
        bool RunAtLeastOnce { get; set; }
        bool? PreviousState { get; set; }
        bool Modified { get; set; }
        WorkflowPresentationStates State { get; set; }
    }
}