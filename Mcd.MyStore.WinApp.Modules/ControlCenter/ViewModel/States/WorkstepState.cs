using System.Diagnostics;
using System.Linq;
using Mcd.MyStore.OpenCloseService.Common.Dto;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States
{
    [DebuggerDisplay("Name {Name}, IsSuccessful {IsSuccessful}, PreviousState {PreviousState}")]
    public class WorkstepState : IState
    {
       public WorkstepState(string name, bool? isSuccessful, bool runAtLeastOnce, bool? previousState, bool isRunning=false)
        {
            Name = name;
            IsSuccessful = isSuccessful;
            RunAtLeastOnce = runAtLeastOnce;
            PreviousState = previousState;
            State = isRunning ? WorkflowPresentationStates.Working : WorkflowPresentationStates.Ready;
        }

        public string Name { get; set; }
        public bool? IsSuccessful { get; set; }
        public bool RunAtLeastOnce { get; set; }
        public bool? PreviousState { get; set; }
        public WorkflowPresentationStates State { get; set; }
        public bool Modified { get; set; }

        public bool HasChanges(WorkflowState currentState, WorkflowStates previousStates)
        {
            foreach (var previousState in previousStates.WorkflowState)
            {
                foreach (var prerequisiteState in previousState.PrerequisiteStates)
                {
                    var currentPrerequisiteState = currentState.PrerequisiteStates.FirstOrDefault(x => x.Name == prerequisiteState.Name);
                    if (prerequisiteState.IsSuccessful != currentPrerequisiteState.IsSuccessful) return true;
                }
            }
            return false;
        }
    }
}