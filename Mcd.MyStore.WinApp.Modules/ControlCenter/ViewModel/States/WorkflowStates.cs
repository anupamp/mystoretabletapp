﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States;
using Mcd.MyStore.OpenCloseService.Common.Dto;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel
{
    public class WorkflowStates
    {
        public WorkflowStates()
        {
            WorkflowState = new List<WorkflowState>();
        }

        public List<WorkflowState> WorkflowState { get; set; }

        public WorkflowState AddWorkflowState(string name, bool? isSuccessful, DateTime workingBusinessDay)
        {
            var state = new WorkflowState(name, isSuccessful, workingBusinessDay);
            WorkflowState.Add(state);
            return state;
        }

        public bool HasChanges(WorkflowStates previousStates)
        {
            if (previousStates == null) return true;

            foreach (var previousState in previousStates.WorkflowState)
            {
                var currentState = WorkflowState.FirstOrDefault(x => x.Name == previousState.Name);
                if (currentState.IsSuccessful != previousState.IsSuccessful)
                    return true;

                if (currentState.HasChanges(previousState, previousStates))
                    return true;
            }

            return false;
        }

        public List<WorkflowState> GetChanges(WorkflowStates previousStates)
        {
            var resultList = new List<WorkflowState>();

            foreach (var previousState in previousStates.WorkflowState)
            {
                var currentState = WorkflowState.FirstOrDefault(x => x.Name == previousState.Name);
                if (!currentState.HasChanges(previousState, previousStates)) continue;

                var state = new WorkflowState(currentState.Name, currentState.IsSuccessful, currentState.WorkingBusinessDay);
                foreach (var change in currentState.GetChanges(previousState, previousStates))
                {
                    if (change is PrerequisiteState)
                    {
                        var pc = previousState.PrerequisiteStates.FirstOrDefault(x => x.Name == change.Name);
                        state.AddPrerequisiteState(change.Name, change.IsSuccessful, change.RunAtLeastOnce, pc != null ? pc.PreviousState : null);
                    }
                    else if (change is WorkstepState)
                    {
                        var pw = previousState.WorkstepStates.FirstOrDefault(x => x.Name == change.Name);
                        state.AddWorkstepState(change.Name, change.IsSuccessful, change.RunAtLeastOnce, pw != null ? pw.PreviousState : null);
                    }
                }
                resultList.Add(state);
            }

            return resultList;
        }

        public void Update(WorkflowStates currentState)
        {
            foreach (var newWorkflowState in currentState.WorkflowState)
            {
                var currentWorkflowState = WorkflowState.FirstOrDefault(x => x.Name == newWorkflowState.Name && x.WorkingBusinessDay == newWorkflowState.WorkingBusinessDay);
                if (currentWorkflowState == null) continue;

                foreach (var prerequisiteState in newWorkflowState.PrerequisiteStates)
                {
                    var currentPrerequisiteState = currentWorkflowState.PrerequisiteStates.FirstOrDefault(x => x.Name == prerequisiteState.Name);
                    if (currentPrerequisiteState == null) continue;
                    if (currentPrerequisiteState.State == WorkflowPresentationStates.Finished) continue;

                    if (prerequisiteState.State == WorkflowPresentationStates.Ready && currentPrerequisiteState.State == WorkflowPresentationStates.Working)
                    {
                        currentPrerequisiteState.State = WorkflowPresentationStates.Presenting;
                        currentPrerequisiteState.Modified = true;
                    }
                    else if (prerequisiteState.State == WorkflowPresentationStates.Working)
                    {
                        currentPrerequisiteState.PreviousState = currentPrerequisiteState.IsSuccessful;
                        currentPrerequisiteState.IsSuccessful = prerequisiteState.IsSuccessful;
                        currentPrerequisiteState.State = WorkflowPresentationStates.Working;
                    }
                    else
                    {
                        currentPrerequisiteState.State = prerequisiteState.State;
                        currentPrerequisiteState.Modified = false;
                    }
                }

                foreach (var workstepState in newWorkflowState.WorkstepStates)
                {
                    var currentWorkstepState = currentWorkflowState.WorkstepStates.FirstOrDefault(x => x.Name == workstepState.Name);
                    if (currentWorkstepState == null) continue;
                    if (currentWorkstepState.State == WorkflowPresentationStates.Finished) continue;

                    if (workstepState.State == WorkflowPresentationStates.Ready && currentWorkstepState.State == WorkflowPresentationStates.Working)
                    {
                        currentWorkstepState.State = WorkflowPresentationStates.Presenting;
                        currentWorkstepState.Modified = true;
                    }
                    else if (workstepState.State == WorkflowPresentationStates.Working)
                    {
                        currentWorkstepState.PreviousState = currentWorkstepState.IsSuccessful;
                        currentWorkstepState.IsSuccessful = workstepState.IsSuccessful;
                        currentWorkstepState.State = WorkflowPresentationStates.Working;
                    }
                    else
                    {
                        currentWorkstepState.State = workstepState.State;
                        currentWorkstepState.Modified = false;
                    }
                }
            }
        }

        public void Finish(IState state)
        {
            state.Modified = false;
            state.State = WorkflowPresentationStates.Finished;

            //foreach (var workflowState in WorkflowState)
            //{
            //    foreach (var prerequisiteState in workflowState.PrerequisiteStates)
            //    {
            //        if (prerequisiteState.State == WorkflowPresentationStates.Ready)
            //        {
            //            prerequisiteState.Modified = false;
            //            prerequisiteState.State = WorkflowPresentationStates.Finished;
            //        }
            //    }
            //    foreach (var workstepState in workflowState.WorkstepStates)
            //    {
            //        if (workstepState.State == WorkflowPresentationStates.Ready)
            //        {
            //            workstepState.Modified = false;
            //            workstepState.State = WorkflowPresentationStates.Finished;
            //        }
            //    }
            //}
        }

        public void Reset()
        {
            foreach (var workflowState in WorkflowState)
            {
                foreach (var prerequisiteState in workflowState.PrerequisiteStates)
                {
                    prerequisiteState.IsSuccessful = null;
                    prerequisiteState.Modified = false;
                    prerequisiteState.State = WorkflowPresentationStates.Ready;
                }
                foreach (var workstepState in workflowState.WorkstepStates)
                {
                    workstepState.IsSuccessful = null;
                    workstepState.Modified = false;
                    workstepState.State = WorkflowPresentationStates.Ready;
                }
            }
        }
    }
}