﻿namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States
{
    public enum WorkflowPresentationStates
    {
        Ready,
        Working,
        Presenting,
        Finished
    }
}