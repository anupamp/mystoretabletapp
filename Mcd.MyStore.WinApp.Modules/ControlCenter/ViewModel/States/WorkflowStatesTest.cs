using System;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel
{
    public class WorkflowStatesTest
    {
        public void DetectChange()
        {
            var previousStateManager = new WorkflowStates();
            var previousTest1State = previousStateManager.AddWorkflowState("Test 1", null, DateTime.Today);
            var previousTest2State = previousStateManager.AddWorkflowState("Test 2", null, DateTime.Today);
            var previousTest3State = previousStateManager.AddWorkflowState("Test 3", null, DateTime.Today);

            previousTest1State.AddPrerequisiteState("Check 1", null, true);
            previousTest1State.AddPrerequisiteState("Check 2", null, true);
            previousTest1State.AddPrerequisiteState("Check 3", null, true);

            previousTest2State.AddPrerequisiteState("Check 1", null, true);
            previousTest2State.AddPrerequisiteState("Check 2", null, true);
            previousTest2State.AddPrerequisiteState("Check 3", null, true);

            previousTest3State.AddPrerequisiteState("Check 1", null, true);
            previousTest3State.AddPrerequisiteState("Check 2", null, true);
            previousTest3State.AddPrerequisiteState("Check 3", null, true);

            previousTest1State.AddWorkstepState("Workstep 1", null, true);
            previousTest2State.AddWorkstepState("Workstep 1", null, true);
            previousTest3State.AddWorkstepState("Workstep 1", null, true);

            var currentStateManager = new WorkflowStates();
            var currentTest1State = currentStateManager.AddWorkflowState("Test 1", null, DateTime.Today);
            var currentTest2State = currentStateManager.AddWorkflowState("Test 2", null, DateTime.Today);
            var currentTest3State = currentStateManager.AddWorkflowState("Test 3", null, DateTime.Today);

            currentTest1State.AddPrerequisiteState("Check 1", null, true);
            currentTest1State.AddPrerequisiteState("Check 2", null, true);
            currentTest1State.AddPrerequisiteState("Check 3", null, true);

            currentTest2State.AddPrerequisiteState("Check 1", null, true);
            currentTest2State.AddPrerequisiteState("Check 2", null, true);
            currentTest2State.AddPrerequisiteState("Check 3", null, true);

            currentTest3State.AddPrerequisiteState("Check 1", null, true);
            currentTest3State.AddPrerequisiteState("Check 2", null, true);
            currentTest3State.AddPrerequisiteState("Check 3", true, true);

            currentTest1State.AddWorkstepState("Workstep 1", null, true);
            currentTest2State.AddWorkstepState("Workstep 1", null, true);
            currentTest3State.AddWorkstepState("Workstep 1", false, true);

            if (currentStateManager.HasChanges(previousStateManager))
            {
                var changes = currentStateManager.GetChanges(previousStateManager);
            }
        }
    }
}