using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States
{
    [DebuggerDisplay("Name {Name}, IsSuccessful {IsSuccessful}, WorkingBusinessDay {WorkingBusinessDay}")]
    public class WorkflowState
    {
        public WorkflowState(string name, bool? isSuccessful, DateTime workingBusinessDay)
            : this()
        {
            Name = name;
            IsSuccessful = isSuccessful;
            WorkingBusinessDay = workingBusinessDay;
        }


        public WorkflowState()
        {
            PrerequisiteStates = new List<PrerequisiteState>();
            WorkstepStates = new List<WorkstepState>();
        }

        public string Name { get; set; }
        public bool? IsSuccessful { get; set; }
        public DateTime WorkingBusinessDay { get; set; }

        public List<PrerequisiteState> PrerequisiteStates { get; set; }
        public List<WorkstepState> WorkstepStates { get; set; }

        public PrerequisiteState AddPrerequisiteState(string name, bool? isSuccessful, bool runAtLeastOnce, bool? previousState = null, bool isRunning = false)
        {
            var state = new PrerequisiteState(name, isSuccessful, runAtLeastOnce, previousState, isRunning);
            PrerequisiteStates.Add(state);
            return state;
        }

        public WorkstepState AddWorkstepState(string name, bool? isSuccessful, bool runAtLeastOnce, bool? previousState = null, bool isRunning = false)
        {
            var state = new WorkstepState(name, isSuccessful, runAtLeastOnce, previousState, isRunning);
            WorkstepStates.Add(state);
            return state;
        }

        public bool HasChanges(WorkflowState previousState, WorkflowStates previousStates)
        {
            if (previousStates == null) return true;

            foreach (var prerequisiteState in previousState.PrerequisiteStates)
            {
                var currentPrerequisiteState = PrerequisiteStates.FirstOrDefault(x => x.Name == prerequisiteState.Name);
                if (currentPrerequisiteState == null) continue;
                if (prerequisiteState.IsSuccessful != currentPrerequisiteState.IsSuccessful) return true;
            }

            foreach (var workstepState in previousState.WorkstepStates)
            {
                var currentWorkstepState = WorkstepStates.FirstOrDefault(x => x.Name == workstepState.Name);
                if (currentWorkstepState == null) continue;
                if (workstepState.IsSuccessful != currentWorkstepState.IsSuccessful) return true;
            }
            return false;
        }

        public List<IState> GetChanges(WorkflowState previousState, WorkflowStates previousStates)
        {
            var resultList = new List<IState>();

            foreach (var prerequisiteState in previousState.PrerequisiteStates)
            {
                var currentPrerequisiteState = PrerequisiteStates.FirstOrDefault(x => x.Name == prerequisiteState.Name);
                if (currentPrerequisiteState == null) continue;
                if (currentPrerequisiteState.RunAtLeastOnce && currentPrerequisiteState.IsSuccessful != prerequisiteState.IsSuccessful)
                {
                    var state = new PrerequisiteState(currentPrerequisiteState.Name, currentPrerequisiteState.IsSuccessful, currentPrerequisiteState.RunAtLeastOnce, prerequisiteState.IsSuccessful);
                    resultList.Add(state);
                }
            }

            foreach (var workstepState in previousState.WorkstepStates)
            {
                var currentWorkstepState = WorkstepStates.FirstOrDefault(x => x.Name == workstepState.Name);
                if (currentWorkstepState == null) continue;
                if (currentWorkstepState.RunAtLeastOnce && currentWorkstepState.IsSuccessful != workstepState.IsSuccessful)
                {
                    var state = new WorkstepState(currentWorkstepState.Name, currentWorkstepState.IsSuccessful, currentWorkstepState.RunAtLeastOnce, workstepState.IsSuccessful);
                    resultList.Add(state);
                }
            }

            return resultList;
        }
    }
}