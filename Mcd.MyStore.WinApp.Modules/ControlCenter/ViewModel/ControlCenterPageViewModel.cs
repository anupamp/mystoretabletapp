﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.DataObject;
using Mcd.MyStore.OpenCloseService.Common.Dto;
using Mcd.MyStore.OpenCloseService.Common.ServiceContracts;
using Mcd.MyStore.StoreMaintenanceService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.View;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel;
using Mcd.MyStore.Modules.OpenClose.ControlCenter.ViewModel.States;
using Mcd.MyStore.StoreMaintenanceService.Common.Dto.Calendar;
using Mcd.MyStore.StoreMaintenanceService.Common.Dto.HolidayCalendar;
using Mcd.MyStore.SystemMaintenance.Common.Dto;
using Mcd.MyStore.WinApp.Core.Extensions;
using Mcd.MyStore.WinApp.Modules.ControlCenter.View;
using Mcd.MyStore.WinApp.SystemMaintenance.WcfService.Common;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel
{
    public class ControlCenterPageViewModel : ModuleViewModelBase
    {
        private static readonly Object UserInputLock = new Object();
        private static readonly SemaphoreSlim LoadLockSlim = new SemaphoreSlim(1);
        private static readonly object FinishedLock = new object();
        private static readonly Object DialogLock = new object();
        private bool resultProcessing = false;

        private static int _workflowDataChangedNotifications = 0;

        private readonly string _criticalErrorMessage = String.Empty;
        private readonly HashSet<DateTime> _confirmedBusinessDays = new HashSet<DateTime>();
        private DateTime _lastRefreshTime;
        private DateTime _previousBusinessDay;
        private DateTime _currentBusinessDay;
        private DateTime _nextBusinessDay = SystemStatusManager.Instance.CurrentBusinessDay.AddDays(1);
        private bool _settingsLocked;
        private OpenCloseWorkflowModes _currentWorkflowMode;
        private OpenCloseDayCloseModesViewModel _currentDayCloseMode;
        private ObservableCollection<OpenCloseDayCloseModesViewModel> _dayCloseModes;
        private OpenCloseWorkflowModel _nextWorkflow;
        private ObservableCollection<OpenCloseWorkflowModes> _workflowModeItems;
        private ObservableCollection<OpenCloseWorkflowModel> _workflowItems;
        private PopupDialogData _currentShowingDialog = null;
        private Visibility _largeLegacyScreenVisible = Visibility.Collapsed;
        private readonly CoreDispatcher _dispatcher;
        private string _translationPrefix = "KEY_OpenClose_";

        #region Constructor

        public ControlCenterPageViewModel()
        {
            _dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            WorkflowItems = new ObservableCollection<OpenCloseWorkflowModel>();
            WorkflowItems.CollectionChanged += WorkflowItemsOnCollectionChanged;

            WorkflowModeItems = new ObservableCollection<OpenCloseWorkflowModes>(new List<OpenCloseWorkflowModes>
            {
                new OpenCloseWorkflowModes {DisplayName = Translation["KEY_OpenCloseControlCenter_WorkflowMode_Normal"], Mode = OpenCloseWorkflowModeType.Normal},
                new OpenCloseWorkflowModes {DisplayName = Translation["KEY_OpenCloseControlCenter_WorkflowMode_Offline"], Mode = OpenCloseWorkflowModeType.Offline}
            });
            CurrentWorkflowMode = WorkflowModeItems.FirstOrDefault();

            _criticalErrorMessage = Translation["KEY_OpenClose_CriticalError"];

            DayCloseModes = new ObservableCollection<OpenCloseDayCloseModesViewModel>();
            DayCloseModes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_DayCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.DailyClose });
            DayCloseModes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_WeekCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.WeeklyClose });
            DayCloseModes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_MonthCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.MonthlyClose });
            DayCloseModes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_YearCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.YearlyClose });

            CurrentDayCloseMode = DayCloseModes.FirstOrDefault();

            SubscribeNotification(NotificationApplicationType.OpenClose_ControlCenter_WorkflowDataChanged);
            SubscribeNotification(NotificationApplicationType.OpenClose_ControlCenter_UserInputAnswered);
            SubscribeNotification(NotificationApplicationType.BusinessDayIsChanged);
            SubscribeNotification(NotificationApplicationType.OpenClose_LegacyScreen_ScreenUpdate);

            PreviousBusinessDay = SystemStatusManager.Instance.PreviousBusinessDay;
            CurrentBusinessDay = SystemStatusManager.Instance.CurrentBusinessDay;
            CheckNextBusinessDayAsync(false);

            LoadWorkflowData();
        }

        private void WorkflowItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var openCloseWorkflowModel in WorkflowItems)
            {
                openCloseWorkflowModel.PropertyChanged += OpenCloseWorkflowModelOnPropertyChanged;
            }
            Notify("LargeLegacyScreenVisible");
            Notify("CurrentActiveWorkflowItem");
            Notify("CurrentActiveWorkflowItemMessages");
        }

        private void OpenCloseWorkflowModelOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            Notify("LargeLegacyScreenVisible");
            Notify("CurrentActiveWorkflowItem");
            Notify("CurrentActiveWorkflowItemMessages");
        }

        #endregion

        #region Properties

        public readonly Lazy<Int32> OpenCloseMaxAllowedDaysToSkip = new Lazy<int>(() =>
        {
            return WcfConnection.ExecuteActionOnInterface<IOpenCloseService, int>(s => s.GetConfigOpenCloseMaxAllowedDaysToSkip()).Result;
        });

        public DateTime LastRefreshTime
        {
            get { return _lastRefreshTime; }
            set { SetValue(ref _lastRefreshTime, value); }
        }

        public DateTime PreviousBusinessDay
        {
            get { return _previousBusinessDay; }
            set { SetValue(ref _previousBusinessDay, value.Date); }
        }

        public DateTime CurrentBusinessDay
        {
            get { return _currentBusinessDay; }
            set { SetValue(ref _currentBusinessDay, value.Date); }
        }

        public DateTime NextBusinessDay
        {
            get { return _nextBusinessDay; }
            set
            {
                if (value <= _currentBusinessDay) value = _currentBusinessDay.AddDays(1);
                if (SetValue(ref _nextBusinessDay, value.Date))
                {
                    CheckNextBusinessDayAsync();
                    LoadInformationPossibleCloseTypes();
                    LoadWorkflowData();
                }
            }
        }


        public Boolean SettingsLocked
        {
            get { return _settingsLocked; }
            set { SetValue(ref _settingsLocked, value); }
        }

        public WorkflowStates LastState { get; set; }

        public ObservableCollection<OpenCloseWorkflowModel> WorkflowItems
        {
            get { return _workflowItems; }
            set { SetValue(ref _workflowItems, value); }
        }

        public OpenCloseWorkflowModes CurrentWorkflowMode
        {
            get { return _currentWorkflowMode; }
            set
            {
                if (SetValue(ref _currentWorkflowMode, value))
                {
                    WorkflowItems.Clear();
                    LoadWorkflowData();
                }
            }
        }

        public OpenCloseWorkflowModel CurrentActiveWorkflowItem
        {
            get
            {
                if (WorkflowItems == null || !WorkflowItems.Any()) return null;
                var item = WorkflowItems.FirstOrDefault(x => x.WorkstepLegacyScreen != null);
                if (item != null)
                {
                    bool messagesAvailable = item.WorkstepLegacyScreen.Messages.Any(x => x.Trim() != "");
                    item.ShowLegacyScreenPreview = LargeLegacyScreenVisible == Visibility.Collapsed && messagesAvailable;
                }
                return item;
            }
        }

        public ObservableCollection<string> CurrentActiveWorkflowItemMessages
        {
            get
            {
                var item = WorkflowItems.FirstOrDefault(x => x.WorkstepLegacyScreen != null);
                bool messagesAvailable = false;
                if (item != null)
                {
                    messagesAvailable = item.WorkstepLegacyScreen.Messages.Any(x => x.Trim() != "");
                }
                if (CurrentActiveWorkflowItem == null || CurrentActiveWorkflowItem.WorkstepLegacyScreen == null || CurrentActiveWorkflowItem.WorkstepLegacyScreen.Messages == null || !messagesAvailable)
                    return new ObservableCollection<string>();
                return CurrentActiveWorkflowItem.WorkstepLegacyScreen.Messages;
            }
        }

        public ObservableCollection<OpenCloseWorkflowModes> WorkflowModeItems
        {
            get { return _workflowModeItems; }
            set { SetValue(ref _workflowModeItems, value); }
        }

        public OpenCloseWorkflowModel NextWorkflow
        {
            get { return _nextWorkflow; }
            set { SetValue(ref _nextWorkflow, value); }
        }

        public OpenCloseDayCloseModesViewModel CurrentDayCloseMode
        {
            get { return _currentDayCloseMode; }
            set { SetValue(ref _currentDayCloseMode, value); }
        }

        public ObservableCollection<OpenCloseDayCloseModesViewModel> DayCloseModes
        {
            get { return _dayCloseModes; }
            set { SetValue(ref _dayCloseModes, value); }
        }

        public Visibility LargeLegacyScreenVisible
        {
            get { return _largeLegacyScreenVisible; }
            set
            {
                if (SetValue(ref _largeLegacyScreenVisible, value))
                {
                    Notify("CurrentActiveWorkflowItemMessages");
                }
            }
        }

        #endregion

        #region Methods - Confirmation

        public class ConfirmationDataHolder
        {
            public Boolean UpdateError { get; set; }
        }

        public void ConfirmSettingsAsync()
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                bool success = openCloseService.UpdateLogInformationForBusinessDay(CurrentBusinessDay,
                    NextBusinessDay,
                    CurrentDayCloseMode.Value,
                    GetSDMDefinitionWorkflowMode(CurrentWorkflowMode.Mode),
                    null, null);
                WcfConnection.DisconnectWcfService(openCloseService);

                return new ConfirmationDataHolder { UpdateError = !success };
            }, ConfirmSettingsCompleted, false);
        }

        private async void ConfirmSettingsCompleted(ConfirmationDataHolder result)
        {
            if (!result.UpdateError && !_confirmedBusinessDays.Contains(NextBusinessDay))
                _confirmedBusinessDays.Add(NextBusinessDay);
            else if (result.UpdateError)
                _confirmedBusinessDays.Remove(NextBusinessDay);

            SettingsLocked = _confirmedBusinessDays.Contains(NextBusinessDay);

            if (result.UpdateError)
            {
                await PopupMessageHelper.ShowExceptionAsync(Translation["KEY_OpenCloseControlCenter_Message_ErrorConfirm"]);
            }
        }

        public void CheckNextBusinessDayAsync(bool showErrorMessage = true)
        {
            DateTime dateToCheck = NextBusinessDay;
            LoadAsyncData(() => CheckNextBusinessDayInternal(dateToCheck), holidayError =>
            {
                if (holidayError == null) return;

                if (holidayError.Item1 && showErrorMessage)
                {
                    PopupMessageHelper.ShowMessage("Open Close Center",
                        string.Format(Translation["KEY_OpenCloseControlCenter_Message_NextBusinessDayIsHolidayWarning"], holidayError.Item2, holidayError.Item3));
                }
            }, false);
        }

        internal Tuple<bool, DateTime, DateTime> CheckNextBusinessDayInternal(DateTime dateToCheck)
        {
            try
            {
                var storeMaintenanceServiceConnection = WcfConnection.ConnectWcfService<IStoreMaintenanceService>();
                var response = storeMaintenanceServiceConnection.GetPlannedCloseDays(new GetPlannedCloseDaysRequestDto(NextBusinessDay, NextBusinessDay.AddDays(30)));
                WcfConnection.DisconnectWcfService(storeMaintenanceServiceConnection);

                bool holidayError = response.Success && response.PlannedCloseDays.Any(x => x.Date == NextBusinessDay.Date);

                if (holidayError)
                {
                    for (DateTime endDate = NextBusinessDay.AddDays(30), currentDate = NextBusinessDay; currentDate <= endDate; currentDate = currentDate.AddDays(1))
                    {
                        if (response.PlannedCloseDays.All(x => x.Date != currentDate))
                        {
                            _nextBusinessDay = currentDate;
                            Notify("NextBusinessDay");
                            break;
                        }
                    }
                }

                return new Tuple<bool, DateTime, DateTime>(holidayError, dateToCheck, _nextBusinessDay);
            }
            catch (Exception e)
            {
                //LoggingHelper.LogException("Error while checking for holidays", LogEventId.Error, e);
                return null;
            }
        }

        #endregion

        #region Methods - ManagerOverride

        public void ManagerOverridePrerequisiteCheck(OpenCloseWorkflowModel workflowModel, OpenCloseCheckViewModel checkViewModel, string reason = null)
        {
            if (workflowModel == null) return;
            if (checkViewModel == null) return;

            OverridePrerequisiteCheckDialogViewModel dialogModel = new OverridePrerequisiteCheckDialogViewModel();
            dialogModel.Reason = reason;
            dialogModel.CheckItem = checkViewModel;

            PopupDialogData dialogData = PopupDialogManager.CreateDialogData<OverridePrerequisiteCheckDialog, OverridePrerequisiteCheckDialogViewModel>(dialogModel);
            dialogData.OnDialogClose += (PopupDialogData sender, bool? dialogResult) =>
            {
                if (dialogResult.GetValueOrDefault())
                {
                    OverridePrerequisiteCheck(workflowModel, checkViewModel, dialogModel.Username, dialogModel.PasswordInput, dialogModel.Reason);
                }
                PopupMessageHelper.CloseDialog(sender);
            };
            PopupMessageHelper.ShowDialog(dialogData);
        }

        private void OverridePrerequisiteCheck(OpenCloseWorkflowModel workflow, OpenCloseCheckViewModel prerequisiteCheck, string username, string password, string reason)
        {
            LoadAsyncData(() =>
            {
                ISystemMaintenanceServiceForApp maintenanceService = WcfConnection.ConnectWcfService<ISystemMaintenanceServiceForApp>();

                // Check credentials
                MyStorePrincipalDto principal = maintenanceService.IsUserAuthenticated(new UserCredentialDto { UserName = username, Password = password }, false, string.Empty);
                if (!principal.IsAuthenticated)
                {
                    principal = maintenanceService.IsUserAuthenticated(new UserCredentialDto { UserName = username, Password = password.ToUpperInvariant() }, false, string.Empty);
                    if (!principal.IsAuthenticated)
                    {
                        _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            PopupMessageHelper.ShowException(Translation["OverridePrerequisiteCheckInvalidCredentials"]);
                            ManagerOverridePrerequisiteCheck(workflow, prerequisiteCheck, reason);
                        });
                        return false;
                    }
                }
                // Check if credentials are mapped to a Manager (M)
                var roles = maintenanceService.GetOperatorRoles(principal.OperatorId);
                if (roles == null || roles.All(x => !HasRequiredManagerCode(x.RoleCode.Trim())))
                {
                    _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        PopupMessageHelper.ShowException(Translation["OverridePrerequisiteCheckNotManager"]);
                        ManagerOverridePrerequisiteCheck(workflow, prerequisiteCheck, reason);
                    });
                    return false;
                }

                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                var currentInformation = openCloseService.OverridePrerequisiteCheck(workflow.WorkingBusinessDay, workflow.Name, prerequisiteCheck.Name, principal.OperatorId, reason);
                WcfConnection.DisconnectWcfService(openCloseService);
                return currentInformation;
            }, response =>
            {
                if (response) LoadWorkflowData();
            }, true);
        }

        private bool HasRequiredManagerCode(string roleCode)
        {
            try
            {
                string[] requiredRoles = { "M" };
                if (requiredRoles == null || !requiredRoles.Any())
                {
                    return false;
                }
                return requiredRoles.Contains(roleCode);
            }
            catch (Exception e)
            {
                //LoggingHelper.LogException("Error while checking roles in HasRequiredManagerCode", LogEventId.ErrorOpenCloseGetUserRoles, e);
                return false;
            }
        }

        #endregion

        #region Module implementation

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadLockStatusFromService();
        }

        #endregion

        #region Methods - Log Information

        private void LoadInformationPossibleCloseTypes()
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                var result = openCloseService.GetLogInformationPossibleClose(CurrentBusinessDay, NextBusinessDay, SettingsManager.Instance.OperatorId);
                WcfConnection.DisconnectWcfService(openCloseService);
                return result;
            }, result =>
            {
                if (result == null) return;

                var modes = new List<OpenCloseDayCloseModesViewModel>();
                foreach (var openCloseDayCloseTypese in result.CloseTypes)
                {
                    switch (openCloseDayCloseTypese)
                    {
                        case OpenCloseDayCloseTypes.Daily:
                            modes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_DayCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.DailyClose });
                            break;
                        case OpenCloseDayCloseTypes.Weekly:
                            modes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_WeekCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.WeeklyClose });
                            break;
                        case OpenCloseDayCloseTypes.Monthly:
                            modes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_MonthCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.MonthlyClose });
                            break;
                        case OpenCloseDayCloseTypes.Yearly:
                            modes.Add(new OpenCloseDayCloseModesViewModel { DisplayName = Translation["KEY_OpenCloseControlCenter_YearCloseType"], Value = SDMDefinitions.ControlTransactionReasonOrCloseCode.YearlyClose });
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                DayCloseModes.Clear();
                foreach (var mode in modes)
                {
                    DayCloseModes.Add(mode);
                }

                CurrentDayCloseMode = DayCloseModes.FirstOrDefault(x => x.Value == GetSDMDefinitionDayCloseMode(result.Suggestion));

                LoadWorkflowData();
            }, false);
        }

        #endregion

        #region Load Workflows from Service
        private void LoadWorkflowData(bool restartRunning = true)
        {
            //if (!Monitor.TryEnter(LoadLock, 2000))
            //{
            //    return;
            //}

            //var success = await LoadLockSlim.WaitAsync(500);
            //if (!success) return;

            if (_currentShowingDialog != null)
                return;

            LoadAsyncData(() =>
            {
                try
                {
                    Debug.WriteLine(CurrentTimeLogString + " AppService: LoadWorkflowData");

                    List<OpenCloseWorkflowStatusDto> result = new List<OpenCloseWorkflowStatusDto>();
                    IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                    result = openCloseService.GetStatusForWorkflows(NextBusinessDay, CurrentWorkflowMode.Mode, restartRunning, true);
                    WcfConnection.DisconnectWcfService(openCloseService);
                    return result;
                }
                catch (Exception)
                {
                    // WCF sometimes runs in a timeout, in this case the global lock would not release if not released here.
                    LoadLockSlim.Release();
                }

                return null;
            }, LoadWorkflowDataFromServiceFinished, false);
        }
        private void LoadWorkflowDataFromServiceFinished(List<OpenCloseWorkflowStatusDto> resultHolder)
        {
            if (resultHolder == null)
                return;

            List<OpenCloseWorkflowStatusDto> result = resultHolder;

            lock (FinishedLock)
            {
                if (resultProcessing) return;
                resultProcessing = true;
            }

            // remove all old items (not in the new result)
            var oldItems = WorkflowItems.Where(W => !result.Any(R => R.WorkingBusinessDay.Equals(W.WorkingBusinessDay) && R.WorkflowName.Equals(W.Name))).ToList();
            foreach (var item in oldItems)
            {
                WorkflowItems.Remove(item);
            }

            OpenCloseWorkflowItemStatusDto userQustionWorkstepStatus = null;
            OpenCloseWorkflowModel userQuestionWorkflowStatus = null;
            NextWorkflow = null;
            bool isNotValid = false;

            foreach (var item in result)
            {
                OpenCloseWorkflowModel currentWorkflowViewModel = WorkflowItems.SingleOrDefault(W => W.WorkingBusinessDay == item.WorkingBusinessDay && W.Name == item.WorkflowName);
                if (currentWorkflowViewModel == null)
                {
                    int insertIndex = (item.WorkingBusinessDay < SystemStatusManager.Instance.CurrentBusinessDay ? 0 : WorkflowItems.Count);

                    var item1 = item;
                    OpenCloseWorkflowModel nextModel = WorkflowItems.Where(W => W.WorkingBusinessDay == item1.WorkingBusinessDay).OrderByDescending(W => W.Order).FirstOrDefault();
                    if (nextModel != null)
                    {
                        insertIndex = WorkflowItems.IndexOf(nextModel) + 1;
                    }

                    currentWorkflowViewModel = new OpenCloseWorkflowModel();
                    currentWorkflowViewModel.ControlCenterViewModel = this;
                    WorkflowItems.Insert(insertIndex, currentWorkflowViewModel);
                }

                currentWorkflowViewModel.Name = item.WorkflowName;
                currentWorkflowViewModel.DisplayName = Translation["KEY_OpenClose_Workflow_" + item.WorkflowName];
                currentWorkflowViewModel.Workflow = item;
                currentWorkflowViewModel.WorkflowStatus = item.WorkflowStatus;
                currentWorkflowViewModel.CurrentWorkstepName = item.CurrentWorkstepName;
                currentWorkflowViewModel.WorkstepsProgress = (double)item.WorkflowProgressPercent;
                currentWorkflowViewModel.WorkingBusinessDay = item.WorkingBusinessDay;
                currentWorkflowViewModel.SettingsLocked = _confirmedBusinessDays.Contains(CurrentBusinessDay);
                currentWorkflowViewModel.Order = item.Order;
                currentWorkflowViewModel.StartButtonIsEnabled = item.StartIsAllowed;
                currentWorkflowViewModel.ShowAskForWeatherAndComment = item.ShowAskForWeatherAndComment;
                currentWorkflowViewModel.AskForCloseConfirmation = item.ShowAskForCloseConfirmation;
                currentWorkflowViewModel.ConfirmedClose = item.ConfirmedClose;
                currentWorkflowViewModel.IsNextRunningWorkflow = item.IsNextRunningWorkflow;
                if (item.LogInformation != null)
                {
                    currentWorkflowViewModel.AskForWeatherAndCommentIsEnabled = true;
                    currentWorkflowViewModel.CloseWeatherComment = item.LogInformation.WeatherComment;
                    currentWorkflowViewModel.CloseUserComment = item.LogInformation.UserComment;
                }
                if (item.DetailEntryList != null && item.DetailEntryList.Count > 0)
                {
                    currentWorkflowViewModel.WorkflowStarted = item.DetailEntryList.Any(D => D.EventType == SDMDefinitions.OpenCloseLogEntryEventType.WorkflowStarted);
                }


                if (item.IsNextRunningWorkflow)
                {
                    NextWorkflow = currentWorkflowViewModel;
                    //currentWorkflowViewModel.DetailVisibility = Visibility.Visible;
                }

                if (!item.ConfigurationIsValid)
                {
                    currentWorkflowViewModel.WorkflowStatus = WorkflowStatusType.NotValid;
                    isNotValid = true;
                }
                #region add check results

                foreach (var checkItem in item.CheckList)
                {
                    OpenCloseCheckViewModel currentItemViewModel = currentWorkflowViewModel.CheckItems.SingleOrDefault(I => I.Name == checkItem.Name);
                    if (currentItemViewModel == null)
                    {
                        currentItemViewModel = new OpenCloseCheckViewModel();
                        currentWorkflowViewModel.CheckItems.Add(currentItemViewModel);
                    }

                    currentItemViewModel.Workflow = currentWorkflowViewModel;
                    currentItemViewModel.Name = checkItem.Name;
                    currentItemViewModel.TypeName = checkItem.TypeName;
                    currentItemViewModel.DisplayName = Translation["KEY_OpenClose_" + checkItem.TypeName + "_DisplayName"];
                    if (checkItem.UserMessages != null && checkItem.UserMessages.Count > 0)
                    {
                        var userMessageDto = checkItem.UserMessages[checkItem.UserMessages.Count - 1];
                        if (userMessageDto != null)
                        {
                            currentItemViewModel.Description = new UserMessageViewModel(userMessageDto.Message, userMessageDto.CreationDate);
                        }
                    }
                    currentItemViewModel.WorkingDateTime = currentWorkflowViewModel.WorkingBusinessDay;
                    currentItemViewModel.IsSuccessful = checkItem.IsSuccessful;
                    currentItemViewModel.RunAtLeastOnce = checkItem.RunAtLeastOnce;
                    currentItemViewModel.IsRunning = checkItem.IsRunning;
                    currentItemViewModel.IsCritical = checkItem.IsCriticalItem;
                    currentItemViewModel.AllowManagerOverride = checkItem.AllowManagerOverride;
                    currentItemViewModel.Importance = checkItem.Importance;
                    currentItemViewModel.MessageOnFailed = checkItem.MessageOnFailed;

                    if (checkItem.HasCriticalError)
                    {
                        currentItemViewModel.Description = new UserMessageViewModel(_criticalErrorMessage, checkItem.CreationDate);
                    }
                }

                currentWorkflowViewModel.ResetButtonIsEnabled = item.CheckList.All(x => !x.IsRunning) && !item.ConfigurationIsValid;

                #endregion

                #region add workstep results

                currentWorkflowViewModel.WorkstepUserMessages.Clear();
                SolidColorBrush questionFontColor = ColorConverter.ColorToBrush("#b04f0e");

                foreach (var workstepItem in item.WorkstepList)
                {
                    #region Add or Update Workstep items

                    OpenCloseWorkstepViewModel currentItemViewModel = currentWorkflowViewModel.WorkstepItems.SingleOrDefault(I => I.Name == workstepItem.Name);
                    if (currentItemViewModel == null)
                    {
                        currentItemViewModel = new OpenCloseWorkstepViewModel();
                        currentWorkflowViewModel.WorkstepItems.Add(currentItemViewModel);
                    }

                    currentItemViewModel.Name = workstepItem.Name;
                    currentItemViewModel.TypeName = workstepItem.TypeName;
                    currentItemViewModel.DisplayName = GetTranslation(workstepItem.TypeName + "_DisplayName");
                    if (workstepItem.UserMessages != null && workstepItem.UserMessages.Count > 0)
                    {
                        var userMessageDto = workstepItem.UserMessages[workstepItem.UserMessages.Count - 1];
                        if (userMessageDto != null)
                        {
                            currentItemViewModel.Description = new UserMessageViewModel(userMessageDto.Message, userMessageDto.CreationDate);
                        }
                    }
                    currentItemViewModel.IsSuccessful = workstepItem.IsSuccessful;
                    currentItemViewModel.RunAtLeastOnce = workstepItem.RunAtLeastOnce;
                    currentItemViewModel.IsRunning = workstepItem.IsRunning;
                    currentItemViewModel.IsCritical = workstepItem.IsCriticalItem;
                    currentItemViewModel.AllowManagerOverride = workstepItem.AllowManagerOverride;
                    currentItemViewModel.Importance = workstepItem.Importance;
                    currentItemViewModel.MessageOnFailed = workstepItem.MessageOnFailed;

                    if (workstepItem.HasCriticalError)
                    {
                        currentItemViewModel.Description = new UserMessageViewModel(_criticalErrorMessage, workstepItem.CreationDate);
                    }

                    #endregion

                    if (workstepItem.TypeName.Equals("UserQuestionWorkstep", StringComparison.OrdinalIgnoreCase) && workstepItem.IsRunning)
                    {
                        userQuestionWorkflowStatus = currentWorkflowViewModel;
                        userQustionWorkstepStatus = workstepItem;
                    }

                    if (workstepItem.UserMessages != null && workstepItem.UserMessages.Count > 0)
                    {
                        foreach (OpenCloseUserMessageDto userMessage in workstepItem.UserMessages)
                        {
                            var messageModel = new UserMessageViewModel(userMessage.Message, userMessage.CreationDate);
                            if (userMessage.EventType == SDMDefinitions.OpenCloseLogEntryEventType.LegacyScreenMessage)
                            {
                                messageModel.MessageColor = questionFontColor;
                            }
                            currentWorkflowViewModel.WorkstepUserMessages.Add(messageModel);
                            if (currentWorkflowViewModel.WorkstepUserMessages.Count > 100) currentWorkflowViewModel.WorkstepUserMessages.RemoveAt(0);
                        }
                    }

                    if (workstepItem.TypeName.Equals("UserQuestionWorkstep", StringComparison.OrdinalIgnoreCase) && workstepItem.IsFinished)
                    {
                        string questionTerm;
                        workstepItem.Parameter.TryGetValue("QuestionTerm", out questionTerm);

                        string resultText = (workstepItem.IsSuccessful ? Translation["KEY_SD_Yes"] : Translation["KEY_SD_No"]);

                        UserMessageViewModel messageModel = new UserMessageViewModel(questionTerm, workstepItem.CreationDate);
                        messageModel.MessageColor = questionFontColor;
                        messageModel.MessageText += " (" + resultText + ")";

                        currentWorkflowViewModel.WorkstepUserMessages.Add(messageModel);
                    }
                }

                #endregion
            }

            #region LegacyBOScreen

            var activeWorkflowViewModel = WorkflowItems.FirstOrDefault(x => x.WorkflowStatus == WorkflowStatusType.Working);
            if (activeWorkflowViewModel != null && activeWorkflowViewModel.WorkflowStatus == WorkflowStatusType.Working && activeWorkflowViewModel.WorkstepLegacyScreen == null)
            {
                activeWorkflowViewModel.WorkstepLegacyScreen = new LegacyScreenOutputMessageViewModel();
            }
            var finishedWorkflowViewModel = WorkflowItems.Where(x => (x.WorkflowStatus == WorkflowStatusType.Finished || x.WorkflowStatus == WorkflowStatusType.Ready) && x.WorkstepLegacyScreen != null);
            foreach (var model in finishedWorkflowViewModel)
            {
                model.WorkstepLegacyScreen = null;
            }
            var legacyScreenViewModels = WorkflowItems.Count(x => x.WorkstepLegacyScreen != null);
            if (legacyScreenViewModels == 0)
            {
                LargeLegacyScreenVisible = Visibility.Collapsed;
            }

            #endregion

            #region Disable all start buttons if one workflow not valid
            if (isNotValid)
            {
                foreach (var item in WorkflowItems)
                {
                    if (item.StartButtonIsEnabled)
                        item.StartButtonIsEnabled = false;
                }
            }
            #endregion

            LastRefreshTime = DateTime.Now;

            lock (UserInputLock)
            {
                if (userQustionWorkstepStatus != null && userQuestionWorkflowStatus != null)
                {
                    ShowUserQuestion(userQuestionWorkflowStatus, userQustionWorkstepStatus);
                }
                //else if (_lastQuestionDialogWindow != null)
                //{
                //    _lastQuestionDialogWindow.Close();
                //    _lastQuestionDialogWindow = null;
                //}
            }

            UpdateSettingsLockedWorkflowItems();

            #region Workflow Autostart

            if (result.All(x => !x.IsRunning))
            {
                OpenCloseWorkflowStatusDto nextAutoResult = result.FirstOrDefault(R => R.StartIsAllowed && R.Autostart);
                if (nextAutoResult != null)
                {
                    //find correct viewmodel
                    OpenCloseWorkflowModel model = WorkflowItems.FirstOrDefault(W => W.WorkingBusinessDay.Equals(nextAutoResult.WorkingBusinessDay) && W.Name.Equals(nextAutoResult.WorkflowName));
                    if (model != null && !model.WorkflowStarted)
                    {
                        model.StartWorkflowCommandClick();
                    }
                }
            }

            #endregion

            #region Detect Changes

            if (LastState == null)
            {
                LastState = GetWorkflowState(WorkflowItems, true);
            }
            else
            {
                var currentState = GetWorkflowState(WorkflowItems);
                LastState.Update(currentState);

                foreach (var openCloseWorkflowModel in WorkflowItems)
                {
                    var previousWorkflowState = LastState.WorkflowState.FirstOrDefault(x => x.Name == openCloseWorkflowModel.Name && x.WorkingBusinessDay == openCloseWorkflowModel.WorkingBusinessDay);
                    if (previousWorkflowState == null) continue;

                    foreach (var openCloseCheckViewModel in openCloseWorkflowModel.CheckItems)
                    {
                        if (openCloseCheckViewModel.Importance != PrerequisiteImportance.CriticalWithStop) continue;

                        var previousCheck = previousWorkflowState.PrerequisiteStates.FirstOrDefault(x => x.Name == openCloseCheckViewModel.Name);
                        if (previousCheck == null) continue;

                        if (previousCheck.State == WorkflowPresentationStates.Presenting && previousCheck.RunAtLeastOnce && !previousCheck.IsSuccessful.GetValueOrDefault() && !string.IsNullOrWhiteSpace(openCloseCheckViewModel.MessageOnFailed))
                        {
                            //MSGBOX?
                            string messageBoxTitle = Translation["KEY_OpenCloseControlCenter_Title"];
                            PopupMessageHelper.ShowMessage(messageBoxTitle, openCloseCheckViewModel.MessageOnFailed.TranslateIfNeeded());
                            LastState.Finish(previousCheck);
                        }
                    }
                    foreach (var openCloseWorkstepViewModel in openCloseWorkflowModel.WorkstepItems)
                    {
                        if (openCloseWorkstepViewModel.Importance != PrerequisiteImportance.Critical) continue;

                        var previousWorkstep = previousWorkflowState.WorkstepStates.FirstOrDefault(x => x.Name == openCloseWorkstepViewModel.Name);
                        if (previousWorkstep == null) continue;

                        if (previousWorkstep.State == WorkflowPresentationStates.Presenting && previousWorkstep.RunAtLeastOnce && !previousWorkstep.IsSuccessful.GetValueOrDefault() && !string.IsNullOrWhiteSpace(openCloseWorkstepViewModel.MessageOnFailed))
                        {
                            //MSGBOX?
                            string messageBoxTitle = Translation["KEY_OpenCloseControlCenter_Title"];
                            PopupMessageHelper.ShowMessage(messageBoxTitle, openCloseWorkstepViewModel.MessageOnFailed.TranslateIfNeeded());
                            LastState.Finish(previousWorkstep);
                        }
                    }
                }
            }

            #endregion

            resultProcessing = false;
        }
        #endregion

        private WorkflowStates GetWorkflowState(IList<OpenCloseWorkflowModel> workflowItems, bool initialState = false)
        {
            var state = new WorkflowStates();
            foreach (var openCloseWorkflowModel in workflowItems)
            {
                var workflowState = state.AddWorkflowState(openCloseWorkflowModel.Name, openCloseWorkflowModel.WorkflowStatus == WorkflowStatusType.Finished, openCloseWorkflowModel.WorkingBusinessDay);
                foreach (var openCloseCheckViewModel in openCloseWorkflowModel.CheckItems)
                {
                    workflowState.AddPrerequisiteState(openCloseCheckViewModel.Name, initialState ? (bool?)null : openCloseCheckViewModel.IsSuccessful, openCloseCheckViewModel.RunAtLeastOnce, null, openCloseCheckViewModel.IsRunning);
                }
                foreach (var openCloseWorkstepViewModel in openCloseWorkflowModel.WorkstepItems)
                {
                    workflowState.AddWorkstepState(openCloseWorkstepViewModel.Name, initialState ? (bool?)null : openCloseWorkstepViewModel.IsSuccessful, openCloseWorkstepViewModel.RunAtLeastOnce, null, openCloseWorkflowModel.WorkflowStatus == WorkflowStatusType.Working);
                }
            }
            return state;
        }

        #region Methods - Get Lock Status
        private void LoadLockStatusFromService()
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                var currentInformation = openCloseService.GetLogInformationsForBusinessDays(CurrentBusinessDay, SystemStatusManager.Instance.PreviousBusinessDay);
                WcfConnection.DisconnectWcfService(openCloseService);
                return currentInformation;
            }, LockStatusLoaded, false);
        }

        private void LockStatusLoaded(IList<OpenCloseLogInformationDto> logInformations)
        {
            logInformations = logInformations ?? new List<OpenCloseLogInformationDto>();

            // Add previous business day to known list
            foreach (var openCloseLogInformationDto in logInformations)
            {
                Debug.WriteLine(CurrentTimeLogString + " ControlCenter: Confirmed Business Day " + openCloseLogInformationDto.NextBusinessDayDate);
                _confirmedBusinessDays.Add(openCloseLogInformationDto.NextBusinessDayDate);
            }

            var current = logInformations.FirstOrDefault(x => x.BusinessDayDate == CurrentBusinessDay);
            if (current != null)
            {
                CurrentWorkflowMode = WorkflowModeItems.FirstOrDefault(x => x.Mode == current.WorkflowMode);
                CurrentDayCloseMode = DayCloseModes.FirstOrDefault(x => x.Value == current.CloseTypeDefinition);
                NextBusinessDay = logInformations[0].NextBusinessDayDate;

                Debug.WriteLine(CurrentTimeLogString + " ControlCenter: Set CurrentWorkflowMode to " + CurrentWorkflowMode);
                Debug.WriteLine(CurrentTimeLogString + " ControlCenter: Set CurrentDayCloseMode to " + CurrentDayCloseMode);
                Debug.WriteLine(CurrentTimeLogString + " ControlCenter: Set NextBusinessDay to " + NextBusinessDay);

                LoadWorkflowData();
            }
            else
            {
                LoadInformationPossibleCloseTypes();
            }

            SettingsLocked = _confirmedBusinessDays.Contains(NextBusinessDay);
            Debug.WriteLine(CurrentTimeLogString + " ControlCenter: Settings locked " + SettingsLocked);
        }
        #endregion

        #region Methods - Weather Input

        public void SaveWeatherAndCommentInput(DateTime workingBusinessDay, string weather, string comment)
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                var b = openCloseService.UpdateLogInformationForBusinessDay(workingBusinessDay, null, null, null, weather, comment);
                WcfConnection.DisconnectWcfService(openCloseService);
                return b;
            }, null, false);
        }

        #endregion

        #region Helper

        private void UpdateSettingsLockedWorkflowItems()
        {
            foreach (var openCloseWorkflowModel in WorkflowItems)
            {
                openCloseWorkflowModel.SettingsLocked = _confirmedBusinessDays.Contains(openCloseWorkflowModel.WorkingBusinessDay);
            }
        }

        private void ResetViewSettings()
        {
            CurrentWorkflowMode = WorkflowModeItems.FirstOrDefault();
            CurrentDayCloseMode = DayCloseModes.FirstOrDefault();

            for (int index = 0; index < WorkflowItems.Count; index++)
            {
                var openCloseWorkflowModel = WorkflowItems[index];
                openCloseWorkflowModel.SetDetailVisibility(index == 0);
            }
        }

        private async void ShowUserQuestion(OpenCloseWorkflowModel workflowModel, OpenCloseWorkflowItemStatusDto workstepStatus)
        {
            lock (DialogLock)
            {
                if (_currentShowingDialog != null)
                    return;

                string questionTerm;
                workstepStatus.Parameter.TryGetValue("QuestionTerm", out questionTerm);
                string useCustomText;
                workstepStatus.Parameter.TryGetValue("UseCustomText", out useCustomText);
                bool useCustomTextParameter;
                Boolean.TryParse(useCustomText, out useCustomTextParameter);

                if (!useCustomTextParameter && !String.IsNullOrWhiteSpace(questionTerm))
                {
                    // Use QuestionTerm 
                    questionTerm = Translation[questionTerm];
                    _currentShowingDialog = PopupMessageHelper.ShowQuestionDialog(Translation["KEY_OpenClose_UserQuestion"], questionTerm, userResult =>
                    {
                        ShowUserQuestionComplete(workflowModel, workstepStatus, userResult);
                    });
                }
                else
                {
                    // Use custom user text
                    GetCustomUserQuestion(workflowModel, workstepStatus, userText =>
                    {
                        _currentShowingDialog = PopupMessageHelper.ShowQuestionDialog(Translation["UserQuestion"], userText, userResult =>
                        {
                            ShowUserQuestionComplete(workflowModel, workstepStatus, userResult);
                        });
                    });
                }
            }
        }

        private void ShowUserQuestionComplete(OpenCloseWorkflowModel workflowModel, OpenCloseWorkflowItemStatusDto workstepStatus, bool userResult)
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                openCloseService.SetUserQuestionResult(workflowModel.Name, workstepStatus.Name, SettingsManager.Instance.OperatorId, userResult, workflowModel.WorkingBusinessDay, _currentBusinessDay, _nextBusinessDay);
                WcfConnection.DisconnectWcfService(openCloseService);
            }, () =>
            {
                _currentShowingDialog = null;
            });
        }

        private void GetCustomUserQuestion(OpenCloseWorkflowModel workflowModel, OpenCloseWorkflowItemStatusDto workstepStatus, Action<string> action)
        {
            LoadAsyncData(() =>
            {
                IOpenCloseService openCloseService = WcfConnection.ConnectWcfService<IOpenCloseService>();
                string userMessage = openCloseService.GetCustomUserQuestion(workflowModel.Name, workstepStatus.Name, SettingsManager.Instance.OperatorId, workflowModel.WorkingBusinessDay);
                WcfConnection.DisconnectWcfService(openCloseService);
                return userMessage;
            }, s =>
            {
                action(s);
            });
        }

        private string GetTranslation(string keySuffix)
        {
            return Translation[_translationPrefix + keySuffix];
        }

        private string GetSDMDefinitionWorkflowMode(OpenCloseWorkflowModeType mode)
        {
            if (mode == OpenCloseWorkflowModeType.Normal)
                return SDMDefinitions.OpenCloseLogInformationWorkflowMode.Normal;
            if (mode == OpenCloseWorkflowModeType.Offline)
                return SDMDefinitions.OpenCloseLogInformationWorkflowMode.Offline;
            throw new ArgumentOutOfRangeException("mode");
        }

        private string GetSDMDefinitionDayCloseMode(OpenCloseDayCloseTypes type)
        {
            if (type == OpenCloseDayCloseTypes.Daily)
                return SDMDefinitions.ControlTransactionReasonOrCloseCode.DailyClose;
            if (type == OpenCloseDayCloseTypes.Weekly)
                return SDMDefinitions.ControlTransactionReasonOrCloseCode.WeeklyClose;
            if (type == OpenCloseDayCloseTypes.Monthly)
                return SDMDefinitions.ControlTransactionReasonOrCloseCode.MonthlyClose;
            if (type == OpenCloseDayCloseTypes.Yearly)
                return SDMDefinitions.ControlTransactionReasonOrCloseCode.YearlyClose;
            throw new ArgumentOutOfRangeException("type");
        }

        #endregion

        #region INotificationModule implementation

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                switch (notification.Type)
                {
                    case NotificationApplicationType.OpenClose_ControlCenter_WorkflowDataChanged:
                        {
                            //Debug.WriteLine("Event: WorkflowDataChanged");
                            bool restartRunning = (notification.Parameter is bool && (bool)notification.Parameter);
                            LoadWorkflowData(restartRunning);
                        }
                        break;
                    case NotificationApplicationType.BusinessDayIsChanged:
                        {
                            //LoggingHelper.LogInformation("OCC business day changed.", LogEventId.Information);
                            SystemStatusManager.Instance.UpdateStatusDataAsync();
                            PreviousBusinessDay = SystemStatusManager.Instance.PreviousBusinessDay;
                            CurrentBusinessDay = SystemStatusManager.Instance.CurrentBusinessDay;
                            NextBusinessDay = CurrentBusinessDay.AddDays(1);

                            ResetViewSettings();
                            LoadLockStatusFromService();
                        }
                        break;
                    case NotificationApplicationType.OpenClose_ControlCenter_UserInputAnswered:
                        {
                            if (_currentShowingDialog != null)
                            {
                                PopupMessageHelper.CloseDialog(_currentShowingDialog);
                                _currentShowingDialog = null;
                            }
                            LoadWorkflowData();
                        }
                        break;
                    case NotificationApplicationType.OpenClose_LegacyScreen_ScreenUpdate:
                        {
                            Notify("CurrentActiveWorkflowItem");
                            Notify("CurrentActiveWorkflowItemMessages");
                        }
                        break;
                }
            });
        }

        #endregion
    }
}
