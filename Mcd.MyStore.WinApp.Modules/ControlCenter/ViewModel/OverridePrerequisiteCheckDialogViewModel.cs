﻿using Mcd.MyStore.WinApp.Core.Manager.PopupDialog;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.ControlCenter.DataObject;

namespace Mcd.MyStore.WinApp.Modules.ControlCenter.ViewModel
{
    public class OverridePrerequisiteCheckDialogViewModel : PopupDialogContentViewModel
    {
        private string _reason;
        private string _username;
        private string _passwordInput;

        public string Reason
        {
            get { return _reason; }
            set
            {
                if (SetValue(ref _reason, value, "Reason"))
                {
                    Notify("IsSaveEnabled");
                }
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                if (SetValue(ref _username, value, "Username"))
                {
                    Notify("IsSaveEnabled");
                }
            }
        }

        public string PasswordInput
        {
            get { return _passwordInput; }
            set
            {
                if (SetValue(ref _passwordInput, value, ""))
                {
                    Notify("IsSaveEnabled");
                }
            }
        }

        public bool IsSaveEnabled
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Reason)
                    && !string.IsNullOrWhiteSpace(Username)
                    && !string.IsNullOrWhiteSpace(PasswordInput)
                    && Username.Length > 1
                    && Reason.Length >= 10;
            }
        }

        public OpenCloseCheckViewModel CheckItem { get; set; }

        public string InformationText
        {
            get
            {
                var rawTranslation = Translation["KEY_OpenCloseControlCenter_OverridePrerequisiteCheckReasonInformation"];
                return string.Format(rawTranslation, CheckItem.DisplayName);
            }
        }
    }
}