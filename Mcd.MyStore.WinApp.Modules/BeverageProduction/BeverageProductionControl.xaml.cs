﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.BeverageProduction.ViewModel;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.BeverageProduction
{
    [MenuModuleAttribute("BeverageDessert")]
    public sealed partial class BeverageProductionControl
    {
        public static BeverageProductionControl Instance { get; set; }


        public BeverageProductionControl()
        {
            Instance = this;
            InitializeComponent();
            DataContext = new BeverageProductionViewModel();

        }
    }
}
