﻿using Mcd.MyStore.WinApp.Modules.AppLogin.ViewModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.AppLogin
{
    public sealed partial class UserLogin : UserControl
    {
        public UserLogin()
        {
            this.InitializeComponent();
            DataContext = new UserLoginViewModel();
        }

        private void LoadingTextFadeAway_Completed(object sender, object e)
        {
            LoadingTextFadeIn.Begin();
            LoginButton.Focus(FocusState.Programmatic);
        }

        private void UserNameTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                UserPasswordBox.Focus(FocusState.Keyboard);
            }
        }

        private void UserPasswordBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                LoginButton.Focus(FocusState.Programmatic);
            }
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            UsernameTextBox.Text = UsernameTextBox.Text.ToUpper();
            UsernameTextBox.SelectionStart = UsernameTextBox.Text.Length;
        }
    }
}
