﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System.Globalization;

namespace Mcd.MyStore.WinApp.Modules.AppLogin.ViewModel
{
    public class LangaugeItemViewModel : ViewModelBase
    {
        #region Member - Culture
        private CultureInfo _culture;
        public CultureInfo Culture
        {
            get { return _culture; }
            set { SetValue(ref _culture, value); }
        }
        #endregion

        #region Member - Text
        public string Text
        {
            get
            {
                return _culture.Parent.NativeName;
            }
        }
        #endregion

    }
}
