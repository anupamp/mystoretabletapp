﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.SystemMaintenance.Common;
using Mcd.MyStore.WinApp.Core.Manager;
using Mcd.MyStore.WinApp.Core.Manager.ExternalDataPack;
using Mcd.MyStore.WinApp.Core.Manager.Language;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.Notification;
using Mcd.MyStore.WinApp.Core.Manager.Resources;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.Manager.User;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.Inventory.Manager;
using Mcd.MyStore.WinApp.SystemMaintenance.WcfService.Common;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;

namespace Mcd.MyStore.WinApp.Modules.AppLogin.ViewModel
{
    public class UserLoginViewModel : ViewModelBase
    {
        #region Member - UserLoadingState
        private LoadingState _userLoadingState = LoadingState.None;
        public LoadingState UserLoadingState
        {
            get { return _userLoadingState; }
            set
            {
                if (SetValue(ref _userLoadingState, value))
                {
                    Notify("LoadingText");
                }
            }
        }
        #endregion



        #region Member - LoadingText
        public string LoadingText
        {
            get
            {
                switch (_userLoadingState)
                {
                    case LoadingState.Booting:
                        return "Booting system...";
                    case LoadingState.WaitForService:
                        return "Waiting for service...";
                    case LoadingState.LoadingManager:
                        return "Waiting for the manager...";
                    default:
                        return Translation["Login_Loading_" + _userLoadingState.ToString()];
                }
            }
        }
        #endregion

        public ObservableCollection<LangaugeItemViewModel> LanguageList { get; set; }
        #region Member - SelectedLanguageItem
        private LangaugeItemViewModel _selectedLanguageItem;
        public LangaugeItemViewModel SelectedLanguageItem
        {
            get { return _selectedLanguageItem; }
            set
            {
                if (SetValue(ref _selectedLanguageItem, value))
                {
                    LanguageManager.Instance.CurrentCulture = _selectedLanguageItem.Culture;
                }
            }
        }
        #endregion

        #region Member - IsLoginVisible
        private bool _isLoginVisible = false;
        public bool IsLoginVisible
        {
            get { return _isLoginVisible; }
            set
            {
                if (SetValue(ref _isLoginVisible, value))
                {
                    IsUserInputEnabled = value;
                }
            }
        }
        #endregion

        #region Member - IsUserInputEnabled
        private bool _isUserInputEnabled = true;
        public bool IsUserInputEnabled
        {
            get { return _isUserInputEnabled; }
            set { SetValue(ref _isUserInputEnabled, value); }
        }
        #endregion

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetValue(ref _username, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetValue(ref _password, value); }
        }


        public ICommand LoginCommand { get; set; }

        public UserLoginViewModel()
        {
            LanguageManager.Instance.OnCurrentCultureIsChanged += Instance_OnCurrentCultureIsChanged;
            LanguageList = new ObservableCollection<LangaugeItemViewModel>();

            LoginCommand = new RelayCommand(() =>
            {
                IsUserInputEnabled = false;
                CheckUserLoginAndStartApp();
            });

            StartInitLoadingAsync();
        }

        private void Instance_OnCurrentCultureIsChanged(object sender, EventArgs e)
        {
            Notify("Translation");
            Notify("LoadingText");
        }


        private async void StartInitLoadingAsync()
        {
            UserLoadingState = LoadingState.Booting;
            await Task.Delay(1500);

            UserLoadingState = LoadingState.WaitForService;
            ApplicationResourcesManager.Instance.UpdateResourcesDictionary(Application.Current.Resources);

            await SettingsManager.Instance.InitManager();
            await WaitForService();
            UserLoadingState = LoadingState.LoadingTranslations;
            await LanguageManager.Instance.InitManager();
            FillLanguageData();

        

            UserLoadingState = LoadingState.LoadingManager;

            InventoryRetryManager.Instance.Init();
            await ExternalDataPackManager.Instance.InitManager();
            await ModuleManager.Instance.InitManager();
            await SystemStatusManager.Instance.InitUpdate();
            NotificationManager.Instance.InitManager();
            CyclicTaskManager.InitManager();

            UserLoadingState = LoadingState.ReadyForLogin;
            IsLoginVisible = true;
        }

        [DebuggerStepThrough]
        private Task WaitForService()
        {
            return Task.Run(async () =>
            {
                while (true)
                {
                    ServiceStatusDto serviceSatus = null;
                    try
                    {
                        serviceSatus = await WcfConnection.ExecuteActionOnInterface((ISystemMaintenanceServiceForApp currentInterface) =>
                        {
                            return currentInterface.GetServiceStatus();
                        });
                    }
                    catch (Exception ex)
                    {
                        // service is not ready
                    }

                    if (serviceSatus != null && serviceSatus.IsReady)
                        break;

                    await Task.Delay(1000);
                }
            });
        }

        private void FillLanguageData()
        {
            foreach (var item in LanguageManager.Instance.AvailableCultures)
            {
                LanguageList.Add(new LangaugeItemViewModel()
                {
                    Culture = item
                });
            }
            SelectedLanguageItem = LanguageList.FirstOrDefault();
        }

        private void CheckUserLoginAndStartApp()
        {
            UserLoadingState = LoadingState.IsLoggingIn;
            var username = Username;
            var password = Password;

            Password = String.Empty;

            var isLoggedIn = UserManager.Instance.Login(username, password);
            if (isLoggedIn == false)
            {
                UserLoadingState = LoadingState.LoginFailed;
            }else
            {
                UserLoadingState = LoadingState.ReadyForLogin;
            }
            IsUserInputEnabled = true;
        }

        public enum LoadingState
        {
            None,
            Booting,
            WaitForService,
            LoadingManager,
            ReadyForLogin,
            LoadingTranslations,
            StartDashboard,
            IsLoggingIn,
            LoginFailed
        }
    }
}
