﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.TopProducts.ViewModel;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.TopProducts
{
    [MenuModuleAttribute("Top10Products")]
    public sealed partial class TopProductsControl : UserControl
    {
        public TopProductsControl()
        {
            this.InitializeComponent();
            DataContext = new TopProductsControlViewModel();
        }
    }
}
