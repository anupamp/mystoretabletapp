﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.DataLoading;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.TopProducts.ViewModel
{
    public class TopProductsControlViewModel : ModuleViewModelBase
    {
        private PODSelectionViewModel _podSelectionViewModel = new PODSelectionViewModel();
        public PODSelectionViewModel PODSelectionViewModel
        {
            get { return _podSelectionViewModel; }
        }

        public ObservableCollection<ProductChartItemViewModel> DataItems { get; set; }


        public TopProductsControlViewModel()
        {
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);

            DataItems = new ObservableCollection<ProductChartItemViewModel>();
            _podSelectionViewModel.OnSelectionChanged += PodSelection_OnSelectionChanged;

            IsAutoReloadEnabled = true;

            LoadEmptyInitData();
        }

        private void LoadEmptyInitData()
        {
            for (int i = 0; i < 10; i++)
            {
                ProductChartItemViewModel newModel = new ProductChartItemViewModel()
                {
                    ID = 0,
                    Title = "-"
                };

                newModel.AddDataValue(new ProductChartDataItemViewModel()
                {
                    Value = 0,
                    FillColor = (SolidColorBrush)Application.Current.Resources["LineColorHighlight"]
                });

                DataItems.Add(newModel);
            }
        }

        private void PodSelection_OnSelectionChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        public override void TriggerDataLoading()
        {
            LoadData();
        }

        public void LoadData()
        {
            LoadAsyncData<TopProductsDto>(LoadDataAsync, LoadDataAsyncFinished);
        }

        private TopProductsDto LoadDataAsync()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return null;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();

                var result = service.GetTopProducts(selectedDate, selectedDate, 10, _podSelectionViewModel.CurrentSelection);
                WcfConnection.DisconnectWcfService(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private async void LoadDataAsyncFinished(TopProductsDto result)
        {
            if (result == null || result.TopProducts == null)
            {
                return;
            }

            string oldIDValue = String.Join(",", DataItems.Select(P => P.ID.ToString()).ToArray());
            string newIDValue = String.Join(",", result.TopProducts.Select(P => P.ID.ToString()).ToArray());
            bool hasSameProducts = oldIDValue.Equals(newIDValue, StringComparison.CurrentCultureIgnoreCase);

            if (!hasSameProducts)
            {
                string[] images = result.TopProducts.Select(P => "/Mcd.MyStore.WinApp.Modules/Images/" + P.ID.ToString() + ".png").ToArray();
                await ImageManager.Instance.PreloadImages(images);

                DataItems.Clear();
                foreach (var currentProduct in result.TopProducts)
                {
                    ProductChartItemViewModel newModel = new ProductChartItemViewModel()
                    {
                        ID = currentProduct.ID,
                        Title = currentProduct.Name,
                        //TopImage = await ImageManager.Instance.GetImage("/Mcd.MyStore.WinApp.Modules/Images/" + currentProduct.ID.ToString() + ".png")
                    };

                    newModel.AddDataValue(new ProductChartDataItemViewModel()
                    {
                        Value = (int)currentProduct.Value,
                        FillColor = (SolidColorBrush)Application.Current.Resources["LineColorHighlight"]
                    });

                    DataItems.Add(newModel);
                }
            }
            else
            {
                int count = result.TopProducts.Count;
                for (int i = 0; i < count; i++)
                {
                    DataItems[i].DataValues[0].Value = (int)Math.Round(result.TopProducts[i].Value);
                }
            }

            ModuleManager.Instance.RestartReloadTimer();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadData();
            }
        }
    }
}
