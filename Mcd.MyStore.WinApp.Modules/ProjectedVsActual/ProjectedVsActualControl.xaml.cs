﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.ProjectedVsActual.ViewModel;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.ProjectedVsActual
{
    [MenuModuleAttribute("ProjectedVsActual")]
    public sealed partial class ProjectedVsActualControl : UserControl
    {
        public ProjectedVsActualControl()
        {
            this.InitializeComponent();
            DataContext = new ProjectedVsActualControlViewModel();
        }
    }
}
