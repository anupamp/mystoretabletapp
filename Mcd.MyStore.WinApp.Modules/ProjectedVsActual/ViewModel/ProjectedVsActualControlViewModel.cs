﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using Mcd.MyStore.WinApp.Modules.ProjectedVsActual.ViewModel.DataViewModel;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace Mcd.MyStore.WinApp.Modules.ProjectedVsActual.ViewModel
{
    public class ProjectedVsActualControlViewModel : ModuleViewModelBase
    {
        public ObservableCollection<ProjectedVsActualItemViewModel> ProjectedVsActualList { get; set; }
        public ObservableCollection<ProductChartItemViewModel> GuestCountDataItems { get; set; }
        public ObservableCollection<ProductChartItemViewModel> CrewHoursDataItems { get; set; }

        #region Member TotalGuestCountProjected
        private int _totalGuestCountProjected;
        public int TotalGuestCountProjected
        {
            get { return _totalGuestCountProjected; }
            set
            {
                if (SetValue<int>(ref _totalGuestCountProjected, value))
                {
                    Notify("TotalGuestCountDifference");
                }
            }
        }
        #endregion
        #region Member TotalGuestCountActual
        private int _totalGuestCountActual;
        public int TotalGuestCountActual
        {
            get { return _totalGuestCountActual; }
            set
            {
                if (SetValue<int>(ref _totalGuestCountActual, value))
                {
                    Notify("TotalGuestCountDifference");
                }
            }
        }
        #endregion
        #region Member - TotalGuestCountDifference
        public int TotalGuestCountDifference
        {
            get { return TotalGuestCountActual - TotalGuestCountProjected; }
        }
        #endregion

        #region Member TotalCrewHoursProjected
        private int _totalCrewHoursProjected;
        public int TotalCrewHoursProjected
        {
            get { return _totalCrewHoursProjected; }
            set
            {
                if (SetValue<int>(ref _totalCrewHoursProjected, value))
                {
                    Notify("TotalCrewHoursDifference");
                }
            }
        }
        #endregion
        #region Member TotalCrewHoursActual
        private int _totalCrewHoursActual;
        public int TotalCrewHoursActual
        {
            get { return _totalCrewHoursActual; }
            set
            {
                if (SetValue<int>(ref _totalCrewHoursActual, value))
                {
                    Notify("TotalCrewHoursDifference");
                }
            }
        }
        #endregion
        #region Member - TotalCrewHoursDifference
        public int TotalCrewHoursDifference
        {
            get { return TotalCrewHoursActual - TotalCrewHoursProjected; }
        }
        #endregion


        #region Member LegendName1
        private string _legendName1;
        public string LegendName1
        {
            get { return _legendName1; }
            set { SetValue<string>(ref _legendName1, value); }
        }
        #endregion
        #region Member LegendName2
        private string _legendName2;
        public string LegendName2
        {
            get { return _legendName2; }
            set { SetValue<string>(ref _legendName2, value); }
        }
        #endregion

        public ProjectedVsActualControlViewModel()
        {
            IsAutoReloadEnabled = true;
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            ProjectedVsActualList = new ObservableCollection<ProjectedVsActualItemViewModel>();
            GuestCountDataItems = new ObservableCollection<ProductChartItemViewModel>();
            CrewHoursDataItems = new ObservableCollection<ProductChartItemViewModel>();
            LoadEmptyInitData();
        }

        private async void LoadEmptyInitData()
        {
            try
            {
                LegendName1 = "Projected";
                LegendName2 = "Actual";

                DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

                SystemStatusForBusinessDayDto statusDto = await SystemStatusManager.Instance.GetOpeningAndClosingTimeForBusinessDayTask(selectedDate);
                if (statusDto == null)
                    return;

                DateTime startDate = statusDto.OpeningTime.AddTicks(-selectedDate.Ticks);
                DateTime endDate = statusDto.ClosingTime.AddTicks(-selectedDate.Ticks);
                for (DateTime i = startDate; i < endDate; i = i.AddHours(1))
                {
                    ProjectedVsActualList.Add(new ProjectedVsActualItemViewModel()
                    {
                        Time = i,
                        GuestCountProjected = 0,
                        GuestCountActual = 0,
                        CrewHoursProjected = 0,
                        CrewHoursActual = 0
                    });

                    #region Create empty Chart - GuestCountDataItems
                    ProductChartItemViewModel newItem = new ProductChartItemViewModel()
                    {
                        ID = i,
                        Title = i.ToString("HH")
                    };
                    newItem.AddDataValue(new ProductChartDataItemViewModel()
                    {
                        Value = 0,
                        FillColor = (SolidColorBrush)Application.Current.Resources["GuestCountColor1Brush"]
                    });
                    newItem.AddDataValue(new ProductChartDataItemViewModel()
                    {
                        Value = 0,
                        FillColor = (SolidColorBrush)Application.Current.Resources["GuestCountColor2Brush"]
                    });

                    GuestCountDataItems.Add(newItem);
                    #endregion

                    #region Create empty Chart - CrewHoursDataItems

                    ProductChartItemViewModel newItem2 = new ProductChartItemViewModel()
                    {
                        ID = i,
                        Title = i.ToString("HH")
                    };
                    newItem2.AddDataValue(new ProductChartDataItemViewModel()
                    {
                        Value = 0,
                        FillColor = (SolidColorBrush)Application.Current.Resources["CrewHoursColor1Brush"]
                    });
                    newItem2.AddDataValue(new ProductChartDataItemViewModel()
                    {
                        Value = 0,
                        FillColor = (SolidColorBrush)Application.Current.Resources["CrewHoursColor2Brush"]
                    });

                    CrewHoursDataItems.Add(newItem2);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

        }

        public void LoadData()
        {
            LoadAsyncData<HourlySalesWidgetData>(LoadDataAsync, LoadDataAsyncFinished);
        }

        private HourlySalesWidgetData LoadDataAsync()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return null;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                var result = service.GetProjectedVsActualWWC(selectedDate);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadDataAsyncFinished(HourlySalesWidgetData result)
        {
            if (result == null)
            {
                return;
            }

            int currentHour = DateTime.Now.Hour;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;
            int totalGuestCount = 0;
            foreach (var currentHourlyItem in result.HourlyMetrics)
            {
                int guestCount = (int)Math.Round(currentHourlyItem.Value.ActualSales);
                totalGuestCount += guestCount;

                ProjectedVsActualItemViewModel foundItem = ProjectedVsActualList.SingleOrDefault(I => I.Time.Equals(currentHourlyItem.Key.AddTicks(-selectedDate.Ticks)));
                if (foundItem != null)
                {
                    foundItem.GuestCountActual = (int)currentHourlyItem.Value.ActualGuests;
                    foundItem.GuestCountProjected = (int)currentHourlyItem.Value.ProjectedGuests;
                    foundItem.CrewHoursActual = (int)currentHourlyItem.Value.ActualCrewHours;
                    foundItem.CrewHoursProjected = (int)currentHourlyItem.Value.ProjectedCrewHours;
                }
            }
            TotalGuestCountProjected = ProjectedVsActualList.Sum(P => P.GuestCountProjected);
            TotalGuestCountActual = ProjectedVsActualList.Sum(P => P.GuestCountActual);
            TotalCrewHoursProjected = ProjectedVsActualList.Sum(P => P.CrewHoursProjected);
            TotalCrewHoursActual = ProjectedVsActualList.Sum(P => P.CrewHoursActual);


            foreach (var currentGuestCount in GuestCountDataItems)
            {
                ProjectedVsActualItemViewModel foundItem = ProjectedVsActualList.FirstOrDefault(I => I.Time == ((DateTime)currentGuestCount.ID));
                currentGuestCount.DataValues[0].Value = foundItem.GuestCountProjected;
                currentGuestCount.DataValues[1].Value = foundItem.GuestCountActual;
            }

            foreach (var currentCrewHour in CrewHoursDataItems)
            {
                ProjectedVsActualItemViewModel foundItem = ProjectedVsActualList.FirstOrDefault(I => I.Time == ((DateTime)currentCrewHour.ID));
                currentCrewHour.DataValues[0].Value = foundItem.CrewHoursProjected;
                currentCrewHour.DataValues[1].Value = foundItem.CrewHoursActual;
            }
            ModuleManager.Instance.RestartReloadTimer();
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadData();
            }
        }

        public override void TriggerDataLoading()
        {
            LoadData();
        }
    }
}
