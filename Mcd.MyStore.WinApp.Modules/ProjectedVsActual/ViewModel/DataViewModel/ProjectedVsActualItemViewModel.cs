﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;

namespace Mcd.MyStore.WinApp.Modules.ProjectedVsActual.ViewModel.DataViewModel
{
    public class ProjectedVsActualItemViewModel : ViewModelBase
    {
        private DateTime _time = DateTime.MinValue;
        public DateTime Time
        {
            get { return _time; }
            set
            {
                SetValue(ref _time, value);
                Notify("TimeText");
            }
        }

        public string TimeText
        {
            get { return _time.ToString("HH\\:mm"); }
        }

        #region Guest Count
        private int _guestCountProjected = 0;
        public int GuestCountProjected
        {
            get { return _guestCountProjected; }
            set { SetValue<int>(ref _guestCountProjected, value); }
        }

        private int _guestCountActual = 0;
        public int GuestCountActual
        {
            get { return _guestCountActual; }
            set { SetValue<int>(ref _guestCountActual, value); }
        }

        #region Member - GuestCountDifference
        private int _guestCountDifference;
        public int GuestCountDifference
        {
            get { return _guestCountDifference; }
            set { SetValue(ref _guestCountDifference, value); }
        }
        #endregion

        #endregion

        #region Crew Hours
        private int _crewHoursProjected = 0;
        public int CrewHoursProjected
        {
            get { return _crewHoursProjected; }
            set { SetValue<int>(ref _crewHoursProjected, value); }
        }

        private int _crewHoursActual = 0;
        public int CrewHoursActual
        {
            get { return _crewHoursActual; }
            set { SetValue<int>(ref _crewHoursActual, value); }
        }

        #region Member - CrewHoursDifference
        private int _crewHoursDifference;
        public int CrewHoursDifference
        {
            get { return _crewHoursDifference; }
            set { SetValue(ref _crewHoursDifference, value); }
        }
        #endregion

        #endregion
    }
}
