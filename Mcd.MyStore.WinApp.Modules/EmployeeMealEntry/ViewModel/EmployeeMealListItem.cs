﻿using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.ViewModel
{
    public class EmployeeMealListItem : ViewModelBase
    {
        #region Member - ItemName
        private string _itemName;
        public string ItemName
        {
            get { return _itemName; }
            set { SetValue(ref _itemName, value); }
        }
        #endregion

        #region Member - ItemID
        private int _itemID;
        public int ItemID
        {
            get { return _itemID; }
            set { SetValue(ref _itemID, value); }
        }
        #endregion

        #region Member - ItemCode
        private int _itemCode;
        public int ItemCode
        {
            get { return _itemCode; }
            set { SetValue(ref _itemCode, value); }
        }
        #endregion

        #region Member - Quantity
        private int _quantity;
        public int Quantity
        {
            get { return _quantity; }
            set
            {
                if (SetValue(ref _quantity, value))
                {
                    Notify("TotalPrice");
                }
            }
        }
        #endregion

        #region Member - Price
        private decimal _price;
        public decimal Price
        {
            get { return _price; }
            set
            {
                if (SetValue(ref _price, value))
                {
                    Notify("TotalPrice");
                }
            }
        }
        #endregion

        #region Member - TotalPrice
        public decimal TotalPrice
        {
            get { return _quantity * _price; }
        }
        #endregion

        public ICommand ChangeQuantityCommand { get; set; }

        private EmployeeMealEntryViewModel _moduleViewModel;

        public EmployeeMealListItem(EmployeeMealEntryViewModel moduleViewModel)
        {
            _moduleViewModel = moduleViewModel;
            ChangeQuantityCommand = new RelayCommand(ChangeQuantityCommandClick);
        }

        private object ChangeQuantityCommandClick(object parameter)
        {
            string command = (parameter as string);
            if (!string.IsNullOrEmpty(command))
            {
                if (command == "-")
                {
                    if (Quantity > 0)
                    {
                        Quantity--;
                    }
                }
                else
                {
                    Quantity++;
                }

                _moduleViewModel.SaveMealEntryItems();
            }

            return true;
        }
    }
}
