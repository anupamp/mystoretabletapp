﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.ViewModel
{
    public class EmployeeUserListItem : ViewModelBase
    {
        #region Member - PartyID
        private int _partyID;
        public int PartyID
        {
            get { return _partyID; }
            set { SetValue(ref _partyID, value); }
        }
        #endregion

        #region Member - EmployeeNumber
        private int _employeeNumber;
        public int EmployeeNumber
        {
            get { return _employeeNumber; }
            set { SetValue(ref _employeeNumber, value); }
        }
        #endregion


        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }
        #endregion

    }
}
