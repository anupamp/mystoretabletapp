﻿using Mcd.MyStore.Cash.Common;
using Mcd.MyStore.Cash.Common.DataContract;
using Mcd.MyStore.Cash.WcfService.Common;
using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.WinApp.Core;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Core.UI.Common;
using Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.DataObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.ViewModel
{
    public class EmployeeMealEntryViewModel : ModuleViewModelBase
    {
        #region Member - SelectedEmployeeUser
        private EmployeeUserListItem _selectedEmployeeUser;
        public EmployeeUserListItem SelectedEmployeeUser
        {
            get { return _selectedEmployeeUser; }
            set
            {
                if (SetValue(ref _selectedEmployeeUser, value))
                {
                    Notify("IsProductSelectionActive");
                    Notify("EmployeeMealListTitle");
                    UpdateMealEntryItems();
                }
            }
        }
        #endregion

        #region Member - IsProductSelectionActive
        public bool IsProductSelectionActive
        {
            get { return (_selectedEmployeeUser != null && !_readOnlyMode); }
        }
        #endregion

        #region Member - ReadOnlyMode
        private bool _readOnlyMode = true;
        public bool ReadOnlyMode
        {
            get { return _readOnlyMode; }
            set
            {
                if (SetValue(ref _readOnlyMode, value))
                {
                    Notify("SelectUserOrReadOnlyModeText");
                }
            }
        }
        #endregion

        #region Member - SelectUserOrReadOnlyModeText
        public string SelectUserOrReadOnlyModeText
        {
            get { return (_readOnlyMode ? Translation["EmployeeMealEntry_ReadOnlyMode"] : Translation["EmployeeMealEntry_SelectUser"]); }
        }
        #endregion

        #region Member - EmployeeMealListTitle
        public string EmployeeMealListTitle
        {
            get
            {
                return (_selectedEmployeeUser != null && (_selectedEmployeeUser.PartyID != (int)SDMDefinitions.ItemTransaction_Operator.StorewideEmployeeMeal)
                    ? String.Format(Translation["EmployeeMealEntry_EmployeeMealList"], _selectedEmployeeUser.Name, _selectedEmployeeUser.EmployeeNumber)
                    : Translation["EmployeeMealEntry_EmployeeMealListNoUser"]);
            }
        }
        #endregion

        #region Member - SearchText
        private string _lastSearchText = String.Empty;
        private string _searchText = String.Empty;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (SetValue(ref _searchText, value))
                {
                    LoadMoreProducts();
                }
            }
        }
        #endregion

        #region Member - WorkerSearchText
        private string _workerSearchText;
        public string WorkerSearchText
        {
            get { return _workerSearchText; }
            set
            {
                if (SetValue(ref _workerSearchText, value))
                {
                    RefreshWorkerList();
                }
            }
        }
        #endregion

        #region Member - ShowOnlyWorkersWithoutMeals
        private bool _showOnlyWorkersWithoutMeals = false;
        public bool ShowOnlyWorkersWithoutMeals
        {
            get { return _showOnlyWorkersWithoutMeals; }
            set
            {
                if (SetValue(ref _showOnlyWorkersWithoutMeals, value))
                {
                    RefreshWorkerList();
                }
            }
        }
        #endregion


        public ObservableCollection<EmployeeMealListItem> MealItems { get; set; }
        public ObservableCollection<EmployeeUserListItem> AvailableEmployees { get; set; }
        public ObservableCollection<ProductListItem> ProductList { get; set; }

        public RelayCommand ReloadDataCommand { get; set; }

        private LastResponseDto _lastResponse = new LastResponseDto();
        private readonly object _saveLockObject = new object();
        public ICommand AddProductCommand { get; set; }

        public override string TranslationPrefix
        {
            get
            {
                return "EmployeeMealEntry_";
            }
        }

        public EmployeeMealEntryViewModel()
        {
            MealItems = new ObservableCollection<EmployeeMealListItem>();
            AvailableEmployees = new ObservableCollection<EmployeeUserListItem>();
            ProductList = new ObservableCollection<ProductListItem>();

            AddProductCommand = new RelayCommand(AddProductCommandClick);

            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);

            ReloadDataCommand = new RelayCommand(() => { LoadData(); });
        }

        private object AddProductCommandClick(object parameter)
        {
            ProductListItem productItem = (parameter as ProductListItem);
            if (productItem != null)
            {
                EmployeeMealListItem findModel = MealItems.FirstOrDefault(M => M.ItemID == productItem.ID);
                if (findModel != null)
                {
                    findModel.Quantity++;
                }
                else
                {
                    MealItems.Add(new EmployeeMealListItem(this)
                    {
                        ItemCode = productItem.ItemCode,
                        ItemName = productItem.Name,
                        ItemID = productItem.ID,
                        Quantity = 1,
                        Price = productItem.Price
                    });
                }

                SaveMealEntryItems();
            }

            return true;
        }

        private async void LoadData()
        {
            ShowLoadingIndicator();
            try
            {
                DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;
                if (SystemStatusManager.Instance.PreviousBusinessDayIsClosed)
                {
                    ReadOnlyMode = (selectedDate != SystemStatusManager.Instance.CurrentBusinessDay);
                }
                else
                {
                    ReadOnlyMode = (selectedDate != SystemStatusManager.Instance.PreviousBusinessDay);
                }
                Notify("IsProductSelectionActive");

                await WcfConnection.ExecuteActionOnInterface((ICashManagementServiceForApp currentInterface) =>
                {
                    _lastResponse.ResultProducts = currentInterface.GetEmployeeMealPossibleProducts(selectedDate, selectedDate).ToList();
                    _lastResponse.ResultExistingEmployeeMeals = currentInterface.GetDefinedEmployeeMealsForPeriod(selectedDate, selectedDate, includeProposals: true).ToList();
                    _lastResponse.ResultWorkers = currentInterface.GetWorkersForEmployeeMeal(selectedDate, selectedDate).ToList();
                });

                RefreshWorkerList();

                LoadMoreProducts();

                if (_selectedEmployeeUser != null)
                {
                    UpdateMealEntryItems();
                }
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }

            HideLoadingIndicator();

        }

        private void RefreshWorkerList()
        {
            int selectedID = (_selectedEmployeeUser != null ? _selectedEmployeeUser.PartyID : 0);
            AvailableEmployees.Clear();

            List<WorkerEntry> orderedWorkers = _lastResponse.ResultWorkers.OrderBy(W => W.FullName).ToList();

            if (!String.IsNullOrEmpty(_workerSearchText))
            {
                orderedWorkers = orderedWorkers.Where(W => (W.FullName.IndexOf(_workerSearchText, StringComparison.CurrentCultureIgnoreCase) > -1)
                    || (W.EmployeeNumber.ToString().IndexOf(_workerSearchText, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            }

            foreach (var item in orderedWorkers)
            {
                if (!_showOnlyWorkersWithoutMeals || !_lastResponse.ResultExistingEmployeeMeals.Any(M => M.workerPartyId == item.PartyId))
                {
                    AvailableEmployees.Add(new EmployeeUserListItem()
                    {
                        PartyID = item.PartyId,
                        EmployeeNumber = item.EmployeeNumber,
                        Name = (item.EmployeeNumber == (int)SDMDefinitions.ItemTransaction_Operator.StorewideEmployeeMeal ? Translation["EmployeeMealEntry_Storewide"] : item.FullName)
                    });
                }
            }

            if (selectedID != 0)
            {
                SelectedEmployeeUser = AvailableEmployees.FirstOrDefault(A => A.PartyID == selectedID);
            }
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                LoadData();
            }
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        internal void LoadMoreProducts()
        {
            Debug.WriteLine(">>>>>>>>>>>>>>> Load more Products");
            if (!_lastSearchText.Equals(_searchText))
            {
                ProductList.Clear();
                _lastSearchText = _searchText;
            }

            List<Mcd.MyStore.Cash.Common.EmployeeMealEntry> filteredProducts = null;

            if (String.IsNullOrEmpty(_searchText))
            {
                filteredProducts = _lastResponse.ResultProducts.ToList();
            }
            else
            {
                filteredProducts = _lastResponse.ResultProducts
                    .Where(P => (P.ItemDescription.IndexOf(_searchText, StringComparison.CurrentCultureIgnoreCase) > -1)
                        || (P.ItemCode.ToString().IndexOf(_searchText, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            }

            ProductListItem lastItem = ProductList.LastOrDefault();
            int skipCount = 0;

            if (lastItem != null)
            {
                var foundItemDto = filteredProducts.FirstOrDefault(I => I.ItemID == lastItem.ID);
                if (foundItemDto != null)
                {
                    skipCount = filteredProducts.IndexOf(foundItemDto) + 1;
                }
            }

            foreach (var item in filteredProducts.Skip(skipCount).Take(50))
            {
                ProductList.Add(new ProductListItem()
                {
                    ID = item.ItemID,
                    ItemCode = item.ItemCode,
                    Name = item.ItemDescription,
                    Price = item.RegularPrice
                });
            }
        }

        private void UpdateMealEntryItems()
        {
            MealItems.Clear();

            if (_lastResponse.ResultExistingEmployeeMeals == null)
                return;

            if (_selectedEmployeeUser == null)
                return;

            EmployeeMealWorkerEntry selectedEntry = _lastResponse.ResultExistingEmployeeMeals.FirstOrDefault(E => E.workerPartyId == _selectedEmployeeUser.PartyID);

            if (selectedEntry != null)
            {
                foreach (var item in selectedEntry.employeeMealEntryList)
                {
                    MealItems.Add(new EmployeeMealListItem(this)
                    {
                        ItemCode = item.ItemCode,
                        ItemName = item.ItemDescription,
                        ItemID = item.ItemID,
                        Quantity = item.Quantity,
                        Price = item.RegularPrice
                    });
                }
            }
        }

        public async void SaveMealEntryItems()
        {
            try
            {
                DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;
                List<Mcd.MyStore.Cash.Common.EmployeeMealEntry> saveList = new List<MyStore.Cash.Common.EmployeeMealEntry>();

                lock (_saveLockObject)
                {
                    foreach (var item in MealItems)
                    {
                        saveList.Add(new MyStore.Cash.Common.EmployeeMealEntry()
                        {
                            BusinessDayDate = selectedDate,
                            ItemCode = item.ItemCode,
                            ItemDescription = item.ItemName,
                            ItemID = item.ItemID,
                            Quantity = item.Quantity,
                            RegularPrice = item.Price
                        });
                    }
                }

                if (_lastResponse.ResultExistingEmployeeMeals != null)
                {
                    EmployeeMealWorkerEntry selectedEntry = _lastResponse.ResultExistingEmployeeMeals.FirstOrDefault(E => E.workerPartyId == _selectedEmployeeUser.PartyID);
                    if (selectedEntry == null)
                    {
                        _lastResponse.ResultExistingEmployeeMeals.Add(new EmployeeMealWorkerEntry()
                        {
                            workerPartyId = _selectedEmployeeUser.PartyID,
                            employeeMealEntryList = saveList
                        });
                    }
                    else
                    {
                        selectedEntry.employeeMealEntryList = saveList;
                    }
                }

                if (saveList.Count > 0)
                {
                    await WcfConnection.ExecuteActionOnInterface((ICashManagementServiceForApp currentInterface) =>
                    {
                        currentInterface.SaveEmployeeMeals(selectedDate, _selectedEmployeeUser.PartyID, saveList, "MyStoreApp");
                    });
                }

            }
            catch (Exception ex)
            {
                Logger.Write(ex);
            }
        }
    }
}
