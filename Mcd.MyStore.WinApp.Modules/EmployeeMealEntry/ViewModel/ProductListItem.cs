﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.ViewModel
{
    public class ProductListItem : ViewModelBase
    {
        #region Member - ID
        private int _id;
        public int ID
        {
            get { return _id; }
            set
            {
                if (SetValue(ref _id, value))
                {
                    Notify("Title");
                }
            }
        }
        #endregion

        #region Member - ItemCode
        private int _itemCode;
        public int ItemCode
        {
            get { return _itemCode; }
            set { SetValue(ref _itemCode, value); }
        }
        #endregion


        #region Member - Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (SetValue(ref _name, value))
                {
                    Notify("Title");
                }
            }
        }
        #endregion

        #region Member - Price
        private decimal _price;
        public decimal Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }
        #endregion


        public string Title
        {
            get { return _name + "\n(" + _itemCode.ToString() + ")"; }
        }
    }
}
