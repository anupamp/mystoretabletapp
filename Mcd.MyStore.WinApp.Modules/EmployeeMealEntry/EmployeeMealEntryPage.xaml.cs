﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.ViewModel;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry
{
    [MenuModuleAttribute("EmployeeMealEntry")]

    public sealed partial class EmployeeMealEntryPage : UserControl
    {
        private EmployeeMealEntryViewModel _viewModel = null;

        public EmployeeMealEntryPage()
        {
            this.InitializeComponent();
            _viewModel = new EmployeeMealEntryViewModel();
            DataContext = _viewModel;


        }

        private void ProductScroller_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var verticalOffset = ProductScroller.VerticalOffset;
            var maxVerticalOffset = ProductScroller.ScrollableHeight;

            if (verticalOffset >= (maxVerticalOffset - 150))
            {
                _viewModel.LoadMoreProducts();
            }
        }

        private void CheckBox_Checked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
}
