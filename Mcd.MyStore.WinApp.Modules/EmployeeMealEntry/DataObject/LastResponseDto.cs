﻿using Mcd.MyStore.Cash.Common;
using Mcd.MyStore.Cash.Common.DataContract;
using System.Collections.Generic;

namespace Mcd.MyStore.WinApp.Modules.EmployeeMealEntry.DataObject
{
    public class LastResponseDto
    {
        public List<Mcd.MyStore.Cash.Common.EmployeeMealEntry> ResultProducts { get; set; }
        public List<EmployeeMealWorkerEntry> ResultExistingEmployeeMeals { get; set; }
        public List<WorkerEntry> ResultWorkers { get; set; }
    }
}
