﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Modules.OrderVolumes.ViewModel
{
    public class OrderVolumesItemViewModel : ViewModelBase
    {
        #region Member Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetValue<string>(ref _name, value); }
        }
        #endregion

        #region Member StoreWide
        private int _storewide;
        public int StoreWide
        {
            get { return _storewide; }
            set { SetValue<int>(ref _storewide, value); }
        }
        #endregion

        #region Member Fresh
        private int _fresh;
        public int Fresh
        {
            get { return _fresh; }
            set { SetValue<int>(ref _fresh, value); }
        }
        #endregion

        #region Member Together
        private int _together;
        public int Together
        {
            get { return _together; }
            set { SetValue<int>(ref _together, value); }
        }
        #endregion

        #region Member Connect
        private int _connect;
        public int Connect
        {
            get { return _connect; }
            set { SetValue<int>(ref _connect, value); }
        }
        #endregion
    }
}
