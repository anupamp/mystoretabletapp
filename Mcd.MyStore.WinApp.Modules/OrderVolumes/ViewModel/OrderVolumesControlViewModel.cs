﻿using Mcd.MyStore.Common.DataLoading;
using Mcd.MyStore.Common.Manager.Notification;
using Mcd.MyStore.GeneralReportingService.Common;
using Mcd.MyStore.GeneralReportingService.Common.Dto;
using Mcd.MyStore.GeneralReportingService.Common.ServiceContracts;
using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Module;
using Mcd.MyStore.WinApp.Core.Manager.SystemStatus;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Mcd.MyStore.WinApp.Modules.OrderVolumes.ViewModel
{
    public class OrderVolumesControlViewModel : ModuleViewModelBase
    {
        public ObservableCollection<OrderVolumesItemViewModel> DataItems { get; set; }

        #region Member TotalStoreWide
        private int _totalStoreWide;
        public int TotalStoreWide
        {
            get { return _totalStoreWide; }
            set { SetValue<int>(ref _totalStoreWide, value); }
        }
        #endregion

        #region Member TotalFresh
        private int _totalFresh;
        public int TotalFresh
        {
            get { return _totalFresh; }
            set { SetValue<int>(ref _totalFresh, value); }
        }
        #endregion

        #region Member TotalTogether
        private int _totalTogether;
        public int TotalTogether
        {
            get { return _totalTogether; }
            set { SetValue<int>(ref _totalTogether, value); }
        }
        #endregion

        #region Member TotalConnect
        private int _totalConnect;
        public int TotalConnect
        {
            get { return _totalConnect; }
            set { SetValue<int>(ref _totalConnect, value); }
        }
        #endregion



        public OrderVolumesControlViewModel()
        {
            IsAutoReloadEnabled = true;
            SubscribeNotification(NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged);
            DataItems = new ObservableCollection<OrderVolumesItemViewModel>();
            LoadEmptyInitData();
        }

        private void LoadEmptyInitData()
        {

        }

        private void PodSelection_OnSelectionChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        public void LoadData()
        {
            LoadAsyncData<TopProductsDto>(LoadDataAsync, LoadDataAsyncFinished);
        }

        private TopProductsDto LoadDataAsync()
        {
            if (!SystemStatusManager.Instance.GlobalUserSelectedDay.HasValue)
                return null;

            DateTime selectedDate = SystemStatusManager.Instance.GlobalUserSelectedDay.Value;

            try
            {
                var service = WcfConnection.ConnectWcfService<IGeneralReportingServiceForApp>();
                var result = service.GetTopProducts(selectedDate, selectedDate, 25, AppReportPODEnum.All);
                WcfConnection.DisconnectWcfService<IGeneralReportingServiceForApp>(service);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Write(ex);
                return null;
            }
        }

        private void LoadDataAsyncFinished(TopProductsDto result)
        {
            if (result == null || result.TopProducts == null)
            {
                return;
            }

            DataItems.Clear();

            foreach (var item in result.TopProducts.OrderBy(P => P.Name))
            {
                DataItems.Add(new OrderVolumesItemViewModel()
                {
                    Name = item.Name,
                    StoreWide = (int)item.Value,
                    Fresh = (int)item.Kiosk,
                    Together = (int)item.DriveThru,
                    Connect = (int)item.FrontCounter
                });
            }

            TotalStoreWide = (int)result.TopProducts.Sum(P => P.Value);
            TotalFresh = (int)result.TopProducts.Sum(P => P.Kiosk);
            TotalTogether = (int)result.TopProducts.Sum(P => P.DriveThru);
            TotalConnect = (int)result.TopProducts.Sum(P => P.FrontCounter);

            ModuleManager.Instance.RestartReloadTimer();
        }

        public override void ModuleShowing(NavigationArgs navigationArgs)
        {
            LoadData();
        }

        public override void ModuleHiding(NavigationHideCloseArgs args)
        {
        }

        public override void ApplicationClosing(NavigationHideCloseArgs args)
        {
        }

        public override void ReceiveNotification(NotificationArgs<NotificationApplicationType> notification)
        {
            if (notification.Type == NotificationApplicationType.WinApp_OnGlobalSelectedDateChanged)
            {
                ModuleManager.Instance.RestartReloadTimer();
                LoadData();
            }
        }

        public override void TriggerDataLoading()
        {
            LoadData();
        }
    }
}
