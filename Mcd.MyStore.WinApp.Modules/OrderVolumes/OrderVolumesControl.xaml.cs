﻿using Mcd.MyStore.WinApp.Core.Attributes;
using Mcd.MyStore.WinApp.Modules.OrderVolumes.ViewModel;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.OrderVolumes
{
    [MenuModuleAttribute("OrderVolumes")]
    public sealed partial class OrderVolumesControl : UserControl
    {
        public OrderVolumesControl()
        {
            this.InitializeComponent();
            DataContext = new OrderVolumesControlViewModel();
        }
    }
}
