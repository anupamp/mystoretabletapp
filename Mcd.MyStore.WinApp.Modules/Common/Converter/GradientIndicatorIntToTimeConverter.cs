﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace Mcd.MyStore.WinApp.Modules.Common.Converter
{
    public class GradientIndicatorIntToTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string result = string.Empty;

            string val = value + "";
            if (String.IsNullOrEmpty(val))
            {
                val = "0";
            }

            int intVal;
            if (int.TryParse(val, out intVal))
            {
                TimeSpan t = TimeSpan.FromSeconds(intVal);
                //                result = string.Format("{0}'{1:D2}\"", t.Minutes, t.Seconds);
                result = ((t < TimeSpan.Zero) ? "-" : "") + t.ToString("mm\\'ss\\'\\'");

            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
