﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mcd.MyStore.WinApp.Modules.Common
{
    public interface IMainModuleLoadControl
    {
        void LoadData();
    }
}
