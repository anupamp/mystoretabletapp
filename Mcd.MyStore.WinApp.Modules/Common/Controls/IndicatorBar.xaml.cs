﻿using Mcd.MyStore.WinApp.Core.UI.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public sealed partial class IndicatorBar : UserControl
    {
        public int Position
        {
            get { return (int)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }
        public static DependencyProperty PositionProperty = DependencyProperty.Register("Position", typeof(int), typeof(IndicatorBar), new PropertyMetadata(0));

        public int TextPosition
        {
            get { return (int)GetValue(TextPositionProperty); }
            set { SetValue(TextPositionProperty, value); }
        }
        public static DependencyProperty TextPositionProperty = DependencyProperty.Register("TextPosition", typeof(int), typeof(IndicatorBar), new PropertyMetadata(0));

        public int ThresholdPosition
        {
            get { return (int)GetValue(ThresholdPositionProperty); }
            set { SetValue(ThresholdPositionProperty, value); }
        }
        public static DependencyProperty ThresholdPositionProperty = DependencyProperty.Register("ThresholdPosition", typeof(int), typeof(IndicatorBar), new PropertyMetadata(0));

        public Brush BarColor
        {
            get { return (Brush)GetValue(BarColorProperty); }
            set { SetValue(BarColorProperty, value); }
        }
        public static DependencyProperty BarColorProperty = DependencyProperty.Register("BarColor", typeof(Brush), typeof(IndicatorBar), new PropertyMetadata(new SolidColorBrush(Colors.Transparent)));

        public int CanvasWidth
        {
            get { return (int)GetValue(CanvasWidthProperty); }
            set { SetValue(CanvasWidthProperty, value); }
        }
        public static DependencyProperty CanvasWidthProperty = DependencyProperty.Register("CanvasWidth", typeof(int), typeof(IndicatorBar), new PropertyMetadata(0, CanvasWidthChanged));
        private static void CanvasWidthChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ThresholdBarPositionChanged(dependencyObject, dependencyPropertyChangedEventArgs);
            ValueChanged(dependencyObject, dependencyPropertyChangedEventArgs);
        }


        /// <summary> 
        /// Gets or sets the current value. Displayed as the white circle
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public int Value
        {
            get
            {
                int value = (int)GetValue(ValueProperty);
                ValueTime = Convert.ToString(value);
                return value;
            }
            set
            {
                SetValue(ValueProperty, value);
                ValueTime = Convert.ToString(value);
            }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(IndicatorBar),
            new PropertyMetadata(10, ValueChanged));

        private static void ValueChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((IndicatorBar)dependencyObject).UpdateValues();
        }

        string _ValueTime;
        public string ValueTime
        {
            get
            {
                return _ValueTime;
            }
            set
            {
                string val = value;
                int intVal = 0;
                if (int.TryParse(val, out intVal))
                {
                    TimeSpan t = TimeSpan.FromSeconds(intVal);
                    _ValueTime = ((t < TimeSpan.Zero) ? "-" : "") + t.ToString("mm\\'ss\\'\\'");
                    // string.Format("{0:D2}':{1:D2}''", t.Minutes, t.Seconds);
                }
            }
        }

        /// <summary>
        /// Gets or sets the threshold value. Displayed as red line. Aware: Displayed position is determined by ThresholdBarPositionPercent, not Threshold
        /// </summary>
        /// <value>
        /// The threshold.
        /// </value>
        public int Threshold
        {
            get { return (int)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register("Threshold", typeof(int), typeof(IndicatorBar),
            new PropertyMetadata(100, ValueChanged));



        /// <summary>
        /// Gets or sets the threshold bar position percent.Displayed as red line. Aware: Displayed position is determined by ThresholdBarPositionPercent, not Threshold
        /// </summary>
        /// <value>
        /// The threshold bar position percent.
        /// </value>
        public double ThresholdBarPositionPercent
        {
            get
            {
                return (double)GetValue(ThresholdBarPositionPercentProperty);
            }
            set
            {
                SetValue(ThresholdBarPositionPercentProperty, value);
            }
        }
        public static readonly DependencyProperty ThresholdBarPositionPercentProperty = DependencyProperty.Register("ThresholdBarPositionPercent", typeof(double), typeof(IndicatorBar),
            new PropertyMetadata(0.2, ThresholdBarPositionChanged));
        private static void ThresholdBarPositionChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            IndicatorBar indicator = (IndicatorBar)dependencyObject;
            indicator.ThresholdPosition = (int)Math.Round(indicator.ThresholdBarPositionPercent * (double)indicator.CanvasWidth);
        }

        public IndicatorBar()
        {
            InitializeComponent();
            SizeChanged += IndicatorBar_SizeChanged;
            TextLabel.SizeChanged += TextLabel_SizeChanged;
        }

        void TextLabel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateValues();
        }

        void IndicatorBar_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CanvasWidth = (int)e.NewSize.Width;
        }

        
        private void UpdateValues()
        {
            IndicatorBar indicator = this;
            try
            {

                int currentValue = Math.Max(indicator.Value, 0);
                if (indicator.Threshold > 0)
                {
                    double maxValue = indicator.Threshold / indicator.ThresholdBarPositionPercent;
                    double currentValueinPercent = currentValue / maxValue;

                    indicator.Position = (int)Math.Round(Math.Min(currentValueinPercent, 1.0) * (double)indicator.CanvasWidth);
                }
                else
                {
                    indicator.Position = 0;
                }

                if (currentValue <= indicator.Threshold)
                {
                    indicator.BarColor = ColorConverter.ColorToBrush("#00b27e");
                }
                else
                {
                    indicator.BarColor = ColorConverter.ColorToBrush("#ed4962");
                }

                int labelWidth = (int)indicator.TextLabel.ActualWidth;

                if (indicator.Position >= labelWidth)
                {
                    indicator.TextPosition = indicator.Position - (int)(labelWidth / 2.0M);

                }
                else
                {
                    indicator.TextPosition = 0;
                }

                //Fix positions
                if (indicator.TextPosition <= 0)
                {
                    indicator.TextPosition = 0;
                }
                if (indicator.TextPosition + labelWidth > indicator.CanvasWidth)
                {
                    indicator.TextPosition = indicator.CanvasWidth - labelWidth;
                }

            }
            catch(Exception ex)
            {

            }
        }
    }
}
