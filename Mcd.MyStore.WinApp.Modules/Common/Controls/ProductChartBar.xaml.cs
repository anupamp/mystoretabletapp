﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public sealed partial class ProductChartBar : UserControl
    {
        #region Dependenxy - BarValueLabelVisbility
        public Visibility BarValueLabelVisbility
        {
            get { return (Visibility)GetValue(BarValueLabelVisbilityProperty); }
            set { SetValue(BarValueLabelVisbilityProperty, value); }
        }
        public static DependencyProperty BarValueLabelVisbilityProperty = DependencyProperty.Register("BarValueLabelVisbility", typeof(Visibility), typeof(ProductChartBar), new PropertyMetadata(Visibility.Visible));
        #endregion
        public ProductChartBar()
        {
            this.InitializeComponent();
        }
    }
}
