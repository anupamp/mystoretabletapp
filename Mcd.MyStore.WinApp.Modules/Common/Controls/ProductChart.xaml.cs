﻿using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public partial class ProductChart : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private bool SetProperty<T>(ref T storage, T value, [CallerMemberName] String propertyName = null)
        {
            if (!object.Equals(storage, value))
            {
                storage = value;
                Notifiy(propertyName);
                return true;
            }
            return false;
        }

        private void Notifiy([CallerMemberName] string propertyName = null)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private int _chartViewHeight = 0;
        private int _chartViewWidth = 0;

        #region Intern Model Props
        private ObservableCollection<int> _horizontalLines;
        public ObservableCollection<int> HorizontalLines
        {
            get { return _horizontalLines; }
            set { SetProperty<ObservableCollection<int>>(ref _horizontalLines, value); }
        }

        private int _scalaHeight;
        public int ScalaHeight
        {
            get { return _scalaHeight; }
            set { SetProperty<int>(ref _scalaHeight, value); }
        }

        private int _itemColumnWidth = 1;
        public int ItemColumnWidth
        {
            get { return _itemColumnWidth; }
            set { SetProperty<int>(ref _itemColumnWidth, value); }
        }

        private int _dataItemColumnWidth = 1;
        public int DataItemColumnWidth
        {
            get { return _dataItemColumnWidth; }
            set { SetProperty<int>(ref _dataItemColumnWidth, value); }
        }

        #endregion

        #region Dependency Props
        #region Dependency - ItemsSource
        public ObservableCollection<ProductChartItemViewModel> ItemsSource
        {
            get { return (ObservableCollection<ProductChartItemViewModel>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<ProductChartItemViewModel>), typeof(ProductChart), new PropertyMetadata(new ObservableCollection<ProductChartItemViewModel>(), ItemsSourceChanged));
        private static void ItemsSourceChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler handler = (s, args) => OnCollectionChanged(dependencyObject, e);
            if (e.OldValue is ObservableCollection<ProductChartItemViewModel>)
            {
                ((ObservableCollection<ProductChartItemViewModel>)e.OldValue).CollectionChanged -= handler;
            }
            if (e.NewValue is ObservableCollection<ProductChartItemViewModel>)
            {
                ((ObservableCollection<ProductChartItemViewModel>)e.NewValue).CollectionChanged += handler;
            }

            UpdateHandler(dependencyObject, e);
            ((ProductChart)dependencyObject).UpdateChartSizes();
        }

        private static void UpdateHandler(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            EventHandler chnageHandler = (s, args) => OnValueChanged(dependencyObject, e);
            if (e.OldValue is ObservableCollection<ProductChartItemViewModel>)
            {
                ObservableCollection<ProductChartItemViewModel> oldModules = (ObservableCollection<ProductChartItemViewModel>)e.OldValue;
                foreach (var item in oldModules)
                {
                    item.OnValueChanged -= chnageHandler;
                }
            }
            if (e.NewValue is ObservableCollection<ProductChartItemViewModel>)
            {
                ObservableCollection<ProductChartItemViewModel> newModels = (ObservableCollection<ProductChartItemViewModel>)e.NewValue;
                foreach (var item in newModels)
                {
                    item.OnValueChanged -= chnageHandler;
                    item.OnValueChanged += chnageHandler;
                }
            }
        }

        private static void OnCollectionChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            UpdateHandler(dependencyObject, e);
            ((ProductChart)dependencyObject).UpdateChartSizes();
        }
        private static void OnValueChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            ((ProductChart)dependencyObject).UpdateChartSizes();
        }

        #endregion

        #region Dependenxy - AxisXTitle
        public string AxisXTitle
        {
            get { return (string)GetValue(AxisXTitleProperty); }
            set { SetValue(AxisXTitleProperty, value); }
        }
        public static DependencyProperty AxisXTitleProperty = DependencyProperty.Register("AxisXTitle", typeof(string), typeof(ProductChart), new PropertyMetadata("Axis X Title"));
        #endregion

        #region Dependenxy - AxisYTitle
        public string AxisYTitle
        {
            get { return (string)GetValue(AxisYTitleProperty); }
            set { SetValue(AxisYTitleProperty, value); }
        }
        public static DependencyProperty AxisYTitleProperty = DependencyProperty.Register("AxisYTitle", typeof(string), typeof(ProductChart), new PropertyMetadata("Axis Y Title"));
        #endregion

        #region Dependenxy - AxisYTitleVisibility
        public Visibility AxisYTitleVisibility
        {
            get { return (Visibility)GetValue(AxisYTitleVisibilityProperty); }
            set { SetValue(AxisYTitleVisibilityProperty, value); }
        }
        public static DependencyProperty AxisYTitleVisibilityProperty = DependencyProperty.Register("AxisYTitleVisibility", typeof(Visibility), typeof(ProductChart), new PropertyMetadata(Visibility.Visible));
        #endregion

        #region Dependenxy - BarValueLabelFontSize
        public int BarValueLabelFontSize
        {
            get { return (int)GetValue(BarValueLabelFontSizeProperty); }
            set { SetValue(BarValueLabelFontSizeProperty, value); }
        }
        public static DependencyProperty BarValueLabelFontSizeProperty = DependencyProperty.Register("BarValueLabelFontSize", typeof(int), typeof(ProductChart), new PropertyMetadata(30));
        #endregion

        #region Dependency - HorizontalLabelFontSize
        public int HorizontalLabelFontSize
        {
            get { return (int)GetValue(HorizontalLabelFontSizeProperty); }
            set { SetValue(HorizontalLabelFontSizeProperty, value); }
        }
        public static readonly DependencyProperty HorizontalLabelFontSizeProperty = DependencyProperty.Register("HorizontalLabelFontSize", typeof(int), typeof(ProductChart), new PropertyMetadata(15));
        #endregion



        #region Dependenxy - BarValueLabelVisbility
        public Visibility BarValueLabelVisbility
        {
            get { return (Visibility)GetValue(BarValueLabelVisbilityProperty); }
            set { SetValue(BarValueLabelVisbilityProperty, value); }
        }
        public static DependencyProperty BarValueLabelVisbilityProperty = DependencyProperty.Register("BarValueLabelVisbility", typeof(Visibility), typeof(ProductChart), new PropertyMetadata(Visibility.Visible));
        #endregion

        #region Dependenxy - BarTopImageVisbility
        public Visibility BarTopImageVisbility
        {
            get { return (Visibility)GetValue(BarTopImageVisbilityProperty); }
            set { SetValue(BarTopImageVisbilityProperty, value); }
        }
        public static DependencyProperty BarTopImageVisbilityProperty = DependencyProperty.Register("BarTopImageVisbility", typeof(Visibility), typeof(ProductChart), new PropertyMetadata(Visibility.Visible, OnBarTopImageVisbilityChanged));
        private static void OnBarTopImageVisbilityChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            ((ProductChart)dependencyObject).UpdateChartSizes();
        }
        #endregion

        #region Dependenxy - AxisYLineCount
        public double AxisYLineCount
        {
            get { return (double)GetValue(AxisYLineCountProperty); }
            set { SetValue(AxisYLineCountProperty, value); }
        }
        public static DependencyProperty AxisYLineCountProperty = DependencyProperty.Register("AxisYLineCount", typeof(double), typeof(ProductChart), new PropertyMetadata(10.0, OnAxisYLineCountChanged));
        private static void OnAxisYLineCountChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            ((ProductChart)dependencyObject).UpdateChartSizes();
        }
        #endregion
        #region Dependenxy - AxisXTitleHeight
        public double AxisXTitleHeight
        {
            get { return (double)GetValue(AxisXTitleHeightProperty); }
            set { SetValue(AxisXTitleHeightProperty, value); }
        }
        public static DependencyProperty AxisXTitleHeightProperty = DependencyProperty.Register("AxisXTitleHeight", typeof(int), typeof(ProductChart), new PropertyMetadata(60));
        #endregion

        #region Dependenxy - AxisXTitleVisibility
        public Visibility AxisXTitleVisibility
        {
            get { return (Visibility)GetValue(AxisXTitleVisibilityProperty); }
            set { SetValue(AxisXTitleVisibilityProperty, value); }
        }
        public static DependencyProperty AxisXTitleVisibilityProperty = DependencyProperty.Register("AxisXTitleVisibility", typeof(Visibility), typeof(ProductChart), new PropertyMetadata(Visibility.Visible));
        #endregion

        #endregion


        public ProductChart()
        {
            this.InitializeComponent();

            ScalaHeight = 50;

            HorizontalLines = new ObservableCollection<int>();
        }


        private void ChartViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _chartViewWidth = (int)e.NewSize.Width;
            _chartViewHeight = (int)e.NewSize.Height;
            UpdateChartSizes();
        }

        public void UpdateChartSizes()
        {
            try
            {
                if (ItemsSource.Count == 0 || _chartViewWidth == 0)
                {
                    return;
                }

                ItemColumnWidth = (int)Math.Floor((double)_chartViewWidth / ItemsSource.Count);
                DataItemColumnWidth = ItemColumnWidth;
                DataItemColumnWidth = (int)Math.Floor((double)(ItemColumnWidth - 10) / (double)ItemsSource[0].DataValues.Count);

                int maxValue = Math.Max(ItemsSource.Max(I => I.DataValues.Max(D => D.Value)), 20);

                int chartHeightWithoutImage = _chartViewHeight - (BarTopImageVisbility == Visibility.Visible ? 130 : 60);
                double valueToPixelHeight = (double)chartHeightWithoutImage / (double)maxValue;

                if (valueToPixelHeight <= 0)
                {
                    return;
                }

                int maxPointValue = (int)(_chartViewHeight / valueToPixelHeight);

                int maxScalaValue = (int)(Math.Ceiling(maxPointValue / 10.0) * 10);
                int scalaLineValue = (int)Math.Floor((double)maxScalaValue / AxisYLineCount);
                scalaLineValue = (int)(Math.Ceiling(scalaLineValue / 5.0) * 5);


                foreach (var currentItem in ItemsSource)
                {
                    foreach (var currentDataItem in currentItem.DataValues)
                    {
                        currentDataItem.Height = Math.Max((int)(currentDataItem.Value * valueToPixelHeight) - 4, 1);
                    }
                }

                // update Scala
                ScalaHeight = (int)(scalaLineValue * valueToPixelHeight);
                if (ScalaHeight > 0)
                {
                    int linesCount = (int)Math.Floor((double)_chartViewHeight / ScalaHeight) - 1;
                    HorizontalLines.Clear();
                    for (int i = linesCount; i >= 0; i--)
                    {
                        HorizontalLines.Add(i * scalaLineValue);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
