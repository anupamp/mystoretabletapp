﻿using Mcd.MyStore.WinApp.Core.UI.Common;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Windows.Foundation;
using Windows.UI.Xaml.Input;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public sealed partial class SwipableContent : INotifyPropertyChanged
    {
        private string _current;
        private Point initialpoint;
        private object _itemsSource;
        private const int swipeThreshold = 1500;


        public ICommand OnLeft { get; set; }
        public ICommand OnRight { get; set; }


        #region Events
        public delegate void SwipeHandler(int delta);
        public event SwipeHandler OnSwipedGesture;
        #endregion

        public SwipableContent()
        {
            InitializeComponent();

            OnLeft = new RelayCommand(OnLeftCommand);
            OnRight = new RelayCommand(OnRightCommand);

            DataContext = this;
        }

        private void OnLeftCommand()
        {
            if (OnSwipedGesture != null)
                OnSwipedGesture(-1);
        }

        private void OnRightCommand()
        {
            if (OnSwipedGesture != null)
                OnSwipedGesture(1);
        }

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected void Notify([CallerMemberName] string propertyName = null)
        {
            var eventHandler = PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


        private void UIElement_OnManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            initialpoint = e.Position;
        }

        private void UIElement_OnManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            Point currentpoint = e.Position;
            if (currentpoint.X - initialpoint.X >= swipeThreshold)
            {
                OnRightCommand();
                e.Complete();
            }
            else if (currentpoint.X - initialpoint.X <= swipeThreshold)
            {
                OnLeftCommand();
                e.Complete();
            }
        }
    }
}
