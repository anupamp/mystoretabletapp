﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using DevExpress.Core.Extensions;
using Mcd.MyStore.WinApp.Core.MVVM;
using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public sealed partial class ScrollPagerControl : INotifyPropertyChanged
    {
        private ScrollPageItemViewModel _selectedPageItem;

        public ScrollPagerControl()
        {
            InitializeComponent();
            PageItems = new ObservableCollection<ScrollPageItemViewModel>();
        }

        public ObservableCollection<ScrollPageItemViewModel> PageItems { get; private set; }

        public ScrollPageItemViewModel SelectedPageItem
        {
            get { return _selectedPageItem; }
            set
            {
                if (SetValue(ref _selectedPageItem, value))
                {
                    PageItems.Where(x => x.IsActive).ForEach(x => x.IsActive = false);
                    var scrollPageItemViewModel = PageItems.FirstOrDefault(x => x == value);
                    if (scrollPageItemViewModel != null)
                    {
                        scrollPageItemViewModel.IsActive = true;
                    }
                }
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetValue<T>(ref T storage, T value, [CallerMemberName] String propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            Notify(propertyName);
            return true;
        }

        protected void Notify([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}