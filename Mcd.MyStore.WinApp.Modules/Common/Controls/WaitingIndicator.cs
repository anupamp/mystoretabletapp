﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public class WaitIndicator : Control
    {
        public static readonly DependencyProperty LoadingTextProperty = DependencyProperty.Register("LoadingText", typeof(string), typeof(WaitIndicator), null);
        public string LoadingText
        {
            get
            {
                return (string)this.GetValue(WaitIndicator.LoadingTextProperty);
            }
            set
            {
                this.SetValue(WaitIndicator.LoadingTextProperty, value);
            }
        }

        public static readonly DependencyProperty SubContentProperty = DependencyProperty.Register("SubContent", typeof(object), typeof(WaitIndicator), null);
        public object SubContent
        {
            get
            {
                return this.GetValue(WaitIndicator.SubContentProperty);
            }
            set
            {
                this.SetValue(WaitIndicator.SubContentProperty, value);
            }
        }
    }
}
