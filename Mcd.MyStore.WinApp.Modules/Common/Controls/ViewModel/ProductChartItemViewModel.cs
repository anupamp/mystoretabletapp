﻿using Mcd.MyStore.WinApp.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel
{
    public class ProductChartItemViewModel : ViewModelBase
    {
        public event EventHandler OnValueChanged;

        public ObservableCollection<ProductChartDataItemViewModel> DataValues { get; private set; }

        #region Member ID
        private object _id;
        public object ID
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }
        #endregion


        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue<string>(ref _title, value); }
        }

        private BitmapImage _image;
        public BitmapImage TopImage
        {
            get { return _image; }
            set { SetValue<BitmapImage>(ref _image, value); }
        }

        public ProductChartItemViewModel()
        {
            DataValues = new ObservableCollection<ProductChartDataItemViewModel>();
        }

        public void AddDataValue(ProductChartDataItemViewModel dataModel)
        {
            dataModel.OnValueChanged += (object sender, EventArgs e) =>
            {
                if (OnValueChanged != null)
                {
                    OnValueChanged(this, null);
                }
            };
            DataValues.Add(dataModel);
        }
    }

    public class ProductChartDataItemViewModel : ViewModelBase
    {
        public event EventHandler OnValueChanged;

        private int _value;
        public int Value
        {
            get { return _value; }
            set
            {
                if (SetValue<int>(ref _value, value))
                {
                    if (OnValueChanged != null)
                    {
                        OnValueChanged(this, null);
                    }
                }
            }
        }

        private SolidColorBrush _fillColor;
        public SolidColorBrush FillColor
        {
            get { return _fillColor; }
            set { SetValue<SolidColorBrush>(ref _fillColor, value); }
        }

        private int _height;
        public int Height
        {
            get { return _height; }
            set { SetValue<int>(ref _height, value); }
        }


        private int _newHeight;
        public int NewHeight
        {
            get { return _newHeight; }
            set
            {
                if (SetValue<int>(ref _newHeight, value))
                {
                    StartUpdateTimer();
                }
            }
        }

        private bool _isUpdateTimerRunning;
        private async void StartUpdateTimer()
        {
            if (_isUpdateTimerRunning)
            {
                return;
            }

            _isUpdateTimerRunning = true;

            while (_isUpdateTimerRunning)
            {
                await Task.Delay(10);

                if (_height < _newHeight)
                {
                    Height += 2;
                }
                else if (_height > _newHeight)
                {
                    Height -= 2;
                }
                else
                {
                    _isUpdateTimerRunning = false;
                }
            }
        }
    }
}
