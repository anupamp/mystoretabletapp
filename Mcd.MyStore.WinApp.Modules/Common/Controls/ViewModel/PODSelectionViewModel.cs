﻿using Mcd.MyStore.GeneralReportingService.Common;
using Mcd.MyStore.WinApp.Core.MVVM;
using System;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel
{
    public class PODSelectionViewModel : ViewModelBase
    {
        private bool _internalUpdate = false;

        #region Member StorewideChecked
        private bool _storewideChecked;
        public bool StorewideChecked
        {
            get { return _storewideChecked; }
            set
            {
                if (SetValue<bool>(ref _storewideChecked, value) && value)
                {
                    SelectionChanged();
                }
            }
        }
        #endregion

        #region Member KioskChecked
        private bool _kioskChecked;
        public bool KioskChecked
        {
            get { return _kioskChecked; }
            set
            {
                if (SetValue<bool>(ref _kioskChecked, value) && value)
                {
                    SelectionChanged();
                }
            }
        }
        #endregion

        #region Member DriveThruChecked
        private bool _driveThruChecked;
        public bool DriveThruChecked
        {
            get { return _driveThruChecked; }
            set
            {
                if (SetValue<bool>(ref _driveThruChecked, value) && value)
                {
                    SelectionChanged();
                }
            }
        }
        #endregion

        #region Member FrontCounterChecked
        private bool _frontCounterChecked;
        public bool FrontCounterChecked
        {
            get { return _frontCounterChecked; }
            set
            {
                if (SetValue<bool>(ref _frontCounterChecked, value) && value)
                {
                    SelectionChanged();
                }
            }
        }
        #endregion

        #region Member - CafeChecked
        private bool _cafeChecked;
        public bool CafeChecked
        {
            get { return _cafeChecked; }
            set
            {
                if (SetValue<bool>(ref _cafeChecked, value) && value)
                {
                    SelectionChanged();
                }
            }
        }
        #endregion


        public AppReportPODEnum CurrentSelection
        {
            get
            {
                if (_storewideChecked)
                    return AppReportPODEnum.All;

                if (_frontCounterChecked)
                    return AppReportPODEnum.FC;

                if (_driveThruChecked)
                    return AppReportPODEnum.DT;

                if (_kioskChecked)
                    return AppReportPODEnum.Kiosk;

                if (_cafeChecked)
                    return AppReportPODEnum.McCafe;

                return AppReportPODEnum.All;
            }
            set
            {
                switch (value)
                {
                    case AppReportPODEnum.Kiosk:
                        _kioskChecked = true;
                        break;
                    case AppReportPODEnum.FC:
                        _frontCounterChecked = true;
                        break;
                    case AppReportPODEnum.DT:
                        _driveThruChecked = true;
                        break;
                    case AppReportPODEnum.All:
                        _storewideChecked = true;
                        break;
                    default:
                        break;
                }
            }
        }


        public PODSelectionViewModel()
        {
            _internalUpdate = true;
            CurrentSelection = AppReportPODEnum.All;
            _internalUpdate = false;
        }

        public event EventHandler OnSelectionChanged;

        private void SelectionChanged()
        {
            if (!_internalUpdate && OnSelectionChanged != null)
            {
                OnSelectionChanged(this, null);
            }
        }
    }
}
