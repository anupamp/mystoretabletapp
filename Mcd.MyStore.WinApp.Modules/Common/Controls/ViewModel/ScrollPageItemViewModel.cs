﻿using Mcd.MyStore.WinApp.Core.MVVM;

namespace Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel
{
    public class ScrollPageItemViewModel : ViewModelBase
    {
        #region Member - IsActive
        private bool _isActive = false;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                SetValue(ref _isActive, value);
            }
        }
        #endregion
    }
}
