﻿using Mcd.MyStore.WinApp.Modules.Common.Controls.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Benutzersteuerelement" ist unter http://go.microsoft.com/fwlink/?LinkId=234236 dokumentiert.

namespace Mcd.MyStore.WinApp.Modules.Common.Controls
{
    public sealed partial class PODSelection : UserControl
    {
        private PODSelectionViewModel _viewModel = new PODSelectionViewModel();
        public PODSelectionViewModel ViewModel
        {
            get { return _viewModel; }
        }

        public PODSelection()
        {
            this.InitializeComponent();
            DataContext = _viewModel;
        }
    }
}
