﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Mcd.MyStore.Portable.TcpServer.Protocol;

namespace Mcd.MyStore.Portable.TcpServer
{
    public class Connection
    {
        public Connection(string id, Stream readStream, Stream writeStream)
            : this(id, readStream, writeStream, null, 0)
        {

        }

        public Connection(string id, Stream readStream, Stream writeStream, string host, int port)
        {
            Id = id;
            ReadStream = readStream;
            WriteStream = writeStream;
            Host = host;
            Port = port;
        }

        public String Id { get; set; }
        public Stream ReadStream { get; set; }
        public Stream WriteStream { get; set; }
        public String Host { get; set; }
        public Int32 Port { get; set; }
        public Boolean Connected { get; set; }
        public CancellationTokenSource CancellationTokenSource { get; set; }

        public async Task<bool> Send(Header obj)
        {
            return await Send(obj, CancellationToken.None);
        }

        public async Task<bool> Send(Header obj, CancellationToken cancellationToken)
        {
            try
            {
                Byte[] data = obj;
                if (!WriteStream.CanWrite)
                {
                    throw new Exception("Unable to read from network stream.");
                }

                await WriteStream.WriteAsync(data, 0, data.Length, cancellationToken);
                await WriteStream.FlushAsync(cancellationToken);
                return true;
            }
            catch (ObjectDisposedException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}