﻿using System;
using System.Collections.Generic;

namespace Mcd.MyStore.Portable.TcpServer
{
    public class StateObject
    {
        public const int BufferSize = 2048;

        public Connection Connection { get; set; }
        public byte[] ReadBuffer = new byte[BufferSize];
        private List<Byte> _resultBuffer = new List<Byte>();
        public int CurrentBytesRead;
        public int HeaderSize;
        public bool Completed { get { return CurrentBytesRead == FrameSize; } }
        public int NextRead;

        public StateObject(Connection connection)
        {
            Connection = connection;
        }

        public int FrameSize { get; set; }
        public int ContentSize { get; set; }

        public bool HeaderCompleted { get; set; }

        public void Append(ref int index, ref int available, int size)
        {
            for (int i = 0; i < size; i++)
            {
                _resultBuffer.Add(ReadBuffer[index + i]);
            }
            CurrentBytesRead += size;
            index += size;
            available -= size;
        }

        public void Reset()
        {
            FrameSize = 0;
            ContentSize = 0;
            HeaderCompleted = false;
            CurrentBytesRead = 0;
            _resultBuffer.Clear();
        }

        public byte GetByte(int index)
        {
            return _resultBuffer[index];
        }
        public byte[] GetBytes(int index, int count)
        {
            var contentLength = new byte[count];
            _resultBuffer.CopyTo(index, contentLength, 0, count);
            return contentLength;
        }
    }
}