﻿using System.Collections.Generic;
using Mcd.MyStore.Common.Manager.Notification;

namespace Mcd.MyStore.Portable.TcpServer.Models
{
    public class JsonNotificationMessage
    {
        public NotificationApplicationType NotificationType { get; set; }
        public object Parameter { get; set; }
    }
}