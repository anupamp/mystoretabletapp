﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;
using Mcd.MyStore.Portable.TcpServer.Interfaces;
using Mcd.MyStore.Portable.TcpServer.Protocol;

namespace Mcd.MyStore.Portable.TcpServer
{
    public abstract class CommunicationContext : ICommunicationContext
    {
        public virtual void ProcessData(Connection connection, byte[] bytes)
        {
            try
            {
                MessageTypes messageType = (MessageTypes)bytes[0];

                Type classtype = Type.GetType("Mcd.MyStore.Portable.TcpServer.Protocol." + messageType);
                if (classtype == null)
                {
                    Error(connection, bytes, CommunicationContextErrors.ClrTypeNotFound);
                    return;
                }

                CallHandlerForType(classtype, connection, bytes);
            }
            catch (Exception e)
            {
                Error(connection, bytes, CommunicationContextErrors.InternalException);
            }
        }

        public async Task<bool> SendAsync(Connection connection, Header message)
        {
            try
            {
                byte[] data = message;
                await connection.WriteStream.WriteAsync(data, 0, data.Length);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public abstract void Disconnected();
        public abstract void Connected();
        public abstract void HandleServerMessage(Connection connection, SimpleMessage message);
        public abstract void HandleServerMessage(Connection connection, JsonMessage message);
        public abstract void HandleServerMessage(Connection connection, PingRequest message);
        public abstract void HandleServerMessage(Connection connection, PongResponse message);

        private static readonly Dictionary<Type, Delegate> ServerMessageHandlerDelegates = new Dictionary<Type, Delegate>();

        private void CallHandlerForType(Type classtype, Connection connection, byte[] bytes)
        {
            try
            {
                Header deserializedObject = (Header)Activator.CreateInstance(classtype, new BinaryReader(new MemoryStream(bytes)));

                if (classtype == typeof(SimpleMessage))
                    HandleServerMessage(connection, (SimpleMessage)deserializedObject);
                else if (classtype == typeof(JsonMessage))
                    HandleServerMessage(connection, (JsonMessage)deserializedObject);
                else if (classtype == typeof(PingRequest))
                    HandleServerMessage(connection, (PingRequest)deserializedObject);
                else if (classtype == typeof(PongResponse))
                    HandleServerMessage(connection, (PongResponse)deserializedObject);
                else
                    UnknownMessage(connection, bytes, CommunicationContextErrors.HandlerNotFound);
            }
            catch (Exception e)
            {
                Error(connection, bytes, CommunicationContextErrors.InternalException);
            }
        }

        protected abstract void UnknownMessage(Connection connection, byte[] bytes, CommunicationContextErrors errorType);
        protected abstract void Error(Connection connection, byte[] bytes, CommunicationContextErrors errorType);
    }
}