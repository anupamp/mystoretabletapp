﻿using System;
using System.IO;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;

namespace Mcd.MyStore.Portable.TcpServer.Protocol
{
    public class JsonMessage : Header
    {
        public String Data { get; set; }
        public String DataType { get; set; }

        public JsonMessage()
        {
            MessageType = MessageTypes.JsonMessage;
        }

        public JsonMessage(BinaryReader br)
            : base(br)
        {
            Data = br.ReadString();
            DataType = br.ReadString();
        }

        public override void Serialize(BinaryWriter bw)
        {
            base.Serialize(bw);
            // add JsonMessage serialization here...
            bw.Write(Data ?? "");
            bw.Write(DataType ?? "");
        }

        public static JsonMessage Deserialize(byte[] data)
        {
            using (var br = new BinaryReader(new MemoryStream(data), DefaultEncoding))
            {
                return new JsonMessage(br);
            }
        }

        public static JsonMessage Deserialize(BinaryReader br)
        {
            return new JsonMessage(br);
        }
    }
}