﻿using System;
using System.IO;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;

namespace Mcd.MyStore.Portable.TcpServer.Protocol
{
    public class PingRequest : Header
    {
        public DateTime TimeStamp { get; set; }

        public PingRequest()
        {
            MessageType = MessageTypes.PingRequest;
            TimeStamp = DateTime.UtcNow;
        }

        public PingRequest(BinaryReader br)
            : base(br)
        {
            TimeStamp = DateTime.FromBinary(br.ReadInt64());
        }

        public override void Serialize(BinaryWriter bw)
        {
            base.Serialize(bw);
            // add PingRequest serialization here...
            bw.Write(TimeStamp.ToBinary());
        }

        public static PingRequest Deserialize(byte[] data)
        {
            using (var br = new BinaryReader(new MemoryStream(data), DefaultEncoding))
            {
                return new PingRequest(br);
            }
        }

        public static PingRequest Deserialize(BinaryReader br)
        {
            return new PingRequest(br);
        }
    }
}