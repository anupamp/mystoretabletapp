﻿using System;
using System.IO;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;

namespace Mcd.MyStore.Portable.TcpServer.Protocol
{
    public class PongResponse : Header
    {
        public PongResponse()
        {
            MessageType = MessageTypes.PongResponse;
        }

        public PongResponse(BinaryReader br) : base(br) { }

        public override void Serialize(BinaryWriter bw)
        {
            base.Serialize(bw);
            // add PingRequest serialization here...
        }

        public static PongResponse Deserialize(byte[] data)
        {
            using (var br = new BinaryReader(new MemoryStream(data), DefaultEncoding))
            {
                return new PongResponse(br);
            }
        }

        public static PongResponse Deserialize(BinaryReader br)
        {
            return new PongResponse(br);
        }
    }
}