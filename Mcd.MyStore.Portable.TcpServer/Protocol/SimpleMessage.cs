﻿using System.IO;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;
using Mcd.MyStore.Common.Manager.Notification;

namespace Mcd.MyStore.Portable.TcpServer.Protocol
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Mcd.MyStore.Portable.TcpServer.Protocol.Header" />
    public class SimpleMessage : Header
    {
        //add SimpleMessage specific properties here
        public NotificationApplicationType SimpleMessageType { get; set; }

        public SimpleMessage()
        {
            MessageType = MessageTypes.SimpleMessage;
        }

        public SimpleMessage(BinaryReader br)
            : base(br)
        {
            // add SimpleMessage deserialization here...
            SimpleMessageType = (NotificationApplicationType)br.ReadByte();
        }

        public override void Serialize(BinaryWriter bw)
        {
            base.Serialize(bw);
            // add SimpleMessage serialization here...
            bw.Write((byte)SimpleMessageType);
        }

        public static SimpleMessage Deserialize(byte[] data)
        {
            using (var br = new BinaryReader(new MemoryStream(data), DefaultEncoding))
            {
                return new SimpleMessage(br);
            }
        }

        public static SimpleMessage Deserialize(BinaryReader br)
        {
            return new SimpleMessage(br);
        }
    }
}