﻿using System;
using System.IO;
using System.Text;
using Mcd.MyStore.Common.Manager.AppService.TcpServer.Enums;

namespace Mcd.MyStore.Portable.TcpServer.Protocol
{
    public abstract class Header
    {
        public const Int32 HeaderSize = 10 + 4;
        public static readonly Encoding DefaultEncoding = Encoding.UTF8;

        public MessageTypes MessageType { get; set; } // 1
        public Byte Version { get; set; } // 1
        public DateTime TimeStamp { get; set; } // 8
        public Int32 PayloadLength { get; set; } // 4

        protected Header()
        {
            TimeStamp = DateTime.Now;
            Version = 1;
        }

        protected Header(BinaryReader br)
        {
            MessageType = (MessageTypes)br.ReadByte();
            Version = br.ReadByte();
            TimeStamp = DateTime.FromBinary(br.ReadInt64());
            PayloadLength = br.ReadInt32();
        }

        public static implicit operator byte[](Header obj)
        {
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms, DefaultEncoding))
            {
                obj.Serialize(bw);
                byte[] serialized = SetContentLength(ms.ToArray());
                return serialized;
            }
        }

        public virtual byte[] Serialize()
        {
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms))
            {
                Serialize(bw);
                return ms.ToArray();
            }
        }
        public virtual void Serialize(BinaryWriter bw)
        {
            bw.Write((byte)MessageType);//1
            bw.Write(Version);//1
            bw.Write(TimeStamp.ToBinary());//8
            bw.Write(0); // dummy
        }

        public static byte[] SetContentLength(byte[] result)
        {
            var contentLength = result.Length - HeaderSize;
            var contentBytes = BitConverter.GetBytes(contentLength);
            Array.Copy(contentBytes, 0, result, HeaderSize - 4, 4);
            return result;
        }
    }
}