﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Mcd.MyStore.Portable.TcpServer.Helper;
using Mcd.MyStore.Portable.TcpServer.Interfaces;
using Mcd.MyStore.Portable.TcpServer.Protocol;
using Sockets.Plugin;

namespace Mcd.MyStore.Portable.TcpServer
{
    public class TcpConnectionManager
    {
        private static readonly SemaphoreSlim ConnectionSemaphore = new SemaphoreSlim(1);
        private static bool _connecting;

        private const int PingIntervalInMilliseconds = 5000;
        private const int PingDelayInMilliseconds = 5000;

        private ICommunicationContext DefaultCommunicationContext { get; set; }
        private Dictionary<Connection, ICommunicationContext> ClientConnections { get; set; }

        public TcpConnectionManager(ICommunicationContext defaultCommunicationContext)
        {
            ClientConnections = new Dictionary<Connection, ICommunicationContext>();
            DefaultCommunicationContext = defaultCommunicationContext;
            Instance = this;
        }

        public static TcpConnectionManager Instance;

        public bool IsConnected { get; private set; }

        public async Task<bool?> Connect(string host, int port = 9980, CancellationTokenSource cancellationTokenSource = null)
        {
            try
            {
                await ConnectionSemaphore.WaitAsync();

                TcpSocketClient client = new TcpSocketClient();
                Debug.WriteLine("AppService: Connecting...");
                await client.ConnectAsync(host, port);

                var connection = new Connection(Guid.NewGuid().ToString(), client.ReadStream, client.WriteStream, host, port);
                connection.Connected = true;
                connection.CancellationTokenSource = new CancellationTokenSource();
                _connecting = true;

                HandleClient(connection, connection.CancellationTokenSource);

                Debug.WriteLine("AppService: Connected.");
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("AppService: Connection refused. " + e.Message);
                _connecting = false;
                return false;
            }
            finally
            {
                ConnectionSemaphore.Release();
            }
        }

        public async Task<bool> Reconnect(Connection connection)
        {
            //if (_connecting) return false;

            //lock (ConnectLock)
            //{
            //    if (_connecting) return false;
            //    _connecting = true;
            //}

            Debug.WriteLine("AppService: Reconnecting...");

            // remove connected client
            lock (ClientConnections)
            {
                Disconnect(connection);
            }

            return await Task.Run(async () =>
            {
                if (connection != null && connection.Host != null && connection.Port > 0)
                {
                    //bool success = false;
                    //while (!success)
                    //{
                    //    success = await Connect(connection.Host, connection.Port, new CancellationTokenSource());
                    //    if (!success)
                    //    {
                    //        await Task.Delay(7500);
                    //    }
                    //}
                    while (true)
                    {
                        try
                        {
                            bool? connected = await Connect(connection.Host, connection.Port, new CancellationTokenSource());
                            if (connected.HasValue && connected.Value) break;
                            await Task.Delay(7500);
                        }
                        catch
                        {
                        }
                    }

                    _connecting = false;
                }

                return false;
            });
        }

        private void Disconnect(Connection connection)
        {
            lock (connection)
            {
                var communicationContext = GetConnectionContext(connection);
                if (communicationContext != null)
                {
                    communicationContext.Disconnected();
                    IsConnected = false;
                }

                if (connection.CancellationTokenSource != null && !connection.CancellationTokenSource.IsCancellationRequested)
                    connection.CancellationTokenSource.Cancel();
                if (connection.ReadStream != null)
                    connection.ReadStream.Dispose();
                if (connection.WriteStream != null)
                    connection.WriteStream.Dispose();

                ClientConnections.Remove(connection);
                var cn = ClientConnections.FirstOrDefault(x => x.Key.Id == connection.Id);
                if (cn.Key != null)
                {
                    ClientConnections.Remove(cn.Key);
                }
            }
        }

        public async void Send(Header obj, Connection connection)
        {
            try
            {
                Byte[] data = obj;
                await connection.WriteStream.WriteAsync(data, 0, data.Length);
                await connection.WriteStream.FlushAsync();
            }
            catch (Exception)
            {
                // Send failed, connection issue, reconnect...
                Reconnect(connection);
            }
        }

        private void HandleClient(Connection connection, CancellationTokenSource cancellationToken)
        {
            lock (ClientConnections)
            {
                ClientConnections.Add(connection, DefaultCommunicationContext);
            }

            // Ping connection to Server
            KeepAliveFactory(connection, cancellationToken.Token);
            Read(new StateObject(connection));

            var communicationContext = GetConnectionContext(connection);
            if (communicationContext != null)
            {
                communicationContext.Connected();
                IsConnected = true;
            }
        }

        void KeepAliveFactory(Connection connection, CancellationToken cancellationToken)
        {
            try
            {
                PeriodicTaskFactory.Start(async () =>
                {
                    if (connection.CancellationTokenSource.IsCancellationRequested)
                        return;

                    var communicationContext = GetConnectionContext(connection);
                    if (communicationContext == null)
                    {
                        Disconnect(connection);
                        return;
                    }

                    bool sentResult = await connection.Send(new PingRequest(), cancellationToken);
                    //Debug.WriteLineIf(sentResult, string.Format("AppService ({0}): Pinging Server... OK", connection.Id));
                    Debug.WriteLineIf(!sentResult, string.Format("AppService ({0}): Pinging Server... failed!", connection.Id));

                    if (!sentResult)
                    {
                        Reconnect(connection);
                    }
                }, intervalInMilliseconds: PingIntervalInMilliseconds, delayInMilliseconds: PingDelayInMilliseconds, cancelToken: cancellationToken);
            }
            catch (OperationCanceledException)
            {
                // Ping was cancelled, ignore...
            }
            catch (Exception)
            {
                Debug.WriteLine("AppService ({0}): Pinging Server... failed!", connection.Id);
                // Ping failed while sending, possible connection issue, reconnect...
                Reconnect(connection);
            }
        }

        private async void Read(StateObject state)
        {
            try
            {
                // The int return value is the amount of bytes read accessible through the Task's Result property.
                int readTask = await state.Connection.ReadStream.ReadAsync(state.ReadBuffer, 0, StateObject.BufferSize);
                PaketReceived(readTask, state);
                Read(state);

                //await readTask.ContinueWith(x =>
                //{
                //    Read(state);
                //}, TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.NotOnRanToCompletion);
            }
            catch (Exception)
            {
                // Receive error, connection lost, reconnect...
                Reconnect(state.Connection);
            }
        }

        void PaketReceived(int readChunk, object state)
        {
            var so = state as StateObject;
            if (so == null) return;

            int available = readChunk;
            int readIndex = 0;

            while (available > 0)
            {
                if (so.CurrentBytesRead == 0 && available > 0)
                {
                    so.HeaderSize = Header.HeaderSize;
                }

                #region Header

                // Header kann vollständig geladen werden
                // [Header]
                if (available > 0 && so.HeaderCompleted == false && available >= so.HeaderSize && so.NextRead == 0)
                {
                    so.Append(ref readIndex, ref available, so.HeaderSize);
                    int contentLength = getContentLength(so);
                    //2013-12-24 - Some types may not have a body
                    //if (contentLength == 0) break;
                    so.FrameSize = so.HeaderSize + contentLength;
                    so.ContentSize = contentLength;
                    so.HeaderCompleted = true;
                }
                else
                    //Header kann nicht vollständig gelesen werden
                    // [Head..]
                    if (available > 0 && so.HeaderCompleted == false && available < so.HeaderSize && so.NextRead == 0)
                    {
                        so.NextRead = so.HeaderSize - available;
                        so.Append(ref readIndex, ref available, available);
                    }
                    //Header kann jetzt vollständig gelesen werden
                    // [....er]
                    else if (available > 0 && so.HeaderCompleted == false && available >= so.NextRead)
                    {
                        so.Append(ref readIndex, ref available, so.NextRead);
                        so.NextRead = 0;
                        int contentLength = getContentLength(so);
                        so.FrameSize = so.HeaderSize + contentLength;
                        so.ContentSize = contentLength;
                        so.HeaderCompleted = true;
                    }
                    //Header kann immernoch nicht vollständig gelesen werden
                    //[..ad..]
                    else if (available > 0 && so.HeaderCompleted == false && available < so.NextRead)
                    {
                        so.NextRead -= available;
                        so.Append(ref readIndex, ref available, available);
                    }

                #endregion

                #region Content

                var clientContext = GetConnectionContext(so.Connection);

                // Header gelesen, Content nicht verfügbar
                if (so.HeaderCompleted && available == 0 && so.ContentSize == 0)
                {
                    if (clientContext == null) break;
                    clientContext.ProcessData(so.Connection, so.GetBytes(0, so.HeaderSize));
                    so.Reset();
                }
                // Header gelesen, Content kann vollständig gelesen werden
                // Header + [Content]
                else if (available > 0 && so.HeaderCompleted && available >= so.ContentSize && so.NextRead == 0)
                {
                    so.Append(ref readIndex, ref available, so.ContentSize);
                    if (so.Completed)
                    {
                        int frameSizeChecked = checked(so.FrameSize);
                        if (frameSizeChecked <= 0) Debugger.Break();
                        if (clientContext == null) break;
                        clientContext.ProcessData(so.Connection, so.GetBytes(0, frameSizeChecked));
                        so.Reset();
                    }
                }
                // Header gelesen, Content kann nicht vollständig gelesen werden
                // Header + [Cont...]
                else if (available > 0 && so.HeaderCompleted && available < so.ContentSize && so.NextRead == 0)
                {
                    so.NextRead = so.ContentSize - available;
                    so.Append(ref readIndex, ref available, available);
                }
                // Header gelesen, Content fragment kann jetzt vollständig gelesen werden
                // Header + [...tent]
                else if (available > 0 && so.HeaderCompleted && available >= so.NextRead)
                {
                    so.Append(ref readIndex, ref available, so.NextRead);
                    so.NextRead = 0;
                    if (so.Completed)
                    {
                        if (clientContext == null) break;
                        clientContext.ProcessData(so.Connection, so.GetBytes(0, so.FrameSize));
                        so.Reset();
                    }
                }
                //Header gelesen, Content fragment kann immernoch nicht vollständig gelesen werden
                // Header + [...t...]
                else if (available > 0 && so.HeaderCompleted && available < so.NextRead)
                {
                    so.NextRead -= available;
                    so.Append(ref readIndex, ref available, available);
                }

                #endregion
            }

            // Reset buffer to get new data
            so.ReadBuffer = new byte[StateObject.BufferSize];
        }

        int getContentLength(StateObject so)
        {
            byte[] contentLengthBytes = so.GetBytes(Header.HeaderSize - 4, 4);
            int contentLength = BitConverter.ToInt32(contentLengthBytes, 0);
            return contentLength;
        }

        protected ICommunicationContext GetConnectionContext(Connection connection)
        {
            lock (ClientConnections)
            {
                var entry = ClientConnections.Where(x => x.Key == connection)
                    .Select(x => x.Value)
                    .FirstOrDefault();
                return entry;
            }
        }
    }
}
