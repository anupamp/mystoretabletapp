﻿using System.Threading.Tasks;
using Mcd.MyStore.Portable.TcpServer.Protocol;

namespace Mcd.MyStore.Portable.TcpServer.Interfaces
{
    public interface ICommunicationContext
    {
        void ProcessData(Connection connection, byte[] bytes);
        Task<bool> SendAsync(Connection connection, Header message);
        void Disconnected();
        void Connected();

        void HandleServerMessage(Connection connection, SimpleMessage message);
        void HandleServerMessage(Connection connection, JsonMessage message);
        void HandleServerMessage(Connection connection, PingRequest message);
        void HandleServerMessage(Connection connection, PongResponse message);
    }
}