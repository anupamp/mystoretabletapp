﻿using Mcd.MyStore.WinApp.Core.Logging;
using Mcd.MyStore.WinApp.Core.Manager.Settings;
using Mcd.MyStore.WinApp.Modules;
using Mcd.MyStore.WinApp.Modules.AppHome;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Die Elementvorlage "Geteilte App" ist unter http://go.microsoft.com/fwlink/?LinkId=234228 dokumentiert.

namespace Mcd.MyStore.WinApp.MyStoreApp
{
    /// <summary>
    /// Stellt das anwendungsspezifische Verhalten bereit, um die Standardanwendungsklasse zu ergänzen.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();

            UnhandledException += (sender, e) =>
            {
                Logger.Write(e.Exception);
                e.Handled = true;
            };

            this.Suspending += OnSuspending;
            DebugSettings.BindingFailed += DebugSettings_BindingFailed;
        }

        void DebugSettings_BindingFailed(object sender, BindingFailedEventArgs e)
        {
            Debug.WriteLine(e.Message);
        }
        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            String version = String.Format("{0}.{1}.{2}.{3}", Package.Current.Id.Version.Major, Package.Current.Id.Version.Minor, Package.Current.Id.Version.Build, Package.Current.Id.Version.Revision);
            Logger.Instance.AddLog("Started MyStore Tablet app, version " + version);
            ApplicationView appView = ApplicationView.GetForCurrentView();
            try
            {
                appView.Title = "MyStore Tablet Application " + version;
            }
            catch(Exception ex)
            {
                Logger.Write(ex);
            }
            base.OnWindowCreated(args);
        }
        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            //SettingsManager.Instance.IsReady = SystemStatusManager.Instance.CurrentBusinessDay != DateTime.MinValue;

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active

            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Associate the frame with a SuspensionManager key                                
                //SuspensionManager.RegisterFrame(rootFrame, "AppFrame");

                //if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                //{
                //    // Restore the saved session state only when appropriate
                //    try
                //    {
                //        await SuspensionManager.RestoreAsync();
                //    }
                //    catch (Exception ex)
                //    {
                //        Logger.Write(ex);
                //        //Something went wrong restoring state.
                //        //Assume there is no state and continue
                //    }
                //}

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }
            if (rootFrame.Content == null || !String.IsNullOrEmpty(args.Arguments))
            {
                //const int retrys = 10;
                //for (int i = retrys; i >= 0; i--)
                //{
                //    if (SettingsManager.Instance.IsReady)
                //    {
                //        break;
                //    }
                //    await Task.Delay(500);
                //}

                //if (!SettingsManager.Instance.IsReady)
                //{
                //    if (!rootFrame.Navigate(typeof(ErrorPage), args.Arguments))
                //    {
                //        throw new Exception("Unexpexted application crash.");
                //    }
                //    await Task.Delay(500);
                //    return;
                //}

                // When the navigation stack isn't restored or there are launch arguments
                // indicating an alternate launch (e.g.: via toast or secondary tile), 
                // navigate to the appropriate page, configuring the new page by passing required 
                // information as a navigation parameter
                if (!rootFrame.Navigate(typeof(ApplicationPage), args.Arguments))
                {
                    if (!rootFrame.Navigate(typeof(ErrorPage)))
                    {
                        throw new Exception("Unexpected application crash.");
                    }
                    await Task.Delay(50);
                    return;
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            //var deferral = e.SuspendingOperation.GetDeferral();
            //await SuspensionManager.SaveAsync();
            //deferral.Complete();
        }
    }
}
